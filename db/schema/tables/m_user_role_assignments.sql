CREATE TABLE `m_user_role_assignments` (
  `idUser` int(11) NOT NULL,
  `idRole` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin
;

ALTER TABLE `m_user_role_assignments`
  ADD UNIQUE KEY `PK_user_role_assignments` (`idUser`,`idRole`)
;

