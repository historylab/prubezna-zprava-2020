CREATE TABLE `m_user_group_role_assignments` (
  `idUserGroup` int(11) NOT NULL,
  `idRole` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin
;

ALTER TABLE `m_user_group_role_assignments`
  ADD UNIQUE KEY `idUserGroup` (`idUserGroup`,`idRole`)
;

