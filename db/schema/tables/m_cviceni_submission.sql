CREATE TABLE `m_cviceni_submission` (
  `entryid` int(11) NOT NULL,
  `emailAddress` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `dateCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastUpdated` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `lms_hash` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `exerciseId` varchar(255) DEFAULT NULL,
  `katalog_id` int(11) NOT NULL,
  `exerciseVersion` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `exerciseMode` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `exerciseData` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin,
  `analytics` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin,
  `time_spent` int(11) DEFAULT NULL,
  `additionalEmailAddresses` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `enrolment_id` int(11) DEFAULT NULL,
  `exerciseSlug` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `done` tinyint(1) DEFAULT NULL,
  `language` varchar(10) DEFAULT NULL,
  `exhibition_id` int(11) DEFAULT NULL,
  `redirect_url` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8
;

ALTER TABLE `m_cviceni_submission`
  ADD PRIMARY KEY (`entryid`)
;

ALTER TABLE `m_cviceni_submission`
  MODIFY `entryid` int(11) NOT NULL AUTO_INCREMENT
;

ALTER TABLE `m_cviceni_submission` 
  ADD CONSTRAINT `FK_exhibition` FOREIGN KEY (`exhibition_id`) REFERENCES `m_exhibition`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
;