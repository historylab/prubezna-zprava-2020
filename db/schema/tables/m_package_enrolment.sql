CREATE TABLE `m_package_enrolment` (
  `id` bigint(10) NOT NULL,
  `package_id` int(11) DEFAULT NULL,
  `code` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `type` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin
;

ALTER TABLE `m_package_enrolment`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UQ_Code` (`code`)
;

ALTER TABLE `m_package_enrolment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT
;

ALTER TABLE `m_package_enrolment`
  ADD CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `m_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE `m_package_enrolment`
  ADD CONSTRAINT `fk_package_id` FOREIGN KEY (`package_id`) REFERENCES `m_packages` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE `m_package_enrolment`
  ADD CONSTRAINT `fk_group_id` FOREIGN KEY (`group_id`) REFERENCES `m_cviceni_group` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
;