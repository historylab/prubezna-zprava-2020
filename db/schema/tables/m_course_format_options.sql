CREATE TABLE `m_course_format_options` (
  `id` bigint(10) NOT NULL,
  `courseid` bigint(10) NOT NULL,
  `format` varchar(21) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `sectionid` bigint(10) NOT NULL DEFAULT '0',
  `name` varchar(100) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `value` longtext COLLATE utf8mb4_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Stores format-specific options for the course or course sect' ROW_FORMAT=COMPRESSED
;

ALTER TABLE `m_course_format_options`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `m_courformopti_couforsecna_uix` (`courseid`,`format`,`sectionid`,`name`),
  ADD KEY `m_courformopti_cou_ix` (`courseid`)
;

ALTER TABLE `m_course_format_options`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT
;

