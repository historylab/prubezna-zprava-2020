CREATE TABLE `m_cviceni_enrolment` (
  `id` int(11) NOT NULL,
  `enrolment_id` int(11) DEFAULT NULL,
  `katalog_id` int(11) NOT NULL,
  `course_id` int(11) DEFAULT NULL,
  `code` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `type` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `original_group_id` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `exerciseId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin
;

ALTER TABLE `m_cviceni_enrolment`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UQ_Code` (`code`)
;

ALTER TABLE `m_cviceni_enrolment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT
;

