CREATE TABLE `m_user_password_resets` (
  `id` bigint(10) NOT NULL,
  `userid` bigint(10) NOT NULL,
  `timerequested` bigint(10) NOT NULL,
  `timererequested` bigint(10) NOT NULL DEFAULT '0',
  `token` varchar(100) COLLATE utf8mb4_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='table tracking password reset confirmation tokens' ROW_FORMAT=COMPRESSED
;

ALTER TABLE `m_user_password_resets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `m_userpassrese_use_ix` (`userid`)
;

ALTER TABLE `m_user_password_resets`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT
;

