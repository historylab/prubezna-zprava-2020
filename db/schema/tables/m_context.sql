CREATE TABLE `m_context` (
  `id` bigint(10) NOT NULL,
  `contextlevel` bigint(10) NOT NULL DEFAULT '0',
  `instanceid` bigint(10) NOT NULL DEFAULT '0',
  `path` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `depth` tinyint(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='one of these must be set' ROW_FORMAT=COMPRESSED
;

ALTER TABLE `m_context`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `m_cont_conins_uix` (`contextlevel`,`instanceid`),
  ADD KEY `m_cont_ins_ix` (`instanceid`),
  ADD KEY `m_cont_pat_ix` (`path`(191))
;

ALTER TABLE `m_context`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT
;

