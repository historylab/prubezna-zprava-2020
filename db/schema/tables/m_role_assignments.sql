CREATE TABLE `m_role_assignments` (
  `id` bigint(10) NOT NULL,
  `roleid` bigint(10) NOT NULL DEFAULT '0',
  `contextid` bigint(10) NOT NULL DEFAULT '0',
  `userid` bigint(10) NOT NULL DEFAULT '0',
  `timemodified` bigint(10) NOT NULL DEFAULT '0',
  `modifierid` bigint(10) NOT NULL DEFAULT '0',
  `component` varchar(100) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `itemid` bigint(10) NOT NULL DEFAULT '0',
  `sortorder` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='assigning roles in different context' ROW_FORMAT=COMPRESSED
;

ALTER TABLE `m_role_assignments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `m_roleassi_sor_ix` (`sortorder`),
  ADD KEY `m_roleassi_rolcon_ix` (`roleid`,`contextid`),
  ADD KEY `m_roleassi_useconrol_ix` (`userid`,`contextid`,`roleid`),
  ADD KEY `m_roleassi_comiteuse_ix` (`component`,`itemid`,`userid`),
  ADD KEY `m_roleassi_rol_ix` (`roleid`),
  ADD KEY `m_roleassi_con_ix` (`contextid`),
  ADD KEY `m_roleassi_use_ix` (`userid`)
;

ALTER TABLE `m_role_assignments`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT
;

