CREATE TABLE `m_cviceni_group_email` (
  `id` int(11) NOT NULL,
  `emailAddress` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin
;

ALTER TABLE `m_cviceni_group_email`
  ADD PRIMARY KEY (`id`)
;

ALTER TABLE `m_cviceni_group_email`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT
;

