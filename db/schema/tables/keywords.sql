CREATE TABLE `keywords`
(
    `id`         INT          NOT NULL AUTO_INCREMENT,
    `name`       VARCHAR(255) NOT NULL,
    `type`       VARCHAR(100) NOT NULL,
    `to_filter`  BOOLEAN      NOT NULL,
    `language`   VARCHAR(10)  NULL,
    `enabled`    BOOLEAN      NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB;