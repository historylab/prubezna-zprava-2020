CREATE TABLE `m_course_sections` (
  `id` bigint(10) NOT NULL,
  `course` bigint(10) NOT NULL DEFAULT '0',
  `section` bigint(10) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `summary` longtext COLLATE utf8mb4_bin,
  `summaryformat` tinyint(2) NOT NULL DEFAULT '0',
  `sequence` longtext COLLATE utf8mb4_bin,
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `availability` longtext COLLATE utf8mb4_bin,
  `timemodified` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='to define the sections for each course' ROW_FORMAT=COMPRESSED
;

ALTER TABLE `m_course_sections`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `m_coursect_cousec_uix` (`course`,`section`)
;

ALTER TABLE `m_course_sections`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT
;

