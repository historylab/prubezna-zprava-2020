CREATE TABLE `m_exhibition` (
  `id` int(11) NOT NULL,
  `code` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `name` varchar(100) NULL,
  `url` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

ALTER TABLE `m_exhibition`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `m_exhibiton_code` (`code`);

ALTER TABLE `m_exhibition`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT
;