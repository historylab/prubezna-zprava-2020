CREATE TABLE `m_package_exercises` (
  `package_id` int(11) NOT NULL,
  `exercise_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin
;

ALTER TABLE `m_package_exercises`
  ADD PRIMARY KEY (`package_id`,`exercise_id`)
;

ALTER TABLE `m_package_exercises`
  ADD CONSTRAINT `fk_package_id` FOREIGN KEY (`package_id`) REFERENCES `m_packages` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `m_package_exercises`
  ADD CONSTRAINT `fk_exercise_id` FOREIGN KEY (`exercise_id`) REFERENCES `m_katalog` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;  