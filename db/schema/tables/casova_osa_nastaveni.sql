CREATE TABLE `casova_osa_nastaveni` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`title` varchar(255) NOT NULL,
	`slug` varchar(255) NOT NULL,
	`idLanguage` int(11) DEFAULT NULL,
	`epochs` text COLLATE utf8mb4_bin NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

