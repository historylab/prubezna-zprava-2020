CREATE TABLE `m_user_lastaccess` (
  `id` bigint(10) NOT NULL,
  `userid` bigint(10) NOT NULL DEFAULT '0',
  `courseid` bigint(10) NOT NULL DEFAULT '0',
  `timeaccess` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='To keep track of course page access times, used in online pa' ROW_FORMAT=COMPRESSED
;

ALTER TABLE `m_user_lastaccess`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `m_userlast_usecou_uix` (`userid`,`courseid`),
  ADD KEY `m_userlast_use_ix` (`userid`),
  ADD KEY `m_userlast_cou_ix` (`courseid`)
;

ALTER TABLE `m_user_lastaccess`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT
;

