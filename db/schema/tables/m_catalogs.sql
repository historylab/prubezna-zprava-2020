CREATE TABLE `m_catalogs` (
  `id` int(11) auto_increment NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `validFrom` datetime DEFAULT NULL,
  `validTo` datetime DEFAULT NULL,
  `isPublic` tinyint(1) NOT NULL,
  `isEnabled` tinyint(1) NOT NULL DEFAULT '0',
  `idLanguage` int(11) DEFAULT NULL,
  primary key(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin
;

