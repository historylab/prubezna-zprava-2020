CREATE TABLE `m_role` (
  `id` bigint(10) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `shortname` varchar(100) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_bin NOT NULL,
  `sortorder` bigint(10) NOT NULL DEFAULT '0',
  `archetype` varchar(30) COLLATE utf8mb4_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='moodle roles' ROW_FORMAT=COMPRESSED
;

ALTER TABLE `m_role`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `m_role_sor_uix` (`sortorder`),
  ADD UNIQUE KEY `m_role_sho_uix` (`shortname`)
;

ALTER TABLE `m_role`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT
;

