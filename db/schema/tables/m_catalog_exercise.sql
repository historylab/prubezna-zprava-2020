CREATE TABLE `m_catalog_exercise` (
  `idCatalog` int(11) NOT NULL,
  `exerciseId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

ALTER TABLE `m_catalog_exercise`
  ADD PRIMARY KEY (`idCatalog`,`exerciseId`);
