CREATE TABLE `m_role_allow_assign` (
  `id` bigint(10) NOT NULL,
  `roleid` bigint(10) NOT NULL DEFAULT '0',
  `allowassign` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='this defines what role can assign what role' ROW_FORMAT=COMPRESSED
;

ALTER TABLE `m_role_allow_assign`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `m_rolealloassi_rolall_uix` (`roleid`,`allowassign`),
  ADD KEY `m_rolealloassi_rol_ix` (`roleid`),
  ADD KEY `m_rolealloassi_all_ix` (`allowassign`)
;

ALTER TABLE `m_role_allow_assign`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT
;

