CREATE TABLE `m_role_allow_switch` (
  `id` bigint(10) NOT NULL,
  `roleid` bigint(10) NOT NULL,
  `allowswitch` bigint(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='This table stores which which other roles a user is allowed ' ROW_FORMAT=COMPRESSED
;

ALTER TABLE `m_role_allow_switch`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `m_rolealloswit_rolall_uix` (`roleid`,`allowswitch`),
  ADD KEY `m_rolealloswit_rol_ix` (`roleid`),
  ADD KEY `m_rolealloswit_all_ix` (`allowswitch`)
;

ALTER TABLE `m_role_allow_switch`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT
;

