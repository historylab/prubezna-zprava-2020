CREATE TABLE `m_tracking` (
  `entry_id` int(11) NOT NULL,
  `user_agent` varchar(500) COLLATE utf8mb4_bin DEFAULT NULL,
  `referer` varchar(500) COLLATE utf8mb4_bin DEFAULT NULL,
  `accept_language` varchar(100) COLLATE utf8mb4_bin DEFAULT NULL,
  `accept_encoding` varchar(100) COLLATE utf8mb4_bin DEFAULT NULL,
  `accept` varchar(500) COLLATE utf8mb4_bin DEFAULT NULL,
  `query_string` varchar(500) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin
;

ALTER TABLE `m_tracking`
  ADD PRIMARY KEY (`entry_id`)
;

ALTER TABLE `m_tracking`
  ADD CONSTRAINT `fk_entry_id` FOREIGN KEY (`entry_id`) REFERENCES `m_cviceni_submission` (`entryid`) ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE `m_tracking` ADD INDEX `IX_query_string` (`query_string`);