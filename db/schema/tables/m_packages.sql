CREATE TABLE `m_packages` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `description` text COLLATE utf8mb4_bin NOT NULL,
  `image_url` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `thumb_image_url` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `topics` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT '1',
  `slug` varchar(100) COLLATE utf8mb4_bin NOT NULL,  
  `authors` varchar(100) COLLATE utf8mb4_bin DEFAULT NULL,
  `language_id` int COLLATE utf8mb4_bin DEFAULT NULL,
  `metadata` text COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin
;

ALTER TABLE `m_packages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uq_slug` (`slug`)
;

ALTER TABLE `m_packages` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `m_packages`
  ADD CONSTRAINT `fk_language_id` FOREIGN KEY (`language_id`) REFERENCES `m_languages` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
