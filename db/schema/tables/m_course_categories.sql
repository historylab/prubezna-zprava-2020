CREATE TABLE `m_course_categories` (
  `id` bigint(10) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `idnumber` varchar(100) COLLATE utf8mb4_bin DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_bin,
  `descriptionformat` tinyint(2) NOT NULL DEFAULT '0',
  `parent` bigint(10) NOT NULL DEFAULT '0',
  `sortorder` bigint(10) NOT NULL DEFAULT '0',
  `coursecount` bigint(10) NOT NULL DEFAULT '0',
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `visibleold` tinyint(1) NOT NULL DEFAULT '1',
  `timemodified` bigint(10) NOT NULL DEFAULT '0',
  `depth` bigint(10) NOT NULL DEFAULT '0',
  `path` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `theme` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Course categories' ROW_FORMAT=COMPRESSED
;

ALTER TABLE `m_course_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `m_courcate_par_ix` (`parent`)
;

ALTER TABLE `m_course_categories`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT
;

