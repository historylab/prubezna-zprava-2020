CREATE TABLE `m_external_services_functions` (
  `id` bigint(10) NOT NULL,
  `externalserviceid` bigint(10) NOT NULL,
  `functionname` varchar(200) COLLATE utf8mb4_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='lists functions available in each service group' ROW_FORMAT=COMPRESSED
;

ALTER TABLE `m_external_services_functions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `m_exteservfunc_ext_ix` (`externalserviceid`)
;

ALTER TABLE `m_external_services_functions`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT
;

