CREATE TABLE `m_sessions` (
  `id` bigint(10) NOT NULL,
  `state` bigint(10) NOT NULL DEFAULT '0',
  `sid` varchar(128) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `userid` bigint(10) NOT NULL,
  `sessdata` longtext COLLATE utf8mb4_bin,
  `timecreated` bigint(10) NOT NULL,
  `timemodified` bigint(10) NOT NULL,
  `firstip` varchar(45) COLLATE utf8mb4_bin DEFAULT NULL,
  `lastip` varchar(45) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Database based session storage - now recommended' ROW_FORMAT=COMPRESSED
;

ALTER TABLE `m_sessions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `m_sess_sid_uix` (`sid`),
  ADD KEY `m_sess_sta_ix` (`state`),
  ADD KEY `m_sess_tim_ix` (`timecreated`),
  ADD KEY `m_sess_tim2_ix` (`timemodified`),
  ADD KEY `m_sess_use_ix` (`userid`)
;

ALTER TABLE `m_sessions`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT
;

