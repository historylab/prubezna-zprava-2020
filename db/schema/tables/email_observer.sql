﻿CREATE TABLE `email_observer` (
    `id` INT(255) NOT NULL AUTO_INCREMENT,
    `date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `successful` TINYINT NOT NULL,
    `email_data_to` TEXT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB;