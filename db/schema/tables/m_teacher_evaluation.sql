CREATE TABLE `m_teacher_evaluation` (
  `id` int(11) NOT NULL,
  `entry_id` int(11) NOT NULL,
  `note` text COLLATE utf8mb4_bin NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `score` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin
;

ALTER TABLE `m_teacher_evaluation`
  ADD PRIMARY KEY (`id`)
;

ALTER TABLE `m_teacher_evaluation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT
;

