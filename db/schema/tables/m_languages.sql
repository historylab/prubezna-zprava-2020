CREATE TABLE `m_languages` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `code` varchar(20) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin
;

ALTER TABLE `m_languages`
  ADD PRIMARY KEY (`id`)
;

ALTER TABLE `m_languages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT
;

