CREATE TABLE `m_submission_emailaddresses` (
  `id` int(11) NOT NULL,
  `submission_id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_bin DEFAULT NULL,
  `emailAddress` varchar(255) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin
;

ALTER TABLE `m_submission_emailaddresses`
  ADD PRIMARY KEY (`id`)
;

ALTER TABLE `m_submission_emailaddresses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT
;

