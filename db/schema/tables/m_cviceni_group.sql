CREATE TABLE `m_cviceni_group` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `note` varchar(500) NULL,
  `color` varchar(20) NULL,
  `year_start` int NOT NULL,
  `year_end` int NOT NULL,
  `is_hidden` BOOLEAN NOT NULL DEFAULT FALSE,
  `is_favourite` BOOLEAN NOT NULL DEFAULT FALSE,
  `last_used` DATETIME NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin
;

ALTER TABLE `m_cviceni_group`
  ADD PRIMARY KEY (`id`)
;

ALTER TABLE `m_cviceni_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT
;

ALTER TABLE `m_cviceni_group`
  ADD CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `m_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
;