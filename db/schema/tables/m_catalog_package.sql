CREATE TABLE `m_catalog_package` (
	`catalog_id` INT NOT NULL,
	`package_id` INT NOT NULL 
) ENGINE = InnoDB;

ALTER TABLE `m_catalog_package` ADD CONSTRAINT `fk_packages` 
	FOREIGN KEY (`package_id`) REFERENCES `m_packages`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT; 

ALTER TABLE `m_catalog_package` ADD CONSTRAINT `fk_catalog` 
	FOREIGN KEY (`catalog_id`) REFERENCES `m_catalogs`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;