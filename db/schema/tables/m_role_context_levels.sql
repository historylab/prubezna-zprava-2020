CREATE TABLE `m_role_context_levels` (
  `id` bigint(10) NOT NULL,
  `roleid` bigint(10) NOT NULL,
  `contextlevel` bigint(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Lists which roles can be assigned at which context levels. T' ROW_FORMAT=COMPRESSED
;

ALTER TABLE `m_role_context_levels`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `m_rolecontleve_conrol_uix` (`contextlevel`,`roleid`),
  ADD KEY `m_rolecontleve_rol_ix` (`roleid`)
;

ALTER TABLE `m_role_context_levels`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT
;

