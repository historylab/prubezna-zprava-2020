CREATE TABLE `m_user_groups` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `peepingExtra` BOOLEAN NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin
;

ALTER TABLE `m_user_groups`
  ADD PRIMARY KEY (`id`)
;

ALTER TABLE `m_user_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT
;

