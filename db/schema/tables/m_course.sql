CREATE TABLE `m_course` (
  `id` bigint(10) NOT NULL,
  `category` bigint(10) NOT NULL DEFAULT '0',
  `sortorder` bigint(10) NOT NULL DEFAULT '0',
  `fullname` varchar(254) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `shortname` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `idnumber` varchar(100) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `summary` longtext COLLATE utf8mb4_bin,
  `summaryformat` tinyint(2) NOT NULL DEFAULT '0',
  `format` varchar(21) COLLATE utf8mb4_bin NOT NULL DEFAULT 'topics',
  `showgrades` tinyint(2) NOT NULL DEFAULT '1',
  `newsitems` mediumint(5) NOT NULL DEFAULT '1',
  `startdate` bigint(10) NOT NULL DEFAULT '0',
  `enddate` bigint(10) NOT NULL DEFAULT '0',
  `marker` bigint(10) NOT NULL DEFAULT '0',
  `maxbytes` bigint(10) NOT NULL DEFAULT '0',
  `legacyfiles` smallint(4) NOT NULL DEFAULT '0',
  `showreports` smallint(4) NOT NULL DEFAULT '0',
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `visibleold` tinyint(1) NOT NULL DEFAULT '1',
  `groupmode` smallint(4) NOT NULL DEFAULT '0',
  `groupmodeforce` smallint(4) NOT NULL DEFAULT '0',
  `defaultgroupingid` bigint(10) NOT NULL DEFAULT '0',
  `lang` varchar(30) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `calendartype` varchar(30) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `theme` varchar(50) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `timecreated` bigint(10) NOT NULL DEFAULT '0',
  `timemodified` bigint(10) NOT NULL DEFAULT '0',
  `requested` tinyint(1) NOT NULL DEFAULT '0',
  `enablecompletion` tinyint(1) NOT NULL DEFAULT '0',
  `completionnotify` tinyint(1) NOT NULL DEFAULT '0',
  `cacherev` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Central course table' ROW_FORMAT=COMPRESSED
;

ALTER TABLE `m_course`
  ADD PRIMARY KEY (`id`),
  ADD KEY `m_cour_cat_ix` (`category`),
  ADD KEY `m_cour_idn_ix` (`idnumber`),
  ADD KEY `m_cour_sho_ix` (`shortname`(191)),
  ADD KEY `m_cour_sor_ix` (`sortorder`)
;

ALTER TABLE `m_course`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT
;

