CREATE TABLE `m_repository` (
  `id` bigint(10) NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `visible` tinyint(1) DEFAULT '1',
  `sortorder` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='This table contains one entry for every configured external ' ROW_FORMAT=COMPRESSED
;

ALTER TABLE `m_repository`
  ADD PRIMARY KEY (`id`)
;

ALTER TABLE `m_repository`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT
;

