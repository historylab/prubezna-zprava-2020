CREATE TABLE `m_course_modules` (
  `id` bigint(10) NOT NULL,
  `course` bigint(10) NOT NULL DEFAULT '0',
  `module` bigint(10) NOT NULL DEFAULT '0',
  `instance` bigint(10) NOT NULL DEFAULT '0',
  `section` bigint(10) NOT NULL DEFAULT '0',
  `idnumber` varchar(100) COLLATE utf8mb4_bin DEFAULT NULL,
  `added` bigint(10) NOT NULL DEFAULT '0',
  `score` smallint(4) NOT NULL DEFAULT '0',
  `indent` mediumint(5) NOT NULL DEFAULT '0',
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `visibleoncoursepage` tinyint(1) NOT NULL DEFAULT '1',
  `visibleold` tinyint(1) NOT NULL DEFAULT '1',
  `groupmode` smallint(4) NOT NULL DEFAULT '0',
  `groupingid` bigint(10) NOT NULL DEFAULT '0',
  `completion` tinyint(1) NOT NULL DEFAULT '0',
  `completiongradeitemnumber` bigint(10) DEFAULT NULL,
  `completionview` tinyint(1) NOT NULL DEFAULT '0',
  `completionexpected` bigint(10) NOT NULL DEFAULT '0',
  `showdescription` tinyint(1) NOT NULL DEFAULT '0',
  `availability` longtext COLLATE utf8mb4_bin,
  `deletioninprogress` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='course_modules table retrofitted from MySQL' ROW_FORMAT=COMPRESSED
;

ALTER TABLE `m_course_modules`
  ADD PRIMARY KEY (`id`),
  ADD KEY `m_courmodu_vis_ix` (`visible`),
  ADD KEY `m_courmodu_cou_ix` (`course`),
  ADD KEY `m_courmodu_mod_ix` (`module`),
  ADD KEY `m_courmodu_ins_ix` (`instance`),
  ADD KEY `m_courmodu_idncou_ix` (`idnumber`,`course`),
  ADD KEY `m_courmodu_gro_ix` (`groupingid`)
;

ALTER TABLE `m_course_modules`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT
;

