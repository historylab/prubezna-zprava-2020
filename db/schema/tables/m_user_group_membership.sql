CREATE TABLE `m_user_group_membership` (
  `idUserGroup` int(11) NOT NULL,
  `idUser` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin
;

ALTER TABLE `m_user_group_membership`
  ADD UNIQUE KEY `PK_user_group_membership` (`idUser`,`idUserGroup`)
;

