CREATE TABLE `m_user` (
  `id` bigint(10) NOT NULL,
  `auth` varchar(20) COLLATE utf8mb4_bin NOT NULL DEFAULT 'manual',
  `confirmed` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `suspended` tinyint(1) NOT NULL DEFAULT '0',
  `username` varchar(100) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `idnumber` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `firstname` varchar(100) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `lastname` varchar(100) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `googleId` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `institution` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `department` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `address` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `lang` varchar(30) COLLATE utf8mb4_bin NOT NULL DEFAULT 'cs',
  `firstaccess` bigint(10) NOT NULL DEFAULT '0',
  `lastaccess` bigint(10) NOT NULL DEFAULT '0',
  `lastlogin` bigint(10) NOT NULL DEFAULT '0',
  `currentlogin` bigint(10) NOT NULL DEFAULT '0',
  `lastip` varchar(45) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `secret` varchar(15) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `picture` bigint(10) NOT NULL DEFAULT '0',
  `url` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_bin,
  `descriptionformat` tinyint(2) NOT NULL DEFAULT '1',
  `timecreated` bigint(10) NOT NULL DEFAULT '0',
  `timemodified` bigint(10) NOT NULL DEFAULT '0',
  `is_teacher` tinyint(1) NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `password_digest` varchar(255) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED
;

ALTER TABLE `m_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `m_user_email` (`email`),
  ADD KEY `m_user_del_ix` (`deleted`),
  ADD KEY `m_user_con_ix` (`confirmed`),
  ADD KEY `m_user_fir_ix` (`firstname`),
  ADD KEY `m_user_las_ix` (`lastname`),
  ADD KEY `m_user_las2_ix` (`lastaccess`),
  ADD KEY `m_user_ema_ix` (`email`),
  ADD KEY `m_user_aut_ix` (`auth`),
  ADD KEY `m_user_idn_ix` (`idnumber`(191))
;

ALTER TABLE `m_user`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT
;

