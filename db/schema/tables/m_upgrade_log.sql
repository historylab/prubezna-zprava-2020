CREATE TABLE `m_upgrade_log` (
  `id` bigint(10) NOT NULL,
  `type` bigint(10) NOT NULL,
  `plugin` varchar(100) COLLATE utf8mb4_bin DEFAULT NULL,
  `version` varchar(100) COLLATE utf8mb4_bin DEFAULT NULL,
  `targetversion` varchar(100) COLLATE utf8mb4_bin DEFAULT NULL,
  `info` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `details` longtext COLLATE utf8mb4_bin,
  `backtrace` longtext COLLATE utf8mb4_bin,
  `userid` bigint(10) NOT NULL,
  `timemodified` bigint(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Upgrade logging' ROW_FORMAT=COMPRESSED
;

ALTER TABLE `m_upgrade_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `m_upgrlog_tim_ix` (`timemodified`),
  ADD KEY `m_upgrlog_typtim_ix` (`type`,`timemodified`),
  ADD KEY `m_upgrlog_use_ix` (`userid`)
;

ALTER TABLE `m_upgrade_log`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT
;

