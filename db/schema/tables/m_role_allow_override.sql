CREATE TABLE `m_role_allow_override` (
  `id` bigint(10) NOT NULL,
  `roleid` bigint(10) NOT NULL DEFAULT '0',
  `allowoverride` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='this defines what role can override what role' ROW_FORMAT=COMPRESSED
;

ALTER TABLE `m_role_allow_override`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `m_rolealloover_rolall_uix` (`roleid`,`allowoverride`),
  ADD KEY `m_rolealloover_rol_ix` (`roleid`),
  ADD KEY `m_rolealloover_all_ix` (`allowoverride`)
;

ALTER TABLE `m_role_allow_override`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT
;

