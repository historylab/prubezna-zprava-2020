CREATE TABLE `m_katalog` (
  `id` int(11) NOT NULL,
  `code` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `image_url` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `thumb_image_url` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT '1',
  `slug` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `exerciseId` int(11) NOT NULL DEFAULT '0',
  `language` varchar(10) COLLATE utf8mb4_bin DEFAULT NULL,
  `metadata` text COLLATE utf8mb4_bin DEFAULT NULL,
  `is_package` TINYINT NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin
;

ALTER TABLE `m_katalog`
  ADD PRIMARY KEY (`id`)
;

ALTER TABLE `m_katalog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT
;

