CREATE TABLE `m_catalog_usergroup` (
  `idCatalog` int(11) NOT NULL,
  `idUserGroup` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin
;

ALTER TABLE `m_catalog_usergroup`
  ADD UNIQUE KEY `PK_catalog_usergroup` (`idCatalog`,`idUserGroup`)
;

