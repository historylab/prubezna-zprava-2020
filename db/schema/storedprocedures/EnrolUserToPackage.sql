DROP PROCEDURE IF EXISTS EnrolUserToPackage;

DELIMITER $$
CREATE PROCEDURE `EnrolUserToPackage`
(IN `PackageId` INT, IN `UserId` INT, IN `GroupId` INT, IN `Type` INT, OUT `Id` int)
    MODIFIES SQL DATA
BEGIN
  DECLARE _code VARCHAR(30);
  DECLARE _ret INT;

SELECT random_num
FROM (SELECT hex(FLOOR(RAND() * 1116777216)) AS random_num
  WHERE "random_num" NOT IN (SELECT `code` FROM m_cviceni_enrolment union SELECT `code` FROM m_package_enrolment) LIMIT 1
) c INTO _code;

INSERT INTO m_package_enrolment(package_id, `code`, type, user_id, group_id) 
values (PackageId, _code, Type, UserId, GroupId);

SELECT LAST_INSERT_ID() INTO _ret;

SET Id = _ret;
END
$$
DELIMITER ;