DROP PROCEDURE IF EXISTS EnrolUserToExercise;

DELIMITER $$
CREATE PROCEDURE `EnrolUserToExercise`
(IN `ExerciseId` INT, IN `UserId` INT, IN `GroupId` INT, IN `Type` INT, OUT `Id` int)
    MODIFIES SQL DATA
BEGIN
  DECLARE _code VARCHAR(30);
  DECLARE _exerciseId INT;
  DECLARE _ret INT;

SELECT random_num
FROM (SELECT hex(FLOOR(RAND() * 1116777216)) AS random_num
  WHERE "random_num" NOT IN (SELECT `code` FROM m_cviceni_enrolment) LIMIT 1
) c INTO _code;

SELECT e.exerciseId FROM (SELECT k.exerciseId from m_katalog k where k.id = exerciseId) e INTO _exerciseId;

INSERT INTO m_cviceni_enrolment(katalog_id, exerciseId, `code`, type, user_id, group_id, original_group_id) 
values (exerciseId, _exerciseId, _code, Type, UserId, GroupId, GroupId);

SELECT LAST_INSERT_ID() INTO _ret;

SET Id = _ret;
END
$$
DELIMITER ;