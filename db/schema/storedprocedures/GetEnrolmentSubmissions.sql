DROP PROCEDURE IF EXISTS GetEnrolmentSubmissions;

DELIMITER $$

CREATE PROCEDURE `GetEnrolmentSubmissions`(IN `EnrolmentId` INT, IN `UserId` INT)
BEGIN
    select e.emailAddress, time_spent as timeSpent, ifnull(s.entryId, -1) as entryId, (case when gem.id is null then 0 else 1 end) as from_group, s.updatedAt as lastUpdated, note, score, te.updatedAt as noteUpdatedAt
    from (
                                          select enr.user_id, enr.id as enrolment_id, em.emailAddress
            FROM m_cviceni_submission s
                left join m_submission_emailAddresses em on s.entryid = em.submission_id
                left join m_teacher_evaluation e on e.entry_id = s.entryid
                left join m_cviceni_enrolment enr on enr.code = s.lms_hash
        UNION
            select sv.user_id, enrolment_id, emailAddress
            from EnrolmentWithEmailsAndSubmissionsView sv) e

        left join m_submission_emailAddresses em on e.emailAddress = em.emailAddress
        left join m_cviceni_submission s on s.entryid = em.submission_id
        left join m_teacher_evaluation te on te.entry_id = s.entryid
        left join m_cviceni_enrolment enrol on enrol.id = e.enrolment_id
        left join m_cviceni_group_email gem on gem.group_id = enrol.group_id and gem.emailAddress = e.emailAddress
    where e.user_id = UserId and e.enrolment_id = EnrolmentId
    ORDER BY e.emailAddress;
END

$$
DELIMITER ;