DROP PROCEDURE IF EXISTS GetExercisesForUser;

DELIMITER $$

CREATE PROCEDURE `GetExercisesForUser`(IN `idUser` INT, IN `idLanguage` INT)
BEGIN
	DECLARE i INT;
    
    select count(c.id) INTO i from m_catalogs c 
    	left join m_catalog_userGroup cu on cu.idCatalog = c.id
		left join m_user_groups g on g.id = cu.idUserGroup
		left join m_user_group_membership gm on gm.idUserGroup = g.id
		left join m_user u on u.id = gm.idUser
        where u.id = idUser and c.idLanguage = idLanguage;
        
    /* user is not member of any catalog group => return all public exercises */
	IF i = 0 THEN
		select k.* from m_catalogs c 
		left join m_catalog_exercise ce on ce.idCatalog = c.id
		left join m_katalog k on ce.exerciseId = k.exerciseId and k.enabled = 1
		where c.isPublic = 1 and c.idLanguage = idLanguage;
    ELSE
        select k.* from v_effective_exercises e join m_katalog k on e.exerciseId = k.exerciseId and k.enabled = 1 where e.idUser = idUser;

	END IF;
END

$$
DELIMITER ;