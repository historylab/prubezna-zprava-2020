CREATE OR REPLACE view CatalogExerciseView
AS
SELECT * FROM (
SELECT DISTINCT `name`, `slug`, `exerciseId`, image_url, thumb_image_url, url FROM `m_katalog` where enabled=1
union 
select max(`name`), max(`slug`), exerciseId, max(image_url), max(thumb_image_url), max(url) FROM `m_katalog` where enabled=0 and exerciseId not in (SELECT DISTINCT exerciseId FROM `m_katalog` where enabled=1) group by exerciseId
) c order by c.exerciseId