
CREATE OR REPLACE view EnrolmentWithEmailsAndSubmissionsView
AS
SELECT ce.id AS enrolment_id,
  ce.katalog_id          AS katalog_id,
  ce.code                AS code,
  ce.type                AS type,
  ce.user_id             AS user_id,
  ce.group_id            AS group_id,
  ce.is_active           AS is_active,
  g.name                 AS name,
  gem.emailaddress       AS emailaddress,
  subm.submissions_count AS submissions_count,
  subm.updatedat         AS updatedat,
  subm.lastEntryId       AS lastEntryId
FROM m_cviceni_enrolment ce
  LEFT JOIN m_cviceni_group g ON g.id = ce.group_id AND g.user_id = ce.user_id
  LEFT JOIN m_cviceni_group_email gem ON gem.group_id = g.id
  LEFT JOIN (
                      SELECT e.emailaddress   AS emailaddress,
    max(entryId) lastEntryId,
    max(s.updatedat) AS updatedat,
    count(0)             AS submissions_count,
    s.lms_hash       AS lms_hash
  FROM m_cviceni_submission s
    LEFT JOIN m_submission_emailAddresses e
    ON s.entryid = e.submission_id
  GROUP BY e.emailaddress, s.lms_hash) subm
  ON trim(lower(ce.code)) = trim(lower(subm.lms_hash))
    AND trim(lower(subm.emailaddress)) = trim(lower(gem.emailaddress))