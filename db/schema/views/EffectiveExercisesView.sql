CREATE OR REPLACE view v_effective_exercises
AS
select distinct ugm.idUser, k.exerciseId from m_catalogs c 
left join m_catalog_exercise ce on ce.idCatalog = c.id
left join m_katalog k on ce.exerciseId = k.exerciseId and k.enabled = 1
left join m_catalog_userGroup ug on ug.idCatalog = c.id
left join m_user_group_membership ugm on ugm.idUserGroup = ug.idUserGroup