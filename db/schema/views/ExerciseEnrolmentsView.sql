CREATE OR REPLACE VIEW ExerciseEnrolmentsView  
AS  
select 
	u.id AS userId,
	slug,
	ce.id AS id,
	ce.code AS code,
	k.url AS url,
	ce.type AS type,
	g.id AS group_id,
	k.name AS name,
	k.thumb_image_url AS image_url,
	1 AS is_active,
    0 as is_package,
	ifnull(gg.groupCount, 0) AS total,
	ifnull(sub1.submitted, 0) AS submitted 
FROM m_cviceni_enrolment ce
LEFT JOIN CatalogExerciseView k on (ce.exerciseId = k.exerciseId)
LEFT JOIN m_cviceni_group g on g.id = ce.group_id
left join m_user u on u.id = ce.user_id
LEFT JOIN (
	select count(*) submitted, e.id as id, null as updatedAt
	from m_cviceni_submission s
	left join m_cviceni_enrolment e on e.id = s.enrolment_id
	left join m_submission_emailAddresses se on se.submission_id = s.entryid
	where se.emailAddress is not null 
	group by id
) sub1 on sub1.id = ce.id
left join (
	select e.id, count(*) as groupCount from m_cviceni_enrolment e
	left join m_cviceni_group_email g on g.group_id = e.group_id
	group by e.id
) gg on gg.id = ce.id
WHERE ce.is_active = 1