CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `submissionsview`  AS  
select 
	`e`.`emailAddress` AS `emailaddress`,
	max(`s`.`entryid`) AS `lastEntryId`,
	max(`s`.`updatedAt`) AS `updatedat`,
	count(0) AS `submissions_count`,`s`.`lms_hash` AS `lms_hash` 
from (`m_cviceni_submission` `s` 
	left join `m_submission_emailaddresses` `e` on((`s`.`entryid` = `e`.`submission_id`))
) group by `e`.`emailAddress`,`s`.`lms_hash`;