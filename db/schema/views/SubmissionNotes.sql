CREATE OR REPLACE view SubmissionNotes
AS
select * from (
  select 
    entryId,
    (case when JSON_VALID(exerciseData) = 1 then
       JSON_UNQUOTE(JSON_EXTRACT(exerciseData, REPLACE(JSON_UNQUOTE(JSON_SEARCH(exerciseData, 'one', 'text-notes')), '.id', '.value')))
    else NULL end) as notes
  from m_cviceni_submission  
    WHERE exerciseData is not null
     and JSON_VALID(exerciseData) = 1
) as c where c.notes is not null and (c.notes not in ('null', 'notes'))