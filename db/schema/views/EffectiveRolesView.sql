CREATE OR REPLACE view v_effective_roles
AS
select 0 as id, u.id as idUser, r.id as idRole from m_user_groups g
left join m_user_group_membership mu on mu.idUserGroup = g.id
left join m_user u on mu.idUser = u.id
left join m_user_group_role_assignments uga on uga.idUserGroup = g.id
left join m_role r on uga.idRole = r.id
UNION
select 0 as id, userId as idUser, roleId as idRole from m_role_assignments