CREATE OR REPLACE VIEW PackageEnrolmentsView  
AS  
select 
	u.id AS userId,
    p.slug,
	pe.id AS id,
	pe.code AS code,
	'' AS url,
	pe.type AS type,
	g.id AS group_id,
	p.name AS name,
	p.thumb_image_url AS image_url,
	1 AS is_active,
    1 as is_package,
	0 AS total,
	0 AS submitted 
FROM m_package_enrolment pe
LEFT JOIN m_packages p on pe.package_id = p.id
LEFT JOIN m_cviceni_group g on g.id = pe.group_id
left join m_user u on u.id = pe.user_id
WHERE pe.is_active = 1