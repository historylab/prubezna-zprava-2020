FROM node:14.15-alpine3.11
#RUN apk add --no-cache curl make gcc g++ linux-headers binutils-gold gnupg libstdc++ libc6-compat python

COPY ./app/package.json /tmp/client/package.json
COPY ./app/package-lock.json /tmp/client/package-lock.json
RUN cd /tmp/client && npm ci --only=production --silent --loglevel=error
RUN mkdir -p /usr/app/client && cp -a /tmp/client/node_modules /usr/app/client/

WORKDIR /usr/app