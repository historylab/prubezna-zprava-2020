const auth = require("./../auth");
const seq = require("./../sequelize");
const flash = require("../flash_res");

let wrap = fn => (...args) => fn(...args).catch(args[2]);

const validateSchema = require("./../schemas/validator");

module.exports = app => {
  app.delete(
    "/api/note",
    auth.ensureLoggedIn,
    validateSchema("deleteEnrolment"),
    wrap(async (req, res) => {
      res.json({ success: true });
    })
  );

  app.post(
    "/api/evaluation",
    auth.ensureLoggedIn,
    validateSchema("addEvaluation"),
    wrap(async (req, res) => {
      const data = req.body;
      const userId = req.user.id;
      const { enrolmentId, note, entryId, score } = data;
      const t = req.t;
      const { Enrolment, Evaluation, Submission } = seq();
      const enrolment = await Enrolment.findByIdForUser(userId, enrolmentId);
      const config = app.get("config");
      if (enrolment === null) {
        res.status(404).send();
        return;
      }

      const submission = await Submission.findOne({
        where: { lms_hash: enrolment.code, id: entryId }
      });
      if (submission === null) {
        res.status(404).send();
        return;
      }

      let evaluation = await Evaluation.findOne({
        where: { user_id: userId, entry_id: entryId }
      });

      if (evaluation) {
        await evaluation.update({ note, score });
      } else {
        evaluation = await Evaluation.create({
          note,
          score,
          entry_id: entryId,
          user_id: userId
        });
      }

      const emails = await submission.getEmails().map(e => e.emailAddress);
      const catalog = (await enrolment.getCatalogExercise()).get({
        plain: true
      });
      const urlTemplate = config.application.exerciseViewUrl;
      const messageData = {
        from: config.email.from,
        to: emails,
        template: "exerciseEvaluated",
        language: submission.language,
        contentData: {
          submission: { viewUrl: submission.viewUrl },
          catalog: { name: catalog.name },
          evaluation,
          viewUrl: urlTemplate.format(submission.id)
        }
      };

      global.dispatcher.dispatch("email", messageData);

      flash(res, {
        type: "success",
        text: t("addEvaluation.success", "Vaše hodnocení bylo uloženo.")
      });
      res.redirect(303, `/api/my/exercise/${enrolment.code}`);
    })
  );
};
