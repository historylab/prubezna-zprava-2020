const expressAjv = require("express-ajv");
const schema = expressAjv.schema;

schema.addSchema("enrolToPackage", require("./enrolToPackage"));
schema.addSchema("enrolToPackageExercise", require("./enrolToPackageExercise"));

module.exports = expressAjv.validatorFactory;
