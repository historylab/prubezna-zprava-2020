const validateSchema = require("./schemas/validator");

module.exports = (app, auth, seq, wrap, flash) => {
  const transformTryItUrl = (req, o) => {
    const { tryItUrl, exercises, ...rest } = o;
    let newTryItUrl = tryItUrl || "";
    if (req.user) {
      newTryItUrl = newTryItUrl.replace(
        "{emailAddress}",
        "&" + req.user.emailAddress
      );
    } else {
      newTryItUrl = tryItUrl.replace("{emailAddress}", "");
    }
    return { ...rest, tryItUrl: newTryItUrl, exercises };
  };

  app.get(
    "/api/catalog/package/:slug([a-z0-9A-Z-]+)",
    wrap(async (req, res) => {
      const slug = req.params.slug;
      const { Package } = seq();
      const pkg = await Package.findBySlug(slug);
      if (pkg) {
        const { Language, ...p } = transformTryItUrl(req, pkg);
        res.json({
          ...p,
          exercises: pkg.exercises.map(e => transformTryItUrl(req, e))
        });
        return;
      }

      res.status(404).send();
    })
  );

  app.post(
    "/api/package/:packageId([0-9]{1,4})/exercise/:exerciseId([0-9]{1,5})/enrol",
    auth.ensureLoggedIn,
    //validateSchema("enrolToPackageExercise"),
    wrap(async (req, res) => {
      const data = req.body;
      const userId = req.user.id;
      const { packageId, exerciseId } = req.params;

      /*const { type, groupId, groupName } = data;
      const { Group, Package, sequelize } = seq();
      const pkg = await Package.findById(packageId);
      if (pkg === null) {
        res.status(404).send();
        return;
      }
      let group = null;
      if (groupId > 0) {
        group = await Group.findById(groupId);
        if (group === null) {
          res.status(404).send();
          return;
        }
        await group.updateAttributes({ lastUsedAt: sequelize.fn("NOW") });
      } else {
        const year = new Date().getFullYear();
        group = await Group.create({
          name: groupName,
          yearStart: year,
          yearEnd: year + 1,
          user_id: userId,
          lastUsedAt: sequelize.fn("NOW")
        });
      }

      const code = await pkg.enrol(req.user, group, type);

      res.json({ code });*/
      flash(res, {
        type: "success",
        //t(
        text:
          //"shareWithOthers.success",
          "Vyplněné cvičení bylo odesláno na emailové adresy ve třídě."
        //)
      });
      res.json({ code: "1234567" });
    })
  );

  app.post(
    "/api/package/enrol",
    auth.ensureLoggedIn,
    validateSchema("enrolToPackage"),
    wrap(async (req, res) => {
      const data = req.body;
      const userId = req.user.id;
      const { packageId, type, groupId, groupName } = data;
      const { Group, Package, sequelize } = seq();
      const pkg = await Package.findById(packageId);
      if (pkg === null) {
        res.status(404).send();
        return;
      }
      let group = null;
      if (groupId > 0) {
        group = await Group.findById(groupId);
        if (group === null) {
          res.status(404).send();
          return;
        }
        await group.updateAttributes({ lastUsedAt: sequelize.fn("NOW") });
      } else {
        const year = new Date().getFullYear();
        group = await Group.create({
          name: groupName,
          yearStart: year,
          yearEnd: year + 1,
          user_id: userId,
          lastUsedAt: sequelize.fn("NOW")
        });
      }

      const code = await pkg.enrol(req.user, group, type);

      res.json({ code });
    })
  );

  app.get(
    "/api/my/package/:code([a-z0-9A-Z]+)",
    auth.ensureLoggedIn,
    wrap(async (req, res, next) => {
      const code = req.params.code;
      const { Package, PackageEnrolment, CatalogExercise } = seq();
      const enrolment = await PackageEnrolment.findByCodeForUser(
        req.user.id,
        code
      );
      const pkg = await Package.findById(enrolment.package_id, {
        include: [{ model: CatalogExercise, as: "exercises" }]
      });

      const result = {
        id: enrolment.id,
        code: enrolment.code,
        ...pkg.toJSON()
      };

      result.exercises = result.exercises.map(e => {
        const { m_package_exercises, ...rest } = e;
        return {
          ...rest,
          enrolment: {
            enrolled: false
          }
        };
      });

      /*
      total: 10,
            submitted: 8,
            enrolled: true,
            code: "12376E27
      */

      const tmp = {
        emailAddresses: [
          {
            id: 2514,
            name: "Jan Novák",
            emailAddress: "jan@novak.cz",
            group_id: 978,
            hasSubmissions: false
          },
          {
            id: 2513,
            emailAddress: "frantisek@novak.cz",
            group_id: 978,
            hasSubmissions: false
          },
          {
            id: 2512,
            emailAddress: "fejk@email.cz",
            group_id: 978,
            hasSubmissions: true
          },
          {
            id: 2511,
            emailAddress: "adam@gmaul.cz",
            group_id: 978,
            hasSubmissions: true
          }
        ],
        submissions: [
          {
            emailAddresses: [2514],
            viewUrl: "aaaa",
            timeSpent: 23 * 60 * 1000 + 47,
            entryId: 1,
            submittedOn: +Date.now(),
            exerciseId: 46,
            hidden: true
          },
          {
            emailAddresses: [2512, 2513],
            entryId: 2,
            exerciseId: 2,
            starred: true
          },
          {
            emailAddresses: [2511, 2512, 2514],
            exerciseId: 34
          }
        ]
      };

      res.json({ ...result, ...tmp });
    })
  );
};
