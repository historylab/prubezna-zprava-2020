const _ = require("lodash");

const dbmysql = require("./../dbmysql");
const auth = require("./../auth");
const seq = require("./../sequelize");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;

let wrap = fn => (...args) => fn(...args).catch(args[2]);

module.exports = app => {
  app.get(
    "/api/my/exercise/:code([a-z0-9A-Z]+)",
    auth.ensureLoggedIn,
    (req, res, next) => {
      const code = req.params.code;
      dbmysql.fetchExerciseByCode(req.user, code, (error, exercise) => {
        if (error) {
          next(error);
          return;
        }
        if (!exercise) {
          res.status(404);
          return;
        }
        res.json(exercise);
      });
    }
  );

  app.get(
    "/api/group/:id/exercises",
    auth.ensureLoggedIn,
    wrap(async (req, res, next) => {
      const { Group } = seq();
      let groupId = -1;
      const id = req.params.id;
      if (!id) {
        res.status(404).send();
        returnl;
      }
      const group = await Group.findOne({
        attributes: ["id", "name", "color"],
        where: { user_id: req.user.id, id }
      });

      if (group === null) {
        res.status(404).send();
        return;
      }

      groupId = group.id;

      dbmysql.fetchExerciseList(req.user.id, groupId, (error, data) => {
        if (error) {
          next(error);
          return;
        }
        res.json({ group, exercises: data.exercises });
      });
    })
  );

  app.get(
    "/api/my/groups",
    auth.ensureLoggedIn,
    wrap(async (req, res) => {
      const { Group, sequelize } = seq();

      const groups = await Group.findAll({
        where: { user_id: req.user.id },
        order: [
          ["lastUsedAt", "DESC"],
          ["isHidden", "ASC"],
          ["isFavourite", "DESC"],
          ["name", "ASC"]
        ],
        attributes: {
          exclude: ["user_id", "note"]
        }
      });

      return res.json(groups);
    })
  );
};
