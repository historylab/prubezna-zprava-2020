"use strict";

module.exports = app => {
  function getEnvironment() {
    return Object.keys(process.env)
      .filter(key => /^REACT_APP_/i.test(key))
      .reduce(
        (env, key) => {
          env[key] = process.env[key];
          return env;
        },
        { NODE_ENV: process.env.NODE_ENV || "development" }
      );
  }

  app.get("/api/env", (req, res) => {
    res.type(".js");
    res.status(200).send("window._env=" + JSON.stringify(getEnvironment()));
  });
};
