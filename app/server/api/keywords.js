module.exports = (app, auth, seq, wrap) => {
  const {KeyWord, Language} = seq();
  app.get(
    "/api/keywords",
    wrap(async (req, res) => {
      const language = await Language.getByCode(req.language);
      const keyWords = await KeyWord.getEnabledKeywords(language.code)
      res.json({keyWords});
    })
  );
  app.get(
    "/api/keywords/to_package",
    wrap(async (req, res) => {
      const language = await Language.getByCode(req.language);
      const keyWords = await KeyWord.getEnabledToPackageKeywords(language.code);
      res.json({keyWords});
    })
  );
  app.get(
    "/api/keywords/to_filter",
    wrap(async (req, res) => {
      const language = await Language.getByCode(req.language);
      const keyWords = await KeyWord.getEnabledToFilterKeywords(language.code);
      res.json({keyWords});
    })
  );
};