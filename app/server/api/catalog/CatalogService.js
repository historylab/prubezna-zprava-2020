const dbmysql = require("../../dbmysql");
//const cacheMiddleware = require("../middleware/cacheMiddleware");

module.exports = (app, auth, seq, wrap) => {
  const { User, CatalogExercise, Language } = seq();

  const postProcess = (req, exercises) => {
    return exercises
      .filter(e => true)
      .map(e => e.toJSON())
      .map(e => {
        const {
          code,
          enabled,
          language,
          slug,
          instructionsUrl,
          tryItUrl,
          image_url,
          url,
          ...rest
        } = e;

        return rest;
      });
    /*.map(e => {
        if (req.user) {
          e.tryItUrl = e.tryItUrl.replace(
            "{emailAddress}",
            "&" + req.user.emailAddress
          );
        } else {
          e.tryItUrl = e.tryItUrl.replace("{emailAddress}", "");
        }
        return e;
      });*/
  };

  const fetchCatalog = async (
    user,
    exerciseId = -1,
    slug = "",
    language,
    cb
  ) => {
    const { CatalogExercise, Language } = seq();

    let findOptions = { where: { enabled: true } };
    if (exerciseId > 0) {
      findOptions = { where: { enabled: true, id: exerciseId } };
    } else if ((slug || "").match(/./)) {
      findOptions = { where: { enabled: true, slug } };
    } else {
      findOptions = {
        where: { enabled: true, language: Language.getCode(language) }
      };
    }

    findOptions = { ...findOptions, order: ["name"] };

    const catalogExercises = await CatalogExercise.findAll(findOptions);

    cb(catalogExercises, null);
  };

  function fetchExercise(code, slug, req, res, next) {
    fetchCatalog(req.user, code, slug, req.language, (error, exercises) => {
      if (error) {
        next(error);
        return;
      }

      exercises = postProcess(req, exercises);

      if (code) {
        if (exercises.length !== 1) {
          res.status(404);
          res.send({});
          return;
        }
        res.json({ exercise: exercises[0] });
        return;
      }
    });
  }

  app.get(
    "/api/catalog/:code([0-9]+)",
    //cacheMiddleware,
    (req, res, next) => {
      fetchExercise(req.params.code, null, req, res, next);
    }
  );

  app.get(
    "/api/catalog/:slug([a-z][a-z|\\-|0-9]+)$",
    //cacheMiddleware,
    wrap(async (req, res) => {
      const slug = req.params.slug;
      const { CatalogExercise } = seq();

      const findOptions = { where: { enabled: true, slug } };

      const catalogExercise = await CatalogExercise.findOne(findOptions);

      const result = {
        code,
        catalogUrl,
        enabled,
        metadata,
        language,
        instructionsUrl,
        tryItUrl,
        thumb_image_url,
        url,
        ...rest
      } = catalogExercise.toJSON();

      res.json({ exercise: result });
    })
  );

  app.get(
    "/api/catalog",
    (req, res, next) => {
      const idUser = req.user ? req.user.id : -1;
      const languageCode = Language.getCode(req.query.lang || req.language);
      req.language = languageCode;
      req.cacheKey = `/api/catalog-idUser=${idUser};languageCode=${languageCode};`;
      next();
    },
    //cacheMiddleware,
    wrap(async (req, res) => {
      const language = await Language.getByCode(req.language);
      let exercises = [];
      if (req.user) {
        const user = User.build(req.user);
        exercises = await user.getEffectiveExercises(language.id);
      }

      exercises = exercises.filter(e => e.id);

      if (exercises.length === 0) {
        exercises = await CatalogExercise.getPublicCatalogExercises(
          language.id
        );
      }

      exercises = postProcess(req, exercises);

      res.json({ exercises });
    })
  );
};
