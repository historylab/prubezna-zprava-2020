const _ = require("lodash");
const axios = require("axios");
const validateSchema = require("./schemas/validator");
const logger = require("./../../logger")("user");
const flash = require("../../flash_res");

module.exports = (app, auth, seq, wrap) => {
  app.post(
    "/api/user/setLanguage",
    validateSchema("setLanguage"),
    auth.ensureLoggedIn,
    wrap(async (req, res) => {
      const { User, Language } = seq();
      const userId = req.user.id;
      const languageCode = Language.getCode(
        req.body.language || req.query.lang || req.language
      );
      const user = await User.findById(userId);
      user.language = languageCode;
      user.lastAccess = Date.now();
      await user.save();
      res.json();
    })
  );

  app.get(
    "/api/user/:id([0-9]{0,10})?",
    auth.ensureLoggedIn,
    wrap(async (req, res) => {
      const requestedUserId = req.params.id;
      const { User } = seq();

      const attributes = [
        "id",
        "userName",
        "firstName",
        "lastName",
        "emailAddress",
        "confirmed",
        "suspended"
      ];
      if (requestedUserId) {
        const user = await User.findById(requestedUserId, {
          attributes
        });
        if (user === null) {
          res.status(404).send("Uživatel nenalezen");
          return;
        }
        res.json(user);
        return;
      }

      const users = await User.findAll({
        attributes,
        order: ["userName"]
      });
      res.json(users);
    })
  );

  const verifyReCaptcha = (req, res, next) => {
    const config = app.get("config");
    const secretKey = config.recaptcha.secretKey;
    const requestData = { secret: secretKey, response: req.body.recaptcha };
    const transformRequest = (jsonData = {}) =>
      Object.entries(jsonData)
        .map(x => `${encodeURIComponent(x[0])}=${encodeURIComponent(x[1])}`)
        .join("&");

    axios
      .post(
        "https://www.google.com/recaptcha/api/siteverify",
        transformRequest(requestData),
        { headers: { "Content-Type": "application/x-www-form-urlencoded" } }
      )
      .then(response => {
        const result = response.data;
        console.info("verifyReCaptcha", result);
        if (result.success) {
          next();
          return;
        }
        res.status(406);
        res.send("ReCAPTCHA not verified.");
      });
  };

  app.post(
    "/api/user/registration",
    validateSchema("createUser"),
    verifyReCaptcha,
    wrap(async (req, res) => {
      const registrationData = req.body;
      const { User, Language } = seq();
      const t = req.t;
      const config = app.get("config");

      try {
        const user = User.build({ ...registrationData });
        user.userName = user.email;
        user.password_confirmation = registrationData.passwordConfirmation;
        user.confirmed = true;
        user.language = Language.getCode(registrationData.language);
        user.isTeacher =
          (registrationData.teacher || "").toLowerCase() === "ano";

        await user.save();

        const baseUrl = config.application.historyLabUrl;

        const userDto = {
          firstName: user.firstName,
          lastName: user.lastName,
          emailAddress: user.emailAddress,
          institution: user.institution
        };

        const messageData = {
          from: config.email.from,
          to: [user.emailAddress],
          template: "userRegistered",
          contentData: {
            loginUrl: baseUrl + "/moje",
            userDto,
            userStr: JSON.stringify(userDto)
          }
        };

        global.dispatcher.dispatch("email", messageData);

        flash(res, {
          type: "success",
          text: t(
            "user.accountCreated",
            "Váš účet byl vytvořen, můžete se přihlásit do HistoryLabu."
          )
        });
        res.status(200).json({});
      } catch (error) {
        logger.error(JSON.stringify(error));
        if (error.name && error.name === "SequelizeUniqueConstraintError") {
          const innerError = error.errors[0];
          delete innerError.parent;
          delete innerError.original;
          delete innerError.sql;
          delete innerError.instance;

          const message = t(
            "user.accountExists",
            "Tato emailová adresa je již zaregistrována. Pokud jste zapomněli heslo, můžete jej obnovit odkazem na přihlašovací stránce."
          );

          res.json({
            error: {
              code: "UniqueValidation",
              message,
              target: "emailAddress",
              innerError,
              form: {
                emailAddress: message
              }
            }
          });
          return;
        }

        if (error.name && error.name === "SequelizeValidationError") {
          res.status(400).json({ message: JSON.stringify(error) });
          return;
        }
        res.status(500).json({ message: JSON.stringify(error) });
      }
    })
  );
};
