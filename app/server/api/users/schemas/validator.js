const expressAjv = require("express-ajv");
const schema = expressAjv.schema;

schema.addSchema("createUser", require("./createUser"));
schema.addSchema("updateUser", require("./updateUser"));
schema.addSchema("userChangePassword", require("./userChangePassword"));
schema.addSchema("updatePassword", require("./updatePassword"));
schema.addSchema("setLanguage", require("./setLanguage"));

module.exports = expressAjv.validatorFactory;
