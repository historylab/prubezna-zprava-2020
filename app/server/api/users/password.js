const crypto = require("crypto");
const _ = require("lodash");
const validateSchema = require("./schemas/validator");
const flash = require("../../flash_res");

module.exports = (app, auth, seq, wrap) => {
  app.get(
    "/api/user/reset",
    wrap(async (req, res) => {
      const { PasswordReset } = seq();
      const token = req.query.resetPasswordToken;
      const t = req.t;
      let reset = await PasswordReset.findOne({
        where: { token }
      });
      if (!reset) {
        res.status(404).json({
          message: t(
            "password.missingReset",
            "Záznam neexistuje, zopakujte proces obnovy hesla znovu."
          )
        });
        return;
      }
      if (reset && reset.requestedOn > Date.now() + 360000) {
        res.status(400).json({
          message: t(
            "password.resetExpired",
            "Žádost o obnovu hesla vypršela, zopakujte proces obnovy hesla znovu."
          )
        });
      }
      res.status(200).json({});
    })
  );

  app.post(
    "/api/user/updatePassword",
    validateSchema("updatePassword"),
    wrap(async (req, res) => {
      const { User, PasswordReset } = seq();
      const token = req.body.token;
      const t = req.t;
      let reset = await PasswordReset.findOne({
        where: { token }
      });
      if (!reset) {
        res.status(404).json({
          message: t(
            "password.missingReset",
            "Záznam neexistuje, zopakujte proces obnovy hesla znovu."
          )
        });
        return;
      }
      if (reset && reset.requestedOn > Date.now() + 360000) {
        res.status(400).json({
          message: t(
            "password.resetExpired",
            "Žádost o obnovu hesla vypršela, zopakujte proces obnovy hesla znovu."
          )
        });
        return;
      }

      const user = await User.findById(reset.userId);
      await user.update({
        password: req.body.password,
        password_confirmation: req.body.password
      });

      reset.destroy();

      flash(res, {
        type: "success",
        text: t(
          "password.resetSuccess",
          "Heslo bylo změněno, můžete se přihlásit."
        )
      });

      res.status(200).json({});
    })
  );

  app.post(
    "/api/user/forgottenPassword",
    wrap(async (req, res) => {
      const emailAddress = req.body.emailAddress;
      const t = req.t;
      if (emailAddress === "") {
        res.status(400).send("email required");
      }
      const { User, PasswordReset } = seq();
      const config = app.get("config");

      const user = await User.findOne({
        where: {
          email: emailAddress
        }
      });

      if (user === null) {
        res.json({
          error: {
            code: "EmailNotFound",
            message: "Email address was not found in database.",
            target: "emailAddress",
            form: {
              emailAddress: t(
                "forgottenPassword.emailNotFound",
                "Emailová adresa nebyla rozpoznána, zopakujte akci nebo se zkuste zaregistrovat."
              )
            }
          }
        });
      } else {
        const token = crypto.randomBytes(20).toString("hex");

        const urlTemplate = config.application.resetPasswordUrl;

        let reset = await PasswordReset.findOne({
          where: { userId: user.id }
        });

        if (reset) {
          await reset.update({ requestedOn: Date.now(), token });
        } else {
          reset = await PasswordReset.create({
            token,
            userId: user.id,
            requestedOn: Date.now(),
            reRequestedOn: Date.now()
          });
        }

        const messageData = {
          from: config.email.from,
          to: [emailAddress],
          language: user.language,
          template: "passwordReset",
          contentData: {
            resetUrl: urlTemplate.format(token)
          }
        };

        global.dispatcher.dispatch("email", messageData);

        flash(res, {
          type: "success",
          text: t(
            "password.resetEmailSent",
            "Email s odkazem na obnovu hesla byl odeslán"
          )
        });
        res.status(200).json({});
      }
    })
  );
};
