const _ = require("lodash");
const Sequelize = require("sequelize");
const SequelizeValidationError = Sequelize.ValidationError;
const Op = Sequelize.Op;
const validateSchema = require("./schemas/validator");

module.exports = (app, auth, seq, wrap) => {
  app.get(
    "/api/group/:id([0-9]{0,10})",
    auth.ensureLoggedIn,
    wrap(async (req, res) => {
      const groupId = req.params.id;
      const userId = req.user.id;
      const { Group } = seq();

      if (groupId) {
        const group = await Group.findByIdForUser(userId, groupId);
        if (group === null) {
          res.status(404).send();
          return;
        }
        const emailAddresses = (await group.getMembers()).map(e => {
          return {
            id: e.id,
            emailAddress: e.emailAddress,
            group_id: e.group_id,
            name: e.name,
            hasSubmissions: e.hasSubmissions === 1
          };
        });
        group.color = group.color || "";
        group.note = group.note || "";
        res.json({
          ...group.toJSON(),
          emailAddresses: _.orderBy(emailAddresses, ["emailAddress"])
        });
        return;
      }

      const groups = await Group.findAll({
        where: { user_id: userId },
        order: ["name"]
      });
      res.json({ groups });
    })
  );

  app.get(
    "/api/group",
    auth.ensureLoggedIn,
    wrap(async (req, res) => {
      const userId = req.user.id;
      const { Group } = seq();

      const groups = await Group.findAll({
        where: { user_id: userId },
        order: ["name"],
        attributes: {
          include: [
            [
              Group.sequelize.literal(
                "(SELECT COUNT(*) FROM m_cviceni_enrolment AS e WHERE e.group_id = `Group`.`id`)"
              ),
              "exerciseCount"
            ]
          ],
          exclude: ["user_id"]
        }
      });
      res.json({ groups });
    })
  );

  app.post(
    "/api/group/emailAddress",
    auth.ensureLoggedIn,
    validateSchema("groupAddEmailAddress"),
    wrap(async (req, res) => {
      const data = req.body;
      const userId = req.user.id;
      const { groupId, emailAddresses } = data;
      const t = req.t;
      const { Group, GroupEmail } = seq();

      const group = await Group.findByIdForUser(userId, groupId);
      if (group === null) {
        res.status(404).send();
        return;
      }

      const emails = (await group.getEmails()).map(e =>
        e.emailAddress.toLowerCase()
      );

      const missingEmails = emailAddresses
        .filter(e => !emails.includes(e))
        .map(e => {
          return { emailAddress: e, group_id: group.id };
        });

      if (missingEmails.length === 0) {
        req.flash("warning", t("emailAddressClass.exists"));
      } else if (missingEmails.length === emailAddresses.length) {
        req.flash("success", t("emailAddressClass.added"));
      } else if (missingEmails.length === 1) {
        req.flash("success", t("emailAddressClass.oneAdded"));
      } else {
        req.flash("warning", t("emailAddressClass.someAdded"));
      }

      await GroupEmail.bulkCreate(missingEmails);

      const referrer = req.get("Referrer");
      const refMatch = referrer.match(/\/cviceni\/detail\/(.+)/);
      if (refMatch) {
        res.redirect("/api/my/exercise/" + refMatch[1]);
        return;
      }

      res.redirect(`/api/group/${group.id}`);
    })
  );

  app.delete(
    "/api/group/emailAddress",
    auth.ensureLoggedIn,
    validateSchema("groupRemoveEmailAddress"),
    wrap(async (req, res) => {
      const data = req.body;
      const userId = req.user.id;
      const { groupId, emailAddress, id } = data;
      const t = req.t;
      const { Group, GroupEmail } = seq();

      const group = await Group.findByIdForUser(userId, groupId);
      if (group === null) {
        res.status(404).send();
        return;
      }

      const groupEmail = await GroupEmail.findOne({
        where: {
          group_id: group.id,
          id,
          emailAddress: { [Op.like]: emailAddress }
        }
      });

      if (groupEmail === null) {
        res.status(404).send();
        return;
      }

      groupEmail.destroy();

      req.flash("success", t("emailAddressClass.removed"));

      const referrer = req.get("Referrer");
      const refMatch = referrer.match(/\/cviceni\/detail\/(.+)/);
      if (refMatch) {
        res.redirect(303, "/api/my/exercise/" + refMatch[1]);
        return;
      }

      res.redirect(303, `/api/group/${group.id}`);
    })
  );

  app.post(
    "/api/group",
    auth.ensureLoggedIn,
    validateSchema("groupCreate"),
    wrap(async (req, res) => {
      const userId = req.user.id;
      const t = req.t;
      const {
        id,
        name,
        note,
        color,
        yearStart,
        yearEnd,
        isFavourite,
        isHidden
      } = req.body;
      const { Group } = seq();

      try {
        if (id > 0) {
          const group = await Group.findByIdForUser(userId, id);
          await group.update({
            name,
            note,
            color,
            yearStart,
            yearEnd,
            isFavourite,
            isHidden
          });

          req.flash("success", t("class.updated"));
          res.send(204); // no content
          return;
        }

        const group = await Group.create({
          name,
          note,
          color,
          yearStart,
          yearEnd,
          user_id: userId
        });
        req.flash("success", t("class.created"));
        res
          .status(201)
          .set({ Location: "/tridy/" + group.id })
          .send();
      } catch (err) {
        if (err instanceof SequelizeValidationError) {
          const errors = err["errors"].map(e => {
            const { message, path, validatorKey, value } = e;
            return {
              message,
              i18message: t("group." + path + "." + validatorKey),
              path,
              validatorKey,
              value
            };
          });
          res
            .status(400)
            .json({ name: "ValidationError", errors: errors })
            .send();
          return;
        }

        throw err;
      }
    })
  );

  app.put(
    "/api/group/toggleFavourite",
    auth.ensureLoggedIn,
    validateSchema("groupUpdate"),
    wrap(async (req, res) => {
      const userId = req.user.id;
      const t = req.t;
      const id = req.body.id;

      const { Group } = seq();
      const group = await Group.findByIdForUser(userId, id);
      if (!group) {
        res.status(404).send();
        return;
      }
      await group.update({ isFavourite: !group.isFavourite });
      req.flash("success", t("class.updated"));
      res.status(200).send();
    })
  );

  app.put(
    "/api/group",
    auth.ensureLoggedIn,
    validateSchema("groupUpdate"),
    wrap(async (req, res) => {
      const userId = req.user.id;
      const t = req.t;
      const {
        id,
        name,
        note,
        color,
        yearStart,
        yearEnd,
        isFavourite,
        isHidden
      } = req.body;
      const { Group } = seq();

      try {
        const group = await Group.findByIdForUser(userId, id);
        await group.update({
          name,
          note,
          color,
          yearStart,
          yearEnd,
          isFavourite,
          isHidden
        });

        req.flash("success", t("class.updated"));

        group.color = group.color || "";
        group.note = group.note || "";
        res.json({ ...group.toJSON() });
      } catch (err) {
        if (err instanceof SequelizeValidationError) {
          const errors = err["errors"].map(e => {
            const { message, path, validatorKey, value } = e;
            return {
              message,
              i18message: t("group." + path + "." + validatorKey),
              path,
              validatorKey,
              value
            };
          });
          res
            .status(400)
            .json({ name: "ValidationError", errors: errors })
            .send();
          return;
        }

        throw err;
      }
    })
  );
};
