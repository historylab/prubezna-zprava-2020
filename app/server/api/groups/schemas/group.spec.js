/**
 * @jest-environment node
 */

const request = require("supertest");

process.env.NODE_ENV = "testing";
process.env.REACT_APP_GOOGLE_CONSUMER_KEY = "1";
process.env.APP_SECRET = "1";

const app = require("../../../server");

describe("the server group API", () => {
  it("returns expected validation failure message for name", async () => {
    const response = await request(app).post("/api/group");
    expect(response.body).toMatchSnapshot();
  });

  it("returns expected validation failure messages for year start", async () => {
    const response = await request(app)
      .post("/api/group")
      .send({ name: "x" })
      .set("Accept", "application/json");
    expect(response.body).toMatchSnapshot();
  });

  it("returns expected validation failure messages for year span", async () => {
    const response = await request(app)
      .post("/api/group")
      .send({ name: "x", yearStart: 2010, yearEnd: 2020 })
      .set("Accept", "application/json");
    expect(response.body).toMatchSnapshot();
  });
});
