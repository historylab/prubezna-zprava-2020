const expressAjv = require("express-ajv");
const schema = expressAjv.schema;

schema.addSchema("groupAddEmailAddress", require("./addEmailAddress"));
schema.addSchema("groupRename", require("./renameGroup"));
schema.addSchema("groupRemoveEmailAddress", require("./removeEmailAddress"));
schema.addSchema("groupCreate", require("./groupCreate"));
schema.addSchema("groupUpdate", require("./groupUpdate"));

module.exports = expressAjv.validatorFactory;
