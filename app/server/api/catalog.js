const dbmysql = require("./../dbmysql");
const cacheMiddleware = require("../middleware/cacheMiddleware");

module.exports = (app, auth, seq, wrap) => {
  const { User, CatalogExercise, Language } = seq();

  postProcess = (req, exercises) => {
    return exercises
      .filter(e => true)
      .map(e => e.toJSON())
      .map(e => {
        if (req.user) {
          e.tryItUrl = e.tryItUrl.replace(
            "{emailAddress}",
            "&" + req.user.emailAddress
          );
        } else {
          e.tryItUrl = e.tryItUrl.replace("{emailAddress}", "");
        }
        return e;
      });
  };

  function fetchExercise(code, slug, req, res, next) {
    dbmysql.fetchCatalog(
      req.user,
      code,
      slug,
      req.language,
      (error, exercises) => {
        if (error) {
          next(error);
          return;
        }

        exercises = postProcess(req, exercises);

        if (code) {
          if (exercises.length !== 1) {
            res.status(404);
            res.send({});
            return;
          }
          res.json({ exercise: exercises[0] });
          return;
        }
      }
    );
  }

  app.get(
    "/api/catalog/:code([0-9]+)",
    //cacheMiddleware,
    (req, res, next) => {
      fetchExercise(req.params.code, null, req, res, next);
    }
  );

  app.get(
    "/api/catalog/:slug([a-z][a-z|\\-|0-9]+)$",
    //cacheMiddleware,
    wrap(async (req, res) => {
      const { CatalogExercise } = seq();

      let findOptions = {
        where: { enabled: true, slug: req.params.slug },
        order: ["name"],
        attributes: { exclude: ["id"] }
      };

      var exercise = await CatalogExercise.findOne(findOptions);
      if (exercise) {
        res.json({ exercise: postProcess(req, [exercise])[0] });
        return;
      }
      res.status(404);
      res.send({});
    })
  );

  app.get(
    "/api/catalog",
    (req, res, next) => {
      const idUser = req.user ? req.user.id : -1;
      const languageCode = Language.getCode(req.query.lang || req.language);
      req.language = languageCode;
      req.cacheKey = `/api/catalog-idUser=${idUser};languageCode=${languageCode};`;
      next();
    },
    //cacheMiddleware,
    wrap(async (req, res) => {
      const language = await Language.getByCode(req.language);

      let exercises = [];
      if (req.user) {
        const user = User.build(req.user);
        exercises = await user.getEffectiveExercises(language.id);
      }

      exercises = exercises.filter(e => e.id);

      if (exercises.length === 0) {
        exercises = await CatalogExercise.getPublicCatalogExercises(
          language.id
        );
      }

      exercises = postProcess(req, exercises);

      res.json({ exercises });
    })
  );
};
