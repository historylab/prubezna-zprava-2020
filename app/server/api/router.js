const auth = require("./../auth");
const seq = require("./../sequelize");
const flash = require("./../flash_res");

let wrap = fn => (...args) => fn(...args).catch(args[2]);

module.exports = app => {
  require("./auth")(app, seq, wrap);
  require("./my")(app);
  require("./catalog/CatalogService")(app, auth, seq, wrap);
  require("./exercise")(app);
  require("./keywords")(app, auth, seq, wrap);
  require("./timeline")(app, auth, seq, wrap);
  require("./groups/groups")(app, auth, seq, wrap);
  require("./enrolment")(app);
  require("./evaluation")(app);
  require("./users/users")(app, auth, seq, wrap);
  require("./users/password")(app, auth, seq, wrap);
  require("./peeping/peeping")(app, auth, seq, wrap);
  require("./packages/PackageService")(app, auth, seq, wrap, flash);
  require("./environment")(app);
};
