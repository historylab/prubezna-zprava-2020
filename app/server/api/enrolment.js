const auth = require("./../auth");
const seq = require("./../sequelize");

let wrap = fn => (...args) => fn(...args).catch(args[2]);

const validateSchema = require("./../schemas/validator");

const config = require("./../config");

module.exports = app => {
  app.delete(
    "/api/enrolment",
    auth.ensureLoggedIn,
    validateSchema("deleteEnrolment"),
    wrap(async (req, res) => {
      const data = req.body;
      const userId = req.user.id;
      const { enrolmentId } = data;
      const t = req.t;
      const { Enrolment } = seq();
      const enrolment = await Enrolment.findByIdForUser(userId, enrolmentId);
      if (enrolment === null) {
        res.status(404).send();
        return;
      }

      await enrolment.update({ is_active: false });

      req.flash({
        type: "success",
        text: t(
          "deleteEnrolment.success",
          "Cvičení bylo odstraněno z Vašeho seznamu."
        )
      });
      res.json({ success: true });
    })
  );

  app.post(
    "/api/enrolment/send",
    auth.ensureLoggedIn,
    validateSchema("sendEnrolment"),
    wrap(async (req, res) => {
      const data = req.body;
      const userId = req.user.id;
      const { enrolmentId } = data;
      const t = req.t;
      const { Enrolment, Group } = seq();
      const enrolment = await Enrolment.findByIdForUser(userId, enrolmentId);
      if (enrolment === null) {
        res.status(404).send();
        return;
      }

      const group = await Group.findById(enrolment.group_id);
      const emails = await group.getEmails().map(e => e.emailAddress);
      const catalog = (await enrolment.getCatalogExercise()).get({
        plain: true
      });
      const urlTemplate = config.application.exerciseAssignUrl;

      emails.forEach(email => {
        const messageData = {
          from: config.email.from,
          to: email,
          template: "assignment",
          language: catalog.language,
          contentData: {
            catalog: { name: catalog.name },
            assignmentUrl: urlTemplate.format(
              catalog.slug,
              enrolment.code,
              email
            )
          }
        };

        global.dispatcher.dispatch("email", messageData);
      });

      res.json({
        success: true,
        notifications: [
          {
            type: "success",
            text: t(
              "sendEnrolment.success",
              "Zadání cvičení bylo odesláno na zadané emailové adresy."
            )
          }
        ]
      });
    })
  );

  app.post(
    "/api/enrolment/shareWithOthers",
    auth.ensureLoggedIn,
    validateSchema("shareWithOthers"),
    wrap(async (req, res) => {
      const data = req.body;
      const userId = req.user.id;
      const { enrolmentId, submissionId } = data;
      const t = req.t;
      const { Enrolment, Group, Submission } = seq();
      const enrolment = await Enrolment.findByIdForUser(userId, enrolmentId);
      if (enrolment === null) {
        res.status(404).send();
        return;
      }

      const group = await Group.findById(enrolment.group_id);
      const emails = await group.getEmails().map(e => e.emailAddress);

      const submission = await Submission.findById(submissionId);
      if (submission === null || submission.enrolmentId !== enrolmentId) {
        res.status(404).send();
        return;
      }

      const catalog = (await enrolment.getCatalogExercise()).get({
        plain: true
      });
      const urlTemplate = config.application.exerciseViewUrl;
      const messageData = {
        from: config.email.from,
        to: emails[0],
        bcc: emails.slice(1),
        template: "shareWithOthers",
        language: submission.language,
        contentData: {
          submission: { viewUrl: submission.viewUrl },
          catalog: { name: catalog.name },
          viewUrl: urlTemplate.format(submission.id)
        }
      };

      global.dispatcher.dispatch("email", messageData);

      req.flash({
        type: "success",
        text: t(
          "shareWithOthers.success",
          "Vyplněné cvičení bylo odesláno na emailové adresy ve třídě."
        )
      });
      res.json({ success: true });
    })
  );
};
