const passport = require("passport");
const uuidv4 = require("uuid/v4");
const { OAuth2Client } = require("google-auth-library");
const flash = require("../flash_res");
const config = require("./../config");

module.exports = (app, seq, wrap) => {
  const { User } = seq();

  const onSuccessfulLogin = async (req, res, user) => {
    if (!user) {
      user = await User.findByEmailAddress(req.user.emailAddress);
    }

    //global.dispatcher.dispatch("system", { topic: "user.loggedIn", user });
    user.lastAccess = Date.now();
    user.lastLogin = Date.now();
    if (user.firstAccess < 0) {
      user.firstAccess = Date.now();
    }
    await user.save();

    res.send({
      firstName: user.firstName,
      lastName: user.lastName,
      emailAddress: user.emailAddress,
      language: user.language,
      expiration: config.maxAge * 1000
    });
  };

  app.post(
    "/api/login",
    passport.authenticate("local"),
    wrap(async (req, res) => {
      flash(res, {
        type: "success",
        text: req.t("login.success", "Přihlášení bylo úspěšné.")
      });

      onSuccessfulLogin(req, res, null);
    })
  );

  app.get("/api/logout", (req, res) => {
    const user = req.user;
    global.dispatcher.dispatch("system", { topic: "user.loggedOut", user });
    req.logout();
    req.session.destroy(function (err) {
      res.clearCookie("connect.sid");
      res.status(200).send();
    });
  });

  app.get(
    "/api/auth/google",
    passport.authenticate("google", {
      scope: "https://www.google.com/m8/feeds"
    })
  );

  app.get(
    "/api/auth/google/callback",
    (req, res) => {
      passport.authenticate("google", {
        failureRedirect: "/prihlasit",
        scope: [
          "https://www.googleapis.com/auth/userinfo.profile",
          "https://www.googleapis.com/auth/userinfo.email"
        ]
      })(req, res);
    },
    (req, res) => {
      return res
        .status(200)
        .cookie("jwt", signToken(req.user), {
          httpOnly: true
        })
        .redirect("/");
    }
  );

  const verifyGoogleToken = async token => {
    const client = new OAuth2Client(process.env.REACT_APP_GOOGLE_CONSUMER_KEY);
    const ticket = await client.verifyIdToken({
      idToken: token,
      audience: process.env.REACT_APP_GOOGLE_CONSUMER_KEY
    });
    const payload = ticket.getPayload();
    return payload;
  };

  app.post(
    "/api/auth/google",
    wrap(async (req, res) => {
      const token = req.body;
      const emailAddress = token.profileObj.email;
      const verified = await verifyGoogleToken(token.tokenObj.id_token);

      const googleId = token.profileObj.googleId;

      flash(res, {
        type: "success",
        text: req.t("login.googleSuccess")
      });

      let user = await User.findByEmailAddress(emailAddress);
      if (!user) {
        let language = "cs";
        if (/^en-/.test(verified.locale)) {
          language = "en";
        }

        const password = uuidv4();
        user = User.build({
          googleId,
          firstName: verified.given_name,
          lastName: verified.family_name,
          firstAccess: Date.now(),
          password,
          password_confirmation: password,
          userName: emailAddress,
          emailAddress,
          language,
          confirmed: true,
          isTeacher: false,
          authenticationMode: "google"
        });

        await user.save();
      }

      res.user = user;
      req.user = user;
      req.session.passport = { user: user.id };
      user.googleId = googleId;
      onSuccessfulLogin(req, res, user);
      return;
    })
  );
};
