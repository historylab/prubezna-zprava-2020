module.exports = (app, auth, seq, wrap) => {
  const { TimelineSetup, Language } = seq();

  app.get(
    "/api/timeline/setup",
    wrap(async (req, res) => {
      const language = await Language.getByCode(req.language);
      let timelineData = await TimelineSetup.getTimelineSetupData(language.id);
      res.json({ timelineData });
    })
  );
};
