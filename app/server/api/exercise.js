const dbmysql = require("./../dbmysql");
const auth = require("./../auth");
const seq = require("./../sequelize");
const { Op } = require("sequelize");
const _ = require("lodash");

let wrap = fn => (...args) => fn(...args).catch(args[2]);

const validateSchema = require("./../schemas/validator");

module.exports = app => {
  app.post(
    "/api/exercise/emailAddress",
    auth.ensureLoggedIn,
    validateSchema("addEmailAddress"),
    wrap(async (req, res) => {
      const data = req.body;
      const userId = req.user.id;
      const enrolmentId = data.enrolmentId;
      const t = req.t;
      const { Enrolment } = seq();
      const enrolment = await Enrolment.findByIdForUser(userId, enrolmentId);
      if (enrolment === null) {
        res.status(404).send();
        return;
      }

      if (enrolment.type == "class") {
        res
          .status(400)
          .send(
            t(
              "emailAddress.cannotAddToClassExercise",
              "Nelze přidat emailovou adresu k tomuto záznamu."
            )
          );
        return;
      }

      const { Group, GroupEmail } = seq();

      const group = await Group.findById(enrolment.group_id);
      if (group === null) {
        res.status(404).send();
        return;
      }

      const emails = (await group.getEmails()).map(e =>
        e.emailAddress.toLowerCase()
      );

      const missingEmails = _.uniq(data.emailAddresses)
        .filter(e => !emails.includes(e))
        .map(e => {
          return { emailAddress: e, group_id: group.id };
        });

      if (missingEmails.length === 0) {
        req.flash(
          "warning",
          t(
            "emailAddress.exists",
            "Emailové adresy už byly přidány v minulosti."
          )
        );
      } else if (missingEmails.length === data.emailAddresses) {
        req.flash(
          "success",
          t("emailAddress.added", "Emailové adresy byly přidány.")
        );
      } else {
        req.flash(
          "warning",
          t("emailAddress.someAdded", "Některé emailové adresy byly přidány.")
        );
      }

      await GroupEmail.bulkCreate(missingEmails);
      res.redirect(`/api/my/exercise/${enrolment.code}`);
    })
  );

  app.delete(
    "/api/exercise/emailAddress",
    auth.ensureLoggedIn,
    validateSchema("removeEmailAddress"),
    wrap(async (req, res) => {
      const data = req.body;
      const userId = req.user.id;
      const enrolmentId = data.enrolmentId;
      const t = req.t;
      const { Enrolment } = seq();

      const enrolment = await Enrolment.findByIdForUser(userId, enrolmentId);
      if (enrolment === null) {
        res.status(404).send();
        return;
      }

      if (enrolment.type == "class") {
        res
          .status(400)
          .send(
            t(
              "emailAddress.cannotRemoveFromClassExercise",
              "Nelze odebrat emailovou adresu z tohoto záznamu."
            )
          );
        return;
      }

      const { GroupEmail } = seq();
      const groupEmail = await GroupEmail.findOne({
        where: {
          group_id: enrolment.group_id,
          emailAddress: { [Op.like]: data.emailAddress }
        }
      });
      if (groupEmail === null) {
        res.status(404).send();
        return;
      }

      groupEmail.destroy();

      req.flash(
        "success",
        t("emailAddress.removed", "Emailová adresa byla odebrána.")
      );
      res.redirect(303, `/api/my/exercise/${enrolment.code}`);
    })
  );

  app.post(
    "/api/exercise/enrol",
    auth.ensureLoggedIn,
    validateSchema("enrolToExercise"),
    wrap(async (req, res) => {
      const data = req.body;
      const userId = req.user.id;
      const { exerciseId, type, groupId, groupName } = data;
      const { Group, CatalogExercise, sequelize } = seq();
      const exercise = await CatalogExercise.findOne({ where: { exerciseId } });
      if (exercise === null) {
        res.status(404).send();
        return;
      }

      let group = null;
      if (groupId > 0) {
        group = await Group.findById(groupId);
        if (group === null) {
          res.status(404).send();
          return;
        }
        await group.updateAttributes({ lastUsedAt: sequelize.fn("NOW") });
      } else {
        const year = new Date().getFullYear();
        group = await Group.create({
          name: groupName,
          yearStart: year,
          yearEnd: year + 1,
          user_id: userId,
          lastUsedAt: sequelize.fn("NOW")
        });
      }

      const enrolment = await dbmysql.enrolToExercise(
        exerciseId,
        userId,
        group.id,
        type
      );

      res.json({ code: enrolment.code });
    })
  );

  app.post(
    "/api/exercise/finished",
    validateSchema("finished"),
    wrap(async (req, res) => {
      const data = req.body;
      const { entryId, secret } = data;
      const { Submission, CatalogExercise } = seq();
      const submission = await Submission.findById(entryId);
      if (submission === null) {
        res.status(404).send();
        return;
      }

      if (secret !== global.gConfig.secret) {
        res.status(500).send();
        return;
      }

      const enrolment = await submission.getEnrolment();
      let catalog = null;
      if (enrolment) {
        catalog = await enrolment.getCatalogExercise();
      } else {
        catalog = await CatalogExercise.findByExerciseId(submission.exerciseId);
      }
      const emails = await submission.getEmails().map(e => e.emailAddress);

      const messageData = {
        from: global.gConfig.email.from,
        to: emails,
        template: "exerciseFinished",
        language: submission.language,
        contentData: {
          submission: { viewUrl: submission.viewUrl },
          catalog: { name: catalog.name },
          enrolment
        }
      };

      global.dispatcher.dispatch("email", messageData);

      res.json({});
    })
  );

  app.post(
    "/api/exercise/restoreGroup",
    auth.ensureLoggedIn,
    validateSchema("restoreGroup"),
    wrap(async (req, res) => {
      const data = req.body;
      const userId = req.user.id;
      const { enrolmentId } = data;
      const t = req.t;
      const { Enrolment } = seq();
      const enrolment = await Enrolment.findByIdForUser(userId, enrolmentId);
      if (enrolment === null) {
        res.status(404).send();
        return;
      }

      await enrolment.update({ group_id: enrolment.original_group_id });

      req.flash("success", t("restoreGroup.success", "Třída byla obnovena."));
      res.redirect(`/api/my/exercise/${enrolment.code}`);
    })
  );

  app.post(
    "/api/exercise/changeGroup",
    auth.ensureLoggedIn,
    validateSchema("changeGroup"),
    wrap(async (req, res) => {
      const data = req.body;
      const userId = req.user.id;
      const { enrolmentId, groupId } = data;
      const t = req.t;
      const { Group, Enrolment, sequelize } = seq();
      const enrolment = await Enrolment.findByIdForUser(userId, enrolmentId);
      if (enrolment === null) {
        res.status(404).send();
        return;
      }

      const group = await Group.findByIdForUser(userId, groupId);
      if (group === null) {
        res.status(404).send();
        return;
      }

      await enrolment.update({ group_id: groupId });

      await group.updateAttributes({ lastUsedAt: sequelize.fn("NOW") });

      req.flash("success", t("changeGroup.success", "Třída byla změněna."));
      res.redirect(`/api/my/exercise/${enrolment.code}`);
    })
  );
};
