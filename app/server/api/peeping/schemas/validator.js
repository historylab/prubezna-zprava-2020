const expressAjv = require("express-ajv");
const schema = expressAjv.schema;

schema.addSchema("peepingSearch", require("./search"));

module.exports = expressAjv.validatorFactory;
