const _ = require("lodash");
const dbmysql = require("./../../dbmysql");
const { formatTime } = require("../../utils");
const Json2csvParser = require("json2csv").Parser;

const validateSchema = require("./schemas/validator");
const config = require("./../../config");
const urlTemplate = config.application.exerciseViewUrl;

module.exports = (app, auth, _, wrap) => {
  app.get(
    "/api/peeping/filter",
    auth.ensureLoggedIn,
    (req, res, next) => auth.checkUserAccessRights(["peeping"], req, res, next),
    wrap(async (_, res) => {
      const data = await dbmysql.getDataForPeepingFilter();
      res.json(data);
    })
  );

  function processResult(data) {
    return data.map(e => {
      return {
        ...e,
        timeSpent: formatTime(e.timeSpent / 1000),
        time: e.timeSpent,
        viewUrl: e.entryId > 0 ? urlTemplate.format(e.slug, e.entryId) : null,
        from:
          e.query_string && /from=/.test(e.query_string)
            ? e.query_string.match(/from=([^&]+)/)[1]
            : ""
      };
    });
  }

  app.post(
    "/api/peeping",
    auth.ensureLoggedIn,
    (req, res, next) => auth.checkUserAccessRights(["peeping"], req, res, next),
    //validateSchema("peepingSearch"),
    wrap(async (req, res) => {
      const pageSize = 50;
      const { criteria, sortedBy } = req.body;
      let page = req.body.page || 0;

      const options = {
        page: page < 0 ? 0 : page,
        limit: pageSize,
        forExport: false,
        includeNotes: false
      };

      const data = await dbmysql.getDataForPeeping(criteria, sortedBy, {
        ...options,
        count: false
      });

      const count = await dbmysql.getDataForPeeping(criteria, sortedBy, {
        ...options,
        count: true
      });

      const result = processResult(data);
      const paging = count[0];
      paging.pageSize = pageSize;
      paging.pageCount = Math.round(Math.ceil(paging.total / pageSize));

      res.json({ data: result, paging });
    })
  );

  app.get(
    "/api/peeping/export",
    auth.ensureLoggedIn,
    (req, res, next) => auth.checkUserAccessRights(["peeping"], req, res, next),
    wrap(async (req, res) => {
      const c = req.query.c;
      const buffer = Buffer.from(c, "base64");
      const input = buffer.toString("ascii");
      const { criteria, sortedBy, options } = JSON.parse(input);

      const data = await dbmysql.getDataForPeeping(criteria, sortedBy, {
        ...options,
        count: false,
        forExport: true
      });

      const result = processResult(data);

      const fields = [
        "entryId",
        "dateCreated",
        "lmsHash",
        "exerciseVersion",
        "exerciseMode",
        "timeSpent",
        "time",
        "done",
        "exerciseDescription",
        "groupName",
        "exerciseId",
        "name",
        "firstname",
        "lastname",
        "teacherEmail",
        "viewUrl",
        "pupilEmail",
        "from",
        "zam",
        "notes"
      ];
      const json2csvParser = new Json2csvParser({ fields });
      const csv = json2csvParser.parse(result);

      res.setHeader("Content-disposition", "attachment; filename=export.csv");
      res.setHeader("Content-type", "text/csv");
      res.send(csv);
    })
  );
};
