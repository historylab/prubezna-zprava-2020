const config = require("./config");

if (config.queues.redis.port > 0) {
  global.dispatcher = require("./queue/dispatcher");
}

module.exports = (app, logger) => {
  logger.info("Environment: development ...");
  /*app.use(
    "/",
    createProxyMiddleware({ target: "http://localhost:3000", ws: true })
  );*/
};
