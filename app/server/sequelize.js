const Sequelize = require("sequelize");
const config = require("./config");
const logger = require("./logger")("db");

module.exports = () => {
  const db = config.db;
  const sequelize = new Sequelize(db.database, db.user, db.password, {
    host: db.host,
    dialect: "mysql",
    operatorsAliases: false,
    logging: logger.debug,
    pool: {
      max: 5,
      min: 0,
      acquire: 10000,
      idle: 10000
    }
  });

  const models = {
    ...require("./models/role")(sequelize),
    ...require("./models/group")(sequelize),
    ...require("./models/catalogExercise")(sequelize),
    ...require("./models/timelineSetup")(sequelize),
    ...require("./models/enrolment")(sequelize),
    ...require("./models/user")(sequelize),
    ...require("./models/evaluation")(sequelize),
    ...require("./models/submission")(sequelize),
    ...require("./models/passwordReset")(sequelize),
    ...require("./models/language")(sequelize),
    ...require("./models/exhibition")(sequelize),
    ...require("./models/package")(sequelize),
    ...require("./models/packageEnrolment")(sequelize),
    ...require("./models/keyWord")(sequelize),
    ...require("./models/emailObserver")(sequelize),
    sequelize
  };

  return models;
};
