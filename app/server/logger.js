const winston = require("winston");
const format = winston.format;
const transports = winston.transports;

const loggerFactory = moduleName => {
  function isObject(val) {
    return (
      val != null && typeof val === "object" && Array.isArray(val) === false
    );
  }

  function isObjectObject(o) {
    return (
      isObject(o) === true &&
      Object.prototype.toString.call(o) === "[object Object]"
    );
  }

  const excludedKeys = ["password", "Authorization"];
  const deepRegexReplace = (value, keys) => {
    if (typeof value === "undefined" || typeof keys === "undefined") return {};

    if (Array.isArray(value)) {
      for (let i = 0; i < value.length; i = i + 1) {
        value[i] = deepRegexReplace(value[i], keys);
      }
      return value;
    }

    if (!isObject(value)) {
      return value;
    }

    if (typeof keys === "string") {
      keys = [keys];
    }

    if (!Array.isArray(keys)) {
      return value;
    }

    for (let j = 0; j < keys.length; j++) {
      for (let key in value) {
        if (value.hasOwnProperty(key)) {
          if (new RegExp(keys[j], "i").test(key)) value[key] = "***";
        }
      }
    }

    for (let key in value) {
      if (value.hasOwnProperty(key)) {
        value[key] = deepRegexReplace(value[key], keys);
      }
    }

    return value;
  };

  var logger = winston.createLogger({
    level: "info",
    format: format.combine(
      format.colorize(),
      format.timestamp({
        format: "YYYY-MM-DD HH:mm:ss"
      }),
      format.printf(log => {
        return `${log.timestamp} [${moduleName}] ${log.level}: ${JSON.stringify(
          deepRegexReplace(log.message, excludedKeys)
        )}}`;
      })
    ),
    transports: [new transports.Console()]
  });

  return logger;
};

module.exports = moduleName => loggerFactory(moduleName);
