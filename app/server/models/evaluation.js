"use strict";

const Sequelize = require("sequelize");

module.exports = sequelize => {
  var Evaluation = sequelize.define(
    "Evaluation",
    {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      note: Sequelize.TEXT,
      entry_id: Sequelize.INTEGER,
      user_id: Sequelize.INTEGER,
      score: Sequelize.STRING
      //emailAddress: Sequelize.INTEGER
    },
    {
      tableName: "m_teacher_evaluation"
    }
  );

  return { Evaluation };
};
