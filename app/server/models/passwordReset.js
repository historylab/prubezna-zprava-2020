"use strict";

const Sequelize = require("sequelize");

module.exports = sequelize => {
  var PasswordReset = sequelize.define(
    "PasswordReset",
    {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      userId: {
        type: Sequelize.INTEGER,
        field: "userid"
      },
      requestedOn: {
        type: Sequelize.BIGINT,
        field: "timerequested"
      },
      reRequestedOn: {
        type: Sequelize.BIGINT,
        field: "timererequested"
      },
      token: {
        type: Sequelize.STRING,
        len: [1, 32]
      }
    },
    {
      timestamps: false,
      tableName: "m_user_password_resets"
    }
  );

  return { PasswordReset };
};
