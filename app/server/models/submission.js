"use strict";

const Sequelize = require("sequelize");
const config = require("./../config");

require("./../utils");

module.exports = sequelize => {
  var Submission = sequelize.define(
    "Submission",
    {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        field: "entryId"
      },
      lms_hash: Sequelize.STRING,
      enrolmentId: {
        field: "enrolment_id",
        allowNull: true,
        type: Sequelize.INTEGER
      },
      katalog_id: Sequelize.INTEGER,
      language: {
        type: Sequelize.STRING,
        allowNull: true,
        len: [0, 10]
      },
      teacherEmailAddress: {
        type: Sequelize.STRING,
        allowNull: true,
        field: "additionalEmailAddresses"
      },
      emailAddress: Sequelize.STRING,
      analytics: Sequelize.TEXT,
      exerciseData: Sequelize.TEXT,
      done: Sequelize.BOOLEAN,
      exerciseVersion: Sequelize.STRING,
      exerciseId: Sequelize.INTEGER,
      exerciseSlug: Sequelize.STRING,
      exerciseMode: Sequelize.INTEGER,
      timeSpent: { type: Sequelize.INTEGER, field: "time_spent" },
      exhibition_id: { type: Sequelize.INTEGER, allowNull: true },
      redirect_url: { type: Sequelize.STRING, allowNull: true, len: [0, 255] }
    },
    {
      getterMethods: {
        viewUrl() {
          const urlTemplate = config.application.exerciseViewUrl;
          return urlTemplate.format(this.exerciseSlug, this.id);
        },
        isEnglish() {
          return (this.language || "").match(/en/);
        },
        isDone() {
          let done = this.done;
          if (typeof done !== "boolean") {
            done = done === 1 || done === "1" || done === "true";
          }
          return done;
        }
      },
      timestamps: false,
      tableName: "m_cviceni_submission"
    }
  );

  var SubmissionEmail = sequelize.define(
    "SubmissionEmail",
    {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      emailAddress: {
        type: Sequelize.STRING,
        isEmail: true,
        len: [1, 100]
      },
      name: {
        type: Sequelize.STRING,
        len: [1, 100]
      },
      entryId: {
        type: Sequelize.INTEGER,
        field: "submission_id"
      }
    },
    {
      timestamps: false,
      tableName: "m_submission_emailAddresses"
    }
  );

  Submission.hasMany(SubmissionEmail, {
    as: "Emails",
    foreignKey: "submission_id",
    targetKey: "id"
  });

  const { Enrolment } = require("./enrolment")(sequelize);

  // emailAddress exerciseCode katalog_id exerciseVersion
  // exerciseMode exerciseData analytics time_spent additionalEmailAddresses
  Submission.belongsTo(Enrolment, {
    as: "Enrolment",
    foreignKey: "enrolment_id",
    targetKey: "id"
  });

  return { Submission, SubmissionEmail };
};
