"use strict";

const Sequelize = require("sequelize");
const _ = require("lodash");

module.exports = sequelize => {
  var Group = sequelize.define(
    "Group",
    {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false,
        len: [1, 100]
      },
      user_id: Sequelize.INTEGER,
      note: { type: Sequelize.STRING, len: [1, 500], allowNull: true },
      color: { type: Sequelize.STRING, len: [1, 20], allowNull: true },
      yearStart: {
        type: Sequelize.INTEGER,
        allowNull: false,
        field: "year_start",
        validate: {
          min: 1900,
          max: 3000,
          isLessThanYearEnd(value) {
            if (parseInt(value) > parseInt(this.yearEnd)) {
              throw new Error("Start must be less than end");
            }
          }
        }
      },
      yearEnd: {
        type: Sequelize.INTEGER,
        allowNull: false,
        field: "year_end",
        validate: {
          min: 1901,
          max: 3000,
          isMoreThanYearStart(value) {
            if (parseInt(value) < parseInt(this.yearStart)) {
              throw new Error("End must be greater than start");
            }
          }
        }
      },
      isFavourite: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
        field: "is_favourite"
      },
      isHidden: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
        field: "is_hidden"
      },
      lastUsedAt: {
        type: Sequelize.DATE,
        allowNull: true,
        field: "last_used"
      }
    },
    {
      timestamps: false,
      tableName: "m_cviceni_group"
    }
  );

  var GroupEmail = sequelize.define(
    "GroupEmail",
    {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      emailAddress: {
        type: Sequelize.STRING,
        isEmail: true,
        len: [1, 100]
      }
    },
    {
      timestamps: false,
      tableName: "m_cviceni_group_email"
    }
  );

  Group.findByIdForUser = async (userId, groupId) => {
    return await Group.findOne({
      where: { user_id: userId, id: groupId },
      attributes: {
        exclude: ["user_id"]
      }
    });
  };

  Group.hasMany(GroupEmail, {
    as: "Emails",
    foreignKey: "group_id",
    targetKey: "id"
  });

  Group.prototype.getMembers = function () {
    const query = `select * from (
SELECT -1 as id, se.emailAddress, -1 as group_id, se.name, 1 as hasSubmissions from m_submission_emailaddresses se
      left join m_cviceni_submission cs on cs.entryid = se.submission_id
      left join m_cviceni_enrolment ce on ce.id = cs.enrolment_id
      where ce.group_id = :idGroup and length(se.emailAddress) > 0
UNION
    select -1 as id, cs.emailAddress, -1 as group_id, null as name, 1 as hasSubmissions from m_cviceni_submission cs
      left join m_cviceni_enrolment ce on ce.id = cs.enrolment_id
      where ce.group_id = :idGroup and length(cs.emailAddress) > 0
UNION
      select id, emailAddress, group_id, null, 0 from m_cviceni_group_email cge where cge.group_id = :idGroup
UNION
      select -1 as id, emailAddress, -1 as group_id, null as name, 1 as hasSubmissions from m_cviceni_group cg
      left join m_cviceni_enrolment ce on cg.id = ce.group_id
      left join m_cviceni_submission cs on ce.id = cs.enrolment_id
      where cg.id = :idGroup
    ) e where e.emailAddress is not null and length(e.emailAddress) > 0
    order by e.emailAddress, e.group_id desc, e.name desc`;

    return new Promise((resolve, reject) => {
      sequelize
        .query(query, {
          plain: false,
          type: Sequelize.QueryTypes.SELECT,
          replacements: { idGroup: this.id },
          raw: true
        })
        .then(results => {
          const groupped = _.groupBy(results, "emailAddress");
          const result = Object.entries(groupped).map(f => {
            return {
              id:
                _.find(
                  f[1].map(x => x.id),
                  g => g > 0
                ) || -1,
              emailAddress: f[0],
              group_id:
                _.find(
                  f[1].map(x => x.group_id),
                  g => g > 0
                ) || -1,
              name: _.find(
                f[1].map(x => x.name),
                g => g
              ),
              hasSubmissions:
                _.find(
                  f[1].map(x => x.hasSubmissions),
                  g => g
                ) || 0
            };
          });
          resolve(result);
        })
        .catch(reject);
    });
  };

  return { Group, GroupEmail };
};
