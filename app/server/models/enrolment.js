"use strict";

const Sequelize = require("sequelize");

module.exports = sequelize => {
  var Enrolment = sequelize.define(
    "Enrolment",
    {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      code: {
        type: Sequelize.STRING,
        len: [1, 50]
      },
      group_id: Sequelize.INTEGER,
      original_group_id: Sequelize.INTEGER,
      user_id: Sequelize.INTEGER,
      is_active: Sequelize.BOOLEAN,
      katalog_id: Sequelize.INTEGER,
      exerciseId: Sequelize.INTEGER,
      type: Sequelize.INTEGER
    },
    {
      getterMethods: {
        typeDescription() {
          switch (this.type) {
            case 0:
              return "individuální cvičení";
            case 1:
              //exercise.type = "group";
              return "skupinové cvičení";
            case 2:
              //exercise.type = "class";
              return "cvičení ve třídě s celou třídou";
            //return;
            default:
              return "?";
          }
        }
      },
      timestamps: false,
      tableName: "m_cviceni_enrolment"
    }
  );

  Enrolment.findByIdForUser = async (userId, enrolmentId) => {
    return await Enrolment.findOne({
      where: { user_id: userId, id: enrolmentId }
    });
  };

  Enrolment.prototype.getCatalogExercise = function () {
    return new Promise((resolve, reject) => {
      let where = { exerciseId: this.exerciseId };
      if (this.exerciseId === 0) {
        where = { katalog_id: this.katalog_id };
      }
      sequelize.models.CatalogExercise.findOne({
        where,
        order: [
          ["enabled", "DESC"],
          ["id", "ASC"]
        ]
      })
        .then(catalogExercise => {
          resolve(catalogExercise);
        })
        .catch(reject);
    });
  };

  return { Enrolment };
};
