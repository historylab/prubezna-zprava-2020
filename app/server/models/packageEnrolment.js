"use strict";

const Sequelize = require("sequelize");
const Op = Sequelize.Op;

module.exports = sequelize => {
  const PackageEnrolment = sequelize.define(
    "PackageEnrolment",
    {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      code: {
        type: Sequelize.STRING,
        len: [1, 50]
      },
      group_id: Sequelize.INTEGER,
      user_id: Sequelize.INTEGER,
      is_active: Sequelize.BOOLEAN,
      package_id: Sequelize.INTEGER,
      type: Sequelize.INTEGER
    },
    {
      getterMethods: {
        typeDescription() {
          switch (this.type) {
            case 0:
              return "individuální cvičení";
            case 1:
              //exercise.type = "group";
              return "skupinové cvičení";
            case 2:
              //exercise.type = "class";
              return "cvičení ve třídě s celou třídou";
            //return;
            default:
              return "?";
          }
        }
      },
      timestamps: false,
      tableName: "m_package_enrolment"
    }
  );

  /*const { Package } = require("./package")(sequelize);
  PackageEnrolment.hasOne(Package, {
    //foreignKey: "exercise_id"
  });

  Package.hasOne(PackageEnrolment, {
    //foreignKey: "package_id"
  });*/

  PackageEnrolment.findByIdForUser = async (userId, enrolmentId) => {
    return await PackageEnrolment.findOne({
      where: { user_id: userId, id: enrolmentId }
    });
  };

  PackageEnrolment.findByCodeForUser = async (userId, code) => {
    return await PackageEnrolment.findOne({
      //include: { model: Package },
      where: {
        [Op.and]: [
          Sequelize.where(
            Sequelize.fn("lower", Sequelize.col("code")),
            Sequelize.fn("lower", code.trim())
          ),
          { user_id: userId }
        ]
      }
    });
  };

  return { PackageEnrolment };
};
