﻿"use strict";

const Sequelize = require("sequelize");

module.exports = sequelize => {
    var EmailObserver = sequelize.define(
        "EmailObserver",
        {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            date: {
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
                allowNull: false
            },
            successful: Sequelize.BOOLEAN,
            email_data_to: {
                type: Sequelize.TEXT,
                allowNull: true,
            }
        },
        {
            timestamps: false,
            tableName: "email_observer"
        }
    );

    return { EmailObserver };
};
