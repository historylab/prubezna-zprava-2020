"use strict";

const Sequelize = require("sequelize");
require("../utils");

module.exports = sequelize => {
  var TimelineSetup = sequelize.define(
    "TimelineSetup",
    {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      title: {
        type: Sequelize.STRING,
        len: [1, 100]
      },
      slug: {
        type: Sequelize.STRING,
        len: [1, 30]
      },
      idLanguage: Sequelize.INTEGER,
      epochs: {
        type: Sequelize.ARRAY(Sequelize.JSON),
        allowNull: true,
        get: function() {
          return JSON.parse(this.getDataValue('epochs'));
        },
        set: function (epochs) {
          this.setDataValue("epochs", JSON.stringify(epochs));
        }
      }
    },
    {
      timestamps: false,
      tableName: "casova_osa_nastaveni",
      getterMethods: {}
    }
  );

  TimelineSetup.getTimelineSetupData = function (idLanguage) {
    return TimelineSetup.findAll({where: {
        idLanguage: idLanguage
      },
      order: [
        ["id", "ASC"]
      ]
    });
};

  return { TimelineSetup };
};
