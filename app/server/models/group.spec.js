const _ = require("lodash");

var chai = require("chai");
var chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);
var expect = chai.expect;

const Sequelize = require("sequelize");

describe("group", () => {
  const sequelize = new Sequelize({ dialect: "mysql" });
  const { Group } = require("./group")(sequelize);

  describe("group validation", () => {
    it("rejects the promise when no values are provided", () => {
      expect(Group.create()).rejected;
    });

    it("returns validation error when start year is null", async () => {
      try {
        await Group.create();
        expect.fail();
      } catch (err) {
        expect(err["name"]).to.be.equal("SequelizeValidationError");
        const error = err["errors"].find(err => err.path === "yearStart");
        expect(error).to.have.property(
          "message",
          "Group.yearStart cannot be null"
        );
      }
    });

    it("returns validation error when end year is null", async () => {
      try {
        await Group.create();
        expect.fail();
      } catch (err) {
        expect(err["name"]).to.be.equal("SequelizeValidationError");
        const error = err["errors"].find(err => err.path === "yearEnd");
        expect(error).to.have.property(
          "message",
          "Group.yearEnd cannot be null"
        );
      }
    });

    it("returns validation error when name is null", async () => {
      try {
        await Group.create();
        expect.fail();
      } catch (err) {
        expect(err["name"]).to.be.equal("SequelizeValidationError");
        const error = err["errors"].find(err => err.path === "name");
        expect(error).to.have.property("message", "Group.name cannot be null");
      }
    });

    it("returns validation error when year start is defined and year end not", async () => {
      try {
        await Group.create({ name: "name", yearStart: 2000 });
        expect.fail();
      } catch (err) {
        expect(err["name"]).to.be.equal("SequelizeValidationError");
        const error = err["errors"].find(err => err.path === "yearStart");
        expect(error).to.have.property(
          "message",
          "Difference is not equal to one year"
        );
      }
    });

    it("returns validation error when year end is defined and year start not", async () => {
      try {
        await Group.create({ name: "name", yearEnd: 2000 });
        expect.fail();
      } catch (err) {
        expect(err["name"]).to.be.equal("SequelizeValidationError");
        const error = err["errors"].find(err => err.path === "yearEnd");
        expect(error).to.have.property(
          "message",
          "Difference is not equal to one year"
        );
      }
    });

    it("returns validation error when year start is defined and year end not", async () => {
      try {
        await Group.create({ name: "name", yearStart: 2000, yearEnd: 2020 });
        expect.fail();
      } catch (err) {
        expect(err["name"]).to.be.equal("SequelizeValidationError");
        const error = err["errors"].find(err => err.path === "yearStart");
        expect(error).to.have.property(
          "message",
          "Difference is not equal to one year"
        );
      }
    });

    it("returns validation error when year start is out of range (minimum)", async () => {
      try {
        await Group.create({ name: "name", yearStart: 1900, yearEnd: 1901 });
        expect.fail();
      } catch (err) {
        expect(err["name"]).to.be.equal("SequelizeValidationError");
        const error = err["errors"].find(err => err.path === "yearStart");
        expect(error).to.have.property(
          "message",
          "Validation min on yearStart failed"
        );
      }
    });

    it("returns validation error when year start is out of range (maximum)", async () => {
      try {
        await Group.create({ name: "name", yearStart: 3001, yearEnd: 3002 });
        expect.fail();
      } catch (err) {
        expect(err["name"]).to.be.equal("SequelizeValidationError");
        const error = err["errors"].find(err => err.path === "yearStart");
        expect(error).to.have.property(
          "message",
          "Validation max on yearStart failed"
        );
      }
    });

    it("is ok when all values are provided", () => {
      expect(
        Group.build({ name: "name", yearStart: 2000, yearEnd: 2001 }).validate()
      ).fulfilled;
    });
  });
});
