"use strict";

const bcrypt = require("bcryptjs");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;

module.exports = sequelize => {
  var User = sequelize.define(
    "User",
    {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      userName: {
        type: Sequelize.STRING,
        len: [1, 100],
        field: "username"
      },
      password: {
        type: Sequelize.VIRTUAL,
        len: [1, 255],
        allowNull: false
      },
      firstName: {
        type: Sequelize.STRING,
        len: [1, 100]
      },
      lastName: {
        type: Sequelize.STRING,
        len: [1, 100]
      },
      emailAddress: {
        type: Sequelize.STRING,
        len: [1, 100],
        field: "email",
        allowNull: false
      },
      confirmed: Sequelize.BOOLEAN,
      suspended: Sequelize.BOOLEAN,
      deleted: Sequelize.BOOLEAN,
      institution: {
        type: Sequelize.STRING,
        allowNull: true,
        len: [0, 255]
      },
      password_confirmation: {
        type: Sequelize.VIRTUAL,
        len: [1, 100]
      },
      language: {
        field: "lang",
        type: Sequelize.STRING,
        allowNull: true,
        len: [0, 10]
      },
      password_digest: Sequelize.STRING,
      isTeacher: { field: "is_teacher", type: Sequelize.BOOLEAN },
      firstAccess: { type: Sequelize.INTEGER, defaultValue: -1 },
      lastAccess: Sequelize.INTEGER,
      lastLogin: Sequelize.INTEGER,
      lastIp: { type: Sequelize.STRING, allowNull: true, len: [0, 45] },
      googleId: { type: Sequelize.STRING, allowNull: true, len: [0, 50] },
      authenticationMode: {
        field: "auth",
        type: Sequelize.STRING,
        allowNull: true,
        len: [0, 20]
      }
    },
    {
      timestamps: false,
      tableName: "m_user",
      validate: {
        passwordConfirmation() {
          if (this.password !== this.password_confirmation) {
            throw new Error("Password confirmation doesn't match password.");
          }
        }
      }
    }
  );

  User.prototype.authenticate = function (password) {
    if (bcrypt.compareSync(password, this.password_digest)) {
      return this;
    }
    return null;
  };

  User.prototype.getEffectiveRoles = function () {
    return new Promise((resolve, reject) => {
      sequelize.models.EffectiveRole.findAll({
        where: { idUser: this.id },
        include: ["Role"]
      })
        .then(effectiveRoles => {
          resolve(effectiveRoles.map(e => e.Role));
        })
        .catch(reject);
    });
  };

  User.prototype.getEffectiveExercises = function (idLanguage) {
    return new Promise((resolve, reject) => {
      sequelize
        .query("call GetExercisesForUser(:idUser, :idLanguage)", {
          model: sequelize.models.CatalogExercise,
          replacements: { idUser: this.id, idLanguage: idLanguage },
          mapToModel: true
        })
        .then(results => {
          resolve(results);
        })
        .catch(reject);
    });
  };

  User.belongsToMany(sequelize.models.Role, {
    as: "Roles",
    through: "m_role_assignments",
    foreignKey: "userid",
    otherKey: "roleid",
    timestamps: false
  });

  User.authenticate = function (userName, password, cb) {
    User.findOne({
      where: {
        email: { [Op.like]: userName.toLowerCase() },
        suspended: false,
        deleted: false,
        confirmed: true
      }
    })
      .then(user => {
        if (user === null) {
          return cb(null, null);
        }
        cb(null, user.authenticate(password));
      })
      .catch(error => cb(error, null));
  };

  User.findByEmailAddress = function (emailAddress) {
    return new Promise((result, reject) => {
      User.findOne({
        where: {
          emailAddress: { [Op.like]: emailAddress },
          suspended: false,
          deleted: false,
          confirmed: true
        }
      })
        .then(result)
        .catch(reject);
    });
  };

  var hasSecurePassword = function (user, options) {
    /*if (user.password !== user.password_confirmation) {
      throw new Sequelize.ValidationError(
        "Password confirmation doesn't match password"
      );
    }*/
    return new Promise((result, reject) => {
      bcrypt.hash(user.get("password"), 10, function (err, hash) {
        if (err) {
          return reject(err);
        }
        user.set("password_digest", hash);
        return result(null, options);
      });
    });
  };

  User.beforeCreate(function (user, options, callback) {
    if (user.emailAddress) {
      user.emailAddress = user.emailAddress.toLowerCase();
    }
    if (!user.userName) {
      user.userName = user.emailAddress;
    }
    if (user.password) {
      return hasSecurePassword(user, options);
    }
    return callback(null, options);
  });

  User.beforeUpdate(function (user, options, callback) {
    user.emailAddress = user.emailAddress.toLowerCase();
    if (user.password) {
      return hasSecurePassword(user, options);
    }
    if (callback) {
      return callback(null, options);
    }
    return true;
  });

  return { User };
};
