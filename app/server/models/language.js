"use strict";

const Sequelize = require("sequelize");
const cache = require("./../cache");

module.exports = sequelize => {
  var Language = sequelize.define(
    "Language",
    {
      id: { type: Sequelize.INTEGER, primaryKey: true },
      name: Sequelize.STRING,
      code: Sequelize.STRING
    },
    {
      timestamps: false,
      tableName: "m_languages"
    }
  );

  const languageMap = {
    cs: "cs",
    "cs-cz": "cs",
    en: "en",
    "en-us": "en",
    "": "cs"
  };

  Language.getCode = function (code) {
    return languageMap[(code || "").toLowerCase()];
  };

  Language.getByCode = function (code) {
    return new Promise((resolve, reject) => {
      code = Language.getCode(code);
      const key = "language(" + code + ")";
      const cached = cache.get(key);
      if (cached) {
        return resolve(cached);
      }

      Language.findOne({
        where: { code }
      })
        .then(language => {
          if (!language) {
            reject(null);
            return;
          }
          cache.set(key, language.toJSON());
          resolve(language.toJSON());
        })
        .catch(reject);
    });
  };

  return { Language };
};
