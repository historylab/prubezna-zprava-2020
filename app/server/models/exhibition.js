"use strict";

const Sequelize = require("sequelize");

module.exports = sequelize => {
  var Exhibition = sequelize.define(
    "Exhibition",
    {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      code: {
        type: Sequelize.STRING,
        len: [1, 10]
      },
      name: {
        type: Sequelize.STRING,
        len: [1, 100]
      },
      url: {
        type: Sequelize.STRING,
        len: [1, 255]
      }
    },
    {
      timestamps: false,
      tableName: "m_exhibition"
    }
  );

  Exhibition.verifyCode = function (code) {
    return new Promise((resolve, reject) => {
      console.info("verifyCode", code);
      if (code === null || code === undefined) {
        resolve(null);
        return;
      }
      sequelize
        .query("select id from m_exhibition where code = :code limit 1", {
          replacements: { code: code },
          mapToModel: false,
          raw: true,
          plain: true
        })
        .then(results => {
          resolve(results ? results.id : null);
        })
        .catch(reject);
    });
  };

  return { Exhibition };
};
