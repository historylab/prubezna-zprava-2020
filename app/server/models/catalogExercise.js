"use strict";

const Sequelize = require("sequelize");
const config = require("./../config");
require("./../utils");

module.exports = sequelize => {
  const CatalogExercise = sequelize.define(
    "CatalogExercise",
    {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      code: {
        type: Sequelize.STRING,
        len: [1, 30]
      },
      name: {
        type: Sequelize.STRING,
        len: [1, 100]
      },
      url: {
        type: Sequelize.STRING,
        len: [1, 255],
        get() {
          const baseUrl = config.application.historyLabUrl;
          return baseUrl + "/cviceni/" + this.slug;
        }
      },
      catalogUrl: {
        type: Sequelize.VIRTUAL,
        len: [1, 255],
        get() {
          if (this.getDataValue("isPackage")) {
            return "/katalog/balicek/" + this.slug;
          }
          return "/katalog/cviceni/" + this.slug;
        }
      },
      image_url: {
        type: Sequelize.STRING,
        len: [1, 255],
        get() {
          const baseUrl = config.application.assetsUrl;
          const image_url = this.getDataValue("image_url");
          if (image_url.match(/^http/)) {
            return image_url;
          }
          return (
            baseUrl +
            "/cviceni-aktualni/assets/img/" +
            this.slug +
            "/" +
            image_url
          );
        }
      },
      thumb_image_url: {
        type: Sequelize.STRING,
        len: [1, 255],
        get() {
          const baseUrl = config.application.assetsUrl;
          const thumb_image_url = this.getDataValue("thumb_image_url");
          if ((thumb_image_url || "").match(/^http/)) {
            return thumb_image_url;
          }
          return (
            baseUrl +
            "/cviceni-aktualni/assets/img/" +
            this.slug +
            "/" +
            thumb_image_url
          );
        }
      },
      enabled: Sequelize.BOOLEAN,
      slug: Sequelize.STRING,
      language: Sequelize.STRING,
      exerciseId: Sequelize.INTEGER,
      metadata: {
        type: Sequelize.TEXT,
        allowNull: true,
        get: function () {
          try {
            return JSON.parse(this.getDataValue("metadata"));
          } catch (e) {
            return null;
          }
        },
        set: function (metadata) {
          this.setDataValue("metadata", JSON.stringify(metadata));
        }
      }
    },
    {
      timestamps: false,
      tableName: "m_katalog",
      getterMethods: {
        instructionsUrl() {
          const assetsUrl = config.application.assetsUrl;
          return (
            assetsUrl +
            "/cviceni-aktualni/assets/pdf/doporuceny-postup/" +
            this.slug +
            ".pdf"
          );
        },
        tryItUrl() {
          const urlTemplate = config.application.exerciseAssignUrl;
          let tryItUrl = urlTemplate.format(
            this.slug,
            this.code,
            "{emailAddress}"
          );
          const url = tryItUrl.substr(0, tryItUrl.indexOf("?"));
          return url + "?lang=" + this.language + "{emailAddress}";
        }
      }
    }
  );

  CatalogExercise.findByExerciseId = async exerciseId => {
    return await CatalogExercise.findOne({
      where: { exerciseId }
    });
  };

  CatalogExercise.getPublicCatalogExercises = async idLanguage => {
    const query = `select distinct k.exerciseId as id, k.name, slug, image_url, thumb_image_url, language, ifnull(metadata, "{}") as metadata from m_catalogs c 
      left join m_catalog_exercise ce on ce.idCatalog = c.id
      left join m_katalog k on ce.exerciseId = k.exerciseId and k.enabled = 1
      where c.isPublic = 1 and c.idLanguage = :idLanguage and k.id is not null
      order by k.name`;

    return await sequelize.query(query, {
      model: sequelize.models.CatalogExercise,
      replacements: { idLanguage: idLanguage },
      mapToModel: true
    });
  };

  function postProcessForCasovaOsa(exercises) {
    let goodExercises = [];
    exercises.forEach(exercise => {
      try {
        exercise.metadata = JSON.parse(exercise.metadata);
        if (exercise.metadata === undefined) return;
        if (exercise.metadata.casovaOsa === undefined) return;
        goodExercises.push(exercise);
      } catch (e) {
        return;
      }
    });

    return goodExercises;
  }

  CatalogExercise.getPublicTimelineExercises = function (idLanguage) {
    const query = `select distinct k.* from m_catalogs c 
		left join m_catalog_exercise ce on ce.idCatalog = c.id
    left join m_katalog k on ce.exerciseId = k.exerciseId and k.enabled = 1
    where c.isPublic = 1 and c.idLanguage = :idLanguage and k.id is not null and k.metadata is not null
    order by k.name;`;

    return new Promise((resolve, reject) => {
      sequelize
        .query(query, {
          model: sequelize.models.CatalogExercise,
          replacements: { idLanguage: idLanguage },
          mapToModel: true
        })
        .then(results => {
          results = postProcessForCasovaOsa(results);
          resolve(results);
        })
        .catch(e => {
          console.error(e);
          reject(e);
        });
    });
  };

  return { CatalogExercise };
};
