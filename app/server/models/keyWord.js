"use strict";
const Sequelize = require("sequelize");
require("../utils");
const Op = Sequelize.Op;
module.exports = sequelize => {
  var KeyWord = sequelize.define(
    "KeyWord",
    {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      name: {
        type: Sequelize.STRING,
        len: [1, 255],
        allowNull: false,
      },
      type: {
        type: Sequelize.STRING,
        len: [1, 100]
      },
      toFilter: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        field: 'to_filter'
      },
      language: {
        type: Sequelize.STRING,
        len: [1, 10]
      },
      enabled: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
      },
    },
    {
      timestamps: false,
      tableName: "keywords"
    }
  );
  KeyWord.getEnabledKeywords = async idLanguage => {
    return KeyWord.groupKeywordsByType(await KeyWord.findAll({
      attributes: ["type", "name"],
      where: {
        enabled: 1,
        language: idLanguage,
        type: {
          [Op.notIn]: ["skills", "topic"]
        }
      },
      order: [
        ["name", "ASC"]
      ]
    }), idLanguage);
  };
  KeyWord.getEnabledToPackageKeywords = async idLanguage => {
    return KeyWord.groupKeywordsByType(await KeyWord.findAll({
      attributes: ["type", "name"],
      where: {
        enabled: 1,
        language: idLanguage,
        type: {
          [Op.in]: ["b4", "skills", "topic"]
        }
      },
      order: [
        ["type", "ASC"]
      ]
    }), idLanguage);
  };
  KeyWord.getEnabledToFilterKeywords = async idLanguage => {
    return KeyWord.groupKeywordsByType(await KeyWord.findAll({
      attributes: ["type", "name"],
      where: {
        enabled: 1,
        language: idLanguage,
        toFilter: 1
      },
      order: [
        ["name", "ASC"]
      ]
    }), idLanguage);
  };
  KeyWord.groupKeywordsByType = (keyWordsData, language) => {
    return keyWordsData.reduce((acc, kw) => {
      const type = kw.type;
      acc[type] = acc[type] || [];
      acc[type].push(kw.name);
      acc[type].sort(new Intl.Collator(language).compare);
      return acc;
    }, {});
  }
  return {KeyWord: KeyWord};
};