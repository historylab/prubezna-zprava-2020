"use strict";

const Sequelize = require("sequelize");

module.exports = sequelize => {
  var Role = sequelize.define(
    "Role",
    {
      id: { type: Sequelize.INTEGER, primaryKey: true },
      name: {
        type: Sequelize.STRING,
        field: "shortname"
      }
    },
    {
      timestamps: false,
      tableName: "m_role"
    }
  );

  var EffectiveRole = sequelize.define(
    "EffectiveRole",
    {
      idUser: { type: Sequelize.INTEGER },
      idRole: { type: Sequelize.INTEGER }
    },
    {
      timestamps: false,
      tableName: "v_effective_roles"
    }
  );

  EffectiveRole.belongsTo(Role, {
    as: "Role",
    foreignKey: "idRole",
    targetKey: "id"
  });

  return { Role, EffectiveRole };
};
