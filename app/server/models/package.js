"use strict";

const Sequelize = require("sequelize");
const config = require("./../config");
require("./../utils");

module.exports = sequelize => {
  const Package = sequelize.define(
    "Package",
    {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      /*code: {
        type: Sequelize.STRING,
        len: [1, 30]
      },*/
      name: {
        type: Sequelize.STRING,
        len: [1, 100]
      },
      authors: {
        type: Sequelize.STRING,
        allowNull: false,
        get() {
          return (this.getDataValue("authors") || "")
            .split(";")
            .map(s => s.trim());
        }
      },
      description: Sequelize.STRING,
      /*taxonomy: {
        type: Sequelize.STRING,
        len: [1, 100]
      },*/
      url: {
        type: Sequelize.VIRTUAL,
        len: [1, 255],
        get() {
          const baseUrl = config.application.historyLabUrl;
          return baseUrl + "/balicek/" + this.slug;
        }
      },
      image_url: {
        type: Sequelize.STRING,
        len: [1, 255],
        get() {
          const baseUrl = config.application.assetsUrl;
          const image_url = this.getDataValue("image_url");
          if (image_url.match(/^http/)) {
            return image_url;
          }
          return (
            baseUrl +
            "/cviceni-aktualni/assets/img/" +
            this.slug +
            "/" +
            image_url
          );
        }
      },
      thumb_image_url: {
        type: Sequelize.STRING,
        len: [1, 255],
        get() {
          const baseUrl = config.application.assetsUrl;
          const thumb_image_url =
            this.getDataValue("thumb_image_url") ||
            this.getDataValue("image_url");
          if (thumb_image_url.match(/^http/)) {
            return thumb_image_url;
          }
          return (
            baseUrl +
            "/cviceni-aktualni/assets/img/" +
            this.slug +
            "/" +
            thumb_image_url
          );
        }
      },
      topics: {
        type: Sequelize.STRING,
        len: [1, 255],
        allowNull: false,
        get() {
          return this.getDataValue("topics").split(";");
        }
      },
      enabled: Sequelize.BOOLEAN,
      slug: Sequelize.STRING
      /*metadata: {
        type: Sequelize.TEXT,
        allowNull: true,
        get: function () {
          try {
            return JSON.parse(this.getDataValue("metadata"));
          } catch (e) {
            return null;
          }
        },
        set: function (metadata) {
          this.setDataValue("metadata", JSON.stringify(metadata));
        }
      }*/
    },
    {
      timestamps: false,
      tableName: "m_packages",
      getterMethods: {
        instructionsUrl() {
          const assetsUrl = config.application.assetsUrl;
          return (
            assetsUrl +
            "/cviceni-aktualni/assets/pdf/doporuceny-postup/" +
            this.slug +
            ".pdf"
          );
        }
        /*tryItUrl() {
          const urlTemplate = config.application.exerciseAssignUrl;
          let tryItUrl = urlTemplate.format(
            this.slug,
            this.code,
            "{emailAddress}"
          );
          const url = tryItUrl.substr(0, tryItUrl.indexOf("?"));
          return url + "?lang=" + this.Language.code + "{emailAddress}";
        }*/
      }
    }
  );

  const { Language } = require("./language")(sequelize);
  const { CatalogExercise } = require("./catalogExercise")(sequelize);

  Package.belongsTo(Language, {
    //as: "Language",
    foreignKey: "language_id",
    targetKey: "id"
  });

  Package.belongsToMany(CatalogExercise, {
    through: "m_package_exercises",
    as: "exercises",
    foreignKey: "package_id",
    targetKey: "id",
    timestamps: false
  });

  CatalogExercise.belongsToMany(Package, {
    through: "m_package_exercises",
    as: "exercises",
    foreignKey: "exercise_id",
    targetKey: "id",
    timestamps: false
  });

  Package.prototype.enrol = async function (user, group, type) {
    let t = -1;
    switch (type) {
      case "individual":
        t = 0;
        break;
      case "group":
        t = 1;
        break;
      case "class":
        t = 2;
        break;
    }

    const result = await sequelize.query(
      "CALL `EnrolUserToPackage`(:packageId, :userId, :groupId, :t);",
      {
        replacements: {
          packageId: this.id,
          userId: user.id,
          groupId: group.id,
          t
        },
        type: sequelize.QueryTypes.CALL
      }
    );

    return result[0].code;
  };

  Package.findBySlug = async slug => {
    const pkg = await Package.findOne({
      include: Language,
      where: { slug }
    });
    const exercises = await sequelize.query(
      `SELECT k.* FROM m_katalog k LEFT JOIN m_package_exercises p ON k.id = p.exercise_id WHERE p.package_id = $package_id`,
      {
        bind: { package_id: pkg.id },
        model: CatalogExercise,
        mapToModel: true
      }
    );

    const { enabled, language_id, ...rest } = pkg.toJSON();

    return {
      ...rest,
      exercises: exercises
        .map(e => e.toJSON())
        .map(e => {
          const { language, metadata, code, enabled, id, slug, ...rest } = e;
          return rest;
        })
    };
  };

  return { Package };
};
