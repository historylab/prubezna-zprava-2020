const seq = require("./sequelize");
const { User } = seq();

const findById = (id, cb) => {
  User.findById(id, {
    attributes: { exclude: ["password_digest"] }
  })
    .then(user => {
      const u = user.toJSON();
      return user
        .getEffectiveRoles()
        .then(roles => {
          u.roles = roles.map(r => r.name);
          cb(null, u);
        })
        .catch(error => {
          cb(error, null);
        });
    })
    .catch(error => {
      cb(error, null);
    });
};

const authenticate = (userName, password, cb) => {
  User.authenticate(userName, password, cb);
};

module.exports = {
  findById,
  authenticate
};
