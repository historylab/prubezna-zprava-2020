const config = require("./config");
const Queue = require("bee-queue");

const emailQueue = new Queue("email", {
  redis: {
    host: config.redis.host
  },
  isWorker: false
});

export default createEmailJob = async data => {
  const job = emailQueue.createJob(data);
  return await job
    .timeout(3000)
    .retries(2)
    .save();
};
