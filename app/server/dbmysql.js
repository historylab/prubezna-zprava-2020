const mysql = require("mysql");
const _ = require("lodash");
const seq = require("./sequelize");
const logger = require("./logger")("db");
const config = require("./config");

const connectionPool = mysql.createPool({
  connectionLimit: 10,
  queueLimit: 1,
  acquireTimeout: 10000,
  ...config.db
});

connectionPool.on("connection", connection => {
  connection.config.queryFormat = (query, values) => {
    if (!values) {
      return query;
    }
    return query.replace(
      /\:(\w+)/g,
      function (txt, key) {
        if (values.hasOwnProperty(key)) {
          return connection.escape(values[key]);
        }
        return txt;
      }.bind(this)
    );
  };
});

function execute(query, parameters, cb) {
  connectionPool.query(query, parameters, (error, results) => {
    if (error) {
      logger.error(error);
      cb(error, null);
      return;
    }

    cb(null, results);
  });
}

function executePromise(query, parameters = {}) {
  return new Promise((resolve, reject) => {
    connectionPool.query(query, parameters, (error, results) => {
      if (error) {
        logger.error(error);
        reject(error);
        return;
      }
      resolve(results);
    });
  });
}

const fetchExerciseList = (userId, groupId = -1, cb) => {
  const query = `SELECT * from (
    select * from ExerciseEnrolmentsView
    union 
    select * from PackageEnrolmentsView) e
  WHERE (group_id = :groupId) ORDER BY name`;

  execute(query, { userId, groupId }, (error, results) => {
    if (error) {
      cb(error, null);
      return;
    }

    results.forEach(setExerciseTypeDescription);

    const assetsUrl = config.application.assetsUrl;
    results = results.map(r => {
      let imageUrl = r.image_url || "";
      if (!imageUrl.match(/^http/)) {
        imageUrl =
          assetsUrl +
          "/cviceni-aktualni/assets/img/" +
          r.slug +
          "/" +
          r.image_url;
      }

      return {
        ...r,
        isPackage: r.is_package,
        detailUrl: `/${r.is_package ? "balicek" : "cviceni"}/detail/${r.code}`,
        url: assetsUrl + "/cviceni/" + r.slug,
        image_url: imageUrl
      };
    });

    cb(null, { exercises: results });
  });
};

const fetchExerciseByEnrolmentId = (userId, enrolmentId) => {
  const exerciseQuery = `SELECT enrolment_id as id, v.code, k.url, v.type, g.name as group_name, 
  g.id as group_id, k.name, image_url, 1 as is_active
    FROM EnrolmentWithEmailsAndSubmissionsView v
    left join m_katalog k on k.id = v.katalog_id
    LEFT JOIN m_cviceni_group g on g.id = v.group_id
          WHERE ce.user_id = :userId AND ce.id = :enrolmentId`;

  return new Promise((resolve, reject) => {
    execute(exerciseQuery, { userId, enrolmentId }, (error, results) => {
      if (error) {
        return reject(error);
      }

      if (results.length == 0) {
        return resolve(null);
      }

      exerciseDataFetch(userId, results[0], (error, exercise) => {
        if (error) {
          return reject(error);
        }

        return resolve(exercise);
      });
    });
  });
};

const fetchExerciseByCode = (user, code, cb) => {
  const userId = user.id;
  const exerciseQuery = `SELECT ce.id as id, ce.code, k.url, ce.type, g.name as groupName, k.slug,
  g.id as groupId, orig.name as originalGroupName, ce.original_group_id as originalGroupId, k.name, image_url as imageUrl, is_active as isActive
    FROM m_cviceni_enrolment ce 
    LEFT JOIN CatalogExerciseView k on k.exerciseId = ce.exerciseId
    LEFT JOIN m_cviceni_group g on g.id = ce.group_id
    LEFT JOIN m_cviceni_group orig on orig.id = ce.original_group_id
          WHERE ce.user_id = :userId AND ce.code = :code`;
  execute(exerciseQuery, { userId, code }, (error, results) => {
    if (error) {
      cb(error, null);
      return;
    }

    if (results.length == 0) {
      cb(null, null);
      return;
    }

    const assetsUrl = config.application.assetsUrl;

    results = results.map(r => {
      const urlTemplate = config.application.exerciseAssignUrl;
      return {
        ...r,
        url: urlTemplate
          .format(r.slug, r.code, null)
          .replace("&email=null", ""),
        tryItUrl: urlTemplate.format(r.slug, r.code, user.emailAddress),
        imageUrl:
          assetsUrl +
          "/cviceni-aktualni/assets/img/" +
          r.slug +
          "/" +
          r.imageUrl
      };
    });

    exerciseDataFetch(userId, results[0], cb);
  });
};

const setExerciseTypeDescription = exercise => {
  switch (exercise.type) {
    case 0:
      exercise.type = "individual";
      exercise.typeDescription = "individuální cvičení";
      break;
    case 1:
      exercise.type = "group";
      exercise.typeDescription = "skupinové cvičení";
      break;
    case 2:
      exercise.type = "class";
      exercise.typeDescription = "cvičení s celou třídou";
      return;
  }

  return exercise;
};

const exerciseDataFetch = (userId, exercise, cb) => {
  const { Group } = seq();
  Group.findById(exercise.groupId).then(group => {
    if (group == null) {
      exerciseDataFetch_(userId, exercise, {}, [], cb);
      return;
    }
    group.getMembers().then(members => {
      //members = members.map(e => e.emailAddress.toLowerCase());
      exerciseDataFetch_(userId, exercise, group, members, cb);
    });
  });
};

const exerciseDataFetch_ = (userId, exercise, group, members, cb) => {
  setExerciseTypeDescription(exercise);

  // z properties udelame objekty
  exercise.group = { id: group.id, name: group.name }; //, emailAddresses: emails };
  exercise.originalGroup = {
    id: exercise.originalGroupId,
    name: exercise.originalGroupName
  };

  delete exercise.groupId;
  delete exercise.groupName;
  delete exercise.originalGroupId;
  delete exercise.originalGroupName;

  const emailsQuery = `
  select * from (select ge.* from m_cviceni_enrolment e
    left join m_cviceni_group g on e.group_id = g.id
    left join m_cviceni_group_email ge on ge.group_id = g.id
    where e.user_id = :userId and e.id = :enrolmentId
    union
    select -1, se.emailAddress, -1
    from m_cviceni_submission s
    left join m_cviceni_enrolment e on e.id = s.enrolment_id
    left join m_submission_emailAddresses se on se.submission_id = s.entryid
    left join m_cviceni_group_email gem on gem.group_id = e.group_id and gem.emailAddress = se.emailAddress
    where gem.id is null and e.user_id = :userId and e.id = :enrolmentId
    ) e
    where emailAddress is not null and length(rtrim(ltrim(emailAddress))) > 0
    order by lower(emailAddress)
  `;

  const submissionQuery = `
  select distinct
	  se.emailAddress, se.name, time_spent as timeSpent, ifnull(s.entryId, -1) as entryId,  
    CONVERT_TZ(TIMESTAMPADD(SECOND, ifnull(time_spent, 0) / 1000, s.dateCreated),'UTC','Europe/Prague') as lastUpdated, 
    te.id as evaluation_id, note, score, te.updatedAt as noteUpdatedAt, s.exerciseSlug
from m_cviceni_enrolment ce
left join m_cviceni_submission s on (s.enrolment_id = ce.id or trim(lower(ce.code)) = trim(lower(s.lms_hash))) 
left join (
  SELECT submission_id, trim(lower(emailAddress)) as emailAddress, name FROM m_submission_emailaddresses where emailAddress is not null and trim(emailAddress) <> '' 
) se on se.submission_id = s.entryId
left join m_teacher_evaluation te on te.entry_id = s.entryid
where ce.id=:enrolmentId and length(ifnull(trim(se.emailAddress), '')) > 0`;

  execute(
    submissionQuery,
    { userId, enrolmentId: exercise.id },
    (error, rawSubmissions) => {
      if (error) {
        cb(error, null);
        return;
      }

      let submissions = Object.values(
        rawSubmissions.reduce((prev, curr) => {
          var entryId = curr.entryId;
          if (prev[entryId]) {
            prev[entryId].emailAddresses.push({
              emailAddress: curr.emailAddress,
              name: curr.name
            });
            prev[entryId].emailAddresses = _.sortBy(
              prev[entryId].emailAddresses,
              ["emailAddress"]
            );
          } else {
            prev[entryId] = curr;
            curr.emailAddresses = [
              { emailAddress: curr.emailAddress, name: curr.name }
            ];
          }
          return prev;
        }, {})
      );

      if (exercise.type !== "group") {
        submissions = submissions.concat(
          members
            .filter(m => {
              var x = submissions.find(
                s =>
                  s.emailAddresses.length === 1 &&
                  s.emailAddresses[0].emailAddress.toLowerCase() ===
                    m.emailAddress.toLowerCase()
              );
              return !x;
            })
            .map(m => {
              return {
                emailAddresses: [{ emailAddress: m.emailAddress, name: m.name }]
              };
            })
        );
      }

      /*const groupSubmissions = _.uniq(
        submissions
          .map(s => s.emailAddress.toLowerCase())
          .filter(e => emails.includes(e))
      );

      // groupovat podle entryId pokud je > 0
      const xx = _.groupBy(submissions, e => {
        return e.entryId > 0 ? e.entryId : -e.index;
      });

      const emailAddresssInSubmissions = _.uniq(
        submissions.map(s => s.emailAddress.toLowerCase())
      );

      const yy = [];
      for (var r in xx) {
        var current = xx[r][0];
        current.emailAddresses = xx[r].map(e => {
          return e.emailAddress;
        });
        delete current.emailAddress;
        yy.push(current);
      }*/

      //submissions = yy;

      /*emails
        .filter(e => !emailAddresssInSubmissions.includes(e))
        .forEach(email => {
          submissions.push({
            code: exercise.code,
            emailAddresses: [email]
          });
        });*/

      const urlTemplate = config.application.exerciseViewUrl;

      submissions = submissions.map(r => {
        return {
          ...r,
          viewUrl:
            r.entryId > 0 ? urlTemplate.format(r.exerciseSlug, r.entryId) : null
        };
      });

      submissions = _.sortBy(submissions, s => {
        return s.emailAddresses[0].emailAddress;
      });

      submissions = submissions.map(s => {
        const { exerciseSlug, ...rest } = s;
        return rest;
      });

      exercise.submissions = {
        //current: submissions.length, // groupSubmissions.length,
        //waiting: emails.length - submissions.length,
        entries: submissions
      };

      exercise.emailAddresses = members;
      cb(null, exercise);
    }
  );
};

const lang = lng => {
  lng = (lng || "").trim().toLowerCase();
  if (lng === "en-us" || lng === "en") {
    return "en-us";
  }

  return "cs-cz";
};

const fetchCatalog = (user, exerciseId = -1, slug = "", language, cb) => {
  const { CatalogExercise, Language } = seq();

  let findOptions = { where: { enabled: true } };
  if (exerciseId > 0) {
    findOptions = { where: { enabled: true, id: exerciseId } };
  } else if ((slug || "").match(/./)) {
    findOptions = { where: { enabled: true, slug } };
  } else {
    findOptions = {
      where: { enabled: true, language: Language.getCode(language) }
    };
  }

  findOptions = {
    ...findOptions,
    order: ["name"],
    attributes: { exclude: ["id"] }
  };

  CatalogExercise.findAll(findOptions)
    .then(results => {
      cb(null, results);
    })
    .catch(e => cb(e, null));
};

const enrolToExercise = (exerciseId, userId, groupId, typeDesc) => {
  const statement =
    "SET @p0=:exerciseId; SET @p1=:userId; SET @p2=:groupId; SET @p3=:type; CALL `EnrolUserToExercise`(@p0, @p1, @p2, @p3, @p4); SELECT id, code FROM m_cviceni_enrolment WHERE id=@p4;";

  let type = -1;
  switch (typeDesc) {
    case "individual":
      type = 0;
      break;
    case "group":
      type = 1;
      break;
    case "class":
      type = 2;
      break;
  }

  return new Promise((resolve, reject) => {
    execute(
      statement,
      { exerciseId, userId, groupId, type },
      (error, results) => {
        if (error) {
          return reject(error);
        }

        const objectifyRawPacket = row => ({ ...row });
        const convertedResponse = results
          .filter(o => Array.isArray(o))
          .map(objectifyRawPacket);

        return resolve(convertedResponse[0]["0"]);
      }
    );
  });
};

const getDataForPeeping = (criteria, sortedBy, options) => {
  const {
    emailAddress,
    lastName,
    dateFrom,
    dateTo,
    lmsHash,
    katalog,
    //timeSpent,
    exerciseMode,
    done,
    teacher,
    isActive,
    from
  } = criteria;

  const { page, limit, count, forExport, includeNotes } = options;

  let query = `SELECT s.entryId, 
  DATE_FORMAT(CONVERT_TZ(TIMESTAMPADD(SECOND, ifnull(time_spent, 0) / 1000, s.dateCreated),'UTC','Europe/Prague'), '%d.%m.%Y %H:%i') as dateCreated,
  s.lms_Hash as lmsHash, exerciseVersion, exerciseMode, time_spent as timeSpent, done, 
  e.id as enrolmentId, e.type, (case e.type when 0 then 'individual' when 1 then 'group' when 2 then 'class' else null end) as exerciseType,
  (case e.type when 0 then 'individuální cvičení' when 1 then 'skupinové cvičení' when 2 then 'cvičení s celou třídou' else null end) as exerciseDescription,
  g.name as groupName, ifnull(k.name, s.exerciseSlug) as name, ifnull(is_active, 0) as isActive, u.firstname, u.lastname, 
  ifnull(u.email, s.additionalEmailAddresses) as teacherEmail, s.exerciseSlug as slug `;

  let sqlFrom = `FROM m_cviceni_submission s
    left join CatalogExerciseView k on k.exerciseid = s.katalog_id
    left join m_cviceni_enrolment e on s.enrolment_id = e.id
    left join m_cviceni_group g on g.id = e.group_id
    left join m_user u on u.id = e.user_id
    left join m_user_groups ug on ug.peepingExtra = 1
    left join m_user_group_membership mug on mug.idUserGroup = ug.id and idUser = u.id
    left join (SELECT entry_id as entryId, query_string FROM m_tracking where query_string REGEXP 'from=[^&]+') t on t.entryId = s.entryId    
  `;

  if (forExport) {
    query += `, k.exerciseId, se.emailAddress as pupilEmail, t.query_string,
  (case when mug.idUser is null then 0 else 1 end) zam `;

    sqlFrom += ` left join (SELECT m.submission_id, GROUP_CONCAT(DISTINCT emailAddress SEPARATOR ', ') as emailAddress
FROM m_submission_emailAddresses m
GROUP BY m.submission_id) se on se.submission_id = s.entryid`;

    if (includeNotes) {
      query += ", notes.notes ";
      sqlFrom += "left join SubmissionNotes notes on notes.entryid = s.entryid";
    }
  }

  let where = "WHERE 1=1";
  if (done > -1) {
    where += " AND done = :done";
  }

  if (dateFrom) {
    where += " AND s.dateCreated >= DATE(:dateFrom)";
  }

  if (dateTo) {
    where += " AND s.dateCreated <= DATE_ADD(DATE(:dateTo), INTERVAL 1 DAY)";
  }

  if (lmsHash > -1) {
    where += ` AND s.lms_Hash IS${lmsHash == 1 ? " NOT" : ""} NULL`;
  }

  if (exerciseMode > -1) {
    where += " AND e.type = :exerciseMode";
  }

  if (katalog > -1) {
    where += " AND k.exerciseId = :katalog";
  }

  let fromFilter = from;
  if (fromFilter && fromFilter.trim().length > 0) {
    fromFilter = `from=.*${fromFilter}[^&]*`;
    where += " AND t.query_string REGEXP :from";
  }

  let orderBy = "order by s.entryid desc ";
  if (sortedBy) {
    let field = "s.entryid";
    let sort = sortedBy.sort || "";
    switch (sortedBy.field) {
      case "entryId":
        field = "s.entryId";
        break;
      case "exerciseMode":
        field = "e.type";
        break;
      case "timeSpent":
        field = "time_spent";
        break;
      case "groupName":
        field = "g.name";
        break;
      case "name":
        field = "k.name";
        break;
      case "dateCreated":
        field = "s.dateCreated";
        break;
      case "teacherEmail":
        field = "u.email";
        break;
    }
    if (
      !sort ||
      (sort.toLowerCase() !== "asc" && sort.toLowerCase() !== "desc")
    ) {
      sort = "asc";
    }
    orderBy = `order by ${field} ${sort} `;
  }

  query += sqlFrom + " " + where + " " + orderBy + " ";

  const c = { ...criteria, from: fromFilter };
  logger.info(options);
  if (count) {
    const countQuery = `select count(*) as total ${sqlFrom} ${where}`;
    logger.info(countQuery);
    return executePromise(countQuery, c);
  }

  if (page >= 0) {
    if (page >= 1) {
      query += ` LIMIT ${limit * (Math.max(1, page) - 1)}, ${limit}`;
    } else {
      query += ` LIMIT ${limit}`;
    }
  }

  logger.info(query);

  return executePromise(query, c);
};

const getDataForPeepingFilter = () => {
  let query = `select distinct k.exerciseId as id, (select name from m_katalog k2 where k2.exerciseId = k.exerciseId order by k2.id desc limit 1) as name from m_katalog k where k.exerciseId > 0 order by name`;
  return executePromise(query);
};

module.exports = {
  fetchExerciseList,
  fetchExerciseByCode,
  fetchExerciseByEnrolmentId,
  enrolToExercise,
  getDataForPeeping,
  getDataForPeepingFilter
};
