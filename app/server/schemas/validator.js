const expressAjv = require("express-ajv");
const schema = expressAjv.schema;

schema.addSchema("addEmailAddress", require("./addEmailAddress"));
schema.addSchema("enrolToExercise", require("./enrolToExercise"));
schema.addSchema("renameGroup", require("./renameGroup"));
schema.addSchema("restoreGroup", require("./restoreGroup"));
schema.addSchema("changeGroup", require("./changeGroup"));
schema.addSchema("deleteEnrolment", require("./deleteEnrolment"));
schema.addSchema("removeEmailAddress", require("./removeEmailAddress"));
schema.addSchema("addEvaluation", require("./addEvaluation"));
schema.addSchema("sendEnrolment", require("./sendEnrolment"));
schema.addSchema("shareWithOthers", require("./shareWithOthers"));
schema.addSchema("finished", require("./finished"));

module.exports = expressAjv.validatorFactory;
