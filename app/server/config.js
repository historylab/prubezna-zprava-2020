const { ConnectionString } = require("connection-string");

const environment = process.env.NODE_ENV || "development";

const config = {};
config.environment = environment;
const isProduction = environment === "production";
config.isProduction = isProduction;

config.queues = {
  redis: {
    host: process.env.REDIS_HOST || "127.0.0.1",
    port: process.env.REDIS_PORT || 6379
  }
};

var port = process.env.PORT || 80;

let connString = new ConnectionString(
  process.env.MYSQL || "mysql://root:moodle@localhost:3306/moodle"
);

config.db = {
  ...connString,
  host: connString.hosts[0].name,
  database: connString.path[0],
  multipleStatements: true
};

const historyLabUrl = process.env.HISTORY_LAB_URL || "http://localhost:" + port;
const assetsUrl = process.env.ASSETS_URL || "http://localhost:8284";
config.application = {
  historyLabUrl,
  assetsUrl
};

config.recaptcha = {
  secretKey: process.env.RECAPTCHA_SECRETKEY
};

config.secret = process.env.APP_SECRET;

let smtp_secure = (process.env.SMTP_SECURE || "").trim().toLowerCase();
let ignoreTLS = (process.env.SMTP_IGNORETLS || "").trim().toLowerCase();

config.email = {
  smtp: {
    host: process.env.SMTP_HOST,
    port: process.env.SMTP_PORT,
    secure: smtp_secure === "true" || smtp_secure === "1",
    from: process.env.SMTP_FROM,
    ignoreTLS: ignoreTLS === "true" || ignoreTLS === "1",
    auth: {
      username: process.env.SMTP_AUTH_USERNAME,
      password: process.env.SMTP_AUTH_PASSWORD
    }
  },
  templatesDir: process.env.EMAIL_TEMPLATES_DIR
};

config.application.resetPasswordUrl = historyLabUrl + "/reset-hesla?token=%s";
config.application.exerciseViewUrl =
  assetsUrl + "/cviceni/prohlizeni/%s?entryId=%s";
config.application.exerciseAssignUrl =
  assetsUrl + "/cviceni/%s?lms_hash=%s&email=%s";

config.db = JSON.parse(JSON.stringify(config.db));

config.maxAge = 7 * 24 * 60 * 60;

if (process.env.CI === 1) {
  console.log(config);
}

module.exports = config;
