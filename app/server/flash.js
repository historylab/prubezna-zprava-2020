"use strict";
const flash = require("express-flash");

module.exports = app => {
  app.use(flash());

  app.use((req, res, next) => {
    res.locals.sessionFlash = req.session.sessionFlash;
    delete req.session.sessionFlash;
    next();
  });

  app.use((req, res, next) => {
    const data = req.flash();
    const keys = Object.keys(data);
    if (keys.length == 0) {
      next();
      return;
    }

    const messages = keys.reduce((prev, curr) => {
      console.info(prev, curr, data[curr]);
      return prev.concat(
        data[curr].map(m => {
          return { type: curr, text: m };
        })
      );
    }, []);

    const encodedData = Buffer.from(
      encodeURIComponent(JSON.stringify(messages))
    ).toString("base64");
    res.header("X-HL-Flash", encodedData);
    next();
  });
};
