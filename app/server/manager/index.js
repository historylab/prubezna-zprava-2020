const express = require("express");
const bodyParser = require("body-parser");
const logger = require("./../logger")("exercise-manager");
const path = require("path");

const app = express();

const port = process.env.PORT || 80;
const wwwRoot = process.env.WWWROOT || "";

global.dispatcher = require("./../queue/dispatcher");

app.use(bodyParser.urlencoded({ extended: true }));
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");
app.use("/static", express.static(path.join(__dirname, "public")));

if (process.env.NODE_ENV !== "production") {
  logger.info("Environment: development ...");
} else {
  logger.info("Environment: production ...");
}

require("./router")(app);

app.use(require("express-ajv").defaultErrorHandler);

app.use(express.static(path.join(__dirname, "public")));
app.use("/cviceni-verze", express.static(path.join(wwwRoot, "cviceni-verze")));
app.use(
  "/cviceni-aktualni",
  express.static(path.join(wwwRoot, "cviceni-aktualni"), {})
);

app.use((error, req, res, next) => {
  console.error(error);
  logger.error(error);
  // Will **not** get called. You'll get Express' default error
  // handler, which returns `error.toString()` in the error body
  if (error.status && error.status === 400) {
    return;
  }
  res.status(500).json({ message: error.message });
});

app.all("*", (req, res) => {
  res.status(404).send();
});

// Start server
app.listen(port, () => {
  logger.info(
    `Exercise manager (formerly known as PHP manager) listening on port ${port}, wwww root set to '${wwwRoot}'.`
  );
});
