const seq = require("./../sequelize");

let wrap = fn => (...args) => fn(...args).catch(args[2]);

const asyncRoute = route => (req, res, next = console.error) =>
  Promise.resolve(route(req, res)).catch(error => {
    logger("exercises").error(error);
    next(error);
  });

const logger = require("./../logger");

module.exports = app => {
  require("./api/exercises")(app, null, seq, wrap, logger("exercises-api"));
  require("./api/create")(app, null, seq, wrap, logger("exercises"));
  require("./routes/view")(app, null, seq, asyncRoute, logger("exercises"));
  require("./routes/create")(app, null, seq, asyncRoute, logger("exercises"));
  require("./routes/save")(app, null, seq, asyncRoute, logger("exercises"));
  require("./routes/static")(app, null, seq, asyncRoute, logger("exercises"));
};
