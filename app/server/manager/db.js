/*

date_default_timezone_set('Europe/Prague');

function get_name_and_version_from_database_new($entryid)
{
    $conn = null;
    try {
        $conn = create_connection();
        return get_name_and_version_from_database($entryid, $conn);
    }
    finally {
        if ($conn) {
            mysqli_close($conn);
        }
    }
}

//Funkce, kterĂĄ vracĂ­ jmĂŠno a verzi cviÄ?enĂ­
function get_name_and_version_from_database($entryid, $conn)
{
    try {
        $stmt = mysqli_prepare($conn, "SELECT exerciseSlug, exerciseVersion FROM m_cviceni_submission WHERE entryid=?");
        if ($stmt) {
            // bind parameters for markers 
            mysqli_stmt_bind_param($stmt, "s", $entryid);
            // execute query 
            mysqli_stmt_execute($stmt);
            // bind result variables 
            mysqli_stmt_bind_result($stmt, $exerciseSlug, $exerciseVersion);
            // fetch value 
            if (mysqli_stmt_fetch($stmt)) {
                return array($exerciseSlug, $exerciseVersion);
            }
            return null;
        }
    } // catch (Exception $e) {
    //TODO logovat
    echo 'Caught exception: ', $e->getMessage(), "\n";
    return null;
    } finally {
        // close statement 
        mysqli_stmt_close($stmt);
    }

}

//Funkce ktera zkontroluje jestli existuje soubor
function check_if_file_exist_in_version($exerciseSlug, $exerciseVersion)
{
    $masterVersion = $exerciseVersion[0];
    print_r('cviceni-verze/v' . $masterVersion . "/" . $exerciseSlug);
    return file_exists('cviceni-verze/v' . $masterVersion . "/" . $exerciseSlug);
}
//
function creat_entryid($hash)
{
$id = uniqid($hash);

return $id;
}

//*
 * Kontrola jestli zadane entryid existuje v databazi
 *
 
function check_existing_entryid($entryid, $conn)
{    
    try {
        $stmt = mysqli_prepare($conn, "SELECT entryid FROM m_cviceni_submission WHERE entryid=?");
        if ($stmt) {
            // bind parameters for markers 
            mysqli_stmt_bind_param($stmt, "s", $entryid);
            // execute query 
            mysqli_stmt_execute($stmt);
            // bind result variables 
            mysqli_stmt_bind_result($stmt, $entryid);
            // fetch value 
            if (mysqli_stmt_fetch($stmt)) {
                return true;
            }
            return false;
        }
    } // catch (Exception $e) {
    //TODO logovat
    echo 'Caught exception: ', $e->getMessage(), "\n";
    return null;
    }  finally {
        // close statement 
        mysqli_stmt_close($stmt);
    }
}

function check_existing_entryid_new($entryid)
{
    $conn = null;
    try {
        $conn = create_connection();
        return check_existing_entryid($entryid, $conn);
    }
    finally {
        if ($conn) {
            mysqli_close($conn);
        }
    }
}

function save_prerequisites_to_database_new($entryids, $exercisesCodes, $hash)
{
    $conn = null;
    try {
        $conn = create_connection();
        return save_prerequisites_to_database($entryids, $exercisesCodes, $hash, $conn);
    }
    finally {
        if ($conn) {
            mysqli_close($conn);
        }
    }
}

//*
 * Ulozi zakladni informace (entryid,exercisesCode,hash) do databaze
 
function save_prerequisites_to_database($entryids, $exercisesCodes, $hash, $conn)
{
    $entryid = 2;
    $katalog_id = 1;
    $exerciseVersion = '4.5.8';
    $exerciseSlug = 'horakova-rezoluce';

    try {
        // $stmt = mysqli_prepare($conn, "SELECT entryid FROM m_cviceni_submission WHERE entryid=?");

        $stmt = mysqli_prepare($conn, "INSERT INTO m_cviceni_submission (exerciseSlug,katalog_id, exerciseVersion) VALUES (?,?,?)");
        if ($stmt) {
            // bind parameters for markers 
            mysqli_stmt_bind_param($stmt, "sss", $exerciseSlug, $katalog_id, $exerciseVersion);
            // execute query 
            return mysqli_stmt_execute($stmt);
        }
    } //catch (Exception $e) {
    //TODO logovat
    echo "TO SNAD NEEE";
    echo 'Caught exception: ', $e->getMessage(), "\n";
    return null;
    } finally {
        // close statement 
        mysqli_stmt_close($stmt);
    }

}

// Funkce, kterĂĄ pĹ?ijĂ­mĂĄ dotaz na databĂĄzi a databĂĄzovĂŠ pĹ?ipojenĂ­ a vracĂ­ vĂ˝sledek jako pole hodnot zĂ­skanĂ˝ch z databĂĄze
function mysqli_get_var($query, $conn)
{
    $res = mysqli_query($conn, $query);
    $row = mysqli_fetch_array($res);
    mysqli_free_result($res);
    return $row;
}

// Funkce, kterĂĄ pĹ?ijĂ­mĂĄ nĂĄzev cviÄ?enĂ­ a jeho verzi a nĂĄslednÄ? vrĂĄtĂ­ cestu k danĂŠmu indexu cviÄ?enĂ­.
// Funkce slouĹžĂ­ pro sprĂĄvnĂŠ zĂ­skĂĄnĂ­ cesty k souborĹŻm cviÄ?enĂ­.
function get_path_by_version($cviceni, $version)
{
    $masterVersion = $version[0];
    $path = "cviceni-verze/v" . $masterVersion . "/" . $cviceni . "/index.html";
    return $path;
}

function get_path_by_version_to_javascript($version)
{
    $masterVersion = $version[0];
    $path = "/cviceni-verze/v" . $masterVersion . "/";
    return $path;

}

function get_data_from_database_new($entryid)
{
    $conn = null;
    try {
        $conn = create_connection();
        return get_data_from_database($entryid, $conn);
    }
    finally {
        if ($conn) {
            mysqli_close($conn);
        }
    }
}

// Dostanu JSON data z databaze
function get_data_from_database($entryid, $conn)
{
    //$stmt = mysqli_prepare($conn, "SELECT exerciseSlug, exerciseVersion, exerciseData FROM m_cviceni_submission WHERE entryid=?");

    try {
        $stmt = mysqli_prepare($conn, "SELECT exerciseData,done,emailAddress FROM m_cviceni_submission WHERE entryid=?");
        if ($stmt) {
            // bind parameters for markers 
            mysqli_stmt_bind_param($stmt, "s", $entryid);
            // execute query 
            mysqli_stmt_execute($stmt);
            // bind result variables 

            //
            mysqli_stmt_bind_result($stmt, $exerciseData);
            
            if (mysqli_stmt_fetch($stmt)) {
                //return array($exerciseSlug, $exerciseVersion,$exerciseData);
                return $exerciseData;

            }
            return false;

            mysqli_stmt_bind_result($stmt, $exerciseData, $done,$user);
            // fetch value 

            if (mysqli_stmt_fetch($stmt)) {
                return array($exerciseData, $done,$user);
            }
            return null;

        }
    } finally {
        // close statement 
        if ($stmt) mysqli_stmt_close($stmt);
             
    }

}

// funkce kterĂĄ poĹĄle email s url cviÄ?enĂ­ ******** POTĹ?EBA DODÄ?LAT -> vizuĂĄlnĂ­ zpracovĂĄnĂ­ emailu, test na servru
function sendEmail($email, $url)
{
    $msg = "VaĹĄe uloĹženĂŠ cviÄ?enĂ­ naleznete na tĂŠto strĂĄnce: " . $url;
    $msg = wordwrap($msg, 70);
    mail("examplehistorylab@gmail.com", "NovÄ? vyplnÄ?nĂŠ cviÄ?enĂ­", $msg, "From: tachpe1997@gmail.com");
    // echo "<br>Email byl odeslĂĄn";
}
//*
 * ulozeni cviceni do databaze (respektive update dat v databazi)
 *
 *SMAZAT
 
//
function save_all_to_database($entryid, $user, $hash, $exerciseSlug, $katalog_id, $exerciseVersion, $exerciseMode, $exerciseData, $analytics, $time_spent, $conn)
{
try {
$stmt = mysqli_prepare($conn, "UPDATE m_cviceni_submission
SET
user = ?,
lastUpdated = CURRENT_TIMESTAMP(),
hash = ?,
exerciseSlug = ?,
katalog_id =?,
exerciseVersion = ?,
exerciseMode = ?,
exerciseData = ?,
analytics = ?,
time_spent = ?
WHERE
entryid = ?;");
if ($stmt) {
 
// bind parameters for markers 
//  mysqli_stmt_bind_param($stmt, "ssssssssss", $user, $hash, $exerciseSlug, $katalog_id, $exerciseVersion, $exerciseMode, $exerciseData, $analytics, $time_spent, $entryid);
// execute query and return tru on success and false on failure
//   return mysqli_stmt_execute($stmt);

//  }
} catch (Exception $e) {
//TODO logovat
echo 'Caught exception: ', $e->getMessage(), "\n";
return false;
} finally {
 
// close statement 
// mysqli_stmt_close($stmt);
}

}

function save_all_to_db_with_hash_new($entryid, $emailZaka, $exerciseData, $exerciseId, $analytics, $time_spent, $done)
{
    $conn = null;
    try {
        $conn = create_connection();
        return save_all_to_db_with_hash($entryid, $emailZaka, $exerciseData, $exerciseId, $analytics, $time_spent, $done, $conn);
    }
    finally {
        if ($conn) {
            mysqli_close($conn);
        }
    }
}

//*
 * ulozeni cviceni do databaze (respektive update dat v databazi)
 *
 *
 
function save_all_to_db_with_hash($entryid, $emailZaka, $exerciseData, $exerciseId, $analytics, $time_spent, $done, $conn)
{
    try {
        $stmt = mysqli_prepare($conn, "UPDATE m_cviceni_submission
            SET
                emailAddress = ?,
                lastUpdated = CURRENT_TIMESTAMP(),
                exerciseData = ?,
                exerciseId = ?,
                analytics = ?,
                time_spent = ?,
                done = ?
            WHERE
                entryid = ?;");
        if ($stmt) {
            // bind parameters for markers 
            mysqli_stmt_bind_param($stmt, "sssssss", $emailZaka, $exerciseData,$exerciseId, $analytics, $time_spent, $done, $entryid);
            // execute query and return tru on success and false on failure
            return mysqli_stmt_execute($stmt);

        }
    } // catch (Exception $e) {
    //TODO logovat
    echo 'Caught exception: ', $e->getMessage(), "\n";
    return false;
    } finally {
        // close statement 
        mysqli_stmt_close($stmt);
    }

}

function save_email_into_database_new($emailZaka, $entryid)
{
    $conn = null;
    try {
        $conn = create_connection();
        return save_email_into_database($emailZaka, $entryid, $conn);
    }
    finally {
        if ($conn) {
            mysqli_close($conn);
        }
    }
}

function save_email_into_database($emailZaka, $entryid, $conn)
{
    try {
        $stmt = mysqli_prepare($conn, "
            INSERT INTO m_submission_emailAddresses
            (submission_id,emailAddress) VALUES (?,?);
            ");
        if ($stmt) {
            // bind parameters for markers 
            mysqli_stmt_bind_param($stmt, "ss", $entryid, $emailZaka);
            // execute query and return tru on success and false on failure
            return mysqli_stmt_execute($stmt);

        }
    } // catch (Exception $e) {
    //TODO logovat
    echo 'Caught exception: ', $e->getMessage(), "\n";
    return false;
    } finally {
        // close statement 
        mysqli_stmt_close($stmt);
    }

}

function save_all_to_db_without_hash_new($entryid, $emailZaka, $exerciseData, $exerciseId, $exerciseMode, $analytics, $time_spent, $done)
{
    $conn = null;
    try {
        $conn = create_connection();
        return save_all_to_db_without_hash($entryid, $emailZaka, $exerciseData, $exerciseId, $exerciseMode, $analytics, $time_spent, $done, $conn);
    }
    finally {
        if ($conn) {
            mysqli_close($conn);
        }
    }
}
//*
 * ulozeni cviceni do databaze (respektive update dat v databazi)
 *
 *
 
function save_all_to_db_without_hash($entryid, $emailZaka, $exerciseData, $exerciseId, $exerciseMode, $analytics, $time_spent, $done, $conn)
{
    try {
        $stmt = mysqli_prepare($conn, "UPDATE m_cviceni_submission
            SET
                emailAddress = ?,
                lastUpdated = CURRENT_TIMESTAMP(),
                exerciseData = ?,
                exerciseId = ?,
                exerciseMode = ?,
                analytics = ?,
                time_spent = ?,
                done = ?
            WHERE
                entryid = ?;");
        if ($stmt) {
            // bind parameters for markers 
            mysqli_stmt_bind_param($stmt, "ssssssss", $emailZaka, $exerciseData, $exerciseId, $exerciseMode, $analytics, $time_spent, $done, $entryid);
            // execute query and return tru on success and false on failure
            return mysqli_stmt_execute($stmt);

        }
    } // catch (Exception $e) {
    //TODO logovat
    echo 'Caught exception: ', $e->getMessage(), "\n";
    return false;
    } finally {
        // close statement 
        mysqli_stmt_close($stmt);
    }

}


function create_first_record_with_hash_new($exerciseSlug, $katalog_id,$enrolment_id, $exerciseVersion, $hash, $exerciseMode, $tracking_info)
{
    $conn = null;
    try {
        $conn = create_connection();
        return create_first_record_with_hash($exerciseSlug, $katalog_id,$enrolment_id, $exerciseVersion, $hash, $exerciseMode, $tracking_info, $conn);
    }
    finally {
        if ($conn) {
            mysqli_close($conn);
        }
    }
}

//vztvoĹ?enĂ­ iniciaÄ?nĂ­ho uloĹženĂ­ do databĂĄze
function create_first_record_with_hash($exerciseSlug, $katalog_id,$enrolment_id, $exerciseVersion, $hash, $exerciseMode, $tracking_info, $conn)
{
    if($enrolment_id < 1)
    {
        $enrolment_id = null;
    }

    try 
    {
        $stmt = mysqli_prepare($conn, "INSERT INTO m_cviceni_submission(exerciseSlug, katalog_id, enrolment_id, lms_hash, exerciseVersion, exerciseMode) values (?,?,?,?,?,?);");
        if ($stmt) {
            // bind parameters for markers 
            mysqli_stmt_bind_param($stmt, "ssssss", $exerciseSlug, $katalog_id, $enrolment_id, $hash, $exerciseVersion, $exerciseMode);
            if (mysqli_stmt_execute($stmt)) 
            {
                $last_id = mysqli_insert_id($conn);
                save_tracking_info($last_id, $tracking_info, $conn);
                return $last_id;
            }
            return false;
        }
        return false;
    } // catch (Exception $e) {
    //TODO logovat
    echo "TO SNAD NEEE";
    echo 'Caught exception: ', $e->getMessage(), "\n";
    return false;
    } finally {
        // close statement 
        mysqli_stmt_close($stmt);

    }
}


function create_first_record_withnout_hash_new($exerciseSlug, $katalog_id, $exerciseVersion, $tracking_info) {
    $conn = null;
    try {
        $conn = create_connection();
        return create_first_record_withnout_hash($exerciseSlug, $katalog_id, $exerciseVersion, $tracking_info, $conn);
    }
    finally {
        if ($conn) {
            mysqli_close($conn);
        }
    }
}

function create_first_record_withnout_hash($exerciseSlug, $katalog_id, $exerciseVersion, $tracking_info, $conn)
{    
    try 
    {
        $stmt = mysqli_prepare($conn, "INSERT INTO m_cviceni_submission(exerciseSlug, katalog_id, exerciseVersion) values (?,?,?);");
        if ($stmt) 
        {
            mysqli_stmt_bind_param($stmt, "sss", $exerciseSlug, $katalog_id, $exerciseVersion);
            if (mysqli_stmt_execute($stmt)) 
            {
                $last_id = mysqli_insert_id($conn);
                save_tracking_info($last_id, $tracking_info, $conn);
                return $last_id;
            }
            return false;
        }
        return false;
    } 
    finally 
    {
        mysqli_stmt_close($stmt);
    }
}

function trim_to_length($str, $maxlen)
{
    if (strlen($str) > $maxlen) 
    {
        return substr($str, 0, $maxlen);
    }

    return $str;
}

function save_tracking_info($entryId, $tracking_info, $conn) 
{ 
    try 
    {
        $stmt = mysqli_prepare($conn, 
            "INSERT INTO `m_tracking` (`entry_id`, `user_agent`, `referer`, `accept_language`, `accept_encoding`, `accept`, `query_string`) VALUES (?, ?, ?, ?, ?, ?, ?)");
        if ($stmt) 
        {
            mysqli_stmt_bind_param($stmt, "issssss", 
                $entryId, 
                trim_to_length($tracking_info["HTTP_USER_AGENT"], 500),
                trim_to_length($tracking_info["HTTP_REFERER"], 500),
                trim_to_length($tracking_info["HTTP_ACCEPT_LANGUAGE"], 20),
                trim_to_length($tracking_info["HTTP_ACCEPT_ENCODING"], 100),
                trim_to_length($tracking_info["HTTP_ACCEPT"], 500),
                trim_to_length($tracking_info["QUERY_STRING"], 500));
            if (mysqli_stmt_execute($stmt)) 
            {
                $last_id = mysqli_insert_id($conn);
                return $last_id;
            }
            return false;
        }
        return false;
    } 
    finally 
    {
        mysqli_stmt_close($stmt);
    }   
}

function chech_if_hash_is_correct_new($exerciseSlug, $hash){
    $conn = null;
    try {
        $conn = create_connection();
        return chech_if_hash_is_correct($exerciseSlug, $hash, $conn);
    }
    finally {
        if ($conn) {
            mysqli_close($conn);
        }
    }
}

//kontrola jestli je hash a exerciseSlug vpoĹ?ĂĄdku a zĂ­skĂĄnĂ­ exersiceMode
function chech_if_hash_is_correct($exerciseSlug, $hash, $conn)
{

    // echo $exerciseSlug." ";
    echo $hash;

    //$stmt = "SELECT lms_hash, type FROM m_katalog as k JOIN m_cviceni_enrolment as e ON (k.id = e.katalog_id) WHERE k.slug = 'zelezna-opona'";

    try {
        mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
        $stmt = mysqli_prepare($conn, "SELECT e.code, e.type,e.id FROM m_katalog as k JOIN m_cviceni_enrolment as e ON (k.id = e.katalog_id) WHERE k.slug = ?;");

        if ($stmt) {

            // bind parameters for markers 
            mysqli_stmt_bind_param($stmt, "s", $exerciseSlug);

            mysqli_stmt_execute($stmt);
            // bind result variables 
            mysqli_stmt_bind_result($stmt, $lms_hash, $exerciseMode,$enrolment_id);
            // fetch value 

            while (mysqli_stmt_fetch($stmt)) {
                $lms_hash_lower = strtolower($lms_hash);
                if ($lms_hash_lower == $hash) {
                    return array($lms_hash, $exerciseMode, $enrolment_id);
                    // return $exerciseMode;
                }
            }
            return null;
        }
    } //catch (Exception $e) {
    //TODO logovat
    echo "TO SNAD NEEE";
    echo 'Caught exception: ', $e->getMessage(), "\n";
    return null;
    }  finally {
        // close statement 
        // mysqli_close($conn);
        mysqli_stmt_close($stmt);
    }

}

function wh_log($log_msg)
{
    $log_filename = "log";
    if (!file_exists($log_filename)) {
        // create directory/folder uploads.
        mkdir($log_filename, 0777, true);
    }
    // .date('Y-m-d H:i:s')

    $log_file_data = $log_filename . '/log_' . date('d-M-Y') . '.log';
    //$log_file_data = $log_filename.'/log_' . "SOMETHING" . '.log';
    file_put_contents($log_file_data, $log_msg . "\n", FILE_APPEND);
    print_r($log_file_data . $log_msg);
}

function get_katalog_id_by_slug_new($slug){
    $conn = null;
    try {
        $conn = create_connection();
        return get_katalog_id_by_slug($slug, $conn);
    }
    finally {
        if ($conn) {
            mysqli_close($conn);
        }
    }
}

function get_katalog_id_by_slug($slug, $conn){
    try {
        $stmt = mysqli_prepare($conn, "SELECT id FROM m_katalog as k WHERE k.slug = ?;");

        if ($stmt) {

            // bind parameters for markers 
            mysqli_stmt_bind_param($stmt, "s", $slug);

            mysqli_stmt_execute($stmt);
            // bind result variables 
            mysqli_stmt_bind_result($stmt, $katalog_id);
            // fetch value 
            if (mysqli_stmt_fetch($stmt)) {
                
                return $katalog_id;

            }
            return false;
        }
    } finally {
        // close statement 
        mysqli_stmt_close($stmt);
    }


}

// Method: POST, PUT, GET etc
// Data: array("param" => "value") ==> index.php?param=value

function CallAPI($method, $url, $data = false)
{
    $curl = curl_init();

    switch ($method)
    {
        case "POST":            
            $data_string = json_encode($data);                                                                                   
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);                                                                  
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);                                                                      
            curl_setopt($curl, CURLOPT_HTTPHEADER, array(                                                                          
                'Content-Type: application/json',                                                                                
                'Content-Length: ' . strlen($data_string))                                                                       
            );                                                                                                                   
                                                                                                                                
            break;
        case "PUT":
            curl_setopt($curl, CURLOPT_PUT, 1);
            break;
        case "PUT":
            curl_setopt($curl, CURLOPT_HTTPGET, 1);
            break;
        default:
            if ($data)
                $url = sprintf("%s?%s", $url, http_build_query($data));
    }

    // Optional Authentication:
 //   curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
   // curl_setopt($curl, CURLOPT_USERPWD, "username:password");

    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

    $result = curl_exec($curl);

    curl_close($curl);

    return $result;
}
*/
