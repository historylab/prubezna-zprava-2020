const path = require("path");

module.exports = {
  getPathBySlug(slug) {
    return path.join("cviceni-aktualni", slug, "index.html");
  }
};
