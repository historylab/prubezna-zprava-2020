const _ = require("lodash");
const path = require("path");
const utils = require("../utils");
const fs = require("fs").promises;
const semver = require("semver");

const config = require("../../config").exerciseManager;

const fileExists = async path => !!(await fs.stat(path).catch(e => false));

module.exports = (app, auth, seq, wrap, logger) => {
  // http://.../cviceni/co-delaji-zeny-na-fotografii?lms_hash=16ED6E36#slide-form
  app.post(
    ["/save-to-database.php", "/cviceni"],
    wrap(async (req, res) => {
      const { Submission, sequelize } = seq();

      const exerciseData = req.body["odpoved"];
      const jsonArray = JSON.parse(exerciseData);

      const entryId = jsonArray["entryId"];
      const submission = await Submission.findOne({ where: { entryId } });
      if (submission === null) {
        res.status(404).send("Záznam nebyl nalezen.");
        return;
      }

      const exerciseId = jsonArray["exercise"]["id"];
      //const exerciseSlug = jsonArray["exercise"]["slug"];
      //const lmsHash = jsonArray["lmsHash"];
      const emailZakaNotFormated = jsonArray["user"]["form"]["email"];
      const emailZaka = (emailZakaNotFormated || "").toLowerCase();
      const emailGroup = jsonArray["user"]["form"]["group"]["members"];
      const analytics = jsonArray["user"]["analytics"];
      const time_spent = jsonArray["user"]["analytics"]["time"]["timeSpent"];
      const exerciseMode = jsonArray["exercise"]["mode"];
      const done = jsonArray["done"];

      await submission.update({
        exerciseData,
        analytics: JSON.stringify(analytics),
        time_spent,
        done,
        exerciseMode,
        exerciseId
      });

      res.status(200).send("x");

      // získání url cvičení
      //$urlcviceni = "http://historylab.westeurope.cloudapp.azure.com:8082/cviceni/prohlizeni/" . $exerciseSlug . "?entryid=" . $entryid;
      // if (!file_exists("html-include/ulozeno.html"))
    })
  );
};
