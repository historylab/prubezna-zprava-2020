const dbmysql = require("../dbmysql");
const auth = require("../auth");
const seq = require("../sequelize");
const { Op } = require("sequelize");
const _ = require("lodash");
const flash = require("../flash_res");

let wrap = fn => (...args) => fn(...args).catch(args[2]);

const validateSchema = require("../schemas/validator");

module.exports = app => {
  app.post(
    "/api/exercise/emailAddress",
    auth.ensureLoggedIn,
    validateSchema("addEmailAddress"),
    wrap(async (req, res) => {
      const data = req.body;
      const userId = req.user.id;
      const enrolmentId = data.enrolmentId;
      const { Enrolment } = seq();
      const enrolment = await Enrolment.findByIdForUser(userId, enrolmentId);
      if (enrolment === null) {
        res.status(404).send("Záznam nenalezen.");
        return;
      }

      if (enrolment.type == "class") {
        res.status(400).send("Nelze přidat emailovou adresu k tomuto záznamu.");
        return;
      }

      const { Group, GroupEmail } = seq();

      const group = await Group.findById(enrolment.group_id);
      if (group === null) {
        res.status(404).send("Třída nenalezena");
        return;
      }

      const emails = (await group.getEmails()).map(e =>
        e.emailAddress.toLowerCase()
      );

      const missingEmails = _.uniq(data.emailAddresses)
        .filter(e => !emails.includes(e))
        .map(e => {
          return { emailAddress: e, group_id: group.id };
        });

      if (missingEmails.length === 0) {
        req.flash("warning", "Emailové adresy už byly přidány v minulosti.");
      } else if (missingEmails.length === data.emailAddresses) {
        req.flash("success", "Emailové adresy byly přidány.");
      } else {
        req.flash("warning", "Některé emailové adresy byly přidány.");
      }

      await GroupEmail.bulkCreate(missingEmails);
      res.redirect(`/api/my/exercise/${enrolment.code}`);
    })
  );

  app.delete(
    "/api/exercise/emailAddress",
    auth.ensureLoggedIn,
    validateSchema("removeEmailAddress"),
    wrap(async (req, res) => {
      const data = req.body;
      const userId = req.user.id;
      const enrolmentId = data.enrolmentId;
      const { Enrolment } = seq();

      const enrolment = await Enrolment.findByIdForUser(userId, enrolmentId);
      if (enrolment === null) {
        res.status(404).send("Záznam nenalezen.");
        return;
      }

      if (enrolment.type == "class") {
        res
          .status(400)
          .send("Nelze odebrat emailovou adresu z tohoto záznamu.");
        return;
      }

      const { GroupEmail } = seq();
      const groupEmail = await GroupEmail.findOne({
        where: {
          group_id: enrolment.group_id,
          emailAddress: { [Op.like]: data.emailAddress }
        }
      });
      if (groupEmail === null) {
        res.status(404).send("Záznam nenalezen");
        return;
      }

      groupEmail.destroy();

      flash(res, {
        type: "success",
        text: "Emailová adresa byla odebrána."
      });
      res.redirect(303, `/api/my/exercise/${enrolment.code}`);
    })
  );

  app.post(
    "/api/exercise/enrol",
    auth.ensureLoggedIn,
    validateSchema("enrolToExercise"),
    wrap(async (req, res) => {
      const data = req.body;
      const userId = req.user.id;
      const { exerciseId, type } = data;
      const { CatalogExercise } = seq();
      const exercise = await CatalogExercise.findOne({ where: { exerciseId } });
      if (exercise === null) {
        res.status(404).send("Katalogové cvičení nenalezeno.");
        return;
      }

      const enrolment = await dbmysql.enrolToExercise(exerciseId, userId, type);

      const code = enrolment.code;
      console.info(
        `Enrolment successful for user ${userId} to exercise ${exerciseId} (type ${type}) => ${code} (${enrolment.id})`
      );

      res.json({ code });
    })
  );

  app.post(
    "/api/exercise/finished",
    validateSchema("finished"),
    wrap(async (req, res) => {
      const data = req.body;
      const { entryId, secret } = data;
      const { Submission, CatalogExercise } = seq();
      const submission = await Submission.findById(entryId);
      if (submission === null) {
        res.status(404).send();
        return;
      }

      if (secret !== global.gConfig.secret) {
        res.status(500).send();
        return;
      }

      const enrolment = await submission.getEnrolment();
      let catalog = null;
      if (enrolment) {
        catalog = await enrolment.getCatalogExercise();
      } else {
        catalog = await CatalogExercise.findByExerciseId(submission.exerciseId);
      }
      const emails = await submission.getEmails().map(e => e.emailAddress);

      const messageData = {
        from: global.gConfig.email.from,
        to: emails,
        template: "exerciseFinished",
        contentData: {
          submission: { viewUrl: submission.viewUrl },
          enrolment,
          catalog: { name: catalog.name }
        }
      };

      global.dispatcher.dispatch("email", messageData);

      res.json({});
    })
  );

  app.post(
    "/api/exercise/changeGroup",
    auth.ensureLoggedIn,
    validateSchema("changeGroup"),
    wrap(async (req, res) => {
      const data = req.body;
      const userId = req.user.id;
      const { enrolmentId, groupId } = data;
      const { Enrolment } = seq();
      const enrolment = await Enrolment.findByIdForUser(userId, enrolmentId);
      if (enrolment === null) {
        res.status(404).send("Záznam nenalezen.");
        return;
      }

      await enrolment.update({ group_id: groupId });

      flash(res, { type: "success", text: "Třída byla změněna." });
      res.redirect(`/api/my/exercise/${enrolment.code}`);
    })
  );
};
