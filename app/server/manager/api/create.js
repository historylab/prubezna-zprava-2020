const _ = require("lodash");
const path = require("path");
const utils = require("../utils");
const fs = require("fs");

const wwwRoot = process.env.WWWROOT;

module.exports = (app, auth, seq, wrap, logger) => {
  app.put(
    "/api/exercise/:slug",
    wrap(async (req, res) => {
      const { slug, hash, language, from } = req.params;
      const { Submission, sequelize } = seq();

      // list($lms_hash, $exerciseMode, $enrolment_id) = chech_if_hash_is_correct_new($exerciseSlug, $hash);
      // "SELECT e.code, e.type,e.id FROM m_katalog as k JOIN m_cviceni_enrolment as e ON (k.id = e.katalog_id) WHERE k.slug = ?
      const query = `SELECT e.code, e.type, e.id FROM m_katalog as k 
      JOIN m_cviceni_enrolment as e ON (k.id = e.katalog_id) 
      WHERE k.slug = :slug AND trim(lower(e.code)) = trim(lower(:lms_hash))`;

      const entry = await sequelize.query(query, {
        replacements: { slug: req.user.id, lms_hash: hash },
        type: sequelize.QueryTypes.SELECT
      });

      if (entry === null) {
        logger.warn(`Record not found for hash=${hash}.`);
        res.status(404);
        return;
      }

      const katalog_id = await sequelize.query(
        "SELECT id FROM m_katalog as k WHERE k.slug = :slug",
        {
          replacements: { slug },
          type: sequelize.QueryTypes.SELECT
        }
      );

      if (katalog_id === null) {
        logger.warn(`Catalog entry not for hash=${hash} and slug=${slug}.`);
        res.status(404);
        return;
      }

      const pathBySlug = utils.getPathBySlug(slug);

      const pathToFile = path.join(wwwRoot, pathBySlug);

      if (!fs.existsSync(pathToFile)) {
        res.status(404).send(`Soubor cvičení '${pathBySlug}' neexistuje.`);
        return;
      }

      fs.readFile(pathToFile, "utf8", async (err, data) => {
        if (err) {
          res.status(500).send(err);
          return;
        }

        version = data.match(/@data-version-global="([^"]+)"@/)[0];

        data = data.replace(
          /\.\.\//gm,
          "/cviceni-verze/v" + exerciseVersion[0] + "/"
        );

        // $entryid = create_first_record_with_hash_new($exerciseSlug, $katalog_id, $enrolment_id, $exerciseVersion, $hash, $exerciseMode, $tracking_info);
        // INSERT INTO m_cviceni_submission(exerciseSlug, katalog_id, enrolment_id, lms_hash, exerciseVersion, exerciseMode) values (?,?,?,?,?,?);")
        const submission = await Submission.create({
          exerciseSlug: slug,
          katalog_id,
          enrolmentId: -1,
          lms_hash: hash,
          exerciseVersion: version,
          exerciseMode: -1,
          language
        });

        // save tracking info
        const trackingInfo = {
          entry_id: submission.id,
          user_agent: req.headers["HTTP_USER_AGENT"] || '',
          referer: req.headers["HTTP_REFERER"] || '',
          accept_language: req.headers["HTTP_ACCEPT_LANGUAGE"] || '',
          accept_encoding: req.headers["HTTP_ACCEPT_ENCODING"] || '',
          accept: req.headers["HTTP_ACCEPT"] || '',
          query_string: req.headers["QUERY_STRING"] || ''
        };
        await sequelize.query(
          `INSERT INTO m_tracking (entry_id, user_agent, referer, accept_language, accept_encoding, accept, query_string) 
          VALUES (:entry_id, :user_agent, :referer, :accept_language, :accept_encoding, :accept, :query_string)`,
          {
            replacements: trackingInfo,
            type: sequelize.QueryTypes.INSERT
          }
        );

        res.redirect(`/exercise/view/${submission.id}`);
      });
    })
  );
};

/*
<?php

require_once "includes/functions.php";

//localhost:8181/cviceni/zelezna-opona?hash=27366CDA
if ($hash) {

    $dataOfCviceni = file_get_contents('cviceni-aktualni/' . $exerciseSlug .'/index.html');

    preg_match('@data-version-global="([^"]+)"@', $dataOfCviceni, $matchesReg);
    $exerciseVersion = $matchesReg[1];

    $entryid = create_first_record_with_hash_new($exerciseSlug, $katalog_id, $enrolment_id, $exerciseVersion, $hash, $exerciseMode, $tracking_info);

    if (!$entryid) {
        $log = "Date: ".date('Y-m-d H:i:s').PHP_EOL.
        "URL:".$qs." HASH: ".$hash.PHP_EOL. 
        "Attempt: "."Nelze vytvorit zaznam a entryid ".PHP_EOL.
        "Where: "."Soubor: zalozit-cviceni.php, Funkce: create_first_record_with_hash() vratila null".PHP_EOL.
        "-------------------------".PHP_EOL;

        wh_log($log);
        http_response_code(500); // nebo se podstrci soubor pro obsluhu chyby 500
        exit;
    }

    include 'zobrazit-cviceni.php';

} else {


    $exerciseMode = null;
    $hash = null;


    //Prubezne ukladani dat z jsonu do promenych

    if (!file_exists('cviceni-aktualni/' . $exerciseSlug)) {

        $log = "Date: ".date('Y-m-d H:i:s').PHP_EOL.
        "URL:".$qs." ". 
        "Attempt: "."Neexistuje soubor cviceni, slag v url neodpovida zadnemu cviceni na disku".PHP_EOL.
        "Where: "."Soubor: zalozit-cviceni.php, Funkce: file_exists() ".PHP_EOL.
        "-------------------------".PHP_EOL;

        wh_log($log);
        http_response_code(404); // nebo se podstrci soubor pro obsluhu chyby 404 (nenalezeno)
        exit;
    }

    $dataOfCviceni = file_get_contents('cviceni-aktualni/' . $exerciseSlug .'/index.html');


    preg_match('@data-version-global="([^"]+)"@', $dataOfCviceni, $matchesReg);
    $exerciseVersion = $matchesReg[1];

    $kat alog_id = get_katalog_id_by_slug_new($exerciseSlug);

    if(!$katalog_id) {
        $log = "Date: ".date('Y-m-d H:i:s').PHP_EOL.
        "URL:".$qs." SLUG: ".$exerciseSlug.PHP_EOL. 
        "Attempt: "."katalog_id se nenasel v databazi katalog_id: ".$katalog_id.PHP_EOL.
        "Where: "."Soubor: zalozit-cviceni.php, Funkce: get_katalog_id_by_slug() vratila null BEZ HASH".PHP_EOL.
        "-------------------------".PHP_EOL;

        wh_log($log);
        http_response_code(500); // nebo se podstrci soubor pro obsluhu chyby 500
        exit;
    }

    $entryid = create_first_record_withnout_hash_new($exerciseSlug, $katalog_id, $exerciseVersion, $tracking_info);
    if (!$entryid) {

        $log = "Date: ".date('Y-m-d H:i:s').PHP_EOL.
        "URL:".$qs." ". 
        "Attempt: "."Nelze vytvorit zaznam a entryid (zadano bez lms_hash parametru)".PHP_EOL.
        "Where: "."Soubor: zalozit-cviceni.php, Funkce: create_first_record_withnout_hash() vratila nullBEZ HASH".PHP_EOL.
        "-------------------------".PHP_EOL;

        wh_log($log);
        http_response_code(500); // nebo se podstrci soubor pro obsluhu chyby 500
        exit;
    }

    include 'zobrazit-cviceni.php';

}
*/
