const _ = require("lodash");
const path = require("path");
const utils = require("./../utils");
const fs = require("fs").promises;
const fileExists = async path => !!(await fs.stat(path).catch(e => false));
const config = require("../../config").exerciseManager;

module.exports = (app, auth, seq, wrap, logger) => {
  app.get(
    [
      "/exercise/view/:slug/:entryId([0-9]{0,10})?",
      "/cviceni/prohlizeni/:slug"
    ],
    wrap(async (req, res) => {
      const entryId =
        req.params.entryId || req.query.entryid || req.query.entryId;
      const pathSlug = req.params.slug;
      const { Submission } = seq();
      const submission = await Submission.findOne({
        attributes: [
          "exerciseData",
          "exerciseSlug",
          "exerciseVersion",
          "done",
          "emailAddress"
        ],
        where: { entryId }
      });
      if (submission === null) {
        res.status(404).send("Záznam nebyl nalezen.");
        return;
      }
      const {
        exerciseSlug,
        exerciseVersion,
        exerciseData,
        done,
        emailAddress
      } = submission;

      if (
        pathSlug &&
        exerciseSlug.trim().toLowerCase() !== pathSlug.trim().toLowerCase()
      ) {
        res
          .status(404)
          .send("Záznam s daným id a pro dané cvičení nebyl nalezen.");
        return;
      }

      const pathByVersion = utils.getPathByVersion(
        exerciseSlug,
        exerciseVersion
      );

      const pathToFile = path.join(config.wwwRoot, pathByVersion);

      const exists = await fileExists(pathToFile);
      if (!exists) {
        logger.warn(`Exercise file ${pathToFile} not found.`);
        res.status(404).send(`Soubor cvičení '${pathByVersion}' neexistuje.`);
        return;
      }

      let data = await fs.readFile(pathToFile, "utf8");

      data = data.replace(
        /\.\.\//gm,
        "/cviceni-verze/v" + exerciseVersion[0] + "/"
      );

      data = data.replace(
        "import: {},",
        `import: {
          user: "${(emailAddress || "").replace('"', "")}",
          exerciseData: ${exerciseData},
          done: ${done}
         },`
      );

      res.send(data);
    })
  );
};
