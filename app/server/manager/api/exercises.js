const _ = require("lodash");
const axios = require("axios");
//const validateSchema = require("./schemas/validator");
const config = require("../../config").exerciseManager;

module.exports = (app, auth, seq, wrap, logger) => {
  app.get(
    "/api/exercise/view/:entryId([0-9]{0,10})?",
    wrap(async (req, res) => {
      res.json(config);
    })
  );
};

/*
<?php


if ($hash == null) {
    $arrayReplace = array("../", "import: {},");
    $arrayWith = array("/cviceni-aktualni/", "import: {
          entryid: ".$entryid.",
          createdAt: '',
          updatedAt: '',
          lmsHash: '',
          exerciseData: '',
          done: false,
        },");

   // $arrayWith = array("/cviceni-aktualni/", "var fromPhpManager = '{ \"entryid\": \"$entryid\", \"exerciseSlug\": \"$exerciseSlug\" }'");
} else {
    $arrayReplace = array("../", "import: {},");
    $arrayWith = array("/cviceni-aktualni/", "import: {
          entryid: $entryid,
          createdAt: '',
          updatedAt: '',
          lmsHash: '$hash',
          exerciseMode: $exerciseMode,
          exerciseData: '',
          done: false,
        },");
 //   $arrayWith = array("/cviceni-aktualni/", "var fromPhpManager = '{ \"entryid\": \"$entryid\", \"hash\": \"$hash\", \"exerciseSlug\": \"$exerciseSlug\", \"mode\": \"$exerciseMode\" }'");
}

if (!file_exists('cviceni-aktualni/' . $exerciseSlug . '/index.html')) {

    $log = "Date: ".date('Y-m-d H:i:s').PHP_EOL.
        "URL:".$qs." ". 
        "Attempt: "."Neexistuje soubor cviceni".PHP_EOL.
        "Where: "."Soubor: zobrazit-cviceni.php, Funkce: file_exists() vratila null".PHP_EOL.
        "-------------------------".PHP_EOL;

        wh_log($log);
    http_response_code(404); // nebo se podstrci soubor pro obsluhu chyby 404 (nenalezeno)
    exit;
}

//zmena kontextu
$cviceni_html = file_get_contents('cviceni-aktualni/' . $exerciseSlug . '/index.html');
$cviceni_html_parametres = str_replace($arrayReplace, $arrayWith, $cviceni_html);

//zobrazeni cviceni
echo $cviceni_html_parametres;
*/