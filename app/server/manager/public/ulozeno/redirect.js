function countdown(seconds, url) {
  seconds = seconds - 1;
  if (seconds < 0) {
    // Chnage your redirection link here
    window.location = url;
  } else {
    // Update remaining seconds
    document.getElementById("countdown").innerHTML = seconds;
    // Count down using javascript
    window.setTimeout(function () {
      countdown(seconds, url);
    }, 1000);
  }
}
