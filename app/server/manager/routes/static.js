const config = require("./../../config");

module.exports = app => {
  app.get("/lms/katalog", (req, res) => {
    const url = config.application.historyLabUrl + "/katalog";
    res.redirect(url);
  });
};
