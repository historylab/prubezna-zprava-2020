const _ = require("lodash");
const config = require("../../config");

module.exports = (app, auth, seq, wrap, logger) => {
  app.get(
    "/cviceni/dokonceno/:entryId",
    wrap(async (req, res) => {
      const { Submission } = seq();
      const entryId = req.params.entryId;
      const submission = await Submission.findOne({ where: { entryId } });
      if (submission === null) {
        res.status(404);
        return;
      }

      const emailAddresses = await submission.getEmails();

      const template = "saved".concat(
        submission.redirect_url ? "_redirect" : "",
        submission.isEnglish ? "_en" : ""
      );

      res.render(template, {
        viewUrl: submission.viewUrl,
        redirect_url: submission.redirect_url,
        emailAddresses: _.uniqBy(
          emailAddresses.map(e => e.emailAddress),
          e => e
        )
      });
    })
  );

  app.post(
    ["/save-to-database.php", "/cviceni"],
    wrap(async (req, res) => {
      const { Submission, SubmissionEmail } = seq();

      const exerciseData = req.body["odpoved"];
      const jsonArray = JSON.parse(exerciseData);

      const entryId = jsonArray["entryId"];
      const submission = await Submission.findOne({
        attributes: { exclude: ["exerciseData"] },
        where: { entryId }
      });
      if (submission === null) {
        res.status(404);
        return;
      }

      const exerciseId = jsonArray["exercise"]["id"];
      const emailZakaNotFormated = jsonArray["user"]["form"]["email"];
      const jmenoZaka = jsonArray["user"]["form"]["name"];
      const groupMembers = jsonArray["user"]["form"]["group"]["members"];
      let teacherEmailAddress = (
        jsonArray["user"]["form"]["teacher"] || ""
      ).toLowerCase();

      const people = [
        {
          emailAddress: emailZakaNotFormated,
          name: jmenoZaka,
          entryId: submission.id
        }
      ].concat(
        groupMembers.map(m => {
          return {
            emailAddress: m,
            name: null,
            entryId: submission.id
          };
        })
      );

      const trimTo = (s, length) => {
        if (s) {
          return s.length <= length ? s : s.substring(0, length);
        }
        return s;
      };

      teacherEmailAddress = trimTo(teacherEmailAddress, 255);

      const analytics = jsonArray["user"]["analytics"];
      const timeSpent = jsonArray["user"]["analytics"]["time"]["timeSpent"];
      const exerciseMode = jsonArray["exercise"]["mode"];
      const done = (jsonArray["done"] || "").toString().trim().toLowerCase();

      const emailRegexp = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

      const submissionEmails = people
        .filter(p => emailRegexp.test(p.emailAddress))
        .map(p => {
          return {
            entryId: p.entryId,
            name: trimTo(p.name, 100),
            emailAddress: trimTo(p.emailAddress.toLowerCase(), 100)
          };
        });

      await SubmissionEmail.bulkCreate(submissionEmails);

      await submission.update({
        emailAddress: emailZakaNotFormated,
        exerciseData,
        analytics: JSON.stringify(analytics),
        teacherEmailAddress,
        timeSpent,
        done: done === "1" || done === "true" || done === true,
        exerciseMode,
        exerciseId
      });

      if (emailRegexp.test(teacherEmailAddress)) {
        await notify(
          submission,
          [{ emailAddress: teacherEmailAddress }],
          "exerciseFinishedTeacher"
        );
      }

      await notify(submission, submissionEmails, "exerciseFinished");

      res.redirect("/cviceni/dokonceno/" + submission.id);
    })
  );

  notify = async (submission, emails, template) => {
    const { CatalogExercise } = seq();
    if (submission === null) {
      return;
    }

    const enrolment = await submission.getEnrolment();
    let catalog = await CatalogExercise.findOne({
      where: { exerciseId: submission.exerciseId }
    });

    if (catalog === null) {
      catalog = await CatalogExercise.findById(submission.exerciseId);
    }

    if (catalog === null) {
      catalog = { name: "N/A " + submission.exerciseId };
    }

    emails = emails.map(e => e.emailAddress);

    const messageData = {
      from: config.email.from,
      to: emails,
      template,
      language: submission.language,
      contentData: {
        submission: { viewUrl: submission.viewUrl },
        catalog: { name: catalog.name },
        enrolment
      }
    };

    global.dispatcher.dispatch("email", messageData);
  };
};
