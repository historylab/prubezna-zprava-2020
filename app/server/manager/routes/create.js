const _ = require("lodash");
const path = require("path");
const utils = require("../utils");
const fs = require("fs").promises;
const validator = require("validator");
const semver = require("semver");
const wwwRoot = process.env.WWWROOT;
const fileExists = async path => !!(await fs.stat(path).catch(e => false));

module.exports = (app, auth, seq, wrap, logger) => {
  // http://.../cviceni/co-delaji-zeny-na-fotografii?lms_hash=16ED6E36#slide-form
  app.get(
    "/cviceni/:slug",
    wrap(async (req, res) => {
      const { slug } = req.params;
      const { lms_hash, email, language, exid, from, redirect } = req.query;
      const { Submission, Exhibition, sequelize } = seq();

      let query = `SELECT e.code, e.type as exerciseMode, k.exerciseId, u.email, e.id as enrolmentId FROM m_cviceni_enrolment e
      left JOIN m_katalog as k ON (k.exerciseId = e.katalog_id or k.id = e.katalog_id) 
      left join m_user u on u.id = e.user_id
      WHERE trim(lower(k.slug)) = trim(lower(:slug)) AND trim(lower(e.code)) = trim(lower(:lms_hash))`;

      if (!lms_hash) {
        query =
          "SELECT NULL AS code, -1 AS exerciseMode, exerciseId, null as email, -1 as enrolmentId FROM m_katalog WHERE slug=:slug AND enabled=1";
      }

      // validate exhibition id
      const exhibitionId = await Exhibition.verifyCode(exid);

      let redirect_url = (redirect || "").trim();
      if (!validator.isURL(redirect_url, { protocols: ["http", "https"] })) {
        redirect_url = null;
      }

      const entry = await sequelize.query(query, {
        replacements: { slug, lms_hash: lms_hash },
        type: sequelize.QueryTypes.SELECT,
        raw: true,
        plain: true
      });

      if (entry === null) {
        let message = lms_hash
          ? `Pro tento kód cvičení ${lms_hash.toUpperCase()} a název '${slug}' nebylo nic nalezeno. Máte správný kód?`
          : `Cvičení '${slug}' nebylo nalezeno.`;
        logger.warn(message);
        res.status(404).send(message);
        return;
      }

      const pathBySlug = utils.getPathBySlug(slug);

      const pathToFile = path.join(wwwRoot, pathBySlug);

      const exists = await fileExists(pathToFile);
      if (!exists) {
        logger.warn(`Exercise file ${pathToFile} not found.`);
        res.status(404).send(`Soubor cvičení '${pathBySlug}' neexistuje.`);
        return;
      }

      let data = await fs.readFile(pathToFile, "utf8");

      if (!data.match(/import: {},/)) {
        res
          .status(500)
          .send(
            "Fatal error: 'import: {},' does not exist in file. Nothing to replace!"
          );
        return;
      }

      const versionFromFile = data.match(/data-version-global="([^"]+)"/)[1];
      const version = semver.valid(versionFromFile);

      if (version === null) {
        logger.warn(
          `Exercise file ${pathToFile} does not contain correct version info '${versionFromFile}'.`
        );
        res
          .status(404)
          .send(
            `Nepodařilo se zjistit verzi ze soubor se cvičením '${pathBySlug}' - '${versionFromFile}'.`
          );
        return;
      }

      data = data.replace(
        /\.\.\//gm,
        `/cviceni-verze/v${semver.major(version)}-${semver.minor(version)}/`
      );

      const submission = await Submission.create({
        exerciseSlug: slug,
        katalog_id: entry.exerciseId,
        exerciseId: entry.exerciseId,
        enrolmentId: entry.enrolmentId,
        exhibition_id: exhibitionId,
        redirect_url: redirect_url,
        done: false,
        lms_hash,
        exerciseVersion: version,
        exerciseMode: entry.exerciseMode,
        emailAddress: email,
        language
      });

      // save tracking info
      await sequelize.query(
        `INSERT INTO m_tracking (entry_id, user_agent, referer, accept_language, accept_encoding, accept, query_string) 
          VALUES (:entry_id, :user_agent, :referer, :accept_language, :accept_encoding, :accept, :query_string)`,
        {
          replacements: {
            entry_id: submission.id,
            user_agent: req.headers["user-agent"] || "",
            referer: req.headers["referer"] || "",
            accept_language: req.headers["accept-language"] || "",
            accept_encoding: req.headers["accept-encoding"] || "",
            accept: req.headers["accept"] || "",
            query_string: req.url
          },
          type: sequelize.QueryTypes.INSERT
        }
      );

      data = data.replace(
        "import: {},",
        `import: {
          entryid: ${submission.id},
          createdAt: '',
          updatedAt: '',
          user: "${(email || "").replace('"', "")}",
          teacher: '${entry.email || ""}',
          lmsHash: '${lms_hash || ""}',
          done: false,
          exerciseMode: ${entry.exerciseMode},
          exerciseData: ''
         },`
      );

      data = data.replace(/\/save-to-database\.php/gm, "/cviceni");

      res.send(data);
    })
  );
};
