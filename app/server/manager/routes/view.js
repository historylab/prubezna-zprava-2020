const _ = require("lodash");
const path = require("path");
const semver = require("semver");
const fs = require("fs").promises;
const fileExists = async path => !!(await fs.stat(path).catch(e => false));

const wwwRoot = process.env.WWWROOT;

module.exports = (app, auth, seq, wrap, logger) => {
  app.get(
    "/cviceni/prohlizeni/:slug",
    wrap(async (req, res) => {
      const entryId = req.query.entryid || req.query.entryId;
      const slug = req.params.slug;
      req.url = `/exercise/view/${slug}/${entryId}`;
      app.handle(req, res);
    })
  );

  app.get(
    "/cviceni/prohlizeni",
    wrap(async (req, res) => {
      const entryId = req.query.entryid || req.query.entryId;
      const slug = (req.query.slug || "n-a").replace(/-\d+$/, "");
      req.url = `/exercise/view/${slug}/${entryId}`;
      app.handle(req, res);
    })
  );

  app.get(
    "/exercise/view/:slug/:entryId([0-9]{0,10})?",
    wrap(async (req, res) => {
      const entryId = req.params.entryId;
      const pathSlug = req.params.slug;
      const { Submission } = seq();
      const submission = await Submission.findOne({
        where: { entryId }
      });
      if (submission === null) {
        res.status(404).send("Záznam nebyl nalezen.");
        return;
      }
      const { exerciseSlug, exerciseData, emailAddress, isDone } = submission;

      if (
        pathSlug &&
        exerciseSlug.trim().toLowerCase() !== pathSlug.trim().toLowerCase()
      ) {
        res
          .status(404)
          .send("Záznam s daným id a pro dané cvičení nebyl nalezen.");
        return;
      }

      const exerciseVersion = semver.parse(submission.exerciseVersion);
      const versions = [
        exerciseVersion.raw,
        `${exerciseVersion.major}.${exerciseVersion.minor}`,
        `${exerciseVersion.major}`
      ];

      const getPathsByVersion = cviceni => {
        return versions
          .map(version => version.replace(".", "-"))
          .map(version => {
            return {
              version: "v" + version,
              path: path.join(
                "cviceni-verze",
                "v" + version,
                cviceni,
                "index.html"
              )
            };
          });
      };

      const pathPromises = getPathsByVersion(exerciseSlug)
        .map(f => {
          return { ...f, fullPath: path.join(wwwRoot, f.path) };
        })
        .map(f => {
          return fileExists(f.fullPath).then(exists => {
            return { ...f, exists };
          });
        });

      const paths = await Promise.all(pathPromises);

      const existing = paths.filter(f => f.exists);

      if (existing.length == 0) {
        const p = paths.map(f => f.path);
        logger.warn(`Exercise files ${p} not found.`);
        res.status(404).send(`Ani jeden ze souborů cvičení '${p}' neexistuje.`);
        return;
      }

      const pathToFile = existing[0].fullPath;
      let data = await fs.readFile(pathToFile, "utf8");

      data = data.replace(
        /\.\.\//gm,
        "/cviceni-verze/" + existing[0].version + "/"
      );

      data = data.replace(
        "import: {},",
        `import: {
          user: "${(emailAddress || "").replace('"', "")}",
          exerciseData: ${exerciseData},
          done: ${isDone}
         },`
      );

      res.send(data);
    })
  );
};
