const session = require("express-session");

module.exports = (app, logger) => {
  console.info("XXXXXXXXXXXXX____________");
  if (process.env.NODE_ENV !== "testing") {
    return;
  }

  console.info("XXXXXXXXXXXXX____________xxxx");
  app.use(
    session({
      secret: "secret!",
      cookie: {}
    })
  );
};
