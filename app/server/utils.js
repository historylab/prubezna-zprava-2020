String.prototype.format = function() {
  return [...arguments].reduce((p, c) => p.replace(/%s/, c), this);
};

function formatTime(seconds) {
  if (seconds <= 0) {
    return "";
  }
  const h = Math.floor(seconds / 3600);
  const m = Math.floor((seconds % 3600) / 60);
  const s = Math.round(seconds % 60);
  return [h, m > 9 ? m : h ? "0" + m : m || "0", s > 9 ? s : "0" + s]
    .filter(a => a)
    .join(":");
}

module.exports = { formatTime };
