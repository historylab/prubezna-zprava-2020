const express = require("express");
const logger = require("./../logger")("queue");

global.dispatcher = require("./dispatcher");
global.dispatcher.createWorkerQueues();

const app = express();

app.set("port", process.env.PORT || 80);
app.listen(app.get("port"), () => {
  logger.info("App listening on port " + app.get("port"));
});
