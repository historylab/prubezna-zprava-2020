const path = require("path");
const CachePugTemplates = require("cache-pug-templates");
const redis = require("redis");
const config = require("./../../config");
const logger = require("./../../logger")("templateCache");

const { port, host } = config.queues.redis;
const redisClient = redis.createClient(port, host);
const templates = path.join(__dirname, "templates");
const cache = new CachePugTemplates(redisClient, templates);
cache.start();

logger.info("Templates were cached.");
