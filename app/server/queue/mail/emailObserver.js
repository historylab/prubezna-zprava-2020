﻿const logger = require("./../../logger")("mailer");
const seq = require("../../sequelize");

module.exports = {
    storeRecord: (successful, data) => {
        const { EmailObserver } = seq();
        EmailObserver.create({ successful: successful, email_data_to: JSON.stringify(data.to) })
        logger.info("Store record to database... " + successful + " " + JSON.stringify(data.to));
    }
};
