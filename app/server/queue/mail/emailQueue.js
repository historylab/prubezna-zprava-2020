const Queue = require("bee-queue");
const logger = require("../../logger")("emailQueue");

function createWorkerQueue(host, port) {
  const mailer = require("./mailer");
  const emailObserver = require("./emailObserver")
  require("./cache");

  const workerQueue = new Queue("email", {
    redis: {
      host,
      port
    },
    isWorker: true,
    activateDelayedJobs: true
  });

  workerQueue.on("ready", () => {
    logger.info("Queue is ready ... waiting for emails to be sent!");
  });

  workerQueue.on("error", err => {
    logger.info(`A queue error happened in email queue: ${err.message}`);
  });

  workerQueue.on("succeeded", (job, result) => {
    logger.info(
      `Job ${job.id} succeeded with result: ${JSON.stringify(result)}`
    );
    emailObserver.storeRecord(1,result);
  });

  workerQueue.on("retrying", (job, err) => {
    logger.error(
      `Job ${job.id} failed with error ${err.message} but will be retried!`
    );
  });

  workerQueue.on("failed", (job, err) => {
    logger.info(`Job ${job.id} failed with error ${err.message}`);
    emailObserver.storeRecord(0, err.message);
  });

  workerQueue.process(function (job, done) {
    logger.info(
      `Email queue is processing job ${job.id} (${job.options.retries})`
    );
    mailer.send(job.data, (error, data) => {
      setTimeout(() => {
        done(error, data);
      }, 2000);
    });
  });

  return workerQueue;
}

function createClientQueue(host, port) {
  return new Queue("email", {
    redis: {
      host,
      port,
      retry_strategy: options => {
        let code = "";
        if (options.error) {
          code = options.error.code;
        } else {
          code = "N/A";
        }
        logger.error(options);
        logger.error("Queue error: " + code);
        if (options.error.code === "ECONNREFUSED") {
          // End reconnecting on a specific error and flush all commands with a individual error
          return new Error("The server refused the connection");
        }
        if (options.total_retry_time > 1000 * 60 * 60) {
          // End reconnecting after a specific timeout and flush all commands with a individual error
          return new Error("Retry time exhausted");
        }
        if (options.times_connected > 10) {
          // End reconnecting with built in error
          return undefined;
        }
        // reconnect after
        return Math.max(options.attempt * 100, 3000);
      }
    },
    isWorker: false
  });
}

module.exports = ({ host, port }) => {
  const clientQueue = createClientQueue(host, port);
  return {
    createWorkerQueue: () => createWorkerQueue(host, port),
    dispatch: data => {
      clientQueue.createJob(data).retries(2).backoff("fixed", 10000).save();
    }
  };
};
