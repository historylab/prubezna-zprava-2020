const nodemailer = require("nodemailer");
const config = require("./../../config");
const Email = require("email-templates");
const smptConfig = config.email.smtp;
const logger = require("./../../logger")("mailer");

const transportConfig = {
  host: smptConfig.host,
  port: smptConfig.port,
  secure: smptConfig.secure,
  ignoreTLS: smptConfig.ignoreTLS,
  auth: {
    user: smptConfig.auth.username,
    pass: smptConfig.auth.password
  },
  pool: true
};

if ((smptConfig.auth.username || "").match(/^\s*$/)) {
  delete transportConfig.auth;
}

const languageMap = {
  cs: "",
  "cs-cz": "",
  en: "en",
  "en-us": "en",
  "": ""
};

module.exports = {
  send: (data, cb) => {
    logger.info("Sending mail ... " + JSON.stringify(data));

    let transporter = nodemailer.createTransport(transportConfig);

    const email = new Email({
      message: {
        from: smptConfig.from, // sender address
        to: data.to, // list of receivers
        cc: data.cc,
        bcc: data.bcc
        //subject: data.subject // Subject line
      },
      send: true, //!dev,
      preview: !config.isProduction,
      transport: transporter,
      views: {
        root: config.email.templatesDir
      }
    });

    let template = data.template;
    let lang = languageMap[(data.language || "").toLowerCase()] || "";
    if (lang.length > 0) {
      template += "." + lang;
    }

    email
      .send({
        template,
        locals: {
          ...data.contentData,
          __all__: JSON.stringify(data.contentData)
        }
      })
      .then(info => {
        info.template = template;
        logger.info(
          `Message sent ${info.messageId}, template used '${template}.`
        );
        const testMessageUrl = nodemailer.getTestMessageUrl(info);
        if (smptConfig.host.match(/\.ethereal\./) && testMessageUrl) {
          logger.info(`Ethereal preview URL: ${testMessageUrl}`);
        }
        global.dispatcher.dispatch("email.sent", info);
        cb(null, data);
      })
      .catch(error => {
        logger.error(
          `Message not sent, template used '${template}, templates directory ${
            config.email.templatesDir
          }: ${error} ${JSON.stringify(error)}`
        );
        cb(error, data);
      });
  }
};
