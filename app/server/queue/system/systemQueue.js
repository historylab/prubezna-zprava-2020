const redis = require("redis");
const Queue = require("bee-queue");
const logger = require("../../logger")("system");

function createWorkerQueue(host, port) {
  const workerQueue = new Queue("system", {
    redis: {
      host,
      port
    },
    isWorker: true
  });

  workerQueue.on("ready", () => {
    logger.info("System queue is ready ...");
  });

  workerQueue.on("error", err => {
    logger.info(`A queue error happened in email queue: ${err.message}`);
  });

  workerQueue.on("succeeded", (job, result) => {
    logger.info(
      `Job ${job.id} succeeded with result: ${JSON.stringify(result)}`
    );
  });

  workerQueue.on("retrying", (job, err) => {
    logger.error(
      `Job ${job.id} failed with error ${err.message} but is being retried!`
    );
  });

  workerQueue.on("failed", (job, err) => {
    logger.info(`Job ${job.id} failed with error ${err.message}`);
  });

  workerQueue.process(function(job, done) {
    const topic = job.data.topic;
    logger.info(`System queue is processing job ${job.id}`);
    logger.info(`Topic ${topic}`);

    switch (topic) {
      case "user.loggedIn":
        const pattern = `*idUser=${job.data.user.id};*`;
        const client = redis.createClient(port, host);
        client.keys(pattern, (error, keys) => {
          if (error) {
            done(error, job.data);
            return;
          }
          client.del(keys, (error, res) => {
            done(error, job.data);
          });
        });
        return;
    }
  });

  return workerQueue;
}

function createClientQueue(host, port) {
  return new Queue("system", {
    redis: {
      host,
      port,
      retry_strategy: options => {
        logger.error("System queue error: " + options.error.code);
        if (options.error.code === "ECONNREFUSED") {
          // End reconnecting on a specific error and flush all commands with a individual error
          return new Error("The server refused the connection");
        }
        if (options.total_retry_time > 1000 * 60 * 60) {
          // End reconnecting after a specific timeout and flush all commands with a individual error
          return new Error("Retry time exhausted");
        }
        if (options.times_connected > 10) {
          // End reconnecting with built in error
          return undefined;
        }
        // reconnect after
        return Math.max(options.attempt * 100, 3000);
      }
    },
    isWorker: false
  });
}

module.exports = ({ host, port }) => {
  const clientQueue = createClientQueue(host, port);
  return {
    createWorkerQueue: () => createWorkerQueue(host, port),
    dispatch: data => clientQueue.createJob(data).save()
  };
};
