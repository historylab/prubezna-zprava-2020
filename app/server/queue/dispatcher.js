const config = require("./../config");
const emailDispatch = require("./mail/emailQueue")(config.queues.redis);
const systemDispatch = require("./system/systemQueue")(config.queues.redis);
const logger = require("./../logger")("dispatcher");

const queues = {
  email: emailDispatch,
  system: systemDispatch
};

module.exports = {
  dispatch: (queueName, data) => {
    logger.info(`Dispatching data to queue '${queueName}' ...`);
    const queue = queues[queueName];
    if (queue) {
      queue.dispatch(data);
      return;
    }

    logger.warn(`Unknown queue name '${queueName}'`);
  },
  createWorkerQueues: () => {
    emailDispatch.createWorkerQueue();
    systemDispatch.createWorkerQueue();
  }
};
