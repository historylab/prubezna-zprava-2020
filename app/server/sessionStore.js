const cookieParser = require("cookie-parser");
const session = require("express-session");
const config = require("./config");
const redis = require("redis");
const redisStore = require("connect-redis")(session);

module.exports = app => {
  const { port, host } = config.queues.redis;
  const redisClient = redis.createClient(port, host);

  redisClient.on("error", err => {
    console.log("Redis error: ", err);
  });

  app.use(cookieParser());

  app.use(
    session({
      secret: process.env.APP_SECRET,
      resave: false, // we support the touch method so per the express-session docs this should be set to false
      proxy: true, // if you do SSL outside of node.
      saveUninitialized: false,
      cookie: { secure: false, maxAge: config.maxAge * 1000 }, // Note that the cookie-parser module is no longer needed
      store: new redisStore({
        host,
        port,
        client: redisClient,
        ttl: config.maxAge
      })
    })
  );
};
