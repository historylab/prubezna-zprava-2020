const passport = require("passport");
const { Strategy } = require("passport-local");
const GoogleStrategy = require("passport-google-oauth20").Strategy;
const cache = require("./cache");
const db = require("./db");

// Configure the local strategy for use by Passport.
//
// The local strategy require a `verify` function which receives the credentials
// (`username` and `password`) submitted by the user.  The function must verify
// that the password is correct and then invoke `cb` with a user object, which
// will be set at `req.user` in route handlers after authentication.
passport.use(
  new Strategy((userName, password, cb) => {
    db.authenticate(userName, password, cb);
  })
);

passport.use(
  new GoogleStrategy(
    {
      clientID: process.env.REACT_APP_GOOGLE_CONSUMER_KEY,
      consumerSecret: process.env.GOOGLE_CONSUMER_SECRET
      //callbackURL: "http://localhost:3000/api/auth/google/callback"
    },
    (token, tokenSecret, profile, done) => {
      User.findOrCreate({ googleId: profile.id }, function (err, user) {
        return done(err, user);
      });
    }
  )
);

// Configure Passport authenticated session persistence.
//
// In order to restore authentication state across HTTP requests, Passport needs
// to serialize users into and deserialize users out of the session.  The
// typical implementation of this is as simple as supplying the user ID when
// serializing, and querying the user record by ID from the database when
// deserializing.
passport.serializeUser((user, cb) => {
  cb(null, user.id);
});

passport.deserializeUser((id, cb) => {
  const key = "user" + id;
  const cachedUser = cache.get(key);
  if (cachedUser) {
    cb(null, cachedUser);
    return;
  }

  db.findById(id, (err, user) => {
    if (err) {
      cb(err);
      return;
    }
    cache.set(key, user, 5 * 60);
    cb(null, user);
  });
});

function ensureLoggedIn(req, res, next) {
  if (!req.isAuthenticated || !req.isAuthenticated()) {
    res.status(401);
    res.send("Authorization required.");
  } else {
    next();
  }
}

function checkUserAccessRights(requestedRoles, req, res, next) {
  if (!req.isAuthenticated || !req.isAuthenticated()) {
    res.status(401);
    res.send("Authorization required.");
    return;
  }

  const roles = req.user.roles;
  const result = requestedRoles.reduce((accumulator, requestedRole) => {
    accumulator.push(
      roles.find(e => e.toLowerCase() === requestedRole.toLowerCase())
    );
    return accumulator;
  }, []);
  const isInRole = result.reduce((acc, curr) => {
    return acc || curr != null;
  }, false);
  if (!isInRole) {
    res.status(401);
    res.send("Authorization & role required.");
    return;
  }
  next();
}

module.exports = {
  ensureLoggedIn,
  checkUserAccessRights
};
