const express = require("express");
const path = require("path");

global.dispatcher = require("./queue/dispatcher");

module.exports = (app, logger) => {
  if (process.env.NODE_ENV !== "production") {
    return;
  }

  logger.info("Environment: production ...");
  app.use(
    "/static",
    express.static(path.join(__dirname, "client", "build", "static"), {
      etag: true
    })
  );
  app.use(
    "/locales",
    express.static(path.join(__dirname, "client", "build", "locales"), {
      etag: true
    })
  );
  app.use(
    "/favicon",
    express.static(path.join(__dirname, "client", "build", "favicon"), {
      etag: true
    })
  );
  // All url paths go to the bundled index.html
  app.get("/*", (req, res) => {
    res.header("Cache-Control", "private, no-cache, no-store, must-revalidate");
    res.header("Expires", "-1");
    res.header("Pragma", "no-cache");
    res.sendFile(path.join(__dirname, "client", "build", "index.html"));
  });
};
