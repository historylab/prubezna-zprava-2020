var i18next = require("i18next");
var middleware = require("i18next-express-middleware");
var Backend = require("i18next-node-fs-backend");

module.exports = app => {
  const i18nextOptions = {
    backend: {
      loadPath: __dirname + "/locales/{{lng}}/{{ns}}.json",
      addPath: __dirname + "/locales/{{lng}}/{{ns}}.missing.json"
    },
    // order and from where user language should be detected
    order: [/*'path', 'session', */ "querystring", "cookie", "header"],

    // keys or params to lookup language from
    lookupQuerystring: "lang",
    lookupCookie: "i18next",
    lookupHeader: "accept-language",
    lookupSession: "lang",
    lookupPath: "lang",
    lookupFromPathIndex: 0,

    preload: ["cs", "en"],
    fallbackLng: "cs",
    // cache user language
    caches: false, // ['cookie']
    debug: false, //process.env.NODE_ENV !== "production",
    saveMissing: true,

    // optional expire and domain for set cookie
    cookieExpirationDate: new Date(),
    cookieDomain: "myDomain",
    cookieSecure: true // if need secure cookie
  };

  i18next.use(Backend).use(middleware.LanguageDetector).init(i18nextOptions);

  //app.use(i18nextMiddleware.handle(i18next));
  app.use(
    middleware.handle(i18next, {
      ignoreRoutes: (req, res, options, i18next) => {
        return !req.url.match(/\/api\//);
      }
    })
  );
};
