"use strict";

const redis = require("redis");
const { port, host } = require("../config").queues.redis;
const redisClient = redis.createClient(port, host);

let redisCacheMiddleware = (req, res, next) => {
  let key = req.cacheKey || "__expIress__" + (req.originalUrl || req.url);
  redisClient.get(key, function(err, reply) {
    if (reply) {
      res.header("content-type", "application/json; charset=utf-8");
      res.send(JSON.parse(reply));
    } else {
      res.sendResponse = res.send;
      res.send = body => {
        if (res.statusCode === 200) {
          redisClient.set(key, JSON.stringify(body));
        }
        res.sendResponse(body);
      };

      next();
    }
  });
};

module.exports = redisCacheMiddleware;
