/*const express = require("express");
const passport = require("passport");
const path = require("path");
const { createProxyMiddleware } = require("http-proxy-middleware");
const bodyParser = require("body-parser");
const helmet = require("helmet");*/
const logger = require("./logger")("server");
/*const cors = require("cors");
const requestIp = require("request-ip");

const app = express();

var port = process.env.PORT || 80;
app.set("port", port);

const config = require("./config");

app.set("config", config);

require("./utils");

global.dispatcher = require("./queue/dispatcher");

app.use(require("cookie-parser")());
app.use(bodyParser.json());

require("./sessionStore")(app);

app.use(cors());
app.use(helmet());
app.use(passport.initialize());
app.use(passport.session());

require("./flash")(app);

app.use(requestIp.mw());

require("./i18n")(app);

require("./api/router")(app);

if (process.env.NODE_ENV !== "production") {
  logger.info("Environment: development ...");
  app.use(express.static(path.join(__dirname, "..", "build")));
  app.use(
    "/",
    createProxyMiddleware({ target: "http://localhost:3000", ws: true })
  );
} else {
  logger.info("Environment: production ...");
  var compression = require("compression");
  app.use(compression());
  app.use(
    "/static",
    express.static(path.join(__dirname, "client", "build", "static"), {
      etag: true
    })
  );
  app.use(
    "/locales",
    express.static(path.join(__dirname, "client", "build", "locales"), {
      etag: true
    })
  );
  app.use(
    "/favicon",
    express.static(path.join(__dirname, "client", "build", "favicon"), {
      etag: true
    })
  );
  // All url paths go to the bundled index.html
  app.get("/*", (req, res) => {
    res.header("Cache-Control", "private, no-cache, no-store, must-revalidate");
    res.header("Expires", "-1");
    res.header("Pragma", "no-cache");
    res.sendFile(path.join(__dirname, "client", "build", "index.html"));
  });
}

app.all("*", (req, res) => {
  res.status(404).send();
});

app.use((error, request, response, next) => {
  const t = request.t;
  if (error.status && error.status === 400 && error.reason) {
    const p = request.url.split("/").pop();
    const errors = error.reason.map(e => {
      const { message, dataPath, keyword } = e;
      return {
        message,
        i18message: t(p + dataPath + "." + keyword),
        path: dataPath.replace(/^\./, ""),
        validatorKey: keyword
      };
    });
    response.status(400).json({ name: "ValidationError", errors });
  }

  return next(error);
});

app.use((error, req, res, next) => {
  console.error(error);
  logger.error(error);
  // Will **not** get called. You'll get Express' default error
  // handler, which returns `error.toString()` in the error body
  if (error.status && error.status === 400) {
    return;
  }
  res.status(500).json({ message: error.message });
});*/

const app = require("./server");

// Start server
app.listen(app.get("port"), () => {
  logger.info("App listening on port " + app.get("port"));
});
