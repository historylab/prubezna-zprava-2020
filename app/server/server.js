const express = require("express");
const passport = require("passport");
const bodyParser = require("body-parser");
const helmet = require("helmet");
const logger = require("./logger")("server");
const cors = require("cors");
const requestIp = require("request-ip");
const session = require("express-session");
const compression = require("compression");

const app = express();

var port = process.env.PORT || 80;
app.set("port", port);

const config = require("./config");

app.set("config", config);

require("./utils");

app.use(require("cookie-parser")());
app.use(bodyParser.json());

require("./sessionStore")(app);

app.use(cors());
app.use(helmet());
app.use(compression());
app.use(passport.initialize());
app.use(passport.session());
require("./flash")(app);
app.use(requestIp.mw());

require("./i18n")(app);

require("./api/router")(app);

logger.info(`Starting app, environment ${process.env.NODE_ENV}`);
switch (process.env.NODE_ENV) {
  case "development":
    require("./server.development")(app, logger);
    break;
  case "production":
    require("./server.production")(app, logger);
    break;
  case "test":
  case "testing":
    app.use(
      session({
        resave: false,
        saveUninitialized: false,
        secret: "secret!"
      })
    );
    break;
}

app.all("*", (req, res) => {
  res.status(404).send();
});

app.use((error, request, response, next) => {
  const t = request.t;
  if (error.status && error.status === 400 && error.reason) {
    const p = request.url.split("/").pop();
    const errors = error.reason.map(e => {
      const { message, keyword, params } = e;
      let dataPath = e.dataPath;
      if (params && !dataPath) {
        dataPath = "." + params.missingProperty;
      }
      return {
        message,
        key: p + dataPath + "." + keyword,
        i18message: t(p + dataPath + "." + keyword),
        path: dataPath.replace(/^\./, ""),
        validatorKey: keyword
      };
    });
    response.status(400).json({ name: "ValidationError", errors });
  }

  return next(error);
});

app.use((error, req, res, next) => {
  console.error(error);
  logger.error(error);
  // Will **not** get called. You'll get Express' default error
  // handler, which returns `error.toString()` in the error body
  if (error.status && error.status === 400) {
    return;
  }
  res.status(500).json({ message: error.message });
});

module.exports = app;
