"use strict";

const logger = require("./logger")("flash");

module.exports = (res, { type, text }) => {
  const messages = [{ type, text }];

  res.locals.getMessages = messages;

  if (!res.locals.getMessages || res.locals.getMessages.length == 0) {
    res.header("X-HL-Flash", null);
    return;
  }

  const data = Buffer.from(
    encodeURIComponent(JSON.stringify(res.locals.getMessages))
  ).toString("base64");

  logger.info("flash " + JSON.stringify(messages));

  res.header("X-HL-Flash", data);
};
