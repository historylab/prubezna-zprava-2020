import React, { Suspense } from "react";
import { I18nextProvider } from "react-i18next";
import i18n from "./i18n";
import { MemoryRouter } from "react-router-dom";
import { library } from '@fortawesome/fontawesome-svg-core';
import { faAngleRight, faInfoCircle, faPlus, faUserPlus } from '@fortawesome/free-solid-svg-icons';
import "../src/fonts/fonts.css";
import "../src/css/index.scss";
import "../src/css/App.css";

library.add(faPlus, faInfoCircle, faUserPlus, faAngleRight);

export const parameters = {
  options: { storySort: "alphabetical" },
  actions: { argTypesRegex: "^on[A-Z].*" }
};

export const decorators = [
  Story => (
    <MemoryRouter>
      <I18nextProvider i18n={i18n}>
        <Suspense fallback="Loading...">
          <Story />
        </Suspense>
      </I18nextProvider>
    </MemoryRouter>
  )
];
