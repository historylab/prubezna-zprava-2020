/*import i18n from "i18next";
import { initReactI18next } from "react-i18next";

import Backend from "i18next-fs-backend";
var cs = require("../../public/locales/cs/translation.json");

i18n.use(initReactI18next).init({
  fallbackLng: "cs",
  debug: true
});

export default i18n;
*/
import i18next from "i18next";
import Backend from "i18next-xhr-backend";
import LanguageDetector from "i18next-browser-languagedetector";

i18next
  .use(Backend)
  .use(LanguageDetector)
  .init({
    initImmediate: true,
    fallbackLng: "cs",
    preload: ["cs"],
    lng: "cs",
    debug: false, //process.env.NODE_ENV !== "production",
    interpolation: {
      escapeValue: false // not needed for react as it escapes by default
    },
    detection: {
      order: ["querystring", "localStorage"],
      checkWhitelist: true
    }
  });

export default i18next;
