import ajv, { ErrorObject } from "ajv";

process.env.NODE_ENV = "testing";
process.env.REACT_APP_GOOGLE_CONSUMER_KEY = "1";
process.env.APP_SECRET = "1";
process.env.REDIS_PORT = -1;

expect.extend({
  async toConformSchema(received, schema) {
    const ajValidator = ajv({ allErrors: true, async: true });
    const validate = ajValidator.compile(schema);
    const result = await validate(received);
    if (!result) {
      //const errors = await this.parseErrors(validate.errors);
      //throw errors;
      //console.info(validate.errors);
      /*{
        keyword: 'required',
        dataPath: '',
        schemaPath: '#/required',
        params: { missingProperty: 'exercises' },
        message: "should have required property 'exercises'"
      }*/
      return {
        pass: false,
        message: () =>
          `expected object ${JSON.stringify(
            received
          )} to conform to schema and not report:\r\n${validate.errors
            .map(e => "-\t" + JSON.stringify(e))
            .concat("\r\n")}`
      };
    }
    return { pass: true };
  }
});
