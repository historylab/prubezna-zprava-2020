import React, { Suspense } from "react";
import { mount } from "@cypress/react";
import { I18nextProvider } from "react-i18next";
import { MemoryRouter } from "react-router-dom";
//import { i18n } from "./../integration/utils";
import faker from "faker";
import { ClassForm } from "./../../src/groups/components/ClassForm";

import "./../../src/css/index.scss";

describe("Class form", () => {
  beforeEach(() => {
    mount(
      <ClassForm group={null} t={key => key} onSubmit={g => console.info(g)} />
    );
  });

  it("Name is required", () => {
    cy.get("input#name").type("A").clear().blur();
    cy.get("input#name + div")
      .should("be.visible")
      .should("have.class", "invalid-feedback")
      .should("have.text", "classForm.name.required");
  });

  it("Year from is required", () => {
    cy.get("input#yearStart").type("1").clear().blur();
    cy.get("input#yearStart + div")
      .should("be.visible")
      .should("have.class", "invalid-feedback")
      .should("have.text", "classForm.yearStart.required");
  });

  it("Year end is required", () => {
    cy.get("input#yearEnd").type("1").clear().blur();
    cy.get("input#yearEnd + div")
      .should("be.visible")
      .should("have.class", "invalid-feedback")
      .should("have.text", "classForm.yearEnd.required");
  });

  it("Year from must be greater than 2000", () => {
    cy.get("input#yearStart").clear().type("3").blur();
    cy.get("input#yearStart + div")
      .should("be.visible")
      .should("have.class", "invalid-feedback")
      .should("have.text", "classForm.yearStart.moreThan");
  });

  it("Year end must be lower than 2500", () => {
    cy.get("input#yearEnd").clear().type("2600").blur();
    cy.get("input#yearEnd + div")
      .should("be.visible")
      .should("have.class", "invalid-feedback")
      .should("have.text", "classForm.yearEnd.lessThan");
  });

  it("Year to must be greater than year from", () => {
    cy.get("input#yearStart").clear().type("2020").blur();
    cy.get("input#yearEnd").clear().type("2010").blur();
    cy.get("input#yearEnd + div")
      .should("be.visible")
      .should("have.class", "invalid-feedback")
      .should("have.text", "classForm.yearEnd.moreThan");
  });

  it("changes year to when year from is changed", () => {
    cy.get("input#yearStart").clear().type("2020");
    cy.get("input#yearEnd").should("have.value", "2021");
  });
});
