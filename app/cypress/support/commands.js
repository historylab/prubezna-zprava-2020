// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
import "./cypress-wait-until";

Cypress.Commands.add("deleteAllEmails", () => {
  function deleteAllEmails() {
    return cy
      .request({
        method: "DELETE",
        url: Cypress.env("SMTP_API")
      })
      .then(() => {
        cy.log("emails deleted");
      });
  }

  return deleteAllEmails();
});

Cypress.Commands.add("getLastEmail", email => {
  function requestEmail(max) {
    email = email.toLowerCase();
    if (max == 0) {
      throw new Error(">");
    }
    return cy
      .request({
        method: "GET",
        url: Cypress.env("SMTP_API") + "?to=" + email,
        log: true,
        qs: { to: email }
        /*headers: {
          "content-type": "application/json"
        },*/
        //qs: {
        //  email
        //} //,
        //json: true
      })
      .then(({ body }) => {
        if (body && body.length > 0) {
          return body;
        }

        //throw new Error(">" + JSON.stringify(body));

        // If body is null, it means that no email was fetched for this address.
        // We call requestEmail recursively until an email is fetched.
        // We also wait for 300ms between each call to avoid spamming our server with requests
        cy.wait(500);

        return requestEmail(max - 1);
      });
  }

  return requestEmail(5);
});

Cypress.Commands.add("clickRecaptcha", () => {
  cy.window().then(win => {
    function tryIt(max) {
      if (max > 0) {
        const cd = win.document.querySelector("iframe[src*='recaptcha']");
        const e = cd && cd.contentDocument.getElementById("recaptcha-token");
        if (e) {
          e.click();
        } else {
          setTimeout(function () {
            tryIt(max - 1);
          }, 100);
        }
      }
    }
    tryIt(10);
  });
});

Cypress.Commands.add(
  "selectNth",
  { prevSubject: "element" },
  (subject, pos) => {
    cy.wrap(subject)
      .children("option")
      .eq(pos)
      .then(e => {
        cy.wrap(subject).select(e.val());
      });
  }
);
