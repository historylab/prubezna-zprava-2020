import i18next from "i18next";
import { initReactI18next } from "react-i18next";

var cs = require("../../public/locales/cs/translation.json");
var csServer = require("../../server/locales/cs/translation.json");

i18next.use(initReactI18next).init({
  lng: "cs",
  fallbackLng: "cs",
  debug: true,
  interpolation: {
    escapeValue: false // not needed for react!!
  },
  resources: { en: { translations: {} } }
});

i18next.addResourceBundle("cs", "translation", cs, true, true);
i18next.addResourceBundle("cs", "translation", csServer, true, true);

export const i18n = key => {
  return i18next.t(key);
};
