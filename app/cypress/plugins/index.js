const webpack = require("@cypress/webpack-preprocessor");

module.exports = (on, config) => {
  require("@cypress/react/plugins/react-scripts")(on, config);
  // IMPORTANT to return the config object
  // with the any changed environment variables

  const options = {
    webpackOptions: require("./../webpack.config.js")
  };
  on("file:preprocessor", webpack(options));

  //on("task", {
  //  failed: require("cypress-failed-log/src/failed")()
  //});

  return config;
};
