import { Server, Model } from "miragejs";
import exercises from "./../fixtures/catalog_cs";

export function makeServer({ environment = "development" } = {}) {
  let server = new Server({
    environment,

    models: {
      exercise: Model
    },

    fixtures: {
      exercises
    },

    seeds(server) {
      server.loadFixtures();
    },

    routes() {
      this.namespace = "api";

      this.get("/catalog", schema => {
        console.info(",,,,,,,,,,,,,,,,,", schema);
        //const d = { exercises: schema.exercises.all() };
        //console.info("data", d);
        return schema.exercises.all();
      });
    }
  });

  return server;
}
