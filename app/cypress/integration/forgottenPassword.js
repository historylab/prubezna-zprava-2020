const faker = require("faker");

describe("Forgotten password page", function () {

  let emailAddress = "admin@localhost.com";

  it("displays correct validation message for email address field", function () {
    cy.visit("/zapomenute-heslo");
    cy.get("button[type=submit]").click();
    cy.get("input#emailAddress + div")
      .should("be.visible")
      .should("have.class", "invalid-feedback")
      .should("have.text", "Emailová adresa nebyla zadána");
  });

  it("displays correct field caption for email address field", function () {
    cy.visit("/zapomenute-heslo?lng=cs");
    cy.get("button[type=submit]").click();
    cy.get("input#emailAddress")
      .parent()
      .get("label")
      .should("be.visible")
      .should("have.text", "Vaše přihlašovací emailová adresa");
  });

  it("displays success toast message when form is submitted with an valid email address", function () {
    cy.visit("/zapomenute-heslo");
    cy.get("input#emailAddress").type(emailAddress);
    cy.get("button[type=submit]").click();
    cy.wait(250);
    cy.get("div.notification-success")
      .should("be.visible")
      .should("have.text", "Email s odkazem na obnovu hesla byl odeslán");
  });

  it("sends an email to an registered email address", function () {    
    cy.deleteAllEmails();
    cy.visit("/zapomenute-heslo");
    cy.get("input#emailAddress").type(emailAddress);
    cy.get("button[type=submit]").click();
    cy.getLastEmail(emailAddress).then(emails => {
      const body = emails[0].html;
      const link = body.match(/href="([^"]*)/);
      assert.isNotNull(link);
      cy.visit(link[1]);
      cy.contains("Obnova hesla");
      const password = faker.internet.password();
      cy.get("#password").type(password);
      cy.get("#passwordConfirmation").type(password);
      cy.get("button[type=submit]").click();
      cy.location("pathname").should("eq", "/prihlasit");
      cy.wait(250);
      cy.get("div.notification-success")
        .should("be.visible")
        .should("have.text", "Heslo bylo změněno, můžete se přihlásit.");
      cy.get("#userName").type(emailAddress);
      cy.get("#password").type(password);
      cy.get("button[type=submit]").click();
      cy.get("div.notification-success")
        .should("be.visible")
        .should("have.text", "Přihlášení bylo úspěšné.");
      cy.location("pathname").should("eq", "/moje");
      cy.get("button.logout").click();
    });
  });
});
