const { apiLogin, clearCookies, expectSuccess } = require("./utils");
const { i18n } = require("./../support/i18n");

describe("Catalog detail page", function () {
  beforeEach(function () {
    clearCookies();
  });

  it("allows user to change class", function () {
    apiLogin();

    cy.visit("/moje");

    cy.get("a.list-detail-link").first().click();
    cy.get("#btnSwitchClass").click();

    let modal = cy.get(".modal-dialog");
    modal.should("be.visible");
    modal
      .get("h5.modal-title")
      .should("have.text", i18n("groupChooser.changeClass"));
    modal.get("button.selectGroup").first().click();
    cy.get(".modal-dialog").should("not.be.visible");

    expectSuccess(i18n("changeGroup.success"));
  });
});
