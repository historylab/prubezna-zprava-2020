import { makeServer } from "../plugins/mirageServer";

let server;

beforeEach(() => {
  server = makeServer();
});

afterEach(() => {
  server.shutdown();
});

describe("Catalog page", function () {
  it("Exercise tiles are displayed", function () {
    cy.visit("/katalog");
  });
});
