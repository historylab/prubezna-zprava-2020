const faker = require("faker");
import i18next from "i18next";
import Backend from "i18next-fs-backend";
var cs = require("../../public/locales/cs/translation.json");

const i18nextOptions = {
  initImmediate: true,
  fallbackLng: "cs",
  debug: true,
  preload: false,
  lng: "cs"
  //resources: { cs }
  /*backend: {
    loadPath: "../../public/locales/{{lng}}/{{ns}}.json"
  }*/
};
console.info(i18next);
i18next
  .use(Backend)
  .init(i18nextOptions)
  .then(i18n => {
    i18next.addResourceBundle("cs", "translation", cs, true, true);
  });
/*
const i18nextOptions = {
  backend: {
    loadPath: __dirname + "/locales/{{lng}}/{{ns}}.json",
    addPath: __dirname + "/locales/{{lng}}/{{ns}}.missing.json"
  },
  // order and from where user language should be detected
  //order: [/*'path', 'session', * "querystring", "cookie", "header"],

export const apiLogin = () => {
  //const userName = ;
  //const password =
  const username = Cypress.env("username") || "admin@historylab.cz";
  const password = Cypress.env("password") || "RsGj5PE@zT";

  // it is ok for the username to be visible in the Command Log
  expect(username, "username was set").to.be.a("string").and.not.be.empty;
  // but the password value should not be shown
  if (typeof password !== "string" || !password) {
    throw new Error("Missing password value, set using CYPRESS_password=...");
  }

  cy.request({
    method: "POST",
    url: "/api/login",
    body: {
      username,
      password
    }
  });
  sessionStorage.setItem("user", {});
  cy.getCookie("connect.sid").should("exist");
};

export const typeAndConfirmPassword = () => {
  const password = faker.internet.password() + faker.random.number();
  cy.get("#password").type(password);
  cy.get("#passwordConfirmation").type(password);
  return password;
};

export const expectSuccess = text => {
  cy.get("div.notification-success:last-of-type")
    .should("be.visible")
    .should("have.text", text);
};

export const expectWarning = text => {
  cy.get("div.notification-error:last-of-type")
    .should("be.visible")
    .should("have.text", text);
};


export const clearCookies = () => {
  cy.window().then(win => {
    win.sessionStorage.clear();
  });
  cy.clearCookies();
  cy.clearLocalStorage();
};

//export const
// be.localized
// https://github.com/cypress-io/cypress-example-recipes/blob/master/examples/extending-cypress__chai-assertions/cypress/support/index.js

export const clearCookies = () => {
  cy.window().then(win => {
    win.sessionStorage.clear();
  });
  cy.clearCookies();
  cy.clearLocalStorage();
};*/
