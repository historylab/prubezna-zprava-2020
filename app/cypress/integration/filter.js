import {makeServer} from "../plugins/mirageServer";

let server;

beforeEach(() => {
  server = makeServer();
});

afterEach(() => {
  server.shutdown();
});

describe("Filtering catalogue page", function () {
  it("Filter bar is displayed", function () {
    cy.visit("/katalog");
    cy.get('[data-cy=filterText]')
      .should("be.visible")
    cy.get('[data-cy=filterOrderButton]')
      .should("be.visible")
  });

  it("Sort by text", function () {
    cy.visit("/katalog");
    cy.get('[data-cy=filterText]')
      .should("be.visible")
      .type("Co ministr vzkazuje učitelům?")
    cy.get('.card-title').should('have.text', 'Co ministr vzkazuje učitelům?')
  });

  it("Sort by order", function () {
    cy.visit("/katalog");
    cy.get('[data-cy=filterOrderButton]').click()
    cy.get('.card-title').first().should('have.text', 'Vztyčování vlajky')
  });

  it("Sort by select", function () {
    cy.visit("/katalog");
    cy.get('[data-cy=filterSelectRVP]').click();
    cy.get('#react-select-6-option-21').click();

    cy.get('.card-title').first().should('have.text', 'Co dělají ženy na fotografii?');
    cy.get(':nth-child(1) > .css-6q0nyr-Svg').click();
    cy.get('.card-title').last().should('have.text', 'Vztyčování vlajky');
  });



});
