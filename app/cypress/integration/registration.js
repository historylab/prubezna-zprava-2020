const faker = require("faker");
const { login, typeAndConfirmPassword, expectSuccess } = require("./utils");
const { i18n } = require("./../support/i18n");

describe("Registration page", function () {
  beforeEach(() => {
    cy.window().then(win => {
      win.sessionStorage.clear();
    });
    cy.clearCookies();
    cy.clearLocalStorage();
  });

  it("displays a correct validation message for missing email value", function () {
    cy.visit("/registrace");
    cy.get("input#emailAddress").type(" ").clear().blur();
    cy.get("input#emailAddress + div")
      .should("be.visible")
      .should("have.class", "invalid-feedback")
      .should("have.text", i18n("signUp.emailAddress.required"));
  });

  it("displays a correct validation message for invalid email value", function () {
    cy.visit("/registrace");
    cy.get("input#emailAddress").type("a@").blur();
    cy.get("input#emailAddress + div")
      .should("be.visible")
      .should("have.class", "invalid-feedback")
      .should("have.text", i18n("signUp.emailAddress.email"));
  });

  it("displays a correct validation message for missing password", function () {
    cy.visit("/registrace");
    cy.clickRecaptcha();
    cy.get("#password").clear().blur();
    cy.get("input#password + div")
      .should("be.visible")
      .should("have.class", "invalid-feedback")
      .should("have.text", i18n("signUp.password.required"));
  });

  it("displays a correct validation message for missing password confirmation", function () {
    cy.visit("/registrace");
    cy.get("#passwordConfirmation").clear().blur();
    cy.get("input#passwordConfirmation + div")
      .should("be.visible")
      .should("have.class", "invalid-feedback")
      .should("have.text", i18n("signUp.passwordConfirmation.required"));
  });

  it("displays a correct validation message for not matching password confirmation", function () {
    cy.visit("/registrace");
    typeAndConfirmPassword();
    cy.get("#passwordConfirmation").type("1").blur();
    cy.get("input#passwordConfirmation + div")
      .should("be.visible")
      .should("have.class", "invalid-feedback")
      .should("have.text", i18n("signUp.passwordConfirmation.oneOf"));
  });

  it("allows to create new account and login in", function () {
    cy.deleteAllEmails();
    cy.visit("/registrace");
    const emailAddress = faker.internet.email();
    cy.get("#firstName").type(faker.name.firstName());
    cy.get("#lastName").type(faker.name.lastName());
    cy.get("input#emailAddress").type(emailAddress);
    cy.get("#institution").type(faker.company.companyName());
    cy.get("#teacher").select("Ano");
    const password = typeAndConfirmPassword();
    cy.clickRecaptcha();
    cy.get("button[type=submit]").click();
    cy.location("pathname").should("eq", "/moje");
    expectSuccess("Váš účet byl vytvořen, můžete se přihlásit do HistoryLabu.");
    login(emailAddress, password);
    expectSuccess("Přihlášení bylo úspěšné.");
    cy.get("button.logout").click();

    /*cy.getLastEmail(emailAddress).then(emails => {
      assert.areEqual(emails[0].subject, "HistoryLab: Potvrzení registrace");
      const body = emails[0].html;
      const link = body.match(/href="([^"]*)/);
      assert.isNotNull(link);
    });*/
  });

  it("allows to create new account and sends a confirmation email", function () {
    cy.deleteAllEmails();
    cy.visit("/registrace");
    const emailAddress = faker.internet.email();
    cy.get("#firstName").type(faker.name.firstName());
    cy.get("#lastName").type(faker.name.lastName());
    cy.get("input#emailAddress").type(emailAddress);
    cy.get("#institution").type(faker.company.companyName());
    cy.get("#teacher").select("Ano");
    const password = typeAndConfirmPassword();
    cy.clickRecaptcha();
    cy.get("button[type=submit]").click();
    cy.location("pathname").should("eq", "/moje");
    expectSuccess("Váš účet byl vytvořen, můžete se přihlásit do HistoryLabu.");

    cy.getLastEmail(emailAddress).then(emails => {
      assert.areEqual(emails[0].subject, "HistoryLab: Potvrzení registrace");
      const body = emails[0].html;
      const link = body.match(/href="([^"]*)/);
      assert.isNotNull(link);
    });
  });
});
