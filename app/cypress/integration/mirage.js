//import { makeServer } from "../../src/server";
// src/server.js
import { Server, Model } from "miragejs";

export function makeServer({ environment = "development" } = {}) {
  let server = new Server({
    environment,

    models: {
      user: Model
    },

    seeds(server) {
      server.create("user", { name: "Bob" });
      server.create("user", { name: "Alice" });
    },

    routes() {
      this.namespace = "api";

      this.get("/users", schema => {
        return schema.users.all();
      });
    }
  });

  return server;
}

let server;

describe("Forgotten Mirage", function () {
  beforeEach(() => {
    server = makeServer({ environment: "test" });
  });

  afterEach(() => {
    server.shutdown();
  });

  it("shows the users from our server", () => {
    server.create("user", { id: 1, name: "Luke" });
    server.create("user", { id: 2, name: "Leia" });

    cy.visit("/");

    cy.get('[data-testid="user-1"]').contains("Luke");
    cy.get('[data-testid="user-2"]').contains("Leia");
  });

  it("shows a message if there are no users", () => {
    // Don't create any users

    cy.visit("/");

    cy.get('[data-testid="no-users"]').should("be.visible");
  });
});
