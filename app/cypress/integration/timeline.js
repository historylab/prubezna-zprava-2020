import { makeServer } from "../plugins/mirageServer";

let server;

beforeEach(() => {
  server = makeServer();
});

afterEach(() => {
  server.shutdown();
});

describe("Timeline page", function () {
  it("Timeline is displayed", function () {
    cy.visit("/timeline");
  });

  it("First item is Co si slibovali od šicího stroje?", function () {
    cy.visit("/timeline");
    cy.wait(100);
    cy.get("[data-cy=exercise-item]").first().contains("Co si slibovali od šicího stroje?");
  });

  it("Click on epoch, show detail and exercises in epoch.", function () {
    cy.visit("/timeline");
    cy.wait(1000);
    cy.get("[data-cy=epoch]").first().click();
    cy.get("[data-cy=epoch-detail]").contains(
      "Modernizace Evropy a rozmach koloniálních říší"
    );
    cy.wait(200);
    cy.get("[data-cy=exercise-item]")
      .first()
      .contains("Co si slibovali od šicího stroje?");
  });

  it("Scroll change exercises", function () {
    cy.visit("/timeline");
    cy.wait(200);
    cy.scrollTo(0, 4000);
    cy.wait(100);
    cy.get("[data-cy=exercise-item]").first().contains("Co natáčeli?");
  });

  it("Show detail of cviceni", function () {
    cy.visit("/timeline");
    cy.wait(100);
    cy.get("[data-cy=exercise-item]").first().click();
    cy.get("[cy-data=exercise-detail]").contains("Co si slibovali od šicího stroje?");
    cy.get("[cy-data=exercise-detail-author]").contains("Václav Sixta");
  });
});
