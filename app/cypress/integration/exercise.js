const { login, clearCookies, expectSuccess } = require("./utils");
const { i18n } = require("./../support/i18n");

describe("Exercise page", function () {
  const userName = "admin@historylab.cz";
  const password = "RsGj5PE@zT";
  const exerciseUrl = "/katalog/cviceni/co-ministr-vzkazuje-ucitelum";

  beforeEach(function () {
    clearCookies();
    cy.visit(exerciseUrl);
  });

  it("displays login form when user is not logged in and wants to create an assignment", function () {
    cy.get("button[role=individual]").click();

    const modalRoot = cy.get("div.modal--login");

    modalRoot.should("be.visible");
    modalRoot
      .get("p.text-center")
      .should("have.text", i18n("catalogExerciseDetail.signInFirst"));
  });

  it("displays the login form first and then class selection dialog when unauthenticated user wants to create an assignment", function () {
    cy.get("button[role=individual]").click();

    login(userName, password);

    const modalRoot = cy.get("div.modal--login");
    modalRoot
      .get("p.text-center")
      .should("not.have.text", "catalogExerciseDetail.chooseOrCreateGroup")
      .should("have.text", i18n("catalogExerciseDetail.chooseOrCreateGroup"));
  });

  it("lets user to sign in and choose group from the list to create an assignment", function () {
    cy.get("button[role=individual]").click();

    login(userName, password);

    const modalRoot = cy.get("div.modal--login");
    modalRoot.get(":nth-child(1) > .selectGroup").click();

    cy.location("pathname").should("match", /\/cviceni\/detail\/[A-Za-z0-9]+$/);
  });

  it("lets user to sign in and select group from the list to create an assignment", function () {
    cy.get("button[role=individual]").click();

    login(userName, password);

    const modalRoot = cy.get("div.modal--login");
    modalRoot.get("select").selectNth(2);

    cy.location("pathname").should("match", /\/cviceni\/detail\/[A-Za-z0-9]+$/);
  });

  it("will not continue when user will not select a group", function () {
    cy.get("button[role=individual]").click();

    login(userName, password);

    const modalRoot = cy.get("div.modal--login");
    modalRoot.get("select").selectNth(0);

    cy.location("pathname").should("match", /\/katalog\/cviceni\//);
  });

  it("lets user to sign in and create new group to create an assignment", function () {
    cy.get("button[role=individual]").click();

    login(userName, password);

    const modalRoot = cy.get("div.modal--login");
    //const submitButton = modalRoot.get("form button[type=submit]");
    modalRoot.get("form button[type=submit]").should("be.disabled");
    modalRoot.get("form input[id=name]").type("1234567890");
    modalRoot.get("form button[type=submit]").should("not.be.disabled");
    modalRoot.get("form button[type=submit]").click();
    cy.location("pathname").should("match", /\/cviceni\/detail\/[A-Za-z0-9]+$/);
  });
});
