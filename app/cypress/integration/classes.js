const { apiLogin, clearCookies, expectSuccess, expectWarning } = require("./utils");
const { i18n } = require("./../support/i18n");
const faker = require("faker");

describe("Classes page", function () {
  beforeEach(function () {
    clearCookies();
  });

  it("displays a correct validation message for missing name value", function () {
    apiLogin();
    cy.visit("/trida/nova");
    cy.get("input#name").type(" ").clear().blur();
    cy.get("input#name + div")
      .should("be.visible")
      .should("have.class", "invalid-feedback")
      .should("have.text", i18n("classForm.name.required"));
  });

  it("redirects to class list after new class is created", function () {
    apiLogin();
    cy.visit("/tridy");
    cy.get("button.btn-primary").click();
    cy.get("input#name").type(faker.name.jobTitle());
    cy.get("button[type=submit]").click();
    /*cy.wait(250);
    cy.get("div.notification-success")
      .should("be.visible")
      .should("have.text", i18n("class.created"));*/
    cy.location("pathname").should("eq", "/tridy");
  });

  it("redirects to class list after a class is updated", function () {
    apiLogin();
    cy.visit("/tridy");
    cy.get("a.class-link:first-of-type").first().click({ force: true });
    cy.get("input#name").type("*");
    cy.get("input#yearStart").clear().type(new Date().getFullYear().toString());
    cy.get("button[type=submit]").click();
    cy.wait(250);
    cy.get("div.notification-success")
      .should("be.visible")
      .should("have.text", i18n("class.updated"));
    cy.location("pathname").should("eq", "/tridy");
  });

  it("is accessible to logged in users only", function () {
    cy.visit("/tridy");
    cy.get("div.login-form-div").should("be.visible");
  });

  it("displays a notification when email is added", function () {
    apiLogin();
    cy.visit("/tridy");
    cy.get("a.class-link:first-of-type").first().click({ force: true });
    cy.waitUntil(() =>
      cy.get("[data-testid=loading-indicator]").should("not.be.visible")
    );
    cy.get("li.nav-item").last().click();

    const emailAddresses = [faker.internet.email(), faker.internet.email()];

    cy.get("#btnAddEmailAddressToGroup").click();

    modal = cy.get(".modal-dialog");
    modal.get("#emailAddresses").should("have.value", "");
    modal.get("#emailAddresses").clear().type(emailAddresses.join(", "));
    modal.get(".btn-primary").click();

    expectSuccess(i18n("emailAddressClass.added"));
  });

  it("lets user to add email addresses to a class and remove one later", function () {
    apiLogin();
    cy.visit("/tridy");
    cy.get("a.class-link:first-of-type").first().click({ force: true });
    cy.waitUntil(() =>
      cy.get("[data-testid=loading-indicator]").should("not.be.visible")
    );
    cy.get("li.nav-item").last().click();

    const emailAddresses = [
      faker.internet.email(),
      faker.internet.email(),
      faker.internet.email()
    ];

    cy.get("#btnAddEmailAddressToGroup").click();

    let modal = cy.get(".modal-dialog");
    modal.get("#emailAddresses").type(emailAddresses[0]);
    modal.get(".btn-primary").click();

    cy.get("#btnAddEmailAddressToGroup").click();
    modal = cy.get(".modal-dialog");
    modal.get("#emailAddresses").should("have.value", "");
    modal.get("#emailAddresses").clear().type(emailAddresses.join(", "));
    modal.get(".btn-primary").click();

    cy.wait(250);
    cy.reload();
    cy.waitUntil(() =>
      cy.get("[data-testid=loading-indicator]").should("not.be.visible")
    );
    cy.get("li.nav-item").last().click();
    cy.contains(emailAddresses[0]).should("be.visible");
    cy.contains(emailAddresses[1]).should("be.visible");

    let cell = cy.contains(emailAddresses[2]);
    cell.should("be.visible");
    cell.parent("tr").find("button").click();

    modal = cy.get(".modal-dialog");
    modal
      .get("h5.modal-title")
      .should("have.text", i18n("deleteEmailDialog.header"));
    modal.get("button.btn-primary").click();

    cy.contains(emailAddresses[2]).should("not.be.visible");
  });

  it("display a notification with a general error", function() {
    apiLogin();

    cy.fixture('classes/errorResponse.json').as("errorResponse");
    cy.server();
    cy.route('api/group', '@errorResponse');

    cy.visit("/trida/nova");
    cy.get("input#name").type("ABC");
    cy.get("button[type=submit]").click();
    cy.wait(250);

    cy.get("[data-testid=loading-indicator]").should("not.be.visible");
    expectWarning("Bad Request (400): [object Object]");
  })
});
