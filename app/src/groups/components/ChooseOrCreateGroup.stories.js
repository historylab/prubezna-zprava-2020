import React from "react";
import { Modal, ModalBody } from "reactstrap";
import { action } from "@storybook/addon-actions";
import { Trans } from "react-i18next";
import "./../../css/index.css";

import ChooseOrCreateGroup from "./ChooseOrCreateGroup";

export default {
  component: ChooseOrCreateGroup,
  title: "Choose or create group",
  // Our exports that end in "Data" are not stories.
  excludeStories: /.*Data$/,
  parameters: { actions: { argTypesRegex: "^on.*" } }
};

const Template = args => (
  <Modal
    isOpen={true}
    className="class-form-modal"
    backdrop={true}
    fade={false}
    role="class"
  >
    <ModalBody>
      <p className="text-center">
        <Trans i18nKey="catalogExerciseDetail.chooseOrCreateGroup"></Trans>
      </p>
      <ChooseOrCreateGroup
        {...args}
        onGroupCreating={action("onGroupCreating")}
        onGroupSelected={action("onGroupSelected")}
      />
    </ModalBody>
  </Modal>
);

const groupsGenerator = n =>
  Array.from({ length: n }, (x, i) => {
    return { id: i, name: i + ".A" };
  });

const groups3 = groupsGenerator(3);
const groups5 = groupsGenerator(5);
const groups20 = groupsGenerator(20);

export const actionsData = {
  onPinTask: action("onPinTask"),
  onArchiveTask: action("onArchiveTask")
};

export const Loading = () => <Template loading={true} />;

export const NoGroups = () => <Template groups={[]} />;

export const Groups3 = () => <Template groups={groups3} />;

export const Groups5 = () => <Template groups={groups5} />;

export const Groups20 = () => <Template groups={groups20} />;
