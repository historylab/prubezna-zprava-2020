import React from "react";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Trans } from "react-i18next";
import { Card, CardHeader, CardBody, CardSubtitle, CardTitle } from "reactstrap";
import "./GroupTile.scss";

const GroupTile = ({ group, location, className, toggleFavourite }) => {
  const color = group.color || "";

  return (
    <Card
      className={`hl-card--class ${className}`}
      style={{ backgroundColor: color }}
    >
      <CardHeader className="hl-card__controls">
        {!group.isHidden && (
          <FontAwesomeIcon
            data-tut="GroupTile_isFavourite"
            icon={[group.isFavourite ? "fas" : "far", "star"]}
            onClick={() => {
              handleIsFavouriteEvent(group);
            }}
          />
        )}
        <Link
          className="hl-show-on-hover"
          to={{
            pathname: "/tridy/" + group.id + "/detail/",
            state: {
              group: group,
              returnTo: location.pathname
            }
          }}
        >
          <FontAwesomeIcon icon="cog" data-tut="GroupTile_detail" />
        </Link>
      </CardHeader>

      <CardBody>
        <Link
          to={{
            pathname: "/tridy/" + group.id,
            state: {
              group: group,
              returnTo: location.pathname
            }
          }}
        >
          <CardTitle tag="h5">
            {group.name}
          </CardTitle>
          <CardSubtitle className="hl-card__exerciseCount">
            { group.exerciseCount > 0 && group.exerciseCount + " " }

            { group.exerciseCount > 4 && <Trans i18nKey="grouptile.exerciseCount">zadaných</Trans> }
            { group.exerciseCount > 1 && group.exerciseCount <= 4 && <Trans i18nKey="grouptile.exerciseCount">zadaná</Trans> }
            { group.exerciseCount === 1 && <Trans i18nKey="grouptile.exerciseCount">zadané</Trans> }
            
            { group.exerciseCount === 0 && <Trans i18nKey="grouptile.exerciseCount">žádná</Trans> }

            {" cvičení"}
          </CardSubtitle>
        </Link>
      </CardBody>
    </Card>
  );

  function handleIsFavouriteEvent(group) {
    toggleFavourite(group);
  }
};

export default GroupTile;
