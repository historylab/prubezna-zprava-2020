import React from "react";
import { action } from "@storybook/addon-actions";
import "./../../css/index.scss";

import ChooseOrCreateGroupModal from "./ChooseOrCreateGroupModal";

export default {
  component: ChooseOrCreateGroupModal,
  title: "Choose or create group",
  // Our exports that end in "Data" are not stories.
  excludeStories: /.*Data$/,
  parameters: { actions: { argTypesRegex: "^on.*" } }
};

const Template = args => (
  <ChooseOrCreateGroupModal
    {...args}
    isOpen={true}
    onGroupCreating={action("onGroupCreating")}
    onGroupSelected={action("onGroupSelected")}
  />
);

const groupsGenerator = n =>
  Array.from({ length: n }, (x, i) => {
    return {
      id: i,
      name: i + ".A",
      yearStart: 2020,
      yearEnd: 2021,
      color: i % 2 ? "blue" : "green"
    };
  });

const groups3 = groupsGenerator(3);
const groups5 = groupsGenerator(5);
const groups20 = groupsGenerator(20);

export const actionsData = {
  onPinTask: action("onPinTask"),
  onArchiveTask: action("onArchiveTask")
};

export const Loading = () => <Template loading={true} />;

export const NoGroups = () => <Template groups={[]} />;

export const Groups3 = () => <Template groups={groups3} />;

export const Groups5 = () => <Template groups={groups5} />;

export const Groups20 = () => <Template groups={groups20} />;
