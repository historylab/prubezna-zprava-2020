import React from "react";
import { action } from "@storybook/addon-actions";
import "./../../css/index.scss";

import ClassForm from "./ClassForm";

export default {
  component: ClassForm,
  title: "Class Form",
  // Our exports that end in "Data" are not stories.
  excludeStories: /.*Data$/
};

export const taskData = {
  id: "1",
  title: "Test Task",
  state: "TASK_INBOX",
  updatedAt: new Date(2018, 0, 1, 9, 0)
};

export const actionsData = {
  onPinTask: action("onPinTask"),
  onArchiveTask: action("onArchiveTask")
};

export const Default = () => (
  <ClassForm task={{ ...taskData }} {...actionsData} />
);

export const Pinned = () => (
  <ClassForm task={{ ...taskData, state: "TASK_PINNED" }} {...actionsData} />
);

export const Archived = () => (
  <ClassForm task={{ ...taskData, state: "TASK_ARCHIVED" }} {...actionsData} />
);
