import React, { Fragment } from "react";
import { Field } from "formik";
import { ReactstrapInput } from "reactstrap-formik";

class GroupTinyForm extends React.Component {
  render() {
    const { t } = this.props;
    return (
      <Field
        label={t("tinyClassForm.name.label")}
        name="name"
        id="name"
        placeholder={t("tinyClassForm.name.placeholder")}
        component={ReactstrapInput}
      />
    );
  }
}

export default GroupTinyForm;
