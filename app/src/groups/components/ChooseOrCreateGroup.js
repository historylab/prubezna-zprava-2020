import React from "react";
import { Container, Row, Col } from "reactstrap";
import GroupSelector from "./GroupSelector";
import GroupTinyForm from "./GroupTinyForm";
import { withTranslation, Trans } from "react-i18next";
import "./ChooseOrCreateGroup.scss";

const ChooseOrCreateGroup = props => {
  const {
    loading,
    groups,
    onGroupCreating,
    onGroupSelected,
    groupName,
    t
  } = props;
  return (
    <Container className="ChooseOrCreateGroup">
      <Row>
        <Col xs="5">
          <h3>
            <Trans i18nKey="chooseOrCreateGroup.choose">Existující třídě</Trans>
          </h3>
          <GroupSelector
            loading={loading}
            groups={groups || []}
            onGroupSelected={onGroupSelected}
            t={t}
          />
        </Col>
        <Col className="col-divider" xs="2">
          <Trans i18nKey="or">Nebo</Trans>
        </Col>
        <Col xs="5">
          <h3>
            <Trans i18nKey="chooseOrCreateGroup.create">Nové třídě</Trans>
          </h3>
          <GroupTinyForm name={groupName} t={t} />
        </Col>
      </Row>
    </Container>
  );
};

export default withTranslation()(ChooseOrCreateGroup);
