import React from "react";
import faker from "faker";
import "./../../css/index.scss";
import "./../../fonts/fonts.css";
import "./../../css/App.css";

import GroupTile from "./GroupTile";

// require("../../icons");


export default {
  component: GroupTile,
  title: "Class Card (GroupTile)",
  // decorators: [d],
  excludeStories: /.*Data$/
};

// export const actionsData = {
//   onPinTask: action("onPinTask"),
//   onArchiveTask: action("onArchiveTask")
// };

export const Default = () => {
  const yearStart = 2015 + (faker.random.number() % 6);
  
  return <GroupTile
          group={{
            id: faker.random.number(),
            name: faker.lorem.words(),
            color: faker.internet.color(),
            yearStart,
            yearEnd: yearStart + 1,
            exerciseCount: faker.random.number(),
            isFavourite: faker.random.boolean(),
            isHidden: false,
          }}
          location={{}}
          className=""
        />;
};

export const Archived = () => {
  const yearStart = 2015 + (faker.random.number() % 6);
  
  return <GroupTile
          group={{
            id: faker.random.number(),
            name: faker.lorem.words(),
            color: faker.internet.color(),
            yearStart,
            yearEnd: yearStart + 1,
            exerciseCount: faker.random.number(),
            isFavourite: false,
            isHidden: true,
          }}
          location={{}}
          className="is-archived"
        />;
};
