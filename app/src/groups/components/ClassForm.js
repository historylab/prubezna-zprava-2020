import React from "react";
import classnames from "classnames";
import * as Yup from "yup";
import { Field, Form, Formik } from "formik";
import { Col, Row, Button, FormGroup, Label, CustomInput } from "reactstrap";
import { ReactstrapInput } from "reactstrap-formik";
import { withTranslation, Trans } from "react-i18next";
import { CirclePicker } from "react-color";
import { faArchive } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import setYupLocale from "../../yup";
import "./ClassForm.scss";

setYupLocale("classForm");

const ClassSchema = Yup.object().shape({
  name: Yup.string().max(50).required(),
  note: Yup.string().max(500),
  color: Yup.string().max(20),
  yearStart: Yup.number().moreThan(1900).lessThan(2500).required(),
  yearEnd: Yup.number()
    .moreThan(1901)
    .lessThan(2500)
    .when("yearStart", (yearStart, schema) => {
      return yearStart ? schema.moreThan(yearStart) : schema.moreThan(1901);
    })
    .required(),
  isHidden: Yup.bool(),
  isFavourite: Yup.bool()
});

export class ClassForm extends React.Component {
  render() {
    const { onSubmit, t, group, isLoading } = this.props;
    const defaultGroup = {
      name: "",
      note: "",
      color: "#f44336",
      yearStart: new Date().getFullYear(),
      yearEnd: new Date().getFullYear() + 1,
      isHidden: false,
      isFavourite: false
    };

    const getLabelWithIcon = (text, icon) => (
      <div>
        {icon && <FontAwesomeIcon icon={icon} className="mr-2" />}
        {t(text)}
      </div>
    );

    return (
      <Formik
        initialValues={group || defaultGroup}
        enableReinitialize={true}
        validationSchema={ClassSchema}
        onSubmit={(values, actions) => {
          onSubmit(values, error => {
            if (error.form) {
              actions.setErrors(error.form);
              return true;
            }
            return false;
          });
        }}
        render={form => {
          const { values, setFieldValue, handleChange, errors } = form;
          return (
            <Form className="ClassForm">
              <Row form>
                <Col>
                  <FormGroup>
                    <Field
                      label={t("classForm.name.label")}
                      name="name"
                      id="name"
                      placeholder={t("classForm.name.placeholder")}
                      component={ReactstrapInput}
                    />
                  </FormGroup>
                </Col>
              </Row>
              <Row form>
                <Col>
                  <FormGroup>
                    <Label for="yearStart">
                      {t("classForm.yearStart.label")}
                    </Label>
                    <Field
                      type="number"
                      label={t("classForm.yearStart.label")}
                      name="yearStart"
                      id="yearStart"
                      className={classnames({
                        "form-control": true,
                        "is-invalid": !!errors["yearStart"]
                      })}
                      placeholder={t("classForm.yearStart.placeholder")}
                      onChange={e => {
                        handleChange(e);
                        if (e.target.value.match(/[0-9]+/)) {
                          values.yearEnd = e.target.value * 1 + 1;
                        }
                      }}
                    />
                    {errors["yearStart"] && (
                      <div className="invalid-feedback">
                        {errors["yearStart"]}
                      </div>
                    )}
                  </FormGroup>
                </Col>
                <Col>
                  <FormGroup>
                    <Field
                      type="number"
                      label={t("classForm.yearEnd.label")}
                      name="yearEnd"
                      id="yearEnd"
                      placeholder={t("classForm.yearEnd.placeholder")}
                      component={ReactstrapInput}
                    />
                  </FormGroup>
                </Col>
              </Row>

              <Row form>
                <Col>
                  <CustomInput
                    type="switch"
                    id="isFavourite"
                    className="form-control-lg"
                    name="isFavourite"
                    label={getLabelWithIcon(
                      "classForm.isFavourite.label",
                      "star"
                    )}
                    checked={values.isFavourite}
                    disabled={values.isHidden}
                    onChange={() =>
                      setFieldValue("isFavourite", !values.isFavourite)
                    }
                  />
                  <CustomInput
                    type="switch"
                    id="isHidden"
                    name="isHidden"
                    className="form-control-lg"
                    label={getLabelWithIcon(
                      "classForm.isHidden.label",
                      faArchive
                    )}
                    checked={values.isHidden}
                    onChange={() => {
                      setFieldValue("isHidden", !values.isHidden);

                      if (!values.isHidden) {
                        setFieldValue("isFavourite", false);
                      }
                    }}
                  />
                </Col>

                <Col>
                  <FormGroup>
                    <Label className="label-color" for="color">
                      {t("classForm.color.label")}
                    </Label>
                    <CirclePicker
                      color={values.color}
                      onChange={e => setFieldValue("color", e.hex)}
                    />
                  </FormGroup>
                </Col>
              </Row>

              <Row form>
                <Col>
                  <FormGroup>
                    <Field
                      type="textarea"
                      rows={7}
                      label={t("classForm.note.label")}
                      name="note"
                      id="note"
                      placeholder={t("classForm.note.placeholder")}
                      component={ReactstrapInput}
                    />
                  </FormGroup>
                </Col>
              </Row>

              <Row form>
                <Col style={{ display: "flex", justifyContent: "center" }}>
                  <FormGroup className="mt-2 text-center">
                    <Button
                      type="submit"
                      className="mt-1"
                      color="primary"
                      disabled={isLoading}
                    >
                      <Trans i18nKey="classForm.save.label">Uložit</Trans>
                    </Button>
                  </FormGroup>
                </Col>
              </Row>
            </Form>
          );
        }}
      />
    );
  }
}

export default withTranslation()(ClassForm);
