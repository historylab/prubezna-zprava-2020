import React from "react";
import { action } from "@storybook/addon-actions";
import "./../../css/index.scss";

import GroupDetail from "./GroupDetail";

export default {
  component: GroupDetail,
  title: "Group Detail",
  // Our exports that end in "Data" are not stories.
  excludeStories: /.*Data$/
};

export const Tab1 = () => (
  <GroupDetail
    activeTab="1"
    group={{
      id: 1,
      name: "1.A",
      yearStart: 2019,
      yearEnd: 2020,
      color: "#e91e63",
      emailAddresses: []
    }}
  />
);

export const Tab2 = () => (
  <GroupDetail
    activeTab="2"
    group={{
      id: 1,
      name: "1.A",
      emailAddresses: [
        {
          name: "Jan Novák",
          emailAddress: "email1@email.em",
          group_id: 1,
          hasSubmissions: true
        },
        { emailAddress: "email2@email.em" },
        { emailAddress: "email3@email.em", group_id: 1, hasSubmissions: false }
      ]
    }}
  />
);

export const New = () => (
  <GroupDetail activeTab="1" group={{ name: "1.A", emailAddresses: [] }} />
);

export const Loading = () => <GroupDetail activeTab="1" isLoading={true} />;
