import React from "react";
import { action } from "@storybook/addon-actions";
import { ApplicationStateProvider } from "./../../ApplicationStateProvider";
import "./../../css/index.scss";
import "./../../fonts/fonts.css";
import "./../../css/App.css";
import axios from "axios";
import GroupList from "./GroupList";
import MockAdapter from "axios-mock-adapter";
import DataService from "./../../DataService";
import faker from "faker";

require("./../../icons");

const d = Story => (
  <ApplicationStateProvider value={{ user: {} }}>
    <Story />
  </ApplicationStateProvider>
);

export default {
  component: GroupList,
  title: "Classes List",
  decorators: [d],
  excludeStories: /.*Data$/
};

export const taskData = {
  id: "1",
  title: "Test Task",
  state: "TASK_INBOX",
  updatedAt: new Date(2018, 0, 1, 9, 0),
  group: {}
};

export const actionsData = {
  onPinTask: action("onPinTask"),
  onArchiveTask: action("onArchiveTask")
};

export const Default = () => {
  const dataService = new DataService();
  dataService.fetchGroups = () => axios.get("/api/group");
  const mock = new MockAdapter(axios, { onNoMatch: "passthrough" });
  mock.onGet("/api/group").reply(200, { 
    "groups":[{"id":975,"name":"238A373F","color":null,"yearStart":2019,"yearEnd":2020,isFavourite:true},
    {"id":981,"name":"2ADB10A3","note":null,"color":null,"yearStart":2019,"yearEnd":2020},
    {"id":976,"name":"2BE8274F","note":null,"color":null,"yearStart":2019,"yearEnd":2020,isFavourite:true},
    {"id":977,"name":"2E5973F3","note":null,"color":null,"yearStart":2018,"yearEnd":2019,isHidden:true},
    {"id":980,"name":"369F1820","note":null,"color":null,"yearStart":2018,"yearEnd":2019},
    {"id":979,"name":"37B1D348","note":null,"color":null,"yearStart":2019,"yearEnd":2020,isHidden:true},
    {"id":978,"name":"A6BDE4A","note":null,"color":null,"yearStart":2020,"yearEnd":2021}] 
  });
  return (<GroupList dataService={dataService} location={{}} group={{ id: 1, name: "1.A", emailAddresses: [] }} />)
};

export const RandomClasses = () => {
  const dataService = new DataService();
  dataService.fetchGroups = () => axios.get("/api/group");
  const mock = new MockAdapter(axios, { onNoMatch: "passthrough" });
  const groups = Array.from(Array(50), (v, i) => i).map(i => {
    const yearStart = 2015 + (faker.random.number() % 6);
    return {
      id: i,
      name: faker.lorem.words(),
      color: faker.internet.color(),
      yearStart,
      exerciseCount: faker.random.number(),
      isFavourite: faker.random.boolean(),
      isHidden: faker.random.boolean()
    };
  });
  mock.onGet("/api/group").reply(200, { groups: groups });
  return (
    <GroupList
      dataService={dataService}
      location={{}}
      group={{ id: 1, name: "1.A", emailAddresses: [] }}
    />
  );
};

export const NoClasses = () => {
  const dataService = new DataService();
  dataService.fetchGroups = () => axios.get("/api/group");
  const mock = new MockAdapter(axios, { onNoMatch: "passthrough" });
  const groups = [];
  mock.onGet("/api/group").reply(200, { groups: groups });
  return (
    <GroupList
      dataService={dataService}
      location={{}}
      group={{ id: 1, name: "1.A", emailAddresses: [] }}
    />
  );
};
