import React from "react";
import { Link } from "react-router-dom";
import classnames from "classnames";
import ClassForm from "./../components/ClassForm";
import DataService from "./../../DataService";
//import EmailAddressesDialog from "./EmailAddressesDialog";
import EmailAddresses from "./../../exercises/EmailAddresses";
//import DeleteExerciseDialog from "./DeleteExerciseDialog";
import MainHeader from "./../../MainHeader";
import { Trans } from "react-i18next";
import {
  TabContent,
  TabPane,
  Nav,
  Row,
  Col,
  Button,
  ButtonGroup,
  NavItem,
  NavLink
} from "reactstrap";

class GroupDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: "1",
      isLoading: true,
      title: "XXX",
      pending: false,
      modal: false,
      error: null,
      data: {
        groups: [],
        warnings: []
      },
      group: {},
      currentBreakpoint: "lg",
      compactType: "vertical",
      mounted: false,
      layouts: { lg: this.props.initialLayout }
    };
  }

  deleteEmailAddressFromGroup = emailAddress => {
    const { exercise } = this.state;
    this.setState({ pending: true, error: null });
    new DataService()
      .deleteEmailAddressFromGroup(exercise.group.id, emailAddress)
      .then(result => {
        this.setState({
          exercise: result.data || result,
          modal: false,
          emailAddress: {}
        });
        this.emailAddressesFormRef && this.emailAddressesFormRef.toggle();
      })
      .then(this.props.processNotifications)
      .catch(this.onError);
  };

  addEmailAddressToGroup = emailAddress => {
    const { exercise } = this.state;
    this.setState({ pending: true, error: null });
    new DataService()
      .addEmailAddressesToGroup(exercise.group.id, emailAddress.emailAddress)
      .then(result => {
        this.setState({ exercise: result.data || result });
        return result;
      })
      .then(this.props.processNotifications)
      .catch(this.onError);
  };

  toggleTab(tab) {
    this.setState({
      activeTab: tab.toString()
    });
  }

  toggleAddEmailAddresses = () => {
    this.setState({
      addEmailAddresses: {
        isOpen: !this.state.addEmailAddresses.isOpen
      }
    });
  };

  render() {
    const { activeTab, title } = this.state;
    return (
      <div id="top" className="template-exercise-details page-group-details">
        <MainHeader />
        <main>
          <section className="section">
            <div className="section-inner section-inner-group-details">
              <header className="App-header">
                <h5 className="label">
                  <Link to="/moje">
                    <Trans i18nKey="exerciseDetail.my">Moje třídy</Trans>
                  </Link>{" "}
                  /{" "}
                  <Trans i18nKey="exerciseDetail.assignedExercise">Třída</Trans>
                </h5>
                <br />
                <h1 className="App-title d-inline-block">
                  <span className="ml-2">{title}</span>
                </h1>
                <span className="ml-2">
                  <Trans i18nKey="classList.header">Detail třídy</Trans>
                </span>
              </header>
              <div className="container-fluid">
                <div>
                  <Row>
                    <Col>
                      <Nav tabs>
                        <NavItem>
                          <NavLink
                            className={classnames({
                              active: activeTab === "1"
                            })}
                            onClick={() => {
                              this.toggleTab(1);
                            }}
                          >
                            <Trans i18nKey="exerciseDetail.submitted">
                              Odevzdaná cvičení
                            </Trans>
                          </NavLink>
                        </NavItem>
                        <NavItem>
                          <NavLink
                            className={classnames({
                              active: activeTab === "2"
                            })}
                            onClick={() => {
                              this.toggleTab(2);
                            }}
                          >
                            <Trans i18nKey="exerciseDetail.classMembers">
                              Členové třídy
                            </Trans>
                          </NavLink>
                        </NavItem>
                      </Nav>
                      <TabContent activeTab={activeTab}>
                        <TabPane tabId="1">
                          <ClassForm />
                        </TabPane>
                        <TabPane tabId="2">
                          <EmailAddresses
                            emailAddresses={[]}
                            addEmailAddress={this.addEmailAddressToGroup}
                            deleteEmailAddress={
                              this.deleteEmailAddressFromGroup
                            }
                          />
                          <ButtonGroup>
                            <Button
                              onClick={this.toggleAddEmailAddresses}
                              outline
                              color="secondary"
                            >
                              <Trans i18nKey="exerciseDetail.addEmailAddresses">
                                Přidat emailové adresy do třídy
                              </Trans>
                            </Button>
                          </ButtonGroup>
                        </TabPane>
                      </TabContent>
                    </Col>
                  </Row>
                </div>
              </div>
            </div>
          </section>
        </main>
      </div>
    );
  }
}

export default GroupDetail;
