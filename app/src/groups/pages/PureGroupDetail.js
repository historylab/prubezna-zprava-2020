import React from "react";
import classnames from "classnames";
import ClassForm from "../components/ClassForm";
import DataService from "../../DataService";
//import EmailAddressesDialog from "./EmailAddressesDialog";
import EmailAddresses from "../../exercises/EmailAddresses";
//import DeleteExerciseDialog from "./DeleteExerciseDialog";
import MainHeader from "../../MainHeader";
import { Trans } from "react-i18next";
import {
  TabContent,
  TabPane,
  Nav,
  Row,
  Col,
  Button,
  ButtonGroup,
  NavItem,
  NavLink
} from "reactstrap";

const PureGroupDetail = ({ group, activeTab, toggleTab, addEmailAddressToGroup, deleteEmailAddressFromGroup }) => {
  const isNew = !(group && group.id);
  return (
    <main>
      <section className="section">
        <div className="section-inner section-inner-group-details">
          <header className="App-header">
            <h5 className="label">
              <a href="/moje">
                <Trans i18nKey="exercisaeDetail.my">Moje třídy</Trans>
              </a>{" "}
              / <Trans i18nKey="exerciseaDetail.assignedExercise">{isNew ? "Nová třída" : "Třída (" + group.id + ")"}</Trans>
            </h5>
          </header>
          <div className="container-fluid">
            <div>
              <Row>
                <Col>
                  <Nav tabs>
                    <NavItem>
                      <NavLink
                        className={classnames({
                          active: activeTab === "1"
                        })}
                        onClick={() => {
                          toggleTab(1);
                        }}
                      >
                        <Trans i18nKey="exercieDetail.submitted">
                          Detaily třídy
                        </Trans>
                      </NavLink>
                    </NavItem>
                    {group && group.id > 0 && (
                    <NavItem>
                      <NavLink
                        className={classnames({
                          active: activeTab === "2"                          
                        })}                        
                        onClick={() => {
                          toggleTab(2);
                        }}
                      >
                        <Trans i18nKey="exerciseDetail.classMembers">
                          Členové třídy
                        </Trans>
                      </NavLink>
                    </NavItem>
                    )}
                  </Nav>                  
                  <TabContent activeTab={activeTab}>
                    <TabPane tabId="1">
                      <ClassForm />
                    </TabPane>
                    <TabPane tabId="2">
                      <EmailAddresses
                        emailAddresses={group.emailAddresses}
                        addEmailAddress={addEmailAddressToGroup}
                        deleteEmailAddress={deleteEmailAddressFromGroup}
                      />
                      <ButtonGroup>
                        <Button
                          //onClick={toggleAddEmailAddresses}
                          outline
                          color="secondary"
                        >
                          <Trans i18nKey="exerciseDetail.addEmailAddresses">
                            Přidat emailové adresy do třídy
                          </Trans>
                        </Button>
                      </ButtonGroup>
                    </TabPane>
                  </TabContent>
                </Col>
              </Row>
            </div>
          </div>
        </div>
      </section>
    </main>
  );
};

export default PureGroupDetail;
