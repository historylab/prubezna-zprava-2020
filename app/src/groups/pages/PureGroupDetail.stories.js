import React from "react";
import { action } from "@storybook/addon-actions";
import "./../../css/index.css";

import PureGroupDetail from "./PureGroupDetail";

export default {
  component: PureGroupDetail,
  title: "Pure Group Detail",
  // Our exports that end in "Data" are not stories.
  excludeStories: /.*Data$/
};

export const taskData = {
  id: "1",
  title: "Test Task",
  state: "TASK_INBOX",
  updatedAt: new Date(2018, 0, 1, 9, 0),
  group: {}
};

export const actionsData = {
  onPinTask: action("onPinTask"),
  onArchiveTask: action("onArchiveTask")
};

export const Tab1 = () => (
  <PureGroupDetail activeTab="1" group={{ id: 1, name: "1.A", emailAddresses: [] }} />
);

export const Tab2 = () => (
  <PureGroupDetail activeTab="2" group={{ id: 1, name: "1.A", emailAddresses: [{emailAddress: 'Jan Novák (email@email.em)', group_id: 1}, {emailAddress: 'email@email.em'}] }} />
);

export const New = () => (
  <PureGroupDetail activeTab="1" group={{ name: "1.A", emailAddresses: [] }} />
);

export const Loading = () => (
  <PureGroupDetail activeTab="1" loading={true} />
);