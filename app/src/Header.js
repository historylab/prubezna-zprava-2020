import React from "react";
import "./css/Header.css";

const Catalog = ({ title, children }) => {
  return (
    <header>
      <h1>{title}</h1>
      {children}
    </header>
  );
};

export default Catalog;
