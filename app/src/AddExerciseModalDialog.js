import React from "react";
//import QRCode from "qrcode.react";
import {
  Progress,
  ModalHeader,
  ModalBody,
  Modal,
  Button,
  ModalFooter,
  Container,
  Row,
  Badge,
  Col
} from "reactstrap";
import ExerciseIcon from "./ExerciseIcon";

const AddExerciseModalDialog = ({
  pending,
  modal,
  toggle,
  exercise,
  onSelected
}) => (
  <Modal isOpen={modal} toggle={toggle} size="lg" backdrop centered>
    <ModalHeader toggle={toggle}>Přidat cvičení</ModalHeader>
    <ModalBody>
      <Container>
        <Row>
          <Col>
            <h2>{exercise.name}</h2>
            {exercise.metadata.anotace.verejna}
          </Col>
          <Col>
            <img width={318} src={exercise.image_url} />
            <div>
              <Badge color="primary">Obraz</Badge>
              <Badge color="secondary">Historie 20.století</Badge>
              <Badge color="success">Československo</Badge>
            </div>
            <div>0:25</div>
          </Col>
        </Row>
        <Row>
          <Col>
            <h5 className="text-center">
              <a target="_blank" href={exercise.url} rel="noopener noreferrer">
                Vyzkoušejte si cvičení sami!
              </a>
            </h5>
          </Col>
        </Row>
      </Container>
    </ModalBody>
    {pending && (
      <ModalFooter>
        <Container>
          <Row>
            <Col>
              <div className="text-center">Prosím, počkejte ...</div>
              <Progress animated striped color="success" value="100" />
            </Col>
          </Row>
        </Container>
      </ModalFooter>
    )}
    {!pending && (
      <ModalFooter>
        <Button
          color="primary"
          onClick={() => onSelected(exercise, "individual")}
        >
          <ExerciseIcon type="user" /> Individuální cvičení
        </Button>
        <Button color="secondary" onClick={() => onSelected(exercise, "group")}>
          <ExerciseIcon type="group" /> Skupinové cvičení
        </Button>
        <Button color="secondary" onClick={() => onSelected(exercise, "class")}>
          <ExerciseIcon type="class" /> Cvičení ve třídě s celou třídou
        </Button>
      </ModalFooter>
    )}
  </Modal>
);

export default AddExerciseModalDialog;
