import withAuthentication from "./withAuthentication";
import withNotification from "./withNotification";
import { withTranslation } from "react-i18next";

export default Component => withTranslation() (
    withNotification(withAuthentication(Component))
);