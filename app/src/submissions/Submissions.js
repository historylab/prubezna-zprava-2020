import React from "react";
import { Table, Badge, Button } from "reactstrap";
import Moment from "moment";
import "moment/min/locales";
import { extendMoment } from "moment-range";
import { If } from "./../If";
import Evaluation from "./Evaluation";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { withTranslation } from "react-i18next";
import { Trans } from "react-i18next";

class Submissions extends React.Component {
  constructor(props) {
    super(props);

    this.dismissCallback = null;

    this.state = {
      modal: false,
      modalNote: false,
      submission: {}
    };
  }

  toggle = () => {
    this.setState({
      modal: !this.state.modal
    });
  };

  toggleNoteModal = submission => {
    this.evaluationDialog.setSumbission(submission, () => {
      this.setState({
        modalNote: !this.state.modalNote
      });
    });
  };

  confirmDeleteEmailAddress = (e, submission) => {
    this.setState({
      modal: !this.state.modal,
      emailAddress: submission.emailAddresses[0]
    });
  };

  shareWithOthers = (e, submission) => {
    this.props.shareWithOthers(submission);
  };

  deleteSubmission = () => {
    const { emailAddress } = this.state;
    return this.props.onSubmissionDelete(emailAddress).then(_ => {
      this.setState({
        modal: false
      });
    });
  };

  evaluate = evaluation => {
    return this.props.evaluate(evaluation).then(_ => {
      this.setState({
        modalNote: false
      });
    });
  };

  render() {
    const { t, submissions, enableShare } = this.props;

    if (submissions.entries.length === 0) {
      return (
        <div>
          <h2>
            <Trans i18nKey="submissions.noSubmissions">
              Nikdo ještě nic neodevzdal ...
            </Trans>
          </h2>
          <p>
            <Trans i18nKey="submissions.subheader">
              Ale mezitím můžete přidat emailové adresy žáků, od nichž očekáváte
              odevzdání.
            </Trans>
          </p>
        </div>
      );
    }

    const moment = extendMoment(Moment);

    const dateFunc = s => {
      const start = moment(s);
      const end = moment();
      const r = moment.range(start, end);
      const days = r.diff("days");
      if (days <= 7) {
        return start.locale("cs").calendar();
      }
      return start.format("DD.MM.YYYY HH:mm");
    };

    const padStart = (s, count, char) => {
      return ("000" + s).slice(-count);
    };

    const toTimeString = ts => {
      let timeSpent = Math.floor(ts / 1000); // seconds
      const sd = 60 * 60 * 24;
      const sh = 60 * 60;
      const days = Math.floor(timeSpent / sd);
      timeSpent -= sd * days;
      const hours = Math.floor(timeSpent / sh);
      timeSpent -= sh * hours;
      const minutes = Math.floor(timeSpent / 60);
      let seconds = timeSpent - 60 * minutes;

      let r = `${padStart(hours, 2, "0")}:${padStart(
        minutes,
        2,
        "0"
      )}:${padStart(seconds, 2, "0")}`;
      if (days > 0) {
        return `${days}d ${r}`;
      }
      return r;
    };

    return (
      <Table striped borderless>
        <thead>
          <tr>
            <th>#</th>
            <th>
              <Trans i18nKey="submissions.student">Student</Trans>
            </th>
            <th />
            <th>
              <Trans i18nKey="submissions.submittedOn">Odevzdáno</Trans>
            </th>
            <th>
              <Trans i18nKey="submissions.timeSpent">Strávený čas</Trans>
            </th>
            <th>
              <Trans i18nKey="groupSubmission.evaluation">Hodnocení</Trans>
            </th>
          </tr>
        </thead>
        <tbody>
          {submissions.entries.map((submission, index) => {
            submission.isValid = submission.entryId > 0;
            return (
              <tr key={index}>
                <th scope="row">{index + 1}</th>
                <td>
                  {submission.emailAddresses.map((e, i) => {
                    return e.name ? (
                      <span key={`{index}_{i}`}>
                        {i > 0 ? ", " : ""}
                        {e.name} ({e.emailAddress})
                      </span>
                    ) : (
                      <span key={`{index}_{i}`}>
                        {i > 0 ? ", " : ""}
                        {e.emailAddress}
                      </span>
                    );
                  })}
                </td>
                <td>
                  <If condition={submission.isValid}>
                    <a
                      target="_blank"
                      href={submission.viewUrl}
                      rel="noopener noreferrer"
                    >
                      <Button outline color="primary" size="sm">
                        <FontAwesomeIcon icon="eye" />{" "}
                        <Trans i18nKey="groupSubmission.view" />
                      </Button>
                    </a>
                  </If>
                </td>
                <td className="text-truncate">
                  {(function () {
                    if (submission.isValid > 0) {
                      moment().locale("cs");
                      return dateFunc(submission.lastUpdated);
                    } else {
                      return (
                        <Badge color="warning">
                          <Trans i18nKey="submissions.notSubmitted" />
                        </Badge>
                      );
                    }
                  })()}
                </td>
                <td className="text-truncate">
                  {(function () {
                    if (submission.timeSpent) {
                      return toTimeString(submission.timeSpent);
                    } else {
                      return null;
                    }
                  })()}
                </td>
                <td>
                  <If condition={submission.isValid}>
                    <Button
                      size="sm"
                      onClick={e => this.toggleNoteModal(submission)}
                    >
                      {submission.note
                        ? t("submissions.edit")
                        : t("submissions.add")}
                    </Button>
                  </If>
                </td>
                <td>
                  <If condition={submission.isValid && enableShare}>
                    <Button
                      outline
                      size="sm"
                      onClick={e => this.shareWithOthers(e, submission)}
                      title={t(
                        "submissions.shareWithOthers",
                        "Sdílet toto odeslané cvičení s ostatními"
                      )}
                    >
                      <FontAwesomeIcon icon="share" /> <Trans i18nKey="share" />
                    </Button>
                  </If>
                </td>
              </tr>
            );
          })}
        </tbody>
        <Evaluation
          isOpen={this.state.modalNote}
          toggle={this.toggleNoteModal}
          submission={this.state.submission}
          evaluate={this.evaluate}
          ref={ref => {
            this.evaluationDialog = ref;
          }}
        />
      </Table>
    );
  }
}

export default withTranslation()(Submissions);

/*
        <DeleteEmailDialog
          isOpen={this.state.modal}
          emailAddress={this.state.emailAddress}
          toggle={this.toggle}
          deleteSubmission={this.deleteSubmission}
        />*/
