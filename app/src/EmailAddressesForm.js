import React from "react";
import { Button, FormGroup, Label, FormFeedback, Input } from "reactstrap";
import { withTranslation, Trans } from "react-i18next";
import i18n from "i18next";

class EmailAddressesForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      emailAddresses: "",
      formErrors: { emailAddresses: "" },
      emailAddressesValid: false,
      formValid: false,
      error: null,
      pending: false
    };
  }

  onClick = () => {
    const { exercise, addEmailAddresses } = this.props;
    const { emailAddresses } = this.state;

    addEmailAddresses(exercise, emailAddresses);
  };

  handleChange = event => {
    const name = event.target.name;
    const value = event.target.value;
    this.setState({ [name]: value }, () => {
      this.validateField(name, value);
    });
  };

  validateField = (fieldName, value) => {
    let fieldValidationErrors = this.state.formErrors;
    let emailAddressesValid = this.state.emailAddressesValid;

    switch (fieldName) {
      case "emailAddresses":
        const regex = /^([\w.-]+@([\w-]+)\.+\w{2,})(\s*[|;|,]\s*([\w.-]+@([\w-]+)\.+\w{2,}))*/;
        emailAddressesValid = value.trim().match(regex);
        fieldValidationErrors.emailAddresses = emailAddressesValid
          ? ""
          : i18n.t("emailAddressesForm.emailAddresses.invalidFormat");
        break;
      default:
        break;
    }
    this.setState(
      {
        formErrors: fieldValidationErrors,
        emailAddressesValid
      },
      this.validateForm
    );
  };

  validateForm = () => {
    this.setState({
      formValid: this.state.emailAddressesValid
    });
  };

  render() {
    const { t } = this.props;
    return (
      <div>
        <FormGroup>
          <Label for="emailAddresses">
            <Trans i18nKey="emailAddressesForm.emailAddresses.label">
              Emailové adresy žáků
            </Trans>
          </Label>
          <Input
            name="emailAddresses"
            id="emailAddresses"
            value={this.state.emailAddresses}
            invalid={!this.state.emailAddressesValid}
            valid={!!this.state.emailAddressesValid}
            onChange={this.handleChange}
            placeholder={t("emailAddressesForm.emailAddresses.placeholder")}
          />
          <FormFeedback>{this.state.formErrors.emailAddresses}</FormFeedback>
        </FormGroup>
        <Button onClick={this.onClick} disabled={!this.state.formValid}>
          <Trans i18nKey="emailAddressesForm.add">Přidat emailové adresy</Trans>
        </Button>
      </div>
    );
  }
}

export default withTranslation()(EmailAddressesForm);
