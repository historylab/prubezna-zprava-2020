import React, { useState } from "react";
import Tour from "reactour";
import { Button } from "reactstrap";
import { Trans } from "react-i18next";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function withTour(Component, stepsFunc) {
  return function Inner(props) {
    const [isTourOpen, setIsTourOpen] = useState(false);
    const steps = stepsFunc(props.t);
    return (
      <div>
        <Button className="btn-tutorial" onClick={() => setIsTourOpen(true)}>
          <FontAwesomeIcon icon="lightbulb" />
          <Trans i18nKey="tutorial.button.label">Nápověda</Trans>
        </Button>
        <Component {...props} />
        <Tour
          startAt={0}
          isOpen={isTourOpen}
          steps={steps}
          onRequestClose={() => setIsTourOpen(false)}
        />
      </div>
    );
  };
}
