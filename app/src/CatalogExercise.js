import React from "react";
import { CardImg } from "reactstrap";
import { Link } from "react-router-dom";
import { If } from "./If";
import "./css/Catalog.css";
import LazyLoad from "react-lazy-load";

const CatalogExercise = ({ exercise, location }) => {
  const { name, metadata, thumb_image_url, catalogUrl } = exercise;
  const { anotace, autor } = metadata;

  const topics = Object.values(metadata.klicovaSlova || {}) || "";
  const hasTopics = topics.length > 0 && topics[0].length > 1;

  let description = (anotace || { verejna: "" }).verejna;

  description =
    description.length < 110
      ? description
      : description.substring(0, 110) + "...";

  return (
    <div className="grid-item">
      <Link
        to={{
          pathname: catalogUrl,
          state: {
            exercise,
            returnTo: location.pathname
          }
        }}
        className="grid-item-inner card"
      >
        <div className="card-image">
          <LazyLoad debounce={false} offsetVertical={300}>
            <CardImg
              className="card-image-file"
              src={thumb_image_url}
              alt="Image"
            />
          </LazyLoad>
        </div>
        <div className="card-text">
          <div className="card-authors">{autor.join(", ")}</div>
          <h3 className="card-title">{name}</h3>
          <If condition={hasTopics}>
            <div className="taxonomy-container topics">
              <ul className="taxonomy">
                {topics.map((tag, i) => (
                  <li key={i} className="taxonomy-item">
                    {tag}
                  </li>
                ))}
              </ul>
            </div>
          </If>
          <div className="card-description">{description}</div>
        </div>
      </Link>
    </div>
  );
};

export default CatalogExercise;
