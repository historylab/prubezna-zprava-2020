import React from "react";
import QRCode from "qrcode.react";
import {
  ModalHeader,
  ModalBody,
  Modal,
  Button,
  ModalFooter,
  Container,
  Row,
  Col
} from "reactstrap";

const ExerciseDetailWithQRCode = ({ modal, toggle, exercise }) => (
  <Modal isOpen={modal} toggle={toggle} backdrop centered>
    <ModalHeader toggle={toggle}>{exercise.name}</ModalHeader>
    <ModalBody>
      <Container>
        <Row>
          <Col xs="4">
            <QRCode size={150} value={exercise.url + "/" + exercise.code} />
          </Col>
          <Col>
            <h1 className="text-center text-uppercase">{exercise.code}</h1>
          </Col>
        </Row>
        <Row>
          <Col>
            <h5 className="text-center">
              <a href={exercise.url}>
                {exercise.url}/{exercise.code}
              </a>
            </h5>
          </Col>
        </Row>
      </Container>
    </ModalBody>
    <ModalFooter>
      <Button color="secondary" onClick={toggle}>
        Zavřít
      </Button>
    </ModalFooter>
  </Modal>
);

export default ExerciseDetailWithQRCode;
