import React from "react";
import { action } from "@storybook/addon-actions";
import { ApplicationStateProvider } from "./ApplicationStateProvider";

import MainHeader from "./MainHeader";

const appStateDecorator = Story => (
  <ApplicationStateProvider value={{ user: {} }}>
    <Story />
  </ApplicationStateProvider>
);

export default {
  component: MainHeader,
  title: " Main header",
  decorators: [appStateDecorator],
  // Our exports that end in "Data" are not stories.
  excludeStories: /.*Data$/
};

export const Default = () => <MainHeader/>;
// How to? isTestEnvironment = 1
// export const Public = () => {
//   return <MainHeader className="header--test"/>;
// }

