import React from "react";
import { ModalHeader, ModalBody, Modal } from "reactstrap";

const ModalDialog = ({ children }) => {
  //const { state = {} } = location;
  //const { modal } = state;
  //const exercise = {};
  return (
    <Modal isOpen={true} size="lg" backdrop centered>
      <ModalHeader>Detail cvičení</ModalHeader>
      <ModalBody>{children}</ModalBody>
    </Modal>
  );
};

export default ModalDialog;
