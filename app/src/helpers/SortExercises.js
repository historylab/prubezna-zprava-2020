/**
 * Class which provide sorting exercises. Is used in component FilerBar.
 */
class SortExercises {

  /**
   * Helper function to decide which exercise have all the selected kw from selects.
   *
   * @param metadataKw - kw which are in exercise metadata (in klicovaSlova)
   * @param filterParameterKW - list of kw which are selected for sorting
   * @returns {boolean} - returns if exercise meets all the requirements
   */
  keywordsSort(metadataKw, filterParameterKW) {
    let result = true;
    Object.entries(metadataKw).forEach(entry => {
      const [nazev, polozky] = entry;
      if (filterParameterKW[nazev].length === 0) {
        return;
      }
      result = result && filterParameterKW[nazev].every(kw => {
        return polozky.some(fKw => fKw === kw)
      })
    });
    return result
  }

  /**
   * Main function to filter exercise by text, oreder and selected kw.
   *
   * @param exercises - list of all exercises
   * @param filterPar - object which contain parameters for sorting
   *
   * @returns {object} fileredResult - returns all exercise which meet all the requirements.
   */
  sortExercises(exercises, filterPar) {
    if (exercises === undefined || exercises.length === 0) return exercises;

    // Filtering by text
    let filteredResult = exercises.filter(exercise => {
      let nazevLowerCase = exercise.metadata.nazev.toLowerCase();
      return nazevLowerCase.includes(filterPar.text.toLowerCase());
    });

    // Filtering by kw
    filteredResult = filteredResult.filter(exercise =>
      this.keywordsSort(
        exercise.metadata.klicovaSlova,
        filterPar.keywords
      )
    );

    // Filtering by order - ASC / DECS
    if (filterPar.order === "ASC") {
      filteredResult.sort((exerciseA, exerciseB) =>
        exerciseA.metadata.nazev.localeCompare(exerciseB.metadata.nazev)
      );
    } else {
      filteredResult.sort((exerciseA, exerciseB) =>
        exerciseB.metadata.nazev.localeCompare(exerciseA.metadata.nazev)
      );
    }

    return filteredResult;
  }
}

export default SortExercises;
