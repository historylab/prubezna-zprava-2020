import React from "react";
import Header from "./../Header";
import { Link } from "react-router-dom";
import { Button, Table } from "reactstrap";
import DataService from "./../DataService";
import withAuthentication from "./../withAuthentication";
import withNotification from "./../withNotification";
import MainHeader from "./../MainHeader";
import MainFooter from "./../MainFooter";

class UserList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      modal: false,
      error: null,
      hasData: false,
      users: []
    };
  }

  refresh = () => {
    this.setState({ error: null, isLoading: true });

    new DataService()
      .fetchUserList()
      .then(result => {
        this.setState({
          users: result.data || result,
          error: null,
          hasData: true,
          isLoading: false
        });
      })
      .then(this.processNotifications)
      .catch(this.onError);
  };

  processNotifications = data => {
    if (data) {
      (data.notifications || []).forEach(notification => {
        this.props.notify(notification);
      });
    }
  };

  onError = error => {
    this.setState({
      error,
      isLoading: false
    });
    this.props.notify(error);
  };

  componentDidMount() {
    this.refresh();
  }

  componentDidUpdate(prevProps, prevState) {
    if (!prevProps.authenticated && this.props.authenticated) {
      this.refresh();
    }
  }

  render() {
    const { hasData, users } = this.state;

    return (
      <div id="top" className="template-users page-users">
        <MainHeader />
        <main>
          <section className="section">
            <div className="section-inner-user">
              <Header title="Uživatelé">
                {!hasData && (
                  <Button id="refreshButton" onClick={this.refresh}>
                    Obnovit
                  </Button>
                )}
              </Header>
              <Table striped borderless>
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Uživatelské jméno</th>
                    <th>Jméno</th>
                    <th>Příjmení</th>
                    <th>Emailová adresa</th>
                    <th>Potvrzen</th>
                    <th>Pozastaven</th>
                  </tr>
                </thead>
                <tbody>
                  {users.map((user, index) => {
                    const {
                      id,
                      userName,
                      firstName,
                      lastName,
                      emailAddress,
                      confirmed,
                      suspended
                    } = user;
                    return (
                      <tr key={index}>
                        <th scope="row">{id}</th>
                        <td>
                          <Link to={`/uzivatel/${id}`}>{userName}</Link>
                        </td>
                        <td>{firstName}</td>
                        <td>{lastName}</td>
                        <td>{emailAddress}</td>
                        <td>{confirmed ? "ano" : "ne"}</td>
                        <td>{suspended ? "ano" : "ne"}</td>
                      </tr>
                    );
                  })}
                </tbody>
              </Table>
            </div>
          </section>
        </main>
        <MainFooter />
      </div>
    );
  }
}

export default withNotification(withAuthentication(UserList));
