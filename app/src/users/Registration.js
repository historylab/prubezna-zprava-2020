import React from "react";
import { Jumbotron } from "reactstrap";
import RegistrationForm from "./RegistrationForm";
import MainHeader from "./../MainHeader";
import DataService from "./../DataService";
import { withTranslation, Trans } from "react-i18next";
import i18n from "i18next";
import withNotification from "./../withNotification";

class Registration extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isPending: false };
  }

  onSubmit = (registrationData, callback) => {
    this.setState({ error: null, isPending: true });
    registrationData.language = (i18n.language || "").toLowerCase();
    new DataService()
      .registerUser(registrationData)
      .then(result => {
        const data = result.data;
        this.setState({
          isPending: false
        });

        if (data && data.error) {
          if (!callback(data.error)) {
            this.onError(data.error);
          }
          return result;
        }
        this.props.history.push("/moje");
        return result;
      })
      .then(this.props.processNotifications)
      .catch(this.onError);
  };

  onError = error => {
    this.setState({
      error,
      isPending: false
    });

    this.props.notify(error);
  };

  render() {
    const { isPending } = this.state;
    return (
      <div>
        <MainHeader />
        <Jumbotron className="text-center">
          <h1 className="display-3">
            <Trans i18nKey="signUp.header">Registrace učitele</Trans>
          </h1>
          <div className="text-left">
            <RegistrationForm onSubmit={this.onSubmit} isPending={isPending} />
          </div>
        </Jumbotron>
      </div>
    );
  }
}

export default withTranslation()(withNotification(Registration));
