import React from "react";
import {CardImg} from "reactstrap";
// import { Link } from "react-router-dom";
// import { If } from "../If";
class TimelineExercise extends React.Component {
  constructor(props) {
    super(props);
    // const keyWords = exercise.klicovaSlova || ""; // TODO
    // const hasKeyWords = keyWords.length > 0 && keyWords[0].length > 1;
    this.description =
      this.props.exercise.metadata.anotace.verejna.length < 110
        ? this.props.exercise.metadata.anotace.verejna
        : this.props.exercise.metadata.anotace.verejna.substring(0, 110) + "...";
    this.times = this.props.exercise.metadata.casovaOsa.roky.join(", ");
    this.makeKeywords = (...types) => {
      if (this.props.exercise.id !== 2) {
        return;
      }
      let kwArray = [];
      types.forEach(type => {
        if (this.props.exercise.klicovaSlova === undefined) return;
        const found = this.props.exercise.klicovaSlova.find(
          el => el.nazev === type
        );
        found.polozky.forEach(polozka => {
          kwArray.push(polozka);
        });
      });
      return kwArray.join(", ");
    };
    this.makeDejiny = () => {
      let poll = [];
      this.props.exercise.metadata.casovaOsa.epochy.forEach(element => {
        if (element.obdobi.length > 0) {
          poll.push(element.nazev);
        }
      });
      return poll.join(", ");
    };
    this.makeDifficulty = difficulty => {
      if (difficulty === "Nízka") {
        return (
          <div className="card-difficulty" title="Jednoduché cvičení">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
              <circle cx="12" cy="12" r="10"/>
              <path d="M8 14s1.5 2 4 2 4-2 4-2"/>
              <line x1="9" y1="9" x2="9.01" y2="9"/>
              <line x1="15" y1="9" x2="15.01" y2="9"/>
            </svg>
          </div>
        );
      }
      return "";
    };
  }

  render() {
    return (
      <div
        className="exercise-item"
        onClick={e => this.props.onClick(e, this.props.exercise)}
        data-cy="exercise-item"
      >
        <div className="grid-item-inner card">
          <div className="card-image">
            <CardImg
              className="card-image-file"
              src={this.props.exercise.thumb_image_url}
              alt="Image"
            />
          </div>

          {this.makeDifficulty(this.props.exercise.obtiznost)}

          <div className="card__meta">
            <div className="card__meta__item card__meta--history">
              {this.makeDejiny()}
            </div>
            <div className="card__meta__item card__meta--year">
              {this.times}
            </div>
            <div className="card__meta__item card__meta--keywords">
              <ul className="keyword">{this.makeKeywords("rvp", "big6")}</ul>
            </div>
          </div>

          <div className="card-text">
            <h3 className="card-title">{this.props.exercise.name}</h3>
          </div>
        </div>
      </div>
    );
  }
}

export default TimelineExercise;
