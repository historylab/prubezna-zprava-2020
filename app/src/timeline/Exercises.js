import React from "react";
import withNotification from "../withNotification";
import { Trans, withTranslation } from "react-i18next";
import ExerciseIcon from "../ExerciseIcon";
import { Button } from "reactstrap";
import TimelineExercise from "./TimelineExercise";
import "./scss/TimelineExercises.scss";
import slugify from "slugify";

class Exercise extends React.Component {
  state = {
    exerciseDetail: {
      visibility: "hidden",
      data: "",
      scroll: ""
    },
    error: null,
    timelineDetailData: this.props.timelineDetailData,
    exercise: {},
    mounted: false,
    allExercises: [],
    exercisesInEpochs: [],
    exerciseInDecades: []
  };

  constructor(props) {
    super(props);
    this.slugifyConfig = { lower: true, strict: true };
    this.handleScroll = this.handleScroll.bind(this);
  }

  componentDidMount() {
    this.setState({ mounted: true });
    window.addEventListener("wheel", this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener("wheel", this.handleScroll);
  }

  handleScroll(event) {
    if (this.state.exerciseDetail.visibility === "hidden") {
      return;
    }
    this.handleExerciseDetailEvent();
  }

  componentDidUpdate() {
    if (
      JSON.stringify(this.state.allExercises) !==
      JSON.stringify(this.props.loadedExercises)
    ) {
      this.setState({
        allExercises: this.props.loadedExercises,
        exerciseInDecades: this.makeArrayOfExercisesInDecades(
          this.props.loadedExercises
        ),
        exercisesInEpochs: this.makeArrayOfExercisesInEpochs(
          this.props.loadedExercises
        )
      });
    }
  }

  /**
   * Render authors in detail cviceni
   */
  renderAuthors() {
    const { t } = this.props;
    const data = this.state.exerciseDetail.data;
    if (data.metadata !== undefined) {
      let exerciseAuthors = data.metadata.autor;
      if (exerciseAuthors[0] !== "") {
        const typeOfAutors =
          exerciseAuthors.length > 1 ? "exerciseByMany" : "exerciseByOne";
        return (
          <small>
            <span>{t(`timeline.exerciseDetail.${typeOfAutors}`) + " "}</span>
            <span cy-data="exercise-detail-author">
              {exerciseAuthors.join(", ")}
            </span>
          </small>
        );
      }
    }
    return (
      <small>
        <span>{t(`timeline.exerciseDetail.exerciseByOne`) + " "}</span>
        <span cy-data="exercise-detail-author">
          {t(`timeline.exerciseDetail.authorNotYet`)}
        </span>
      </small>
    );
  }

  // TODO:
  // - `data.description` => `anotace.verejna`
  // - does not work why? https://fontawesome.com/icons/angle-right?style=solid
  render() {
    const { t } = this.props;
    return (
      <div
        className={
          "timeline__item timeline__exercises " +
          this.state.exerciseDetail.scroll
        }
      >
        <div className={"exercises " + this.state.exerciseDetail.scroll}>
          {this.generateCurrentExercises(this.selectExerciseData())}
        </div>
        <div
          className={"exercise-detail " + this.state.exerciseDetail.visibility}
          cy-data="exercise-detail"
        >
          <div className="detail__image">
            <img src={this.state.exerciseDetail.data.image_url} alt="" />
          </div>
          <div className="detail__hero">
            <div className="detail__close">
              <button onClick={this.handleExerciseDetailEvent.bind(this)}>
                <ExerciseIcon type="times" />
              </button>
            </div>
            <div className="detail__name">
              {this.renderAuthors(t)}
              <h1>{this.state.exerciseDetail.data.name}</h1>
            </div>
            {this.state.exerciseDetail.data !== "" && (
              <div className="detail__text">
                <p>
                  {JSON.stringify(
                    this.state.exerciseDetail.data.metadata.casovaOsa.roky
                  )}
                </p>
                <p>{this.state.exerciseDetail.data.url}</p>
                <p>
                  {JSON.stringify(
                    this.state.exerciseDetail.data.metadata.casovaOsa.epochy
                  )}
                </p>
                <p>{this.state.exerciseDetail.data.metadata.anotace.verejna}</p>
              </div>
            )}
            <div className="detail__buttons">
              <h6>
                <Trans i18nKey="catalogExerciseDetail.enrol">
                  Zadat cvičení třídě
                </Trans>
              </h6>
              <Button
                className="button secondary white"
                // onClick={() => this.onSelected("individual")}
              >
                <ExerciseIcon type="user" />
                <Trans i18nKey="catalogExerciseDetail.individual">
                  Individuální
                </Trans>
              </Button>
              <Button
                className="button secondary white mbm-0"
                // onClick={() => this.onSelected("group")}
              >
                <ExerciseIcon type="group" />
                <Trans i18nKey="catalogExerciseDetail.group">Skupinové</Trans>
              </Button>
              <Button
                className="button secondary white mbm-0"
                // onClick={() => this.onSelected("class")}
              >
                <ExerciseIcon type="class" />{" "}
                <Trans i18nKey="catalogExerciseDetail.class">
                  S celou třídou
                </Trans>
              </Button>
            </div>
          </div>
        </div>
      </div>
    );
  }

  /**
   * Sort every exercies into coresponding decades
   */
  makeArrayOfExercisesInDecades(allExercises) {
    const casovaOsaTimeline = this.props.timeSetup;
    let arrayOfExercisesInDecades = [];
    const timelineEnd = casovaOsaTimeline.end + casovaOsaTimeline.step;
    const timelineStart = casovaOsaTimeline.start - casovaOsaTimeline.step;
    const step = casovaOsaTimeline.step;
    const numberOfDecades =
      (timelineEnd - timelineStart) / casovaOsaTimeline.step;
    for (let index = 0; index < numberOfDecades; index++) {
      let arrayOfExercisesSelected = [];
      const decadeStart = timelineStart + step * index;
      const decadeEnd = timelineStart + step * (index + 1) - 1;
      for (let i = 0; i < allExercises.length; i++) {
        const findYear = year => {
          return year >= decadeStart && year <= decadeEnd;
        };
        if (
          allExercises[i].metadata.casovaOsa.roky.find(year => findYear(year))
        ) {
          arrayOfExercisesSelected.push(allExercises[i]);
        }
      }
      arrayOfExercisesInDecades.push(arrayOfExercisesSelected);
    }
    return arrayOfExercisesInDecades;
  }

  handleOnClickExercise(e, exercises) {
    var exerciseDetail = { ...this.state.exerciseDetail };
    exerciseDetail.visibility = "visible";
    exerciseDetail.data = exercises;
    exerciseDetail.scroll = "unscroll";
    document.body.style.overflowY = "hidden";
    this.setState({ exerciseDetail });
  }

  handleExerciseDetailEvent() {
    let exerciseDetail = { ...this.state.exerciseDetail };
    exerciseDetail.visibility = "hidden";
    exerciseDetail.data = "";
    exerciseDetail.scroll = "";
    document.body.style.overflowY = "";
    this.setState({ exerciseDetail });
  }

  /**
   * Make komponent exercise of all given exercises from server JSON data
   */
  generateCurrentExercises(exercises) {
    return exercises.map(exercise => {
      return (
        <TimelineExercise
          key={"tle_" + exercise.metadata.id}
          exercise={exercise}
          onClick={this.handleOnClickExercise.bind(this)}
          location={this.props.location}
        />
      );
    });
  }

  /**
   * Select specific data to render
   */
  selectExerciseData() {
    let selectedExercise = [];
    if (this.props.currentEpoch.active) {
      const generatedSlug = slugify(
        this.props.currentEpoch.name,
        this.slugifyConfig
      );
      this.state.exercisesInEpochs.forEach(epoch => {
        if (epoch.slug === generatedSlug) {
          selectedExercise = epoch.exercises;
        }
      });
      return selectedExercise;
    }
    this.props.currentDecadeExercises.forEach(decade => {
      if (this.state.exerciseInDecades[decade] === undefined) return; // ONLY FOR THE NOT WORKING ENGLISH THINGS TODO REMOVE
      if (this.state.exerciseInDecades[decade].length !== 0) {
        this.state.exerciseInDecades[decade].forEach(e => {
          if (!selectedExercise.includes(e)) selectedExercise.push(e);
        });
      }
    });
    return selectedExercise;
  }

  /**
   * Make array of epochs with exercises which belongs to coresponding epochs
   */
  makeArrayOfExercisesInEpochs(allExercises) {
    let allEpochs = [];
    if (this.state.timelineDetailData.timelineData === undefined) {
      return;
    }
    this.state.timelineDetailData.timelineData.forEach(position => {
      position.epochs.forEach(epoch => {
        const generatedSlugFromEpoch = slugify(epoch.name, this.slugifyConfig);
        const epochWithExercises = {
          slug: generatedSlugFromEpoch,
          exercises: this.findAllExerciseWithGivenEpoch(
            generatedSlugFromEpoch,
            allExercises
          )
        };
        allEpochs.push(epochWithExercises);
      });
    });
    return allEpochs;
  }

  /**
   *
   * Get array of epoch with exercises by slug of exercise
   *
   * @param {string} slug slug of epoch
   * @param {Array} allExercises all exercises
   */
  findAllExerciseWithGivenEpoch(slug, allExercises) {
    const arrayOfEpochBySlug = [];
    allExercises.forEach(entryExercise => {
      entryExercise.metadata.casovaOsa.epochy.forEach(epocha => {
        if (
          epocha.obdobi.find(
            epochInExercise =>
              slugify(epochInExercise, this.slugifyConfig) === slug
          )
        ) {
          arrayOfEpochBySlug.push(entryExercise);
        }
      });
    });
    return arrayOfEpochBySlug;
  }
}

export default withTranslation()(withNotification(Exercise));
