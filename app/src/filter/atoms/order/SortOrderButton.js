import React from "react";

import "./sortOrderButton.scss";

import {Button} from "react-bootstrap";
import {faSortAlphaDown, faSortAlphaUp} from '@fortawesome/free-solid-svg-icons'
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome"
import 'bootstrap/dist/css/bootstrap.min.css';

/**
 * Component for sort by order.
 */

export const SortOrderButton = ({backgroundColor, direction, onClick}) => {
  let icon;
  if (direction === "up") {
    icon = <FontAwesomeIcon id="basic-addon1" icon={faSortAlphaUp}/>
  } else {
    icon = <FontAwesomeIcon id="basic-addon1" icon={faSortAlphaDown}/>
  }
  return (
    <Button
      data-cy="filterOrderButton"
      variant="info"
      style={backgroundColor && {backgroundColor}}
      onClick={() => onClick(direction)}
    >{icon}
    </Button>
  );
};

SortOrderButton.defaultProps = {
  backgroundColor: null,
  direction: `down`,
};
