import React from "react";

import { SortOrderButton } from "./SortOrderButton";

export default {
  title: "Filter/Order Button",
  component: SortOrderButton,
  argTypes: {
    backgroundColor: { control: "color" },
    onClick: { action: "clicked" },
    direction:{
      control: {
        type: 'inline-radio',
        options: ['down', 'up'],
      }
    }
  }
};

const Template = args => <SortOrderButton {...args} />;

export const SortOrderButtonBasicUp = Template.bind({});
SortOrderButtonBasicUp.args = {
  direction: "up"
};

export const SortOrderButtonBasicDown = Template.bind({});
SortOrderButtonBasicDown.args = {
  direction: "down"
};