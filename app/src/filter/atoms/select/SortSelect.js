import React from "react";
import Select from 'react-select'

/**
 * Component for sort by select kw.
 */
export const SortSelect = ({options, placeholder, onChange}) => {
  return (
    <Select
      closeMenuOnSelect={false}
      isMulti
      options={options}
      placeholder={placeholder}
      onChange={onChange}
    />
  );
};

SortSelect.defaultProps = {};
