import React from "react";

import "./sortText.scss";
import {Form, InputGroup} from "react-bootstrap";
import {faSearch} from '@fortawesome/free-solid-svg-icons'
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome"
import 'bootstrap/dist/css/bootstrap.min.css';

/**
 * Sort exercises by text (input text)
 */
export const SortText = ({backgroundColor, placeholder, onChange}) => {
  return (
    <InputGroup className="mb-3">
      <InputGroup.Prepend>
        <InputGroup.Text id="basic-addon1">
          <FontAwesomeIcon id="basic-addon1" icon={faSearch}/>
        </InputGroup.Text>
      </InputGroup.Prepend>
      <Form.Control
        data-cy="filterText"
        style={backgroundColor && {backgroundColor}}
        type="text"
        placeholder={placeholder}
        onChange={e => {
          onChange(e.target.value)
        }}
      />
    </InputGroup>
  );
};

SortText.defaultProps = {
  backgroundColor: null,
  placeholder: 'Vyhledejte...',
};
