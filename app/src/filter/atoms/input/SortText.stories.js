import React from 'react';

import {SortText} from './SortText';

export default {
  title: 'Filter/Text filter input',
  component: SortText,
  argTypes: {
    backgroundColor: {control: 'color'},
    onChange: {action: 'changed'},
  },
};

const Template = (args) => <SortText {...args} />;

export const SortTextBasic = Template.bind({});
SortTextBasic.args = {
  placeholder: "Vyhledejte..."
};
