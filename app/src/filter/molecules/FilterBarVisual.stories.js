import React from "react";

import {FilterBarVisual} from "./FilterBarVisual";

export default {
  title: "Filter/Filter Bar",
  component: FilterBarVisual,
  argTypes: {
    backgroundColor: {control: "color"},
    onClick: {action: "clicked"}
  }
};

const Template = args => <FilterBarVisual {...args} />;

export const FilterBarVisualBasic = Template.bind({});
FilterBarVisualBasic.args = {
  sortTextSetup: {
    placeholder: "Vyhledejte...",
    onChange: (e) => {
      console.log(`Text changed to: ${e}`)
    }
  },
  sortOrderButton: {
    direction: "down",
    onClick: (e) => {
      console.log(`Sort order changed to: ${e}`)
    }
  },
  rvpSetup: {
    placeholder: "RVP klíčová slova",
    options: [
      {
        "value": "antisemitismus",
        "label": "antisemitismus"
      },
      {
        "value": "dekolonizace",
        "label": "dekolonizace"
      },
      {
        "value": "druhá světová válka",
        "label": "druhá světová válka"
      },
      {
        "value": "důsledky druhé světové války",
        "label": "důsledky druhé světové války"
      },
      {
        "value": "důsledky první světové války",
        "label": "důsledky první světové války"
      },
      {
        "value": "důsledky studené války",
        "label": "důsledky studené války"
      },
      {
        "value": "euroatlantická spolupráce",
        "label": "euroatlantická spolupráce"
      },
      {
        "value": "hospodářská krize",
        "label": "hospodářská krize"
      },
      {
        "value": "hospodářský vývoj ČSR",
        "label": "hospodářský vývoj ČSR"
      },
      {
        "value": "kolonialismus",
        "label": "kolonialismus"
      },
      {
        "value": "národnostní problémy ČSR",
        "label": "národnostní problémy ČSR"
      },
      {
        "value": "politický vývoj ČSR",
        "label": "politický vývoj ČSR"
      },
      {
        "value": "problémy současného světa",
        "label": "problémy současného světa"
      },
      {
        "value": "první světová válka",
        "label": "první světová válka"
      },
      {
        "value": "příčiny studené války",
        "label": "příčiny studené války"
      },
      {
        "value": "rasismus",
        "label": "rasismus"
      },
      {
        "value": "sociální problémy ČSR",
        "label": "sociální problémy ČSR"
      },
      {
        "value": "sport",
        "label": "sport"
      },
      {
        "value": "střetávání západního a východního bloku",
        "label": "střetávání západního a východního bloku"
      },
      {
        "value": "technika",
        "label": "technika"
      },
      {
        "value": "totalitní režimy",
        "label": "totalitní režimy"
      },
      {
        "value": "vzdělání",
        "label": "vzdělání"
      },
      {
        "value": "vznik ČSR",
        "label": "vznik ČSR"
      },
      {
        "value": "vznik České republiky",
        "label": "vznik České republiky"
      },
      {
        "value": "zábava",
        "label": "zábava"
      },
      {
        "value": "Československo 2. pol. 20. století",
        "label": "Československo 2. pol. 20. století"
      },
      {
        "value": "Československo 50. léta",
        "label": "Československo 50. léta"
      },
      {
        "value": "Československo 60. léta",
        "label": "Československo 60. léta"
      },
      {
        "value": "Československo 70. léta",
        "label": "Československo 70. léta"
      },
      {
        "value": "Československo 80. léta",
        "label": "Československo 80. léta"
      },
      {
        "value": "české země ve 20. století",
        "label": "české země ve 20. století"
      },
      {
        "value": "české země ve třech stoletích",
        "label": "české země ve třech stoletích"
      }
    ],
    onChange: (e) => {
      console.log(`RVP changed to: ${e}`)
    }
  },
  konceptSetup: {
    placeholder: "Konceptová klíčová slova",
    options: [
      {"value": "člověk a životní prostředí", "label": "člověk a životní prostředí"},
      {"value": "dějiny ve veřejném prostoru", "label": "dějiny ve veřejném prostoru"},
      {"value": "gender", "label": "gender"},
      {"value": "každodennost", "label": "každodennost"},
      {"value": "migrace", "label": "migrace"},
      {"value": "občanská společnost", "label": "občanská společnost"},
      {"value": "propaganda", "label": "propaganda"},
      {"value": "symboly", "label": "symboly"}, {"value": "umění", "label": "umění"},
      {"value": "vzpomínková kultura", "label": "vzpomínková kultura"}
    ],
    onChange: (e) => {
      console.log(`RVP changed to: ${e}`)
    }
  },
  b4Setup: {
    placeholder: "B4 klíčová slova",
    options: [
      {value: 'dobové perspektivy', label: 'dobové perspektivy'},
      {value: 'příčiny a důsledky', label: 'příčiny a důsledky'},
      {value: 'trvání a změna', label: 'trvání a změna'},
      {value: 'vztah k minulosti', label: 'vztah k minulosti'}
    ],
    onChange: (e) => {
      console.log(`B4 changed to: ${e}`)
    }
  },
  historylabSetup: {
    placeholder: "Historylabová klíčová slova",
    options: [
      {value: 'tvoříme', label: 'tvoříme'},
      {value: 'diskutujeme', label: 'diskutujeme'},
      {value: 'otevíráme otázku', label: 'otevíráme otázku'},
      {value: 'porovnáváme zdroje', label: 'porovnáváme zdroje'},
      {value: 'hledáme klíčové detaily', label: 'hledáme klíčové detaily'},
      {value: 'formulujeme a ověřujeme hypotézu', label: 'formulujeme a ověřujeme hypotézu'},
      {value: 'odhalujeme skrytý význam pramene', label: 'odhalujeme skrytý význam pramene'}
    ],
    onChange: (e) => {
      console.log(`Histoylab changed to: ${e}`)
    }
  }
};
