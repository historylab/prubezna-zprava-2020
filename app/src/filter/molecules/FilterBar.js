import React from "react";

import {FilterBarVisual} from "./FilterBarVisual";

import {withTranslation} from "react-i18next";

import SortExercise from "../../helpers/SortExercises";

/**
 * Component which provide filter of exercises (with logic).
 */
class FilterBar extends React.Component {
  constructor(props) {
    super(props);
    const {t} = this.props;
    this.state = {
      setup: {
        rvp: {
          placeholder: t("filter.rvp", "RVP klíčová slova"),
          options: [],
          onChange: kw => this.keywordsChange("rvp", kw),
        },
        koncept: {
          placeholder: t("filter.koncept", "Konceptová klíčová slova"),
          options: [],
          onChange: kw => this.keywordsChange("koncept", kw),
        },
        b4: {
          placeholder: t("filter.b4", "B4 klíčová slova"),
          options: [],
          onChange: kw => this.keywordsChange("b4", kw),
        },
        historylab: {
          placeholder: t("filter.historylab", "Historylabová klíčová slova"),
          options: [],
          onChange: kw => this.keywordsChange("historylab", kw),
        },
        text: {
          placeholder: t("filter.text", "Vyhledejte..."),
          options: [],
          onChange: this.changeTextAction,
        },
        order: {
          direction: "down",
          onClick: this.orderActionOnClick,
        }
      },
      parametersForSorting: {
        text: "",
        keywords: {
          rvp: [],
          koncept: [],
          b4: [],
          historylab: []
        },
        order: "ASC",
      },
      initDone: false,
    };

    this.SorterHelper = new SortExercise();
  }

  /**
   * After the kw is loaded from server, make the setup for filter bar.
   *
   * @param props - New props received from parent.
   */
  componentWillReceiveProps(props) {
    if (this.state.initDone) return
    if (props.allKeywords === null) return
    let setup = {...this.state.setup}
    Object.entries(props.allKeywords).forEach(entry => {
      const [key, value] = entry;
      setup[key].options = value.map(kw => {
        return {"value": kw, "label": kw}
      });
    })
    this.setState({setup, initDone: true})
  }

  render() {
    return (
      <FilterBarVisual
        rvpSetup={this.state.setup.rvp}
        konceptSetup={this.state.setup.koncept}
        b4Setup={this.state.setup.b4}
        historylabSetup={this.state.setup.historylab}
        sortTextSetup={this.state.setup.text}
        sortOrderButton={this.state.setup.order}
      />
    );
  }

  /**
   * Action for input change (when the text in component SortText changed, this function is called).
   *
   * @param {string} inputString
   */
  changeTextAction = (inputString) => {
    const parametersForSorting = {...this.state.parametersForSorting};
    parametersForSorting.text = inputString;
    this.setState({parametersForSorting}, () => {
      const {allExercises} = this.props;
      const filteredExercises = this.SorterHelper.sortExercises(
        allExercises,
        this.state.parametersForSorting
      );
      this.props.updateExerciseByFilter(filteredExercises);
    });
  }

  /**
   * Action for order change (when the button in component SortOrderButton is clicked, this function is called).
   */
  orderActionOnClick = () => {
    const setup = {...this.state.setup};
    const parametersForSorting = {...this.state.parametersForSorting};

    // Setup type of the order (and type of icon)
    if (setup.order.direction === "up") {
      setup.order.direction = "down";
      parametersForSorting.order = "ASC";
    } else {
      setup.order.direction = "up";
      parametersForSorting.order = "DESC";
    }

    this.setState({setup, parametersForSorting}, () => {
      const {allExercises} = this.props;
      const filteredExercises = this.SorterHelper.sortExercises(
        allExercises,
        this.state.parametersForSorting
      );
      this.props.updateExerciseByFilter(filteredExercises);
    });
  }

  /**
   * Action for all selects, when they are changed (when the values in component SortSelect changed, this function is called).
   *
   * @param {string} type - the type of the kw (rvp, koncept, b4, historylab)
   * @param {object} keywords - object which Select from react-select will send.
   */
  keywordsChange = (type, keywords) => {
    let parametersForSorting = {...this.state.parametersForSorting};
    // If there is no KW, react-select send null (not empty array), so there must be this check.
    if (keywords === null) {
      parametersForSorting.keywords[type] = []
    } else {
      // I only need the keywords, net the object => getting only strings of kw from the object.
      parametersForSorting.keywords[type] = keywords.map(kw => {
        return kw.label
      })
    }
    this.setState({parametersForSorting}, () => {
      const {allExercises} = this.props;
      const filteredExercises = this.SorterHelper.sortExercises(
        allExercises,
        this.state.parametersForSorting
      );
      this.props.updateExerciseByFilter(filteredExercises);
    });

  }

}

export default withTranslation()(FilterBar);
