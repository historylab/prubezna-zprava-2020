import React from "react";

import "./filterBarVisual.scss";


import {Row, Col,} from "react-bootstrap";

import {SortOrderButton} from "../atoms/order/SortOrderButton";

import {SortText} from "../atoms/input/SortText";

import {SortSelect} from "../atoms/select/SortSelect";

/**
 * Component filter bar for sorting exercises, only visual, no logic.
 *
 * @param rvpSetup - setup for rvp select
 * @param konceptSetup - setup for koncept select
 * @param b4Setup - setup for b4 select
 * @param historylabSetup - setup for historylab select
 * @param sortTextSetup - setup for text input
 * @param sortOrderButton - setup for filter order
 *
 */
export const FilterBarVisual = ({rvpSetup, konceptSetup, b4Setup, historylabSetup, sortTextSetup, sortOrderButton}) => {
  return (
    <Row>
      <Col
        data-cy="filterSelectRVP"
      >
        <SortSelect
          options={rvpSetup.options}
          placeholder={rvpSetup.placeholder}
          onChange={rvpSetup.onChange}
        />
      </Col>
      <Col
        data-cy="filterSelectKoncept"
      >
        <SortSelect
          options={konceptSetup.options}
          placeholder={konceptSetup.placeholder}
          onChange={konceptSetup.onChange}
        />
      </Col>
      <Col>
        <SortSelect
          data-cy="filterSelectB4"
          options={b4Setup.options}
          placeholder={b4Setup.placeholder}
          onChange={b4Setup.onChange}
        />
      </Col>
      <Col>
        <SortSelect
          data-cy="filterSelectHistorylab"
          options={historylabSetup.options}
          placeholder={historylabSetup.placeholder}
          onChange={historylabSetup.onChange}
        />
      </Col>
      <Col>
        <SortText
          placeholder={sortTextSetup.placeholder}
          onChange={sortTextSetup.onChange}
        />
      </Col>
      <Col>
        <SortOrderButton
          direction={sortOrderButton.direction}
          onClick={sortOrderButton.onClick}
        />
      </Col>
    </Row>

  );
};

FilterBarVisual.defaultProps = {
  backgroundColor: null,
};
