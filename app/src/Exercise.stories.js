import React from "react";
// import faker from "faker";
import "./css/index.scss";
import "./fonts/fonts.css";
import "./css/App.css";

import Exercise from "./Exercise";

// require("../../icons");


export default {
  component: Exercise,
  title: "Exercise card (assigned)",
  // decorators: [d],
  excludeStories: /.*Data$/
};


export const Default = () => {
  return <Exercise
          exercise={{
            code: '6357ABB',
            image_url: 'https://cviceni-test.historylab.now.sh/dev/assets/img/jak-vnimali-boxersky-zapas/pic-00-1280w-thumb.jpg',
            name: 'Jak vnímali boxerský zápas?',
            submitted: 0,
            total: 16,
            type: 'individual',
            typeDescription: 'individuální cvičení',
          }}
        />;
};

export const InProgress = () => {
  return <Exercise
          exercise={{
            code: '6357ABB',
            image_url: 'https://cviceni-test.historylab.now.sh/dev/assets/img/jak-vnimali-boxersky-zapas/pic-00-1280w-thumb.jpg',
            name: 'Jak vnímali boxerský zápas?',
            submitted: 15,
            total: 16,
            type: 'individual',
            typeDescription: 'individuální cvičení',
          }}
        />;
};

export const Completed = () => {
  return <Exercise
          exercise={{
            code: '6357ABB',
            image_url: 'https://cviceni-test.historylab.now.sh/dev/assets/img/jak-vnimali-boxersky-zapas/pic-00-1280w-thumb.jpg',
            name: 'Jak vnímali boxerský zápas?',
            submitted: 16,
            total: 16,
            type: 'individual',
            typeDescription: 'individuální cvičení',
          }}
        />;
};