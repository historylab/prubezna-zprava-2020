import React from "react";
// import { Jumbotron } from "reactstrap";
import { Link } from "react-router-dom";
import "./css/App.css";
import "./scss/homepage.scss";
import MainHeader from "./MainHeader";
import { withTranslation, Trans } from "react-i18next";

class Home extends React.Component {
  render() {
    return (
      <div id="top" className="template-homepage page-homepage">
        <MainHeader />
        <main>
          <section className="section section--toc">
            <div className="section-inner">
              <h1 className="display-3">
                <Trans i18nKey="welcome">Vítejte</Trans>
              </h1>
              <hr/>
              <Link to="/katalog">
                <h2>
                  <Trans i18nKey="catalog">Katalog</Trans>
                </h2>
              </Link>
              <Link to="/moje">
                <h2>
                  <Trans i18nKey="myExercises">
                    Přehled zadaných cvičení
                  </Trans>
                </h2>
              </Link>
            </div>
          </section>
        </main>
      </div>
    );
  }
}

export default withTranslation()(Home);
