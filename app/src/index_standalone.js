import React from "react";
import { render } from "react-dom";
import App from "./App";
import { HashRouter } from "react-router-dom";

const renderApp = root => {
  /*const initialState = {
	    todos: [
	        {
	            text: "learn Redux",
	            completed: false,
	            id: 0
	        }
	    ]
	}*/
  /*	const todos = (window.todos = todosFactory.create(initialState))
	const store = asReduxStore(todos)
	connectReduxDevtools(require("remotedev"), todos)
*/
  render(
    <HashRouter>
      <App />
    </HashRouter>,
    root
  );
};

export default renderApp;
