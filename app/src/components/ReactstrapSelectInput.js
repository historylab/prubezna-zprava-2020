import React from "react";
import { FormGroup, Label, FormFeedback, Input } from "reactstrap";

const ReactstrapSelectInput = ({
  field,
  form: { touched, errors },
  disabled = false,
  ...props
}) => {
  let error = errors[field.name];
  let touch = touched[field.name];
  return (
    <FormGroup>
      <Label for={props.inputprops.id} className={"label-color"}>
        {props.label}
      </Label>
      <Input
        id={props.inputprops.id}
        {...field}
        {...props}
        type="select"
        invalid={Boolean(touched[field.name] && errors[field.name])}
        placeholder="Test"
      >
        {props.inputprops.options.map((option, index) => {
          if (option.name)
            return (
              <option
                value={option.id}
                key={index}
                selected={props.inputprops.defaultOption === option.id}
              >
                {option.name}
              </option>
            );
          return (
            <option
              value={option}
              key={index}
              selected={props.inputprops.defaultOption === option}
            >
              {option}
            </option>
          );
        })}
      </Input>
      {touch && error && <FormFeedback>{error}</FormFeedback>}
    </FormGroup>
  );
};

export default ReactstrapSelectInput;
