import React from "react";
import Header from "./Header";
import { Link } from "react-router-dom";

const ExerciseDetail = props => {
  return (
    <div>
      <Header title="Detail cvičení">
        <Link to="/katalog">
          <h2>Katalog</h2>
        </Link>
      </Header>
      <div>{JSON.stringify(props.match.params.id)}</div>
      <Link to={`/katalog/pridat/${props.match.params.id}`}>
        <h2>
          <b>+</b>
        </h2>
      </Link>
    </div>
  );
};

export default ExerciseDetail;
