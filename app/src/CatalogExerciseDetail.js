import React from "react";
import { Button, ButtonGroup, Row, Col, Modal, ModalBody } from "reactstrap";
import { Trans, withTranslation } from "react-i18next";
import i18n from "i18next";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import _ from "lodash";
import parse from "url-parse";
import DataService from "./DataService";
import ExerciseIcon from "./ExerciseIcon";
import LoginForm from "./Auth/LoginForm";
import withNotification from "./withNotification";
import { ApplicationStateConsumer } from "./ApplicationStateProvider";
import Layout from "./Layout";
import ChooseOrCreateGroupModal from "./groups/components/ChooseOrCreateGroupModal";

import Logo from "./icons/logo";
import "./CatalogExerciseDetail.scss";

class CatalogExerciseDetail extends React.Component {
  constructor() {
    super();
    this.state = {
      isLoading: true,
      error: null,
      pending: false,
      type: "",
      authModal: false,
      classModal: false,
      notification: null,
      data: {
        exercise: {},
        warnings: []
      }
    };
  }

  refresh = () => {
    this.setState({ isLoading: true });
    const slug = this.props.match.params.slug;
    new DataService().fetchCatalogExercise(slug).then(
      result =>
        this.setState({
          data: result.data,
          isLoading: false
        }),
      error =>
        this.setState({
          error,
          isLoading: false
        })
    );
  };

  enrolUserToExercise = (
    exerciseId,
    groupId,
    type,
    showNotification = true
  ) => {
    return new DataService()
      .enrolUserToExercise(exerciseId, groupId, type)
      .then(response => {
        const { code } = response.data;
        return { success: true, code };
      })
      .catch(error => {
        this.props.notifyError(error);
        return { success: false, code: null };
      });
  };

  onSelected = type => {
    const { authenticated } = this.props;
    this.setState({
      type,
      authModal: !authenticated,
      classModal: authenticated
    });
  };

  onAssignActivity = group => {
    if (group.id < 0) {
      this.onGroupCreating(group);
    } else {
      this.onGroupSelected(group);
    }
  };

  onGroupSelected = group => {
    const { data, type } = this.state;
    const { exercise } = data;
    const { history } = this.props;

    this.enrolUserToExercise(exercise.exerciseId, group, type, false)
      .then(({ code, success }) => {
        this.setState({ pending: false });
        if (success) {
          history.push("/cviceni/detail/" + code);
        }
      })
      .catch(error => {
        this.setState({
          error,
          pending: false
        });
      });
  };

  onGroupCreating = group => {
    this.onGroupSelected({ id: -1, name: group.name });
  };

  toggleAuth = () => {
    this.setState(prevState => ({
      authModal: !prevState.authModal
    }));
  };

  onGroupCreating = group => {
    this.onGroupSelected({ id: -1, name: group.name });
  };

  toggleAuth = () => {
    this.setState(prevState => ({
      authModal: !prevState.authModal
    }));
  };

  toggleClass = () => {
    this.setState(prevState => ({
      classModal: !prevState.classModal
    }));
  };

  notify = notification => {
    this.setState({ notification });
  };

  login = credentials => {
    this.setState({ notification: null });
    this.props
      .login(credentials, this.props.notify)
      .then(_ => {
        this.onSelected(this.state.type);
      })
      .catch(error => {
        this.setState({
          error,
          pending: false
        });
      });
  };

  onAuthSuccess = data => {
    this.props.processNotifications(data);
    this.props.onAuthSuccess(data, null, this.notify);
    this.onSelected(this.state.type);
  };

  onAuthFailure = e => {
    this.props.onAuthFailure(e);
  };

  componentDidMount() {
    this.refresh();
  }

  render() {
    // NOTE: Jakej je rozdíl mezi `pending` a `isLoading`?
    const { pending, data, isLoading } = this.state;
    const { exercise } = data;
    const { authenticated } = this.props;
    if(!isLoading){
    let description = exercise.metadata.anotace.verejna || "";
    if (description) {
      description =
        description.length < 120
          ? description
          : description.substring(0, 120) + "...";
    }
    }

    const tryItUrl = parse(exercise.tryItUrl, true);
    const query = tryItUrl.query;
    query["lang"] = i18n.language;
    tryItUrl.set("query", query);

    // const style = isLoading
    //   ? {}
    //   : { backgroundImage: "url(" + exercise.image_url + ")" };

    // <div id="top" className="loading template-exercise-preview">
    // <MainHeader />
    // <main className="main">
    // <section className="section" style={style}>
    // <div className="section-inner-detail">
    // <div className="section-item ">
    //
    // </div>
    // </div>
    // </section>
    // </main>
    // </div>
    // { i18n.t("catalogExerciseDetail.exerciseBy", { count: authors.length }) }

    const getImage = (image, name = "") => {
      return (
        <img src={image} alt={name} className="CatalogExerciseDetail__image" />
      );
    };
    // TODO: plurál nefunguje
    const getAuthors = authors => {
      return (
        <div className="CatalogExerciseDetail__authors">
          <Trans
            i18nKey="catalogExerciseDetail.exerciseBy"
            count={authors.length}
          ></Trans>
          {" " + authors}
        </div>
      );
    };

    const getName = name => {
      return (
        <div className="CatalogExerciseDetail__name">
          <h1>{name}</h1>
        </div>
      );
    };

    const getMeta = exercise => {
      const topics = (Object.values(exercise.metadata.klicovaSlova) || "");

      return (
        <div className="CatalogExerciseDetail__meta meta">

          {topics && (
            <div className="meta__item meta__item--taxonomy">
              <FontAwesomeIcon
                size="sm"
                icon="tags"
              />
              <ul className="taxonomy">
                {topics.map((tag, index) => (
                  <li key={index} className="taxonomy__item">
                    {tag}
                  </li>
                ))}
              </ul>
            </div>
          )}

          {exercise.metadata.trvani && (
            <div className="meta__item meta__item--time">
              <FontAwesomeIcon size="sm" icon={["fas", "clock"]} />
              <span>{exercise.metadata.trvani + "min"}</span>
            </div>
          )}

          {exercise.instructionsUrl && (
            <a
              className="meta__item meta__item--manual"
              href={ exercise.instructionsUrl }
              download="download"
            >
              <FontAwesomeIcon
                size="sm"
                icon={["far", "file-alt"]}
              />
              <span>
                <Trans i18nKey="catalogExerciseDetail.methodology">
                  Doporučený postup
                </Trans>
              </span>
            </a>
          )}

        </div>
      )
    }

    const getDescription = (description) => {
      return (
        <div className="CatalogExerciseDetail__description">
          <p>{ description }</p>
        </div>
      )
    }

    const getButtons = () => {
      return (
        <div className="CatalogExerciseDetail__buttons">
          <div className="CatalogExerciseDetail__heading">
            <h5>
              <Trans i18nKey="catalogExerciseDetail.enrol">
                Zadat cvičení třídě
              </Trans>:
            </h5>
          </div>

          <ButtonGroup
            size="lg"
            className="CatalogExerciseDetail__enrollmentButtons"
          >
            <Button
              // outline
              color="primary"
              role="individual"
              onClick={() => this.onSelected("individual")}
            >
              <ExerciseIcon type="user" />
              <Trans i18nKey="catalogExerciseDetail.individual">
                Individuální
              </Trans>
            </Button>
            <Button
              // outline
              color="primary"
              onClick={() => this.onSelected("group")}
            >
              <ExerciseIcon type="group" />
              <Trans i18nKey="catalogExerciseDetail.group">
                Skupinové
              </Trans>
            </Button>
            <Button
              // outline
              color="primary"
              onClick={() => this.onSelected("class")}
            >
              <ExerciseIcon type="class" />{" "}
              <Trans i18nKey="catalogExerciseDetail.class">
                S celou třídou
              </Trans>
            </Button>
          </ButtonGroup>

          <div className="CatalogExerciseDetail__or">
            <Trans i18nKey="catalogExerciseDetail.or">nebo</Trans>
          </div>

          <div className="CatalogExerciseDetail__try">
            <a
              target="_blank"
              rel="noopener noreferrer"
              href={tryItUrl}
              className="btn btn-secondary btn-lg"
            >
              <Trans i18nKey="catalogExerciseDetail.try">
                Vyzkoušet cvičení
              </Trans>
            </a>
          </div>
        </div>
      );
    };

    return (
      <Layout
        title={false}
        // isTitleCentered={true}
        isFullWidth={true}
        isHeaderAbsolutePositioned={true}
        isHeaderInverted={true}
        className="CatalogExerciseDetail"
        // style={style}
      >
        <Layout.Content>
          {pending && (
            <Row>
              <Col>
                <div className="text-center">
                  <Trans i18nKey="pleaseWait">
                    Prosím, počkejte ...
                  </Trans>
                </div>
              </Col>
            </Row>
          )}

          {!isLoading && (
            <>
              {exercise.image_url &&
                getImage(exercise.image_url, exercise.name)}
              {exercise.metadata.autor && getAuthors(exercise.metadata.autor.join(", "))}
              {exercise.name && getName(exercise.name)}
              {exercise && getMeta(exercise)}
              {exercise.metadata.anotace.verejna && getDescription(exercise.metadata.anotace.verejna)}
              {getButtons()}

              <Modal
                // TODO: create separate component for the login modal
                isOpen={this.state.authModal}
                toggle={this.toggleAuth}
                className="modal--login"
                backdrop={true}
                role="authenticate"
              >
                <Logo />
                <ModalBody>
                  <p className="text-center">
                    <Trans i18nKey="catalogExerciseDetail.signInFirst">
                      Cvičení si můžete zadat až po přihlášení.
                    </Trans>
                  </p>
                  <LoginForm
                    login={this.login}
                    notification={this.state.notification}
                    onAuthSuccess={this.onAuthSuccess}
                    onAuthFailure={this.onAuthFailure}
                  />
                </ModalBody>
              </Modal>

              {authenticated && <ChooseOrCreateGroupLoader
                isOpen={this.state.classModal}
                toggle={this.toggleClass}
                onAssignActivity={this.onAssignActivity}
              />}
            </>
          )}

        </Layout.Content>
      </Layout>
    );
  }
}

class ChooseOrCreateGroupLoader extends React.Component {
  constructor() {
    super();
    this.state = {
      isLoading: true,
      groups: []
    };
  }

  componentDidMount() {
    this.setState({ isLoading: true });
    new DataService().fetchGroups().then(
      result => {
        let byYears = Object.entries(
          _.groupBy(
            result.data.groups.filter(g => !g.isHidden),
            g => `${g.yearStart} — ${g.yearEnd}`
          ) || {}
        );
        byYears = _.orderBy(byYears, g => g);
        byYears = byYears.reverse();

        const groups = _.flatten(
          byYears.map(
            y => _.orderBy(
              y[1], g => g.isFavourite ? 0 : 1)));

        this.setState({ groups, isLoading: false });
      },
      error =>
        this.setState({
          error,
          isLoading: false
        })
    );
  }

  render() {
    const { isLoading, groups } = this.state;
    return (
      <ChooseOrCreateGroupModal
        loading={isLoading}
        groups={groups}
        onAssignActivity={this.props.onAssignActivity}
        {...this.props}
      />
    );
  }
}

class Wrapper extends React.Component {
  render() {
    return (
      <ApplicationStateConsumer>
        {auth => <CatalogExerciseDetail {...this.props} {...auth} />}
      </ApplicationStateConsumer>
    );
  }
}

export default withTranslation()(withNotification(Wrapper));
