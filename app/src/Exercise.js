import React from "react";
import {
  Card,
  CardImg,
  CardImgOverlay,
  CardTitle,
  CardSubtitle,
  Badge,
  Progress,
  CardBody
} from "reactstrap";
import ExerciseIcon from "./ExerciseIcon";
import { Link } from "react-router-dom";
import { If } from "./If";
import { withTranslation, Trans } from "react-i18next";
import "./Exercise.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const Exercise = ({ exercise, onDetail }) => {
  const current = exercise.submitted;
  const max = exercise.total;

  return (
    <div>
      <Card className="Exercise hl-card hl-card--exercise">
        <Link
          className="list-detail-link"
          to={{ pathname: exercise.detailUrl }}
        >
          <CardImg
            className="card__image"
            src={exercise.image_url}
            width={300}
            height={300}
          />

          <CardImgOverlay
          // className="card-text"
          >
            {!exercise.isPackage && (
              <CardSubtitle className="list-subtitle">
                <ExerciseIcon size="1x" type={exercise.type} />{" "}
                <Trans i18nKey={"exercise." + exercise.type}>
                  {exercise.typeDescription}
                </Trans>
              </CardSubtitle>
            )}

            <CardTitle tag="h3">
              {exercise.isPackage ? <FontAwesomeIcon icon="box-open" /> : ""}{" "}
              {exercise.name}
            </CardTitle>
            {!exercise.isPackage && (
              <If
                condition={
                  max > 0 && current !== max && exercise.type !== "class"
                }
              >
                <CardBody>
                  <If condition={max === 0 && exercise.type !== "class"}>
                    <Badge color="warning">
                      <Trans i18nKey="exercise.noEmailAddresses">
                        Bez zadaných emailových adres
                      </Trans>
                    </Badge>
                  </If>
                  <If
                    condition={
                      max > 0 && current === max && exercise.type !== "class"
                    }
                  >
                    <div>
                      <Trans i18nKey="exercise.allSubmitted">
                        Vše odevzdáno
                      </Trans>
                    </div>
                  </If>
                  <div>
                    <div>
                      {current} / {max}
                    </div>
                    <Progress color="success" value={current} max={max} />{" "}
                  </div>
                </CardBody>
              </If>
            )}
          </CardImgOverlay>
        </Link>
      </Card>
    </div>
  );
};

export default withTranslation()(Exercise);
