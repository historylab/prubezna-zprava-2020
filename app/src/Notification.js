import React from "react";
import { UncontrolledAlert } from "reactstrap";

class Notification extends React.Component {
  constructor(props) {
    super(props);

    this.dismissCallback = null;

    this.state = {
      visible: true,
      dismissed: false
    };
  }

  onDismiss = () => {
    if (this.state.dismissed) {
      return;
    }
    this.setState({ visible: false, dismissed: true });
    this.props.dismiss(this.props.notification);
  };

  componentDidMount = () => {
    const { type } = this.props.notification;
    if (type === "danger") {
      return;
    }
    this.dismissCallback = setTimeout(this.onDismiss, 5000);
  };

  componentWillUnmount() {
    this.dismissCallback && clearTimeout(this.dismissCallback);
  }

  render() {
    const { type, text } = this.props.notification;
    return (
      <UncontrolledAlert
        isOpen={this.state.visible}
        toggle={this.onDismiss}
        color={type}
      >
        {text}
      </UncontrolledAlert>
    );
  }
}
export default Notification;
