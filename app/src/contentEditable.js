import React from "react";

function contentEditable(WrappedComponent) {
  return class extends React.Component {
    state = {
      editing: false
    };

    componentDidMount() {
      this.setState({ value: this.props.value });
    }

    toggleEdit = e => {
      e.stopPropagation();
      if (this.state.editing) {
        this.cancel();
      } else {
        this.edit();
      }
    };

    edit = () => {
      this.setState(
        {
          editing: true
        },
        () => {
          this.domElm.focus();
        }
      );
    };

    save = () => {
      this.setState(
        {
          editing: false,
          text: this.domElm.textContent
        },
        () => {
          if (this.props.onSave && this.isValueChanged()) {
            this.props.onSave(this.domElm.textContent);
          }
        }
      );
    };

    cancel = () => {
      this.setState({
        editing: false
      });
    };

    isValueChanged = () => {
      return this.props.value !== this.domElm.textContent;
    };

    handleKeyUp = e => {
      this.setState({ value: e.target.innerText });
    };

    handleKeyDown = e => {
      const { key, keyCode } = e;
      const { value } = this.state;
      const { dataInputLength } = this.props;
      const ignore = [8, 9, 13, 33, 34, 35, 36, 37, 38, 39, 40, 46];
      switch (key) {
        case "Enter":
        case "Escape":
          this.save();
          break;
        default:
          if (
            ignore.indexOf(keyCode) === -1 &&
            value.length + 1 >= dataInputLength
          ) {
            e.preventDefault();
          }
          break;
      }
    };

    render() {
      let editOnClick = true;
      const { editing } = this.state;
      if (this.props.editOnClick !== undefined) {
        editOnClick = this.props.editOnClick;
      }
      const { onSave, dataInputLength, ...otherProps } = this.props;
      return (
        <WrappedComponent
          className={editing ? "editing" : ""}
          onClick={editOnClick ? this.toggleEdit : undefined}
          contentEditable={editing}
          ref={domNode => {
            this.domElm = domNode;
          }}
          onBlur={this.save}
          onKeyDown={this.handleKeyDown}
          onKeyUp={this.handleKeyUp}
          {...otherProps}
        >
          {this.props.value}
        </WrappedComponent>
      );
    }
  };
}

export default contentEditable;
