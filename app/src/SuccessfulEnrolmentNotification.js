import React from "react";
import { Link } from "react-router-dom";
import { UncontrolledAlert, Button } from "reactstrap";

class SuccessfulEnrolmentNotification extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      visible: true
    };

    this.onDismiss = this.onDismiss.bind(this);
  }

  onDismiss() {
    this.setState({ visible: false });
    this.props.dismiss(this);
  }

  render() {
    const { code, id } = this.props.notification;
    return (
      <UncontrolledAlert
        isOpen={this.state.visible}
        toggle={this.onDismiss}
        color="success"
        key={id}
      >
        <h4 className="alert-heading">Well done!</h4>
        <p>
          Aww yeah, you successfully read this important alert message. This
          example text is going to run a bit longer so that you can see how
          spacing within an alert works with this kind of content.
          <Link onClick={this.onDismiss} to={`/cviceni/detail/${code}`}>
            <span className="h2">{code}</span>
          </Link>
        </p>
        <hr />
        <p className="mb-0">
          Whenever you need to, be sure to use margin utilities to keep things
          nice and tidy.
          <Button onClick={this.onDismiss}>Close</Button>
        </p>
      </UncontrolledAlert>
    );
  }
}

export default SuccessfulEnrolmentNotification;
