import React from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import { withTranslation, Trans } from "react-i18next";

const DeleteEmailDialog = ({ isOpen, emailAddress, toggle, onConfirmed }) => {
  return (
    <Modal isOpen={isOpen} toggle={toggle} backdrop={true}>
      <ModalHeader toggle={toggle}>
        <Trans i18nKey="deleteEmailDialog.header"></Trans>
      </ModalHeader>
      <ModalBody>
        <Trans i18nKey="deleteEmailDialog.confirmation"></Trans>
        <br />
        <b>{emailAddress.emailAddress}</b>
        <p>
          <Trans i18nKey="deleteEmailDialog.note"></Trans>
        </p>
      </ModalBody>
      <ModalFooter>
        <Button color="primary" onClick={onConfirmed}>
          <Trans i18nKey="deleteEmailDialog.yes"></Trans>
        </Button>
        <Button color="secondary" onClick={toggle}>
          <Trans i18nKey="no"></Trans>
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default withTranslation()(DeleteEmailDialog);
