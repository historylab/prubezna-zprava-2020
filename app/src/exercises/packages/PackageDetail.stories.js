import React from "react";
import { action } from "@storybook/addon-actions";

import PackageDetail from "./PackageDetail";

export default {
  component: PackageDetail,
  title: "Package Detail",
  // Our exports that end in "Data" are not stories.
  excludeStories: /.*Data$/
};

export const taskData = {
  id: "1",
  title: "Test Task",
  state: "TASK_INBOX",
  updatedAt: new Date(2018, 0, 1, 9, 0)
};

export const actionsData = {
  onPinTask: action("onPinTask"),
  onArchiveTask: action("onArchiveTask")
};

export const Default = () => (
  <PackageDetail task={{ ...taskData }} {...actionsData} />
);

export const Pinned = () => (
  <PackageDetail
    task={{ ...taskData, state: "TASK_PINNED" }}
    {...actionsData}
  />
);

export const Archived = () => (
  <PackageDetail
    task={{ ...taskData, state: "TASK_ARCHIVED" }}
    {...actionsData}
  />
);
