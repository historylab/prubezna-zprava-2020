import axios from "axios";
import i18n from "i18next";
import adapter from "axios/lib/adapters/http";

class PackageService {
  constructor(baseUrl, port) {
    this.baseUrl = baseUrl;
    this.port = port;
    this.baseURL = "";
    if (baseUrl && port) {
      this.baseURL = `${this.baseUrl}:${this.port}`;
    }
  }

  getPublic(packageName) {
    return axios.request(
      {
        method: "GET",
        url: `/api/catalog/package/${packageName}`,
        baseURL: this.baseUrl,
        data: {},
        headers: {
          "Accept-Language": i18n.language === "cs" ? "cs" : "en-US,en;q=0.5",
          Accept: "application/json; charset=utf-8",
          "Content-Type": "application/json; charset=utf-8"
        }
      },
      adapter
    );
  }

  getDetail(code) {
    return axios.request(
      {
        method: "GET",
        url: `/api/my/package/${code}`,
        baseURL: this.baseUrl,
        data: {},
        headers: {
          "Accept-Language": i18n.language === "cs" ? "cs" : "en-US,en;q=0.5",
          Accept: "application/json; charset=utf-8",
          "Content-Type": "application/json; charset=utf-8"
        }
      },
      adapter
    );
  }

  enrolToExercise(packageEnrolment, exerciseId) {
    return axios
      .request(
        {
          method: "POST",
          url: `/api/package/${packageEnrolment}/exercise/${exerciseId}/enrol`,
          baseURL: this.baseUrl,
          data: {}
        },
        adapter
      )
      .then(this.extractDataAndNotifications);
  }

  create(data) {
    return axios
      .request(
        {
          method: "POST",
          url: `/api/packages`,
          baseURL: `${this.baseUrl}:${this.port}`,
          headers: {
            "Accept-Language": i18n.language === "cs" ? "cs" : "en-US,en;q=0.5",
            Accept: "application/json; charset=utf-8",
            "Content-Type": "application/json; charset=utf-8"
          },
          data
        },
        adapter
      )
      .then(this.extractDataAndNotifications);
  }

  base64DecodeUnicode(str) {
    return decodeURIComponent(
      Array.prototype.map
        .call(atob(str), function (c) {
          return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2);
        })
        .join("")
    );
  }

  extractDataAndNotifications(result) {
    const data = result.data;
    if (result.headers["x-hl-flash"]) {
      const flash = JSON.parse(
        decodeURIComponent(window.atob(result.headers["x-hl-flash"]))
      );
      return { data, notifications: flash || [] };
    }
    return { data, notifications: [] };
  }
}

export default PackageService;
