import PackageService from "./PackageService";
import * as Pact from "@pact-foundation/pact";
import PublicPackageSchema from "./../../../../server/api/packages/schemas/publicPackage.schema.json";
import PublicPackageExample from "./../pages/publicPackage.example.json";

describe("PackageService API", () => {
  const packageService = new PackageService("http://localhost", global.port);

  // a matcher for the content type "application/json" in UTF8 charset
  // that ignores the spaces between the ";2 and "charset"
  const contentTypeJsonMatcher = Pact.Matchers.term({
    matcher: "application\\/json; *charset=utf-8",
    generate: "application/json; charset=utf-8"
  });

  describe("getPackage (public)", () => {
    beforeEach(done => {
      global.provider
        .addInteraction({
          state: "provider returns package for public access",
          uponReceiving: "a GET request to get public package",
          withRequest: {
            method: "GET",
            path: "/api/catalog/package/kazdodennost-normalizace",
            headers: {
              Accept: "application/json; charset=utf-8",
              "Content-Type": "application/json; charset=utf-8"
            }
          },
          willRespondWith: {
            status: 200,
            headers: {
              Accept: "application/json; charset=utf-8",
              "Accept-Language": "en",
              "Content-Type": "application/json; charset=utf-8"
            },
            body: Pact.Matchers.somethingLike(PublicPackageExample)
          }
        })
        .then(() => done());
    });

    it("sends a request according to contract", done => {
      packageService
        .getPublic("kazdodennost-normalizace")
        .then(response => {
          const pkg = response.data;
          (async () => {
            await expect(pkg).toConformSchema(PublicPackageSchema);
          })();
        })
        .then(() => {
          global.provider.verify().then(
            () => done(),
            error => {
              done.fail(error);
            }
          );
        });
    });
  });

  /*describe("createHero()", () => {
    beforeEach(done => {
      // ...
    });

    it("sends a request according to contract", done => {
      beforeEach(done => {
        global.provider
          .addInteraction({
            state: "a hero exists",
            uponReceiving: "a GET request for a hero",
            withRequest: {
              method: "GET",
              path: "/api/packages/42",
              headers: {
                Accept: contentTypeJsonMatcher
              }
            },
            willRespondWith: {
              status: 200,
              headers: {
                "Content-Type": contentTypeJsonMatcher
              },
              body: Pact.Matchers.somethingLike(
                new Hero("Superman", "flying", "DC", 42)
              )
            }
          })
          .then(() => done());
      });

      /*it("sends a request according to contract", done => {
        heroService
          .getHero(42)
          .then(hero => {
            expect(hero.name).toEqual("Superman");
          })
          .then(() => {
            global.provider.verify().then(
              () => done(),
              error => {
                done.fail(error);
              }
            );
          });
      });*
    });
  });*/

  /*describe("postHero()", () => {
    beforeEach(done => {
      global.provider
        .addInteraction({
          state: "provider allows hero creation",
          uponReceiving: "a POST request to create a hero",
          withRequest: {
            method: "POST",
            path: "/api/packages",
            headers: {
              Accept: contentTypeJsonMatcher,
              "Content-Type": contentTypeJsonMatcher
            },
            body: { x: 1 }
          },
          willRespondWith: {
            status: 201,
            headers: {
              "Content-Type": Pact.Matchers.term({
                matcher: "application\\/json; *charset=utf-8",
                generate: "application/json; charset=utf-8"
              })
            },
            body: Pact.Matchers.somethingLike({ x: 20 })
          }
        })
        .then(() => done());
    });

    it("sends a request according to contract", done => {
      packageService
        .create({ x: 1 })
        .then(hero => {
          expect(hero.id).toEqual(42);
        })
        .then(() => {
          global.provider.verify().then(
            () => done(),
            error => {
              done.fail(error);
            }
          );
        });
    });
  });*/
});
