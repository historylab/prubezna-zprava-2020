import React from "react";
import PublicPackage from "./PublicPackage";
import { ApplicationStateProvider } from "../../../ApplicationStateProvider";
//import "./../../../css/Package.css";
import "./../../../css/App.css";
//import "./../../../css/index.scss";
import "./../../../fonts/fonts.css";

import PublicPackageExample from "./PublicPackage.example.json";

const d = Story => (
  <ApplicationStateProvider value={{ user: {} }}>
    <Story />
  </ApplicationStateProvider>
);

export default {
  component: PublicPackage,
  title: "Catalog Package Page",
  decorators: [d],
  excludeStories: /.*Data$/
};

export const packageData = PublicPackageExample;

export const Default = () => <PublicPackage packageData={{ ...packageData }} />;
