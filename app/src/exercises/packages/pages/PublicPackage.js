import React from "react";
import { Link } from "react-router-dom";
import { Row, Col } from "reactstrap";
import "./PublicPackage.scss";
import { Trans } from "react-i18next";
import PublicPackageExercise from "../components/PublicPackageExercise";
import Layout from "../../../Layout";
import Enrolment from "../../components/Enrolment";

export default function PublicPackage(props) {
  const { packageData } = props;

  return (
    <Layout
      subHeader={
        <>
          <h5 className="label">
            <Link to="/tridy">
              <Trans i18nKey="mainHeader.catalog"></Trans>
            </Link>{" "}
            / <Trans i18nKey="exeraciseDetail.assignedExercise">Balíček</Trans>
          </h5>
          <br />
          <h1 className="d-inline-block">
            <span className="ml-2">{packageData.name}</span>
          </h1>
        </>
      }
    >
      <Layout.Content>
        <div className="container-fluid">
          <div>
            <Row>
              <Col>
                {packageData.exercises.map(exercise => (
                  <PublicPackageExercise exercise={exercise} />
                ))}
              </Col>
            </Row>
            <Enrolment package={packageData} />
          </div>
        </div>
      </Layout.Content>
    </Layout>
  );
}
