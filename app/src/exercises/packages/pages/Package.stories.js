import React from "react";
import { action } from "@storybook/addon-actions";
import Package from "./Package";
import { ApplicationStateProvider } from "./../../../ApplicationStateProvider";
import "./../../../css/Package.css";

const d = Story => (
  <ApplicationStateProvider value={{ user: {} }}>
    <Story />
  </ApplicationStateProvider>
);

export default {
  component: Package,
  title: "Package Page",
  // Our exports that end in "Data" are not stories.
  decorators: [d],
  excludeStories: /.*Data$/
};

export const packageData = {
  name: "Každodennost normalizace (z různých perspektiv)",
  description: "",
  tags: ["dobové perspektivy"],
  emailAddresses: [
    {
      id: 2514,
      emailAddress: "jan@novak.cz",
      group_id: 978,
      hasSubmissions: 0
    },
    {
      id: 2513,
      emailAddress: "frantisek@novak.cz",
      group_id: 978,
      hasSubmissions: 0
    },
    {
      id: 2512,
      emailAddress: "fejk@email.cz",
      group_id: 978,
      hasSubmissions: 1
    },
    {
      id: 2512,
      emailAddress: "adam@gmaul.cz",
      group_id: 978,
      hasSubmissions: 1
    }
  ],
  submissions: {
    entries: [
      {
        emailAddresses: ["jan@novak.cz"],
        viewUrl: "",
        timeSpent: 23 * 60 * 1000 + 47,
        entryId: 1,
        exercise: { id: 2904, name: "Proč očišťovali krávy?" },
        hidden: "true"
      },
      {
        emailAddresses: ["frantisek@novak.cz", "fejk@email.cz"],
        entryId: 2,
        exercise: { id: 2867, name: "Co ministr vzkazuje učitelům?" },
        star: "true"
      },
      {
        emailAddresses: ["fejk@email.cz", "frantisek@novak.cz"],
        exercise: { id: 2921, name: "To byla sláva!" }
      }
    ]
  },
  group: {},
  originalGroup: {},
  exercises: [
    {
      instructionsUrl:
        "https://app.testlab4.felk.cvut.cz/cviceni-aktualni/assets/pdf/doporuceny-postup/proc-ocistovali-kravy.pdf",
      tryItUrl:
        "https://app.testlab4.felk.cvut.cz/cviceni/proc-ocistovali-kravy?lang=cs",
      url: "https://testlab4.felk.cvut.cz/cviceni/proc-ocistovali-kravy",
      image_url:
        "https://app.testlab4.felk.cvut.cz/cviceni-aktualni/assets/img/proc-ocistovali-kravy/pic-00-1280w.jpg",
      thumb_image_url:
        "https://app.testlab4.felk.cvut.cz/cviceni-aktualni/assets/img/proc-ocistovali-kravy/pic-00-1280w-thumb.jpg",
      id: 2904,
      code: "cviceniProcOcistovaliKravy",
      name: "Proč očišťovali krávy?",
      description:
        "Snímek žáky konfrontuje s nezvyklou situací. Zemědělci v ochranných oděvech cvičně provádějí chemickou očistu dobytka od zamoření. Zajímavou fotografii využijeme k tomu, aby se žáci pokusili formulovat hypotézy, které si posléze ověří s pomocí dobového popisku fotografie. Hravou formou se seznámí s fenoménem civilní obrany v období normalizace.",
      taxonomy: "lupa;text;svg;znacky",
      topics:
        "studená válka;normalizace;zemědělství;příčiny a důsledky;formulujeme hypotézu",
      enabled: true,
      slug: "proc-ocistovali-kravy",
      authors: "Kamil Činátl",
      exerciseId: 46,
      language: "cs",
      estimatedTime: 30,
      total: 10,
      submitted: 8,
      enrolled: true,
      code: "12376E27"
    },
    {
      instructionsUrl:
        "https://app.testlab4.felk.cvut.cz/cviceni-aktualni/assets/pdf/doporuceny-postup/co-ministr-vzkazuje-ucitelum.pdf",
      tryItUrl:
        "https://app.testlab4.felk.cvut.cz/cviceni/co-ministr-vzkazuje-ucitelum?lang=cs",
      url: "https://testlab4.felk.cvut.cz/cviceni/co-ministr-vzkazuje-ucitelum",
      image_url:
        "https://app.testlab4.felk.cvut.cz/cviceni-aktualni/assets/img/co-ministr-vzkazuje-ucitelum/pic-02-1024h.jpg",
      thumb_image_url:
        "https://app.testlab4.felk.cvut.cz/cviceni-aktualni/assets/img/co-ministr-vzkazuje-ucitelum/pic-02-1024h-thumb.jpg",
      id: 2867,
      code: "cviceniCoMinistrVzkazujeUcitelum",
      name: "Co ministr vzkazuje učitelům?",
      description:
        "V rámci cvičení se žáci prostřednictvím rozhlasového projevu ministra školství ČSR Jaromíra Hrbka ze září 1969 seznamují s normalizačním výkladem událostí roku 1968. S využitím dalších pramenů zkoumají významy, které mohli projevu ministra přikládat doboví posluchači, zejména učitelé. Cvičení využívá obeznámenost žáků se sociálním prostředím školy a podněcuje je k tomu, aby vnímali události pohledem dobových učitelů a promýšleli možné dopady projevu na jejich jednání.",
      taxonomy: "lupa;cteni;text;audio;svg",
      topics:
        "1968;normalizace;škola;ideologie;dobové perspektivy;měníme pohledy",
      enabled: true,
      slug: "co-ministr-vzkazuje-ucitelum",
      authors: "Kamil Činátl",
      exerciseId: 2,
      language: "cs",
      estimatedTime: 30,
      total: 10,
      submitted: 2,
      enrolled: true,
      code: "123A6107"
    },
    {
      instructionsUrl:
        "https://app.testlab4.felk.cvut.cz/cviceni-aktualni/assets/pdf/doporuceny-postup/to-byla-slava.pdf",
      tryItUrl:
        "https://app.testlab4.felk.cvut.cz/cviceni/to-byla-slava?lang=cs",
      url: "https://testlab4.felk.cvut.cz/cviceni/to-byla-slava",
      image_url:
        "https://app.testlab4.felk.cvut.cz/cviceni-aktualni/assets/img/to-byla-slava/pic-03-1280w.jpg",
      thumb_image_url:
        "https://app.testlab4.felk.cvut.cz/cviceni-aktualni/assets/img/to-byla-slava/pic-03-1280w-thumb.jpg",
      id: 2921,
      code: "cviceniToBylaSlava",
      name: "To byla sláva!",
      description:
        "V rámci cvičení si žáci poslechnou reportáž o zahájení provozu pražského metra v roce 1974. Šestiminutová reportáž je rozdělena do pěti částí, ke kterým žáci přiřazují dobové fotografie. Následně zkoumají zaměření jednotlivých částí reportáže a hledají odpověď na otázku, co znamenalo otevření metra pro různé zájmové skupiny (KSČ, posluchače rozhlasu, SSSR). Cvičení dokresluje běžný život v Praze v 70. letech 20. století.",
      taxonomy: "lupa;cteni;text;audio",
      topics:
        "zahájení provozu pražského metra;normalizace;rozhlasová reportáž;dobové perspektivy;analýza audio pramene",
      enabled: true,
      slug: "to-byla-slava",
      authors: "Pavla Sýkorová",
      exerciseId: 34,
      language: "cs",
      estimatedTime: 43
    }
  ]
};

export const actionsData = {
  onPinTask: action("onPinTask"),
  onArchiveTask: action("onArchiveTask")
};

export const Default = () => (
  <Package packageData={{ ...packageData }} activeTab="1" {...actionsData} />
);

export const Submissions = () => (
  <Package packageData={{ ...packageData }} activeTab="2" {...actionsData} />
);

export const ClassTab = () => (
  <Package packageData={{ ...packageData }} activeTab="3" {...actionsData} />
);

export const EmailTemplateTab = () => (
  <Package packageData={{ ...packageData }} activeTab="4" {...actionsData} />
);

export const SettingsTab = () => (
  <Package packageData={{ ...packageData }} activeTab="5" {...actionsData} />
);
