import React from "react";
import { Button, Badge } from "reactstrap";
import DataService from "./../services/PackageService";
import { ApplicationStateConsumer } from "./../../../ApplicationStateProvider";
import Package from "./Package";
import withEverything from "../../../withEverything";
import shared from "./../../shared";
import { flow, getParent, getRoot, types } from "mobx-state-tree";
import { Trans } from "react-i18next";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import PackageService from "./../services/PackageService";

const EmailAddressModel = types.model("EmailAddress", {
  id: types.refinement(types.number, identifier => identifier * 1 > 0),
  name: types.maybeNull(types.string),
  emailAddress: types.string,
  hasSubmissions: false,
  group_id: 0
});

const EmailAddressReference = types.reference(EmailAddressModel, {
  get(identifier, parent) {
    const found = getRoot(parent).emailAddresses.find(
      e => e.id.toString() === identifier.toString()
    );
    return { id: found.id, name: found.name, emailAddress: found.emailAddress };
  },
  // given a user, produce the identifier that should be stored
  set(value) {
    return value.name;
  }
});

const SubmissionModel = types
  .model("Submission", {
    entryId: 0,
    emailAddresses: types.array(EmailAddressReference),
    viewUrl: "",
    timeSpent: 0,
    note: types.maybe(types.string),
    score: types.maybe(types.number),
    exerciseId: 0,
    submittedOn: 0,
    hidden: types.maybe(types.boolean),
    starred: types.maybe(types.boolean)
  })
  .views(self => ({
    getEmailAddressRefs() {
      return getParent(self).emailAddresses;
    },
    getTimeSpent() {
      return self.timeSpent ? shared.toTimeString(self.timeSpent) : null;
    },
    getSubmittedOn() {
      if (self.isValid()) {
        return shared.dateFunc(self.submittedOn);
      }
      return (
        <Badge color="warning">
          <Trans i18nKey="submissions.notSubmitted" />
        </Badge>
      );
    },
    hasSubmissionFor(emailAddress) {
      return self.emailAddresses.findIndex(e => e.id === emailAddress.id) > -1;
    },
    getViewUrl() {
      if (self.isValid()) {
        return (
          <a target="_blank" href={self.viewUrl} rel="noopener noreferrer">
            <Button outline color="primary" size="sm">
              <FontAwesomeIcon icon="eye" />{" "}
              <Trans i18nKey="groupSubmission.view" />
            </Button>
          </a>
        );
      }
      return null;
    },
    isValid() {
      return self.entryId > 0;
    },
    getEmailAddresses() {
      return self.emailAddresses.map((e, i) => {
        return e.name ? (
          <span>
            {i > 0 ? ", " : ""}
            {e.name} ({e.emailAddress})
          </span>
        ) : (
          <span>
            {i > 0 ? ", " : ""}
            {e.emailAddress || e}
          </span>
        );
      });
    }
  }))
  .actions(self => ({
    toggleHidden() {
      self.hidden = !self.hidden;
    },
    toggleStar() {
      self.starred = !self.starred;
    }
  }));

const EnrolmentModel = types
  .model("Enrolment", {
    enrolled: false,
    code: "",
    submitted: 0,
    total: 0,
    pending: false
  })
  .views(self => ({
    badge() {
      if (self.enrolled) {
        return <Badge color="success">Zadáno</Badge>;
      }
      return <Badge color="warning">Nezadáno</Badge>;
    }
  }))
  .actions(self => ({
    enrol: flow(function* enrol() {
      const packageService = new PackageService();
      self.pending = true;
      try {
        const enrolled = yield packageService.enrolToExercise(
          getParent(getParent(getParent(self))).id,
          getParent(self).exerciseId
        );
        self.code = enrolled.data.code;
        self.enrolled = !self.enrolled;
      } catch (error) {
      } finally {
        self.pending = false;
      }
    })
  }));

const ExerciseModel = types.model("Exercise", {
  exerciseId: 0,
  name: "",
  description: "",
  topics: types.array(types.string),
  enrolment: types.maybeNull(EnrolmentModel)
});

const PackageModel = types.model("Package", {
  id: 0,
  code: "",
  exercises: types.array(ExerciseModel),
  submissions: types.array(SubmissionModel),
  emailAddresses: types.array(EmailAddressModel)
});

class PackageDetailPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      error: null,
      pending: false,
      type: "",
      authModal: false,
      activeTab: "1",
      classModal: false,
      notification: null,
      data: {
        exercise: {},
        warnings: []
      }
    };
  }

  refresh = () => {
    this.setState({ isLoading: true });
    const id = this.props.match.params.id;
    new DataService().getDetail(id).then(
      result => {
        this.setState({
          data: result.data,
          model: PackageModel.create(result.data),
          isLoading: false
        });
      },

      error =>
        this.setState({
          error,
          isLoading: false
        })
    );
  };

  toggleTab = tab => {
    this.setState({
      activeTab: tab.toString()
    });
  };

  componentDidMount() {
    this.refresh();
  }

  evaluate = evaluation => {
    this.setState({ pending: true, error: null });
    return shared.evaluate(evaluation).then(result => {
      this.setState({
        exercise: result.data || result,
        isLoading: false,
        pending: false
      });
    });
  };

  render() {
    const { isLoading, activeTab, model } = this.state;

    if (isLoading) {
      return <div>Loading</div>;
    }

    return (
      <Package
        {...this.props}
        activeTab={activeTab}
        packageData={model}
        toggleTab={this.toggleTab}
        evaluate={this.evaluate}
      />
    );
  }
}

class Wrapper extends React.Component {
  render() {
    return (
      <ApplicationStateConsumer>
        {auth => <PackageDetailPage {...this.props} {...auth} />}
      </ApplicationStateConsumer>
    );
  }
}

export default withEverything(Wrapper);
