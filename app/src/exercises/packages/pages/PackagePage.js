import React from "react";
import DataService from "./../services/PackageService";
import withNotification from "./../../../withNotification";
import { withTranslation } from "react-i18next";
import { ApplicationStateConsumer } from "./../../../ApplicationStateProvider";
import PublicPackage from "./PublicPackage";

class CatalogPackagePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      error: null,
      pending: false,
      type: "",
      authModal: false,
      classModal: false,
      notification: null,
      data: {
        exercise: {},
        warnings: []
      }
    };
  }

  refresh = () => {
    this.setState({ isLoading: true });
    const slug = this.props.match.params.slug;
    new DataService().getPublic(slug).then(
      result =>
        this.setState({
          data: result.data,
          isLoading: false
        }),
      error =>
        this.setState({
          error,
          isLoading: false
        })
    );
  };

  enrolUserToExercise = (exerciseId, groupId, type) => {
    return new DataService()
      .enrolUserToExercise(exerciseId, groupId, type)
      .then(response => {
        const { code } = response.data;
        return { success: true, code };
      })
      .catch(error => {
        this.props.notifyError(error);
        return { success: false, code: null };
      });
  };

  onSelected = type => {
    const { authenticated } = this.props;
    this.setState({
      type,
      authModal: !authenticated,
      classModal: authenticated
    });
  };

  onGroupSelected = group => {
    const { data, type } = this.state;
    const { exercise } = data;
    const { history } = this.props;

    this.enrolUserToExercise(exercise.id, group, type, false)
      .then(({ code, success }) => {
        this.setState({ pending: false });
        if (success) {
          history.push("/cviceni/detail/" + code);
        }
      })
      .catch(error => {
        this.setState({
          error,
          pending: false
        });
      });
  };

  onGroupCreating = group => {
    this.onGroupSelected({ id: -1, name: group.name });
  };

  toggleAuth = () => {
    this.setState(prevState => ({
      authModal: !prevState.authModal
    }));
  };

  toggleClass = () => {
    this.setState(prevState => ({
      classModal: !prevState.classModal
    }));
  };

  notify = notification => {
    this.setState({ notification });
  };

  login = credentials => {
    this.setState({ notification: null });
    this.props
      .login(credentials, this.notify)
      .then(_ => {
        this.onSelected(this.state.type);
      })
      .catch(error => {
        this.setState({
          error,
          pending: false
        });
      });
  };

  onAuthSuccess = data => {
    this.props.processNotifications(data);
    this.props.onAuthSuccess(data, null, this.notify);
    this.onSelected(this.state.type);
  };

  onAuthFailure = e => {
    this.props.onAuthFailure(e);
  };

  componentDidMount() {
    this.refresh();
  }

  render() {
    const { data, isLoading } = this.state;

    if (isLoading) {
      return <div>Loading</div>;
    }

    return <PublicPackage {...this.props} packageData={data} />;
  }
}

class Wrapper extends React.Component {
  render() {
    return (
      <ApplicationStateConsumer>
        {auth => <CatalogPackagePage {...this.props} {...auth} />}
      </ApplicationStateConsumer>
    );
  }
}

export default withTranslation()(withNotification(Wrapper));
