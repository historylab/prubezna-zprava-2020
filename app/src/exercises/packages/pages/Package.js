import React from "react";
import { Link } from "react-router-dom";
import {
  TabContent,
  TabPane,
  Nav,
  Row,
  Col,
  Button,
  ButtonGroup,
  NavItem,
  InputGroup,
  NavLink
} from "reactstrap";
import classnames from "classnames";
import PackageSubmissions from "./../components/PackageSubmissions";
import { If } from "../../../If";
import EmailAddressesDialog from "../../EmailAddressesDialog";
import PackageEmailAddresses from "./../components/PackageEmailAddresses";
import DeleteExerciseDialog from "../../DeleteExerciseDialog";
import "./../../../css/exercise-details.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Trans } from "react-i18next";
import i18n from "i18next";
import parse from "url-parse";
import PackageExercise from "../components/PackageExercise";
import Layout from "./../../../Layout";
import { observer } from "mobx-react";

/*class ExercisePackage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      emailAddresses: "",
      error: null,
      pending: false,
      modal: false,
      exercise: {},
      activeTab: "1",
      deleteExercise: {
        isOpen: false
      },
      addEmailAddresses: {
        isOpen: false
      }
    };
  }

  refresh = () => {
    if (this.props.location && this.props.location.state) {
      const { exercise } = this.props.location.state;
      if (exercise != null) {
        this.setState({
          exercise,
          isLoading: false
        });
        return;
      }
    }

    this.setState({ isLoading: true });
    const id = this.props.match.params.id;
    new DataService()
      .fetchMyExercise(id)
      .then(result => {
        this.setState({
          exercise: result.data || result,
          isLoading: false
        });
        return result;
      })
      .then(this.props.processNotifications)
      .catch(onError);
  };

  onError = error => {
    this.setState({
      error,
      isLoading: false
    });
    this.emailAddressesFormRef && this.emailAddressesFormRef.toggle();
    this.props.notify(error);
  };

  addEmailAddresses = (exercise, emailAddresses) => {
    this.setState({ pending: true, error: null });
    new DataService()
      .addEmailAddressesToGroup(exercise.group.id, emailAddresses)
      .then(result => {
        this.setState({ exercise: result.data || result });
        return result;
      })
      .then(this.props.processNotifications)
      .catch(onError)
      .finally(_ => {
        this.toggleAddEmailAddresses();
      });
  };

  onEmailAddressesChanged = e => {
    this.setState({ emailAddresses: e.target.value });
  };

  onSelected = (type, enrol) => {
    const { data } = this.state;
    const { exercise } = data;
    this.setState({ pending: true });
    enrol(exercise.id, type).then(() => {
      this.setState({ pending: false });
    });
  };

  sendExerciseToReceipients = () => {
    const { exercise } = this.state;
    new DataService()
      .sendExerciseToReceipients(exercise.id)
      .then(data => {
        this.props.processNotifications({
          notifications: data.notifications.concat(data.data.notifications)
        });
      })
      .catch(error => {
        onError(error);
      });
  };

  onGroupRenamed = groupName => {
    const { exercise } = this.state;
    this.groupChooserRef.setError(null);
    this.setState({ pending: true, error: null });
    new DataService()
      .changeGroupName(exercise.id, exercise.group.id, groupName)
      .then(result => {
        this.setState({ exercise: result.data || result });
        this.groupChooserRef.toggle();
        return result;
      })
      .then(this.props.processNotifications)
      .catch(error => {
        onError(error);
        this.groupChooserRef.setError(error);
      });
  };

  deleteEnrolment = () => {
    const { exercise } = this.state;
    const { history } = this.props;
    this.setState({ pending: true, error: null });
    new DataService()
      .deleteEnrolment(exercise.id)
      .then(this.props.processNotifications)
      .then(() => {
        history.push("/moje");
      })
      .catch(error => {
        onError(error);
      });
  };

  onGroupSelected = groupId => {
    const { exercise } = this.state;
    this.groupChooserRef.setError(null);
    this.setState({ pending: true, error: null });
    new DataService()
      .changeGroup(exercise.id, groupId)
      .then(result => {
        this.setState({ exercise: result.data || result });
        this.groupChooserRef.toggle();
        return result;
      })
      .then(this.props.processNotifications)
      .catch(error => {
        onError(error);
        this.groupChooserRef.setError(error);
      });
  };

  evaluate = evaluation => {
    const { exercise } = this.state;
    this.setState({ pending: true, error: null });
    return new DataService()
      .evaluate({ enrolmentId: exercise.id, ...evaluation })
      .then(result => {
        this.setState({
          exercise: result.data || result,
          isLoading: false,
          pending: false
        });
        return result;
      })
      .then(result => {
        return new Promise((resolve, reject) => {
          resolve(exercise.submissions);
        });
      })
      .catch(error => {
        return new Promise((resolve, reject) => {
          reject(error);
        });
      });
  };

  deleteEmailAddressFromGroup = emailAddress => {
    const { exercise } = this.state;
    this.setState({ pending: true, error: null });
    new DataService()
      .deleteEmailAddressFromGroup(exercise.group.id, emailAddress)
      .then(result => {
        this.setState({
          exercise: result.data || result,
          modal: false,
          emailAddress: {}
        });
        this.emailAddressesFormRef && this.emailAddressesFormRef.toggle();
      })
      .then(this.props.processNotifications)
      .catch(onError);
  };

  addEmailAddressToGroup = emailAddress => {
    const { exercise } = this.state;
    this.setState({ pending: true, error: null });
    new DataService()
      .addEmailAddressesToGroup(exercise.group.id, emailAddress.emailAddress)
      .then(result => {
        this.setState({ exercise: result.data || result });
        return result;
      })
      .then(this.props.processNotifications)
      .catch(onError);
  };

  shareWithOthers = submission => {
    const { exercise } = this.state;
    return new DataService()
      .shareWithOthers(exercise.id, submission.entryId)
      .then(this.props.processNotifications);
  };

  *componentDidMount() {
    this.refresh();
  }

  componentDidUpdate(prevProps, prevState) {
    if (!prevProps.authenticated && this.props.authenticated) {
      this.refresh();
    }
  }*

  toggleTab(tab) {
    this.setState({
      activeTab: tab.toString()
    });
  }

  copyToClipboard = async text => {
    const notification = {
      type: "info",
      text: i18n.t(
        "package.linkCopied",
        "Odkaz na cvičení byl zkopírován do schránky."
      )
    };

    if (window.navigator.clipboard) {
      await window.navigator.clipboard.writeText(text);
      this.props.notify(notification);
      return;
    }

    try {
      document.querySelector(".exerciseLink").select();
      document.execCommand("copy");
      this.props.notify(notification);
    } catch (e) {
      this.props.notify({
        type: "warning",
        text: i18n.t(
          "package.linkNotCopied",
          "Odkaz na cvičení nebyl zkopírován do schránky."
        )
      });
      return;
    }

    this.props.notify(notification);
  };

  toggleDeleteConfirmation = () => {
    this.setState({
      deleteExercise: {
        isOpen: !deleteExercise.isOpen
      }
    });
  };

  toggleAddEmailAddresses = () => {
    this.setState({
      addEmailAddresses: {
        isOpen: !addEmailAddresses.isOpen
      }
    });
  };*/

function Package(props) {
  const {
    packageData,
    activeTab,
    toggleAddEmailAddresses,
    toggleDeleteConfirmation,
    addEmailAddresses,
    evaluate,
    shareWithOthers,
    addEmailAddressToGroup,
    deleteEmailAddressFromGroup,
    sendExerciseToReceipients,
    deleteEnrolment,
    toggleTab
  } = props;

  const title = packageData.name;
  const exerciseUrl = parse(packageData.url, true);
  let query = exerciseUrl.query;
  query["lang"] = i18n.language;
  exerciseUrl.set("query", query);

  const questionnaireUrl = process.env.REACT_APP_QUESTIONNAIRE_URL || "";
  const showQuestionnaireHint =
    questionnaireUrl !== null && questionnaireUrl.trim().length > 0;

  const runAsTeacher = exercise => {
    const tryItUrl = parse(exercise.tryItUrl, true);
    query = tryItUrl.query;
    query["lang"] = i18n.language;
    tryItUrl.set("query", query);
    return (
      <InputGroup className="mr-2">
        <a href={tryItUrl} target="_blank" rel="noopener noreferrer">
          <Button size="sm" color="link">
            <FontAwesomeIcon className="mr-1" size="xs" icon="play-circle" />
            <Trans i18nKey="package.startExerciseAsTeacher">
              Spustit cvičení jako učitel
            </Trans>
          </Button>
        </a>
      </InputGroup>
    );
  };

  let NavItemWithLogic = (number, i18nKey) => (
    <NavItem>
      <NavLink
        className={classnames({
          active: activeTab * 1 === number * 1
        })}
        onClick={() => {
          toggleTab(number);
        }}
      >
        <Trans i18nKey={i18nKey} />
      </NavLink>
    </NavItem>
  );

  return (
    <Layout
      subHeader={
        <>
          <h5 className="label">
            <Link to="/tridy">
              <Trans i18nKey="mainHeader.myClasses">Moje třídy</Trans>
            </Link>{" "}
            / <Trans i18nKey="exeraciseDetail.assignedExercise">Balíček</Trans>
          </h5>
          <br />
          <h1 className="d-inline-block">
            <span className="ml-2">{title}</span>
          </h1>

          {/*isLoading && (
            <span data-testid="loading-indicator">
              <FontAwesomeIcon icon="spinner" size="3x" spin={true} />
            </span>
          )*/}
        </>
      }
    >
      <Layout.Content>
        <div className="container-fluid">
          <div>
            <Row>
              <Col xs="8">
                <div className="display-flex">
                  {/*<InputGroupAddon
                                onClick={e => this.copyToClipboard(exerciseUrl)}
                                addonType="prepend"
                                title={t(
                                  "package.copyLinkTooltip",
                                  "Zkopírovat do schránky"
                                )}
                              >
                                <Button size="sm" color="link">
                                  <FontAwesomeIcon
                                    className="mr-1"
                                    size="xs"
                                    icon="link"
                                  />
                                  <Trans i18nKey="package.copyLink">
                                    Zkopírovat odkaz pro třídu
                                  </Trans>
                                </Button>
                              </InputGroupAddon>
                              →&nbsp;
                              <Input
                                value={exerciseUrl}
                                className="mr-1 exerciseLink"
                                readOnly
                              />*/}
                </div>
                {showQuestionnaireHint && (
                  <div>
                    <div className="label">
                      <Trans i18nKey="package.instructions">
                        Instrukce k testování:
                      </Trans>
                    </div>
                    <div>
                      <a
                        href={questionnaireUrl}
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        <Trans i18nKey="package.questionnaire">
                          Po dokončení hodiny prosíme o vyplnění dotazníku.
                          Děkujeme.
                        </Trans>
                        <FontAwesomeIcon
                          className="ml-1"
                          size="xs"
                          icon="external-link-alt"
                        />
                      </a>
                    </div>
                  </div>
                )}
              </Col>
              {/*<Col xs="3" className="text-right">
                            <div className="mb-2">
                              {exercise.code && (
                                <QrCode
                                  size={150}
                                  value={exerciseUrl.toString()}
                                />
                              )}
                            </div>
                              </Col>*/}
            </Row>
            {/*<Row>
            <Col>
              <GroupChooser
                exercise={packageData}
                renameGroup={onGroupRenamed}
                selectGroup={onGroupSelected}
                selectOriginalGroup={onOriginalGroupSelected}
                ref={ref => {
                  //this.groupChooserRef = ref;
                }}
              />
            </Col>
            <Col />
              </Row>*/}
            <Row>
              <Col>
                <Nav tabs>
                  {NavItemWithLogic(1, "package.exercises")}
                  {NavItemWithLogic(2, "package.submissions")}
                  {NavItemWithLogic(3, "package.classMembers")}
                  {NavItemWithLogic(4, "package.emailTemplate")}
                  {NavItemWithLogic(5, "package.settings")}
                </Nav>
                <TabContent activeTab={activeTab}>
                  <TabPane tabId="1">
                    {packageData.exercises.map(e => (
                      <PackageExercise exercise={e}>
                        {runAsTeacher(e)}
                        {e.enrolment.enrolled && (
                          <Button
                            outline
                            onClick={_ => e.enrolment.enrol()}
                            color="danger"
                          >
                            Zrušit zadání žákům
                          </Button>
                        )}
                        {!e.enrolment.enrolled && (
                          <Button
                            onClick={_ => e.enrolment.enrol()}
                            color="primary"
                          >
                            {e.enrolment.pending ? "1" : ""} Zadat žákům
                          </Button>
                        )}
                        {
                          <div>
                            <Button outline>Odeslat upozornění žákům</Button>
                          </div>
                        }
                      </PackageExercise>
                    ))}
                  </TabPane>
                  <TabPane tabId="2">
                    <PackageSubmissions
                      submissions={packageData.submissions}
                      exercises={packageData.exercises}
                      evaluate={evaluate}
                      shareWithOthers={shareWithOthers}
                      enableShare={packageData.type === "class"}
                    />
                  </TabPane>
                  <TabPane tabId="3">
                    <PackageEmailAddresses
                      emailAddresses={packageData.emailAddresses}
                      addEmailAddress={addEmailAddressToGroup}
                      deleteEmailAddress={deleteEmailAddressFromGroup}
                      submissions={packageData.submissions}
                      exercises={packageData.exercises}
                    />
                    <ButtonGroup>
                      <Button
                        onClick={toggleAddEmailAddresses}
                        outline
                        color="secondary"
                      >
                        <Trans i18nKey="package.addEmailAddresses">
                          Přidat emailové adresy do třídy
                        </Trans>
                      </Button>
                      <If condition={packageData.emailAddresses.length > 0}>
                        <Button
                          onClick={sendExerciseToReceipients}
                          outline
                          color="secondary"
                        >
                          <Trans i18nKey="package.sendEmail">
                            Odeslat žákům ve třídě email se zadáním
                          </Trans>
                        </Button>
                      </If>
                    </ButtonGroup>
                  </TabPane>
                  <TabPane tabId="5">
                    <ButtonGroup>
                      <Button
                        onClick={toggleDeleteConfirmation}
                        outline
                        color="danger"
                        size="sm"
                      >
                        <Trans i18nKey="package.removeExercise">
                          Odstranit balíček z mého seznamu
                        </Trans>
                      </Button>
                    </ButtonGroup>
                  </TabPane>
                </TabContent>
              </Col>
            </Row>
            <Row className="mt-4 justify-content-end">
              <ButtonGroup>
                <DeleteExerciseDialog
                  isOpen={false}
                  toggle={toggleDeleteConfirmation}
                  onConfirmed={deleteEnrolment}
                />
                <EmailAddressesDialog
                  exercise={packageData}
                  onConfirmed={addEmailAddresses}
                  isOpen={false}
                  toggle={toggleAddEmailAddresses}
                />
              </ButtonGroup>
            </Row>
          </div>
        </div>
      </Layout.Content>
    </Layout>
  );
}

//export default withEverything(ExercisePackage);
export default observer(Package);
