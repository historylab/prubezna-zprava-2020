import React from "react";
import { Card, CardImg, Badge, Progress } from "reactstrap";
import { If } from "./../../../If";
import { withTranslation, Trans } from "react-i18next";
import classnames from "classnames";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { observer } from "mobx-react";

const PackageExercise = ({ exercise, children }) => {
  const current = exercise.submitted;
  const max = exercise.total;

  return (
    <div className="package-exercise-container">
      <Card className="col1 list-box">
        <div className="card-image">
          <CardImg
            className={classnames({
              "card-image-file": true,
              enrolled: exercise.enrolled,
              "not-enrolled": !exercise.enrolled
            })}
            src={exercise.image_url}
            width={300}
            height={300}
          />
        </div>
        <div className="card-text">
          {exercise.enrolment.badge()}[{exercise.enrolment.code}]
          <If
            condition={
              exercise.enrolled &&
              max > 0 &&
              current !== max &&
              exercise.type !== "class"
            }
          >
            <div className="list-content">
              <If condition={max === 0 && exercise.type !== "class"}>
                <Badge color="warning">
                  <Trans i18nKey="exercise.noEmailAddresses">
                    Bez zadaných emailových adres
                  </Trans>
                </Badge>
              </If>
              <If
                condition={
                  max > 0 && current === max && exercise.type !== "class"
                }
              >
                <div className="text-center">
                  <Trans i18nKey="exercise.allSubmitted">Vše odevzdáno</Trans>
                </div>
              </If>
              <div>
                <div className="text-center">
                  {current} / {max}
                </div>
                <Progress color="success" value={current} max={max} />{" "}
              </div>
            </div>
          </If>
        </div>
      </Card>
      <div className="col2">
        {exercise.enrolment.badge()}
        <h3>{exercise.name}</h3>
        <div className="meta">
          <div className="meta-item meta-taxonmy">
            <div className="taxonomy-container topics ">
              <ul className="taxonomy">
                <FontAwesomeIcon size="sm" icon="tags" className="mr-1" />
                {exercise.topics.map((tag, index) => (
                  <li key={index} className="taxonomy-item">
                    {tag}
                  </li>
                ))}
              </ul>
            </div>
          </div>
          <div className="meta-item meta-time">
            <FontAwesomeIcon size="sm" icon="clock" className="mr-1" />
            {exercise.estimatedTime}
            <span> min</span>
          </div>
          <div className="meta-item meta-manual">
            <a
              href={exercise.instructionsUrl}
              download="download"
              className="metodica"
            >
              <FontAwesomeIcon
                size="sm"
                icon={["far", "file-alt"]}
                className="mr-1"
              />
              <Trans i18nKey="catalogExerciseDetail.methodology">
                Doporučený postup
              </Trans>
            </a>
          </div>
        </div>
        <div className="detail-text">
          <p>{exercise.description}</p>
        </div>
      </div>
      {children && children.length > 0 && (
        <div className="col3">{children}</div>
      )}
    </div>
  );
};

export default withTranslation()(observer(PackageExercise));
