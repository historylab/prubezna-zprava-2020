import React from "react";
import { action } from "@storybook/addon-actions";

import "./../../../css/List.css";
import PackageExercise from "./PackageExercise";

export default {
  component: PackageExercise,
  title: "Package Exercise",
  // Our exports that end in "Data" are not stories.
  excludeStories: /.*Data$/
};

export const exerciseData = {
  instructionsUrl:
    "https://app.testlab4.felk.cvut.cz/cviceni-aktualni/assets/pdf/doporuceny-postup/co-ministr-vzkazuje-ucitelum.pdf",
  tryItUrl:
    "https://app.testlab4.felk.cvut.cz/cviceni/co-ministr-vzkazuje-ucitelum?lang=cs",
  url: "https://testlab4.felk.cvut.cz/cviceni/co-ministr-vzkazuje-ucitelum",
  image_url:
    "https://app.testlab4.felk.cvut.cz/cviceni-aktualni/assets/img/co-ministr-vzkazuje-ucitelum/pic-02-1024h.jpg",
  thumb_image_url:
    "https://app.testlab4.felk.cvut.cz/cviceni-aktualni/assets/img/co-ministr-vzkazuje-ucitelum/pic-02-1024h-thumb.jpg",
  id: 2867,
  code: "cviceniCoMinistrVzkazujeUcitelum",
  name: "Co ministr vzkazuje učitelům?",
  description:
    "V rámci cvičení se žáci prostřednictvím rozhlasového projevu ministra školství ČSR Jaromíra Hrbka ze září 1969 seznamují s normalizačním výkladem událostí roku 1968. S využitím dalších pramenů zkoumají významy, které mohli projevu ministra přikládat doboví posluchači, zejména učitelé. Cvičení využívá obeznámenost žáků se sociálním prostředím školy a podněcuje je k tomu, aby vnímali události pohledem dobových učitelů a promýšleli možné dopady projevu na jejich jednání.",
  taxonomy: "lupa;cteni;text;audio;svg",
  topics: "1968;normalizace;škola;ideologie;dobové perspektivy;měníme pohledy",
  enabled: true,
  slug: "co-ministr-vzkazuje-ucitelum",
  authors: "Kamil Činátl",
  exerciseId: 2,
  language: "cs",
  estimatedTime: 30,
  enrolled: true,
  code: "12376E27"
};

export const actionsData = {
  onPinTask: action("onPinTask"),
  onArchiveTask: action("onArchiveTask")
};

export const NotEnrolled = () => (
  <PackageExercise
    exercise={{ ...exerciseData, enrolled: false }}
    {...actionsData}
  />
);

export const EnroledNoSubmissions = () => (
  <PackageExercise
    exercise={{ ...exerciseData, total: 10, submitted: 0 }}
    {...actionsData}
  />
);

export const EnroledWithSubmissions = () => (
  <PackageExercise
    exercise={{ ...exerciseData, total: 10, submitted: 2 }}
    {...actionsData}
  />
);
