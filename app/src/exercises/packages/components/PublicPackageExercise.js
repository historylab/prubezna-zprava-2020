import React from "react";
import { Card, CardImg, Button, InputGroup } from "reactstrap";
import { withTranslation, Trans } from "react-i18next";
import classnames from "classnames";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import parse from "url-parse";
import i18n from "i18next";

const PublicPackageExercise = ({ exercise, children }) => {
  let description = exercise.description || "";
  if (description) {
    description =
      description.length < 120
        ? description
        : description.substring(0, 120) + "...";
  }

  const runAsTeacher = exercise => {
    const tryItUrl = parse(exercise.tryItUrl, true);
    const query = tryItUrl.query;
    query["lang"] = i18n.language;
    tryItUrl.set("query", query);
    return (
      <InputGroup className="mr-2">
        <a href={tryItUrl} target="_blank" rel="noopener noreferrer">
          <Button size="sm" color="link">
            <FontAwesomeIcon className="mr-1" size="xs" icon="play-circle" />
            <Trans i18nKey="package.startExerciseAsTeacher">
              Spustit cvičení jako učitel
            </Trans>
          </Button>
        </a>
      </InputGroup>
    );
  };

  return (
    <div className="package-exercise-container">
      <Card className="col1 list-box">
        <div className="card-image">
          <CardImg
            className={classnames({
              "card-image-file": true
            })}
            src={exercise.image_url}
            width={300}
            height={300}
          />
        </div>
      </Card>
      <div className="col2">
        <h3>{exercise.name}</h3>
        <div className="meta">
          <div className="meta-item meta-taxonmy">
            <div className="taxonomy-container topics ">
              <ul className="taxonomy">
                <FontAwesomeIcon size="sm" icon="tags" className="mr-1" />
                {exercise.topics.map((tag, index) => (
                  <li key={index} className="taxonomy-item">
                    {tag}
                  </li>
                ))}
              </ul>
            </div>
          </div>
          <div className="meta-item meta-time">
            <FontAwesomeIcon size="sm" icon="clock" className="mr-1" />
            {exercise.estimatedTime}
            <span> min</span>
          </div>
          <div className="meta-item meta-manual">
            <a
              href={exercise.instructionsUrl}
              download="download"
              className="metodica"
            >
              <FontAwesomeIcon
                size="sm"
                icon={["far", "file-alt"]}
                className="mr-1"
              />
              <Trans i18nKey="catalogExerciseDetail.methodology">
                Doporučený postup
              </Trans>
            </a>
          </div>
          <div>{runAsTeacher(exercise)}</div>
        </div>
        <div className="detail-text">
          <p>{exercise.description}</p>
        </div>
      </div>
      {children && children.length > 0 && (
        <div className="col3">{children}</div>
      )}
    </div>
  );
};

export default withTranslation()(PublicPackageExercise);
