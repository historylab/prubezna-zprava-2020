import React from "react";
import { Table, Button } from "reactstrap";
import "moment/min/locales";
import { If } from "./../../../If";
import Evaluation from "./../../../submissions/Evaluation";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { withTranslation } from "react-i18next";
import { Trans } from "react-i18next";
import _ from "lodash";
import { observer } from "mobx-react";

class PackageSubmissions extends React.Component {
  constructor(props) {
    super(props);

    this.dismissCallback = null;

    this.state = {
      modal: false,
      modalNote: false,
      submission: {}
    };
  }

  toggle = () => {
    this.setState({
      modal: !this.state.modal
    });
  };

  toggleNoteModal = submission => {
    this.evaluationDialog.setSumbission(submission, () => {
      this.setState({
        modalNote: !this.state.modalNote
      });
    });
  };

  confirmDeleteEmailAddress = (e, submission) => {
    this.setState({
      modal: !this.state.modal,
      emailAddress: submission.emailAddresses[0]
    });
  };

  shareWithOthers = (e, submission) => {
    this.props.shareWithOthers(submission);
  };

  deleteSubmission = () => {
    const { emailAddress } = this.state;
    return this.props.onSubmissionDelete(emailAddress).then(_ => {
      this.setState({
        modal: false
      });
    });
  };

  evaluate = evaluation => {
    return this.props.evaluate(evaluation).then(_ => {
      this.setState({
        modalNote: false
      });
    });
  };

  render() {
    const { t, submissions, exercises } = this.props;

    let getExerciseName = _.memoize(exerciseId => {
      const exercise = exercises.find(e => e.exerciseId === exerciseId);
      if (exercise) {
        return exercise.name;
      }
      return exerciseId;
    });

    if (submissions.length === 0) {
      return (
        <div>
          <h2>
            <Trans i18nKey="submissions.noSubmissions">
              Nikdo ještě nic neodevzdal ...
            </Trans>
          </h2>
          <p>
            <Trans i18nKey="submissions.subheader">
              Ale mezitím můžete přidat emailové adresy žáků, od nichž očekáváte
              odevzdání.
            </Trans>
          </p>
        </div>
      );
    }

    return (
      <Table>
        <thead>
          <tr>
            <th>#</th>
            <th>
              <Trans i18nKey="submissions.student">Student</Trans>
            </th>
            <th>
              <Trans i18nKey="submissions.exercise">Cvičení</Trans>
            </th>
            <th />
            <th>
              <Trans i18nKey="submissions.submittedOn">Odevzdáno</Trans>
            </th>
            <th>
              <Trans i18nKey="submissions.timeSpent">Strávený čas</Trans>
            </th>
            <th />
            <th />
            <th>
              <Trans i18nKey="groupSubmission.evaluation">Hodnocení</Trans>
            </th>
          </tr>
        </thead>
        <tbody>
          {submissions.map((submission, index) => {
            return (
              <tr key={index}>
                <th scope="row">{index + 1}</th>
                <td>{submission.getEmailAddresses()}</td>
                <td>{getExerciseName(submission.exerciseId)}</td>
                <td>{submission.getViewUrl()}</td>
                <td className="text-truncate">{submission.getSubmittedOn()}</td>
                <td className="text-truncate">{submission.getTimeSpent()}</td>
                <td>
                  <FontAwesomeIcon
                    onClick={_ => submission.toggleStar()}
                    icon={[submission.starred ? "fas" : "far", "star"]}
                  />
                </td>
                <td>
                  <FontAwesomeIcon
                    onClick={_ => submission.toggleHidden()}
                    icon={submission.hidden ? "eye-slash" : "eye"}
                  />
                </td>
                <td>
                  <If condition={submission.isValid()}>
                    <Button
                      size="sm"
                      onClick={e => this.toggleNoteModal(submission)}
                    >
                      {submission.note
                        ? t("submissions.edit")
                        : t("submissions.add")}
                    </Button>
                  </If>
                </td>
                <td>
                  <If condition={true /*submission.isValid && enableShare*/}>
                    <Button
                      outline
                      size="sm"
                      onClick={e => this.shareWithOthers(e, submission)}
                      title={t(
                        "submissions.shareWithOthers",
                        "Sdílet toto odeslané cvičení s ostatními"
                      )}
                    >
                      <FontAwesomeIcon icon="share" /> <Trans i18nKey="share" />
                    </Button>
                  </If>
                </td>
              </tr>
            );
          })}
        </tbody>
        <Evaluation
          isOpen={this.state.modalNote}
          toggle={this.toggleNoteModal}
          submission={this.state.submission}
          evaluate={this.evaluate}
          ref={ref => {
            this.evaluationDialog = ref;
          }}
        />
      </Table>
    );
  }
}

export default withTranslation()(observer(PackageSubmissions));

/*
        <DeleteEmailDialog
          isOpen={this.state.modal}
          emailAddress={this.state.emailAddress}
          toggle={this.toggle}
          deleteSubmission={this.deleteSubmission}
        />*/
