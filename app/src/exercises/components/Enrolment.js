import React from "react";
import { Row, Col, Button, Modal, ModalBody } from "reactstrap";
import { Trans, withTranslation } from "react-i18next";
import ExerciseIcon from "./../../ExerciseIcon";
import parse from "url-parse";
import i18n from "i18next";
import withNotification from "./../../withNotification";
import { ApplicationStateConsumer } from "./../../ApplicationStateProvider";
import Logo from "./../../icons/logo";
import LoginForm from "./../../Auth/LoginForm";
import DataService from "./../../DataService";
import ChooseOrCreateGroupModal from "./../../groups/components/ChooseOrCreateGroupModal";

class Enrolment extends React.Component {
  constructor() {
    super();
    this.state = {
      authModal: false,
      classModal: false
    };
  }

  onSelected = type => {
    const { authenticated } = this.props;
    this.setState({
      type,
      authModal: !authenticated,
      classModal: authenticated
    });
  };

  onAssignActivity = group => {
    console.info(group);
    if (group.id < 0) {
      this.onGroupCreating(group);
    } else {
      this.onGroupSelected(group);
    }
  };

  onGroupSelected = group => {
    const { type } = this.state;
    const { history, exercise } = this.props;

    if (this.props.package) {
      this.enrolUserToPackage(this.props.package.id, group, type, false)
        .then(({ code, success }) => {
          this.setState({ pending: false });
          if (success) {
            history.push("/balicek/detail/" + code);
          }
        })
        .catch(error => {
          this.setState({
            error,
            pending: false
          });
        });
      return;
    }

    this.enrolUserToExercise(exercise.id, group, type, false)
      .then(({ code, success }) => {
        this.setState({ pending: false });
        if (success) {
          history.push("/cviceni/detail/" + code);
        }
      })
      .catch(error => {
        this.setState({
          error,
          pending: false
        });
      });
  };

  enrolUserToExercise = (exerciseId, groupId, type) => {
    return new DataService()
      .enrolUserToExercise(exerciseId, groupId, type)
      .then(response => {
        const { code } = response.data;
        return { success: true, code };
      })
      .catch(error => {
        this.props.notifyError(error);
        return { success: false, code: null };
      });
  };

  enrolUserToPackage = (packageId, groupId, type) => {
    return new DataService()
      .enrolUserToPackage(packageId, groupId, type)
      .then(response => {
        const { code } = response.data;
        return { success: true, code };
      })
      .catch(error => {
        this.props.notifyError(error);
        return { success: false, code: null };
      });
  };

  onGroupCreating = group => {
    this.onGroupSelected({ id: -1, name: group.name });
  };

  toggleAuth = () => {
    this.setState(prevState => ({
      authModal: !prevState.authModal
    }));
  };

  toggleClass = () => {
    this.setState(prevState => ({
      classModal: !prevState.classModal
    }));
  };

  login = credentials => {
    this.setState({ notification: null });
    this.props
      .login(credentials, this.props.notify)
      .then(_ => {
        this.onSelected(this.state.type);
      })
      .catch(error => {
        this.setState({
          error,
          pending: false
        });
      });
  };

  onAuthSuccess = data => {
    this.props.onAuthSuccess(data, null, this.props.notify);
    this.onSelected(this.state.type);
  };

  onAuthFailure = e => {
    this.props.onAuthFailure(e);
  };

  render() {
    const props = this.props;
    const exercise = props.exercise || props.package;
    const exerciseUrl = parse(exercise.url, true);
    let query = exerciseUrl.query;
    query["lang"] = i18n.language;
    exerciseUrl.set("query", query);

    const tryItUrl = parse(exercise.tryItUrl, true);
    query = tryItUrl.query;
    query["lang"] = i18n.language;
    tryItUrl.set("query", query);

    console.info(this.props, this.state);

    return (
      <div className="detail-buttons">
        <Row>
          <Col xs="auto">
            <h4>
              <Trans i18nKey="catalogExerciseDetail.enrol">
                Zadat cvičení třídě
              </Trans>
            </h4>
          </Col>
        </Row>
        <Row>
          <Col xs="auto">
            <Button
              role="individual"
              className="button secondary white"
              onClick={() => this.onSelected("individual")}
            >
              <ExerciseIcon type="user" />
              <Trans i18nKey="catalogExerciseDetail.individual">
                Individuální
              </Trans>
            </Button>
          </Col>
          <Col xs="auto">
            <Button
              className="button secondary white mbm-0"
              onClick={() => this.onSelected("group")}
            >
              <ExerciseIcon type="group" />
              <Trans i18nKey="catalogExerciseDetail.group">Skupinové</Trans>
            </Button>
          </Col>
          <Col xs="auto">
            <Button
              className="button secondary white mbm-0"
              onClick={() => this.onSelected("class")}
            >
              <ExerciseIcon type="class" />{" "}
              <Trans i18nKey="catalogExerciseDetail.class">
                S celou třídou
              </Trans>
            </Button>
          </Col>
          <Col>
            <Trans i18nKey="catalogExerciseDetail.or">nebo</Trans>
          </Col>
          <Col xs="auto">
            <a
              target="_blank"
              rel="noopener noreferrer"
              href={tryItUrl}
              className="button secondary white mbm-0"
            >
              <Trans i18nKey="catalogExerciseDetail.try">
                Vyzkoušet cvičení
              </Trans>
            </a>
          </Col>
        </Row>
        <Modal
          // TODO: create separate component for the login modal
          isOpen={this.state.authModal}
          toggle={this.toggleAuth}
          className="modal--login"
          backdrop={true}
          role="authenticate"
        >
          <Logo />
          <ModalBody>
            <p className="text-center">
              <Trans i18nKey="catalogExerciseDetail.signInFirst">
                Cvičení si můžete zadat až po přihlášení.
              </Trans>
            </p>
            <LoginForm
              login={this.login}
              notification={this.state.notification}
              onAuthSuccess={this.onAuthSuccess}
              onAuthFailure={this.onAuthFailure}
            />
          </ModalBody>
        </Modal>
        {this.props.authenticated && (
          <ChooseOrCreateGroupLoader
            isOpen={this.state.classModal}
            toggle={this.toggleClass}
            type={this.props.type}
            onAssignActivity={this.onAssignActivity}
          />
        )}
      </div>
    );
  }
}

class ChooseOrCreateGroupLoader extends React.Component {
  constructor() {
    super();
    this.state = {
      isLoading: true,
      groups: []
    };
  }

  componentDidMount() {
    this.setState({ isLoading: true });
    new DataService().fetchGroups().then(
      result => {
        const groups = result.data.groups;
        this.setState({ groups, isLoading: false });
      },
      error =>
        this.setState({
          error,
          isLoading: false
        })
    );
  }

  render() {
    const { isLoading, groups } = this.state;
    return (
      <ChooseOrCreateGroupModal
        loading={isLoading}
        groups={groups}
        onAssignActivity={this.onAssignActivity}
        {...this.props}
      />
    );
  }
}

class Wrapper extends React.Component {
  render() {
    return (
      <ApplicationStateConsumer>
        {auth => <Enrolment {...this.props} {...auth} />}
      </ApplicationStateConsumer>
    );
  }
}

export default withTranslation()(withNotification(Wrapper));
