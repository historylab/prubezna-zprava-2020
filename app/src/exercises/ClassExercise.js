import React from "react";
import { Container, Row, Col, Button } from "reactstrap";
import GroupChooser from "./../GroupChooser";

class ClassExercise extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      emailAddresses: "",
      error: null,
      pending: false,
      modal: false,
      data: {
        exercise: [],
        warnings: []
      },
      exercise: {}
    };
  }

  onGroupNameChanged = e => {
    const { exercise } = this.state;
    this.props.notify({ text: "Název třídy byl změněn." });
  };

  onSelected = (type, enrol) => {
    const { data } = this.state;
    const { exercise } = data;
    this.setState({ pending: true });
    enrol(exercise.id, type).then(() => {
      this.setState({ pending: false });
    });
  };

  render() {
    const { exercise } = this.props;
    return (
      <div id="ClassExerciseDetail">
        <Container>
          <Row>
            <Col>
              <img
                width={400}
                height={400}
                alt={exercise.name}
                src={exercise.imageUrl}
              />
            </Col>
            <Col>
              <p>{exercise.metadata.anotace.verejna}</p>
              <a href={exercise.url} target="_blank" rel="noopener noreferrer">
                <Button size="lg" color="primary">
                  Spustit cvičení
                </Button>
              </a>
            </Col>
            <Col>
              <h3>{exercise.typeDescription}</h3>
            </Col>
          </Row>
          <Row>
            <Col xs="4">
              <GroupChooser exercise={exercise} />
            </Col>
            <Col />
          </Row>
        </Container>
      </div>
    );
  }
}

export default ClassExercise;
