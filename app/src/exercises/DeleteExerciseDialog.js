import React from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import { withTranslation, Trans } from "react-i18next";

const DeleteExerciseDialog = ({ isOpen, toggle, onConfirmed }) => {
  return (
    <Modal isOpen={isOpen} toggle={toggle} backdrop={true}>
      <ModalHeader toggle={toggle}>
        <Trans i18nKey="deleteExerciseDialog.header"></Trans>
      </ModalHeader>
      <ModalBody>
        <Trans i18nKey="deleteExerciseDialog.confirmation"></Trans>
      </ModalBody>
      <ModalFooter>
        <Button color="primary" onClick={onConfirmed}>
          <Trans i18nKey="deleteExerciseDialog.yes"></Trans>
        </Button>
        <Button color="secondary" onClick={toggle}>
          <Trans i18nKey="no"></Trans>
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default withTranslation()(DeleteExerciseDialog);
