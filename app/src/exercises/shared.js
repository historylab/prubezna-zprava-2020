import DataService from "./../DataService";
import Moment from "moment";
import "moment/min/locales";
import { extendMoment } from "moment-range";

const evaluate = evaluation => {
  return new DataService()
    .evaluate(evaluation)
    .then(result => {
      return result;
    })
    .then(result => {
      return new Promise((resolve, reject) => {
        resolve({});
      });
    })
    .catch(error => {
      return new Promise((resolve, reject) => {
        reject(error);
      });
    });
};

const padStart = (s, count, char) => {
  return ("000" + s).slice(-count);
};

const toTimeString = ts => {
  let timeSpent = Math.floor(ts / 1000); // seconds
  const sd = 60 * 60 * 24;
  const sh = 60 * 60;
  const days = Math.floor(timeSpent / sd);
  timeSpent -= sd * days;
  const hours = Math.floor(timeSpent / sh);
  timeSpent -= sh * hours;
  const minutes = Math.floor(timeSpent / 60);
  let seconds = timeSpent - 60 * minutes;

  let r = `${padStart(hours, 2, "0")}:${padStart(minutes, 2, "0")}:${padStart(
    seconds,
    2,
    "0"
  )}`;
  if (days > 0) {
    return `${days}d ${r}`;
  }
  return r;
};

const moment = extendMoment(Moment);

const dateFunc = s => {
  const start = moment(s);
  const end = moment();
  const r = moment.range(start, end);
  const days = r.diff("days");
  if (days <= 7) {
    return start.locale("cs").calendar();
  }
  return start.format("DD.MM.YYYY HH:mm");
};

export default { evaluate, dateFunc, toTimeString };
