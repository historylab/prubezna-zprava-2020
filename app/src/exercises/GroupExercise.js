import React from "react";
import DataService from "./../DataService";
import { Container, Row, Col, Button } from "reactstrap";
import QrCode from "qrcode.react";
import contentEditable from "./../contentEditable";
import GroupChooser from "./../GroupChooser";
import Submissions from "./../Submissions";
import EmailAddressesForm from "./../EmailAddressesForm";

class GroupExercise extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      emailAddresses: "",
      error: null,
      pending: false,
      data: {
        exercise: [],
        warnings: []
      },
      exercise: {}
    };
  }

  onEmailAddressesChanged = e => {
    this.setState({ emailAddresses: e.target.value });
  };

  onGroupNameChanged = e => {
    this.props.notify({ text: "Název třídy byl změněn." });
  };

  onSelected = (type, enrol) => {
    const { data } = this.state;
    const { exercise } = data;
    this.setState({ pending: true });
    enrol(exercise.id, type).then(() => {
      this.setState({ pending: false });
    });
  };

  render() {
    const { exercise, addEmailAddresses } = this.props;

    return (
      <div id="GroupExerciseDetail">
        <Container>
          <Row>
            <Col>
              <img
                width={400}
                height={400}
                alt={exercise.name}
                src={exercise.imageUrl}
              />
            </Col>
            <Col>
              <p>{exercise.metadata.anotace.verejna}</p>
            </Col>
            <Col>
              <h3>{exercise.typeDescription}</h3>
              <h1 className="text-uppercase">{exercise.code}</h1>
              {exercise.code && <QrCode size={150} value={exercise.url} />}
              <br />
              <a href={exercise.url} target="_blank" rel="noopener noreferrer">
                <Button size="lg" outline color="primary">
                  Vyzkoušet si cvičení
                </Button>
              </a>
            </Col>
          </Row>
          <Row>
            <Col xs="4">
              <GroupChooser exercise={exercise} />
            </Col>
            <Col />
          </Row>
          <Row>
            <Col>
              <Submissions
                submissions={exercise.submissions || { entries: [] }}
                onNoteEdit={this.noteEdit}
                onSubmissionDelete={this.onSubmissionDelete}
              />
            </Col>
          </Row>
          <Row>
            <Col>
              <EmailAddressesForm
                exercise={exercise}
                addEmailAddresses={addEmailAddresses}
              />
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default GroupExercise;
