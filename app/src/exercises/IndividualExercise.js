import React from "react";
import { Container, Row, Col, Button } from "reactstrap";
import QrCode from "qrcode.react";
import Submissions from "./../Submissions";
import EmailAddressesForm from "./../EmailAddressesForm";
import GroupChooser from "./../GroupChooser";

class IndividualExercise extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      emailAddresses: "",
      error: null,
      pending: false,
      modal: false,
      data: {
        exercise: [],
        warnings: []
      },
      exercise: {}
    };
  }

  onGroupNameChanged = e => {
    this.props.notify({ text: "Název třídy byl změněn." });
  };

  onSelected = (type, enrol) => {
    const { data } = this.state;
    const { exercise } = data;
    this.setState({ pending: true });
    enrol(exercise.id, type).then(() => {
      this.setState({ pending: false });
    });
  };

  render() {
    const { exercise, addEmailAddresses } = this.props;

    return (
      <Container>
        <Row>
          <Col>
            <Submissions
              submissions={exercise.submissions || { entries: [] }}
              onNoteEdit={this.noteEdit}
              onSubmissionDelete={this.onSubmissionDelete}
            />
          </Col>
        </Row>
        <Row>
          <Col>
            <EmailAddressesForm
              exercise={exercise}
              addEmailAddresses={addEmailAddresses}
            />
          </Col>
        </Row>
      </Container>
    );
  }
}

export default IndividualExercise;
