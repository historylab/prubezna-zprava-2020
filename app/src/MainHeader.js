import React from "react";
import { Link } from "react-router-dom";
import { Container, Row, Col, Button } from "reactstrap";
import { Trans } from "react-i18next";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { ApplicationStateConsumer } from "./ApplicationStateProvider";
import Logo from "./icons/logo";
import classnames from "classnames";
import env from "@beam-australia/react-env";

import "./MainHeader.scss";

const MainHeader = ({ isHeaderAbsolutePositioned, isHeaderInverted }) => {
  const isTestEnvironment = env("IS_TEST_SERVER") === "1";

  return (
    <ApplicationStateConsumer>
      {({ authenticated, logout, changeLanguage }) => (
        <header
          id="header"
          className={classnames({
            header: true,
            "header--test": isTestEnvironment,
            "is-absolutePositioned": isHeaderAbsolutePositioned,
            "is-inverted": isHeaderInverted
          })}
        >
          <Link to="#" id="mobile-menu-toggle" title="Toggle navigation">
            <span>Menu</span>
            <i className="svg-icon">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="svg-icon-menu"
                viewBox="0 0 75 60"
              >
                <title>Mobile menu</title>
                <line y1="3.5" x2="75" y2="3.5" />
                <line y1="30.5" x2="75" y2="30.5" />
                <line y1="56.5" x2="75" y2="56.5" />
              </svg>
            </i>
          </Link>

          <Container>
            <Row>
              <Col md="auto" className="d-flex align-items-center mr-auto">
                <Link to="/" className="logo">
                  <Logo />
                </Link>

                {isTestEnvironment && (
                  <span className="MainHeader__testServer">
                    <Trans i18nKey="mainHeader.testServer">
                      Testovací server
                    </Trans>
                  </span>
                )}
              </Col>
              <Col sm="auto">
                <div className="main-menu" data-state="closed">
                  <nav id="navigation" role="navigation" data-state="closed">
                    <ul>
                      {!authenticated && (
                        <li>
                          <Link to="/prihlasit">
                            <Trans i18nKey="signIn">Přihlásit se</Trans>
                          </Link>
                        </li>
                      )}

                      {authenticated && (
                        <li>
                          <Button
                            color="link"
                            className="logout"
                            onClick={logout}
                          >
                            <Trans i18nKey="signOut">Odhlásit se</Trans>
                          </Button>
                        </li>
                      )}

                      {authenticated && (
                        <li>
                          <Link to="/tridy">
                            <Trans i18nKey="mainHeader.myClasses" />
                          </Link>
                        </li>
                      )}

                      <li>
                        <Link to="/katalog">
                          <Trans i18nKey="catalog">Katalog cvičení</Trans>
                        </Link>
                      </li>

                      <li className="active">
                        <Link to="/timeline">
                          <Trans i18nKey="timeline.navbar">Časová osa</Trans>
                        </Link>
                      </li>

                      <li>
                        <a
                          target="_blank"
                          rel="noopener noreferrer"
                          href="https://docs.google.com/forms/d/e/1FAIpQLSfxkv8mdXVEBT_VUfz4jHT57uew8PWp9lWzel2mT-zUZ2A8Tg/viewform?usp=sf_link"
                        >
                          <FontAwesomeIcon
                            size="sm"
                            icon={["fas", "question-circle"]}
                            className="mr-1"
                          />
                          Helpdesk
                        </a>
                      </li>
                      <li className="LanguageSwitcher">
                        <Button
                          color="link"
                          onClick={() => changeLanguage("cs")}
                        >
                          Čeština
                        </Button>
                        <span className="LanguageSwitcher__divider">|</span>
                        <Button
                          color="link"
                          onClick={() => changeLanguage("en")}
                        >
                          English
                        </Button>
                      </li>
                    </ul>
                  </nav>
                </div>
              </Col>
            </Row>
          </Container>
        </header>
      )}
    </ApplicationStateConsumer>
  );
};

export default MainHeader;
