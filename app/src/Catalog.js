import React from "react";
import {Row} from "reactstrap";
import {withTranslation} from "react-i18next";
import i18n from "i18next";
import CatalogExercise from "./CatalogExercise";
import DataService from "./DataService";
import withNotification from "./withNotification";
import FilterBar from "./filter/molecules/FilterBar";
import Layout from "./Layout";
import "./css/Catalog.css"; // TODO: convert this to .scss & convert cards to the Card component
import "./Catalog.scss";

class Catalog extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      pending: false,
      modal: false,
      error: null,
      data: {
        exercises: [],
        currentExercises: [],
        warnings: []
      },
      exercise: {},
      currentBreakpoint: "lg",
      mounted: false,
      allKeywords: null
    };
  }

  toggle = () => {
    this.setState({
      modal: !this.state.modal
    });
  };
  refresh = lng => {
    if (!lng) {
      lng = i18n.language;
    }
    this.setState({isLoading: true});
    new DataService().fetchCatalog(lng).then(
      result => {
        let data = {
          exercises: result.data.exercises,
          currentExercises: result.data.exercises
        } || {exercises: [], currentExercises: []};
        this.setState({
          error: null,
          data: data,
          isLoading: false
        });
      },
      error =>
        this.setState({
          error,
          isLoading: false
        })
    );
    new DataService().fetchKeyWords(lng).then(
      result => {
        this.setState({
          error: null,
          allKeywords: result.data.keyWords,
          isLoading: false
        });
      },
      error =>
        this.setState({
          error,
          isLoading: false
        })
    );
  };

  componentDidMount() {
    this.refresh();
    this.setState({mounted: true});
    const that = this;
    i18n.on("languageChanged", function (lng) {
      that.refresh(lng);
    });
  }

  generateCatalog() {
    const {data} = this.state;
    return data.currentExercises.map(exercise => (
      <CatalogExercise
        key={exercise.id}
        exercise={exercise}
        onClick={this.onAdd}
        location={this.props.location}
      />
    ));
  }
  
  getCatalog() {
    if (this.state.isLoading) {
      return (
        <div className="text-center">
          { this.props.t("pleaseWait") }
        </div>
      );
    }

    return (
      <Row className="grid">
        { this.generateCatalog() }
      </Row>
    );
  }

  render() {
    return (
      <Layout
        title={ this.props.t("catalog", "Katalog cvičení") }
        isTitleCentered={true}
        isFullWidth={true}
        className="Catalog"
      >
        <Layout.Content>
          {!this.state.isLoading &&
          <FilterBar
            allExercises={this.state.data.exercises}
            allKeywords={this.state.allKeywords}
            updateExerciseByFilter={this.updateExerciseByFilter}/>
          }
          {this.getCatalog()}
        </Layout.Content>
      </Layout>
    );
  }

  updateExerciseByFilter = (filteredData) => {
    const data = {...this.state.data};
    data.currentExercises = filteredData;
    this.setState({data});
  }
}

export default withTranslation()(withNotification(Catalog));
