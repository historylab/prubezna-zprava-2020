import React, { Suspense } from "react";
import ReactDOM from "react-dom";
import "./css/index.scss";
import App from "./App";
import { BrowserRouter } from "react-router-dom";
import { Route } from "react-router-dom";
import { ApplicationStateProvider } from "./ApplicationStateProvider";
import { NotificationProvider } from "./NotificationProvider";
import "./icons";
import { I18nextProvider } from "react-i18next";

import { i18ninit } from "./i18n";

i18ninit().then(i18n => {
  ReactDOM.render(
    <Suspense fallback="loading">
      <div className="App">
        <I18nextProvider i18n={i18n}>
          <BrowserRouter>
            <NotificationProvider>
              <ApplicationStateProvider i18n={i18n}>
                <Route component={App} />
              </ApplicationStateProvider>
            </NotificationProvider>
          </BrowserRouter>
        </I18nextProvider>
      </div>
    </Suspense>,
    document.getElementById("root")
  );
});
