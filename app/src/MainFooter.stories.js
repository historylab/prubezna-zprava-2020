import React from "react";
import { action } from "@storybook/addon-actions";
import { ApplicationStateProvider } from "./ApplicationStateProvider";

import MainFooter from "./MainFooter";

const appStateDecorator = Story => (
  <ApplicationStateProvider value={{ user: {} }}>
    <Story />
  </ApplicationStateProvider>
);

export default {
  component: MainFooter,
  title: "Main footer",
  decorators: [appStateDecorator],
  // Our exports that end in "Data" are not stories.
  excludeStories: /.*Data$/
};

export const Default = () => <MainFooter/>;

