import "./fonts/fonts.css";
import "./css/index.scss";
import "./css/App.css";
import "react-notifications/lib/notifications.css";
import React, { Component } from "react";
import Catalog from "./Catalog";
import Timeline from "./timeline/Timeline";
import ExerciseDetail from "./exercises/ExerciseDetail";
import ExerciseList from "./ExerciseList";
import Home from "./Home";
import CatalogExerciseDetail from "./CatalogExerciseDetail";
import { Switch, Redirect, Route } from "react-router-dom";
import Peeping from "./peeping/Peeping";
import Registration from "./users/Registration";
import { loadReCaptcha } from "react-recaptcha-google";
import ReactGA from "react-ga";
import withTracker from "./With/withTracker";
import ForgotPasswordForm from "./users/ForgotPassword";
import ResetPassword from "./users/ResetPassword";
import { Trans } from "react-i18next";
import TagManager from "react-gtm-module";
import CookieConsent from "./CookieConsent";
import GroupList from "./groups/pages/GroupList";
import GroupEditPage from "./groups/pages/GroupEditPage";
import { library } from '@fortawesome/fontawesome-svg-core'
import { faAngleRight, faInfoCircle, faPlus, faUserPlus } from '@fortawesome/free-solid-svg-icons'
import PackagePage from "./exercises/packages/pages/PackagePage";
import PackageDetailPage from "./exercises/packages/pages/PackageDetailPage"

library.add(faPlus, faInfoCircle, faUserPlus, faAngleRight);

ReactGA.initialize(process.env.REACT_APP_GOOGLE_ANALYTICS, {
  debug: process.env.NODE_ENV !== "production"
});

TagManager.initialize({
  gtmId: process.env.REACT_APP_GOOGLE_TAG_MANAGER
});

const NoMatch = props => {
  const { location } = props;
  if (location.pathname.match(/^\/cviceni\//)) {
    const url =
      window.location.protocol +
      "//app." +
      window.location.hostname +
      window.location.pathname +
      window.location.search;
    return (
      <div>
        <h1>
          <Trans i18nKey="pageWasMoved">Stránka byla přesunuta</Trans>
        </h1>
        <a href={url}>{url}</a>
      </div>
    );
  }
  return (
    <div>
      <h3>
        No match for <code>{location.pathname}</code>
      </h3>
    </div>
  );
};

class App extends Component {
  componentDidMount() {
    loadReCaptcha();
  }

  render() {
    const { location } = this.props;

    return (
      <>
        <Switch location={location}>
          <Route exact  path="/"                      component={withTracker(Home)} />
          
          <Route exact  path="/katalog"               component={withTracker(Catalog)} />
          <Route        path="/katalog/cviceni/:slug" component={withTracker(CatalogExerciseDetail)} />
          <Route exact  path="/timeline" component={withTracker(Timeline)} />
          <Route exact  path="/trida/:id?"            component={GroupEditPage} />
          <Route exact  path="/tridy"                 component={GroupList} />
            <Redirect   from="/moje" to="/tridy" />
          <Route exact  path="/tridy/:id"             component={withTracker(ExerciseList)}/>
          <Route exact  path="/tridy/:id/detail/"     component={GroupEditPage} />
          <Route        path="/cviceni/detail/:id"    component={withTracker(ExerciseDetail)} />

          <Route        path="/katalog/balicek/:slug" component={withTracker(PackagePage)} />
          <Route        path="/balicek/detail/:id"    component={PackageDetailPage} />
          
          <Route exact  path="/registrace"            component={Registration} />
          <Route exact  path="/prihlasit"             component={withTracker(GroupList)} />
          <Route exact  path="/reset-hesla"           component={ResetPassword} />
          <Route exact  path="/zapomenute-heslo"      component={ForgotPasswordForm} />
          
          <Route exact  path="/peeping"               component={Peeping} />
          
          <Route                                      component={NoMatch} />
        </Switch>
        <CookieConsent />
      </>
    );
  }
}

export default App;
