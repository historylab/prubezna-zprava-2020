import React from "react";
import {
  ModalHeader,
  ModalBody,
  Modal,
  Button,
  ModalFooter,
  UncontrolledAlert,
  Col
} from "reactstrap";
import DataService from "./DataService";
import _ from "lodash";
import { Trans, withTranslation } from "react-i18next";
import i18n from "i18next";
import GroupSelector from "./groups/components/GroupSelector";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

class GroupChooser extends React.Component {
  constructor() {
    super();
    this.state = {
      isLoading: false,
      groupName: "",
      error: null,
      pending: false,
      isOpen: false,
      groups: [],
      formErrors: { groupName: "" },
      groupNameValid: false,
      canChangeGroupName: false,
      selectedGroupId: -1
    };
  }

  toggle = () => {
    const groupName = this.props.exercise.group.name;
    this.setState({
      isOpen: !this.state.isOpen,
      groupName
    });
  };

  setError = error => {
    this.setState({ error });
  };

  componentDidMount() {
    const groupName = this.props.exercise.group.name;
    this.setState({
      error: null,
      groupName,
      pending: true
    });

    this.validateField("groupName", groupName);

    new DataService()
      .fetchMyGroups()
      .then(data => {
        this.setState({ pending: false, groups: data.data });
      })
      .catch(error => {
        this.setState({ pending: false, error });
      });
  }

  handleGroupNameChange = event => {
    const name = event.target.name;
    const value = event.target.value;
    this.setState({ [name]: value }, () => {
      this.validateField(name, value);
    });
  };

  validateField = (fieldName, value) => {
    let fieldValidationErrors = this.state.formErrors;
    let groupNameValid = this.state.groupNameValid;

    switch (fieldName) {
      case "groupName":
        const regex = /^.+/;
        groupNameValid = (value || "").trim().match(regex);
        fieldValidationErrors.groupName = groupNameValid
          ? ""
          : i18n.t("groupChooser.groupName.required", "Název třídy musí zadán");
        break;
      default:
        break;
    }
    this.setState({
      formErrors: fieldValidationErrors,
      groupNameValid,
      canChangeGroupName: groupNameValid
    });
  };

  renameGroup = () => {
    this.props.renameGroup(this.state.groupName);
  };

  selectGroup = () => {
    this.props.selectGroup(this.state.selectedGroupId);
  };

  selectOriginalGroup = () => {
    this.props.selectOriginalGroup();
  };

  render() {
    const { exercise, t, isOpen, error } = this.props;
    const { groups } = this.state;
    const toggle = this.props.toggle;
    const groupsToShow = groups.map(g => _.clone(g));

    if (exercise) {
      const groupId = this.props.exercise.group.id;
      const matchingGroups = groupsToShow.filter(g => g.id * 1 === groupId * 1);
      if (matchingGroups.length > 0) {
        const index = groupsToShow.indexOf(matchingGroups[0]);
        groupsToShow.splice(index, 1);
      }

      if (groupsToShow.length > 0) {
        groupsToShow[0].checked = true;
      }
    }

    return (
      <div className="GroupChooser">
        <div id="btnSwitchClass" onClick={toggle}>
          <FontAwesomeIcon icon="exchange-alt" className="mr-1" />
          <Trans i18nKey="groupChooser.changeClass">Změnit třídu</Trans>
        </div>

        <Modal isOpen={isOpen} toggle={toggle} backdrop centered>
          <ModalHeader toggle={toggle}>
            <Trans i18nKey="groupChooser.changeClass">Změna třídy</Trans>
          </ModalHeader>
          <ModalBody>
            {error && (
              <UncontrolledAlert color="danger">
                <Trans i18nKey="error">Chyba</Trans> {error.response.status}:{" "}
                {error.response.data.message}
              </UncontrolledAlert>
            )}
            <legend className="col-form-label">
              <Trans i18nKey="groupChooser.chooseClassLegend">
                ... nebo vybrat jinou třídu ze seznamu:
              </Trans>
            </legend>
            <Col>
              <div className="groupContainerDiv">
                <GroupSelector
                  loading={false}
                  groups={groupsToShow}
                  onGroupSelected={this.props.handleGroupSelected}
                  t={t}
                />
              </div>
            </Col>
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={toggle}>
              <Trans i18nKey="close">Zavřít</Trans>
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

export default withTranslation()(GroupChooser);
