define(['react', 'react-dom', 'block_myhistorylab/service'], function(React, ReactDOM, DataService) { /* build */

var HistoryLab = (function (React,ReactDOM,DataService) {
  'use strict';

  /* === placeholder === */ 

  return {
    init: function() { console.info('init');},
    render: function(root) { renderApp(root);}
  };

}(React,ReactDOM,DataService));
//# sourceMappingURL=HistoryLab.iife.js.map
return HistoryLab;
});